from django.db import models
from django.contrib.auth.models import User


class TrialEmailFollowUp(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
    )
    email_one = models.BooleanField(default=False)
    email_two = models.BooleanField(default=False)
    email_three = models.BooleanField(default=False)
    email_four = models.BooleanField(default=False)
    email_five = models.BooleanField(default=False)
    last_sent = models.DateTimeField(null=True, blank=True)


