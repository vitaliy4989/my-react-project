from django.contrib import admin
from .models import  TrialEmailFollowUp

class TrialEmailFollowUpAdmin(admin.ModelAdmin):
	readonly_fields = (
        'user',
    )
	search_fields = ['user__username', 'user__email']

admin.site.register(TrialEmailFollowUp, TrialEmailFollowUpAdmin)
