from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils.html import strip_tags
from django.contrib import messages
from transactions.models import MemberAccount, GiftCoupon
import sendgrid
import json
from django.utils import timezone
from .models import TrialEmailFollowUp
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string


@login_required
def gift_coupon(request):
    if request.method == 'GET':
        return render(request, 'emails/send_gift_coupon.html', {
            'gift_coupons': GiftCoupon.objects.filter(purchased_by=request.user)
        })
    elif request.method == 'POST':
        if request.POST.get('recipientEmail'):
            gift_coupon = GiftCoupon.objects.get(pk=int(request.POST.get('couponId')))
            gift_coupon.recipient_email = strip_tags(request.POST.get('recipientEmail'))
            gift_coupon.recipient_message = strip_tags(request.POST.get('recipientMessage'))
            gift_coupon.save()
            try:
                sg = sendgrid.SendGridAPIClient(
                    apikey='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')
                data = {
                    "personalizations": [
                        {
                            "to": [
                                {
                                    "email": gift_coupon.recipient_email
                                }
                            ],
                            "subject": "JSA | Gift Coupon"
                        }
                    ],
                    "from": {
                        "email": "notifications@jamessmithacademy.com"
                    },
                    "content": [
                        {
                            "type": "text/html",
                            "value": render_to_string('transactions/gift_coupon.html',
                                                      {'message': gift_coupon.recipient_message,
                                                       'coupon': gift_coupon.coupon_value})
                        }
                    ]
                }
                sg.client.mail.send.post(request_body=data)
                messages.success(request, 'Your email has been sent!')
                return redirect('emails:gift_coupon')
            except Exception as e:
                messages.error(request,
                               'There was an error. We could not send them the email. Your coupons can be found in your account area.')
                return redirect('emails:gift_coupon')
        return


def trial_subscribe_emails(email, daily, marketing):
    if daily is not None and marketing is not None:
        try:
            sg = sendgrid.SendGridAPIClient(
                apikey='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')
            data = [
                {
                    "email": email
                }
            ]
            response = sg.client.contactdb.recipients.post(request_body=data)
            response_data = json.loads(response.body)
            if response.status_code == 201:
                if response_data['error_count'] <= 0:
                    if response_data['persisted_recipients']:
                        recipient_id = response_data['persisted_recipients']
                        if daily is not None:
                            sg.client.contactdb.lists._(2171871).recipients._(recipient_id[0]).post()
                        if marketing is not None:
                            sg.client.contactdb.lists._(4922426).recipients._(recipient_id[0]).post()
                            sg.client.contactdb.lists._(4931856).recipients._(recipient_id[0]).post()
        except Exception as e:
            pass
    return


def delete_from_trailnopurchase_list(email):
    if email:
        try:
            sg = sendgrid.SendGridAPIClient(
                apikey='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')
            params = {'email': email}
            response = sg.client.contactdb.recipients.search.get(query_params=params)
            response_data = json.loads(response.body)
            recipient_id = response_data['recipients'][0]['id']
            params = {'recipient_id': recipient_id, 'list_id': 4922426}
            sg.client.contactdb.lists._(4922426).recipients._(recipient_id).delete(query_params=params)
        except Exception as e:
            pass
    return


def daily_email(request):
    if request.method == 'GET':
        return render(request, 'emails/daily_emails.html')
    elif request.method == 'POST':
        email = strip_tags(request.POST.get('email'))
        name = strip_tags(request.POST.get('name'))
        try:
            sg = sendgrid.SendGridAPIClient(
                apikey='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')
            data = [
                {
                    "email": email,
                    "name": name
                }
            ]
            response = sg.client.contactdb.recipients.post(request_body=data)
            response_data = json.loads(response.body)
            if response.status_code == 201:
                if response_data['error_count'] <= 0:
                    if response_data['persisted_recipients']:
                        list_id = 2171871
                        recipient_id = response_data['persisted_recipients']
                        response = sg.client.contactdb.lists._(list_id).recipients._(recipient_id[0]).post()
                        if response.status_code == 201:
                            return HttpResponse('Thanks! You have been subscribed.')
                    else:
                        return HttpResponse('Thanks! You have been subscribed.')
                else:
                    return HttpResponse(response_data['errors']['message'])
            else:
                return HttpResponse('There has been an error, please contact us.')
        except Exception as e:
            return HttpResponse('There has been an error, please contact us.')


def waiting_list(request):
    if request.method == 'GET':
        return render(request, 'emails/waiting_list.html')
    if request.method == 'POST':
        email = strip_tags(request.POST.get('email'))
        try:
            sg = sendgrid.SendGridAPIClient(
                apikey='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')
            data = [{"email": email}]
            response = sg.client.contactdb.recipients.post(request_body=data)
            response_data = json.loads(response.body)
            if response.status_code == 201:
                if response_data['error_count'] <= 0:
                    if response_data['persisted_recipients']:
                        list_id = 6047707
                        recipient_id = response_data['persisted_recipients']
                        response = sg.client.contactdb.lists._(list_id).recipients._(recipient_id[0]).post()
                        if response.status_code == 201:
                            messages.success(request,
                                             'Thanks! You are on the waiting list, we will let you know as soon as there is an opening.')
                            return redirect('/emails/waiting-list')
                    else:
                        messages.success(request,
                                         'Thanks! You are on the waiting list, we will let you know as soon as there is an opening.')
                        return redirect('/emails/waiting-list')
                else:
                    messages.error(request, 'Sorry, there was an error.')
                    return redirect('/emails/waiting-list')
            else:
                messages.error(request, 'Sorry, there was an error.')
                return redirect('/emails/waiting-list')
        except Exception as e:
            messages.error(request, 'Sorry, there was an error.')
            return redirect('/emails/waiting-list')


def challenge_waiting_list(request):
    if request.method == 'GET':
        return render(request, 'emails/challenge_waiting_list.html')
    if request.method == 'POST':
        email = strip_tags(request.POST.get('email'))
        if email:
            try:
                sg = sendgrid.SendGridAPIClient(
                    apikey='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')
                data = [{"email": email}]
                response = sg.client.contactdb.recipients.post(request_body=data)
                response_data = json.loads(response.body)
                if response.status_code == 201:
                    if response_data['error_count'] <= 0:
                        if response_data['persisted_recipients']:
                            list_id = 6136702
                            recipient_id = response_data['persisted_recipients']
                            response = sg.client.contactdb.lists._(list_id).recipients._(recipient_id[0]).post()
                            if response.status_code == 201:
                                messages.success(request,
                                                 'Thanks! You are on the waiting list, we will send you an email when challenge enrolment opens!')
                                return redirect('/emails/challenge-waiting-list')
                        else:
                            messages.success(request,
                                             'Thanks! You are on the waiting list, we will send you an email when challenge enrolment opens!')
                            return redirect('/emails/challenge-waiting-list')
                    else:
                        messages.error(request, 'Sorry, there was an error.')
                        return redirect('/emails/challenge-waiting-list')
                else:
                    messages.error(request, 'Sorry, there was an error.')
                    return redirect('/emails/challenge-waiting-list')
            except Exception as e:
                messages.error(request, 'Sorry, there was an error.')
                return redirect('/emails/challenge-waiting-list')
        else:
            messages.error(request, 'Please enter an email.')
            return redirect('/emails/challenge-waiting-list')


def contact_us(request):
    if request.method == 'GET':
        return render(request, 'emails/contact_us.html')
    elif request.method == 'POST':
        try:
            sg = sendgrid.SendGridAPIClient(
                apikey='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')
            data = {"personalizations": [{"to": [{"email": "contact@jamessmithacademy.com"}],
                                          "subject": strip_tags(request.POST.get('subject')) + " - " + request.POST.get(
                                              'name')}],
                    "from": {"email": strip_tags(request.POST.get('name')) + ", <contact@jamessmithacademy.com>",
                             "name": strip_tags(request.POST.get('email'))},
                    "reply_to": {"email": strip_tags(request.POST.get('email'))},
                    "content": [{"type": "text/plain", "value": strip_tags(request.POST.get('message'))}]}
            sg.client.mail.send.post(request_body=data)
            messages.success(request, 'Thank you for contacting us, we will reply within 24h.')
            return redirect('emails:contact_us')
        except Exception as e:
            messages.error(request, e)
            return redirect('emails:contact_us')


def started_trial_email(request):
    if request.user.email:
        try:
            sg = sendgrid.SendGridAPIClient(
                apikey='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')
            data = {
                "personalizations": [
                    {
                        "to": [
                            {
                                "email": request.user.email
                            }
                        ],
                    }
                ],
                "from": {
                    "email": "welcome@jamessmithacademy.com"
                },
                "template_id": 'd-deb61dd311824d7888e5528dbe1bad85',
                "asm": {
                    "group_id": 8373
                }
            }
            sg.client.mail.send.post(request_body=data)
            try:
                TrialEmailFollowUp.objects.get(user=request.user)
            except TrialEmailFollowUp.DoesNotExist:
                TrialEmailFollowUp.objects.create(
                    user=request.user,
                    last_sent=timezone.now()
                )
        except Exception as e:
            pass
    return


def trial_email_followup():
    sg = sendgrid.SendGridAPIClient(
        apikey='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')
    member_accounts = MemberAccount.objects.filter(status='trialing')
    suppressions_response = sg.client.asm.groups._(8373).suppressions.get()
    suppressions_response_body = json.loads(suppressions_response.body)
    for member in member_accounts:
        if member.user.email:
            is_unsubscribed = member.user.email in suppressions_response_body
            if is_unsubscribed == False:
                try:
                    trial_email_follow_up = TrialEmailFollowUp.objects.get(user=member.user)
                except TrialEmailFollowUp.DoesNotExist:
                    trial_email_follow_up = TrialEmailFollowUp.objects.create(
                        user=member.user
                    )
                if trial_email_follow_up.last_sent is not None:
                    today = timezone.now()
                    last_sent = trial_email_follow_up.last_sent
                    time_passed = today - last_sent
                    if trial_email_follow_up.email_one == False and time_passed.days >= 1:
                        trial_email_follow_up.email_one = True
                        trial_email_follow_up.save()
                        template = 'd-79fba9aea5f34ee6bdd54c60556f22c7'
                        print('email 1 sent.')
                    elif trial_email_follow_up.email_two == False and time_passed.days >= 2:
                        trial_email_follow_up.email_two = True
                        trial_email_follow_up.save()
                        template = 'd-c5aac8d247a241b1957d09695794b160'
                        print('email 2 sent.')
                    elif trial_email_follow_up.email_three == False and time_passed.days >= 3:
                        trial_email_follow_up.email_three = True
                        trial_email_follow_up.save()
                        template = 'd-cd5f4d06194948dca2eaec74cbe9c800'
                        print('email 3 sent.')
                    elif trial_email_follow_up.email_four == False and time_passed.days >= 4:
                        trial_email_follow_up.email_four = True
                        trial_email_follow_up.save()
                        template = 'd-7d165af0c7814fba8912a992372a3d82'
                        print('email 4 sent.')
                    elif trial_email_follow_up.email_five == False and time_passed.days >= 5:
                        trial_email_follow_up.email_five = True
                        trial_email_follow_up.save()
                        template = 'd-a198b3232a8a4ce585a35fbfcf4cc94a'
                        print('email 5 sent.')
                    else:
                        template = None
                    if template is not None:
                        data = {
                            "personalizations": [
                                {
                                    "to": [
                                        {
                                            "email": member.user.email
                                        }
                                    ],
                                }
                            ],
                            "from": {
                                "email": "welcome@jamessmithacademy.com"
                            },
                            "template_id": template,
                            "asm": {
                                "group_id": 8373
                            }
                        }
                        sg.client.mail.send.post(request_body=data)
                else:
                    pass
            else:
                print('unsubscribed')

        if timezone.now() > member.trial_end_date:
            member.status = 'canceled'
            member.trial_end_date = None
            member.save()
            try:
                if trial_email_follow_up:
                    trial_email_follow_up.delete()
            except Exception as e:
                print(e)
