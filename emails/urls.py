from django.urls import path
from . import views

urlpatterns = [
    path('daily-email/', views.daily_email, name='daily_email'),
    path('gift-coupon/', views.gift_coupon, name='gift_coupon'),
    path('contact-us/', views.contact_us, name='contact_us'),
    path('waiting-list/', views.waiting_list, name='waiting_list'),
    path('challenge-waiting-list/', views.challenge_waiting_list, name='challenge_waiting_list'),
]
