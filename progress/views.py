from django.template.loader import render_to_string
from .models import Arm, Km, Hip, Images, Waist, Squat, Bench, Deadlift, Calf, Thigh, Chest, Goal, WeeklyGoal
from .serializers import ImagesSerializer, WaistSerializer, HipSerializer, ArmSerializer, KmSerializer, BenchSerializer, \
    DeadliftSerializer, SquatSerializer, ThighSerializer, ChestSerializer, CalfSerializer, GoalSerializer, \
    WeeklyGoalSerializer
from django.shortcuts import get_object_or_404
from transactions.models import MemberAccount
from chat.models import ChatMessage, Chat
from django.core.cache import cache
from users.models import CoachProfile
from django.utils import timezone
import sendgrid
import random
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from django.http import Http404


class WeeklyGoalViewSet(ViewSet):

    def list(self, request):
        queryset = WeeklyGoal.objects.filter(user=request.user).order_by('date')
        serializer = WeeklyGoalSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = WeeklyGoalSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = WeeklyGoal.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = WeeklyGoalSerializer(item)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = WeeklyGoal.objects.get(pk=pk)
        except WeeklyGoal.DoesNotExist:
            return Response(status=404)
        serializer = WeeklyGoalSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = WeeklyGoal.objects.get(pk=pk)
        except WeeklyGoal.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class GoalViewSet(ViewSet):

    def list(self, request):
        try:
            item = Goal.objects.get(user=request.user)
        except Goal.DoesNotExist:
            raise Http404
        except Goal.MultipleObjectsReturned:
            Goal.objects.filter(user=request.user).delete()
            raise Http404
        serializer = GoalSerializer(item, many=False)
        return Response(serializer.data)

    def create(self, request):
        serializer = GoalSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Goal.objects.get(user=request.user)
        except Goal.DoesNotExist:
            return Response(status=404)
        serializer = GoalSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)


class ImagesViewSet(ViewSet):

    def list(self, request):
        queryset = Images.objects.filter(user=request.user).order_by('-date')
        serializer = ImagesSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ImagesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Images.objects.get(pk=pk)
        except Images.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class WaistViewSet(ViewSet):

    def list(self, request):
        queryset = Waist.objects.filter(user=request.user).order_by('date')
        serializer = WaistSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = WaistSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Waist.objects.get(pk=pk)
        except Waist.DoesNotExist:
            return Response(status=404)
        serializer = WaistSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Waist.objects.get(pk=pk)
        except Waist.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class HipViewSet(ViewSet):

    def list(self, request):
        queryset = Hip.objects.filter(user=request.user).order_by('date')
        serializer = HipSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = HipSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Hip.objects.get(pk=pk)
        except Hip.DoesNotExist:
            return Response(status=404)
        serializer = HipSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Hip.objects.get(pk=pk)
        except Hip.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class ArmViewSet(ViewSet):

    def list(self, request):
        queryset = Arm.objects.filter(user=request.user).order_by('date')
        serializer = ArmSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ArmSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Arm.objects.get(pk=pk)
        except Arm.DoesNotExist:
            return Response(status=404)
        serializer = ArmSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Arm.objects.get(pk=pk)
        except Arm.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class KmViewSet(ViewSet):

    def list(self, request):
        queryset = Km.objects.filter(user=request.user).order_by('date')
        serializer = KmSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = KmSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Km.objects.get(pk=pk)
        except Km.DoesNotExist:
            return Response(status=404)
        serializer = KmSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Km.objects.get(pk=pk)
        except Km.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class BenchViewSet(ViewSet):

    def list(self, request):
        queryset = Bench.objects.filter(user=request.user).order_by('date')
        serializer = BenchSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = BenchSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Bench.objects.get(pk=pk)
        except Bench.DoesNotExist:
            return Response(status=404)
        serializer = BenchSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Bench.objects.get(pk=pk)
        except Bench.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class DeadliftViewSet(ViewSet):

    def list(self, request):
        queryset = Deadlift.objects.filter(user=request.user).order_by('date')
        serializer = DeadliftSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = DeadliftSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Deadlift.objects.get(pk=pk)
        except Deadlift.DoesNotExist:
            return Response(status=404)
        serializer = DeadliftSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Deadlift.objects.get(pk=pk)
        except Deadlift.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class SquatViewSet(ViewSet):

    def list(self, request):
        queryset = Squat.objects.filter(user=request.user).order_by('date')
        serializer = SquatSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = SquatSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Squat.objects.get(pk=pk)
        except Squat.DoesNotExist:
            return Response(status=404)
        serializer = SquatSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Squat.objects.get(pk=pk)
        except Squat.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class ThighViewSet(ViewSet):

    def list(self, request):
        queryset = Thigh.objects.filter(user=request.user).order_by('date')
        serializer = ThighSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ThighSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Thigh.objects.get(pk=pk)
        except Thigh.DoesNotExist:
            return Response(status=404)
        serializer = ThighSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Thigh.objects.get(pk=pk)
        except Thigh.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class ChestViewSet(ViewSet):

    def list(self, request):
        queryset = Chest.objects.filter(user=request.user).order_by('date')
        serializer = ChestSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ChestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Chest.objects.get(pk=pk)
        except Chest.DoesNotExist:
            return Response(status=404)
        serializer = ChestSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Chest.objects.get(pk=pk)
        except Chest.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class CalfViewSet(ViewSet):

    def list(self, request):
        queryset = Calf.objects.filter(user=request.user).order_by('date')
        serializer = CalfSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = CalfSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def partial_update(self, request, pk=None):
        try:
            item = Calf.objects.get(pk=pk)
        except Calf.DoesNotExist:
            return Response(status=404)
        serializer = CalfSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Calf.objects.get(pk=pk)
        except Calf.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


# weekly checkin script

def goal_check_in():
    # Get list of active members
    members = MemberAccount.objects.filter(status='active').exclude(membership_name='Standard Plan')

    # Loop through members for email
    for member in members:

        # Try to send them a check in
        try:

            # Get the members checkin - goal object
            member_goal = Goal.objects.get(user=member.user, check_ins=True)

            # If the member has never had a check-in, no time has passed
            if member_goal.last_check_in == None:

                time_passed = None

            # If they have had a check-in
            else:

                # Get todays date
                today = timezone.now()

                # Get their last check-in
                last_checkin = member_goal.last_check_in

                # Calculate the time passwed beteen todays date and the last check-in
                time_passed = today - last_checkin

            # If they have not had a check-in or the time passed is greater than 6 days, check-in
            if time_passed == None or time_passed.days > 6:

                # If member has a first name, set name to first name, else use username
                if member.user.first_name:
                    name = member.user.first_name
                else:
                    name = member.user.username

                # List of difference responses
                chat_comment_one = 'Hi ' + name + ', just checking in to see how you are progressing. Let me know if there is anything you need, or if you have any questions.'
                chat_comment_two = 'Just checking in to make sure everything is going ok? Need any help with anything?'
                chat_comment_three = 'Hi ' + name + ', just wondered whether you need any help with anything? Checking in for the week.'
                chat_comment_four = 'Hi ' + name + ', just doing the check-in to make sure everything is ok, have any questions or anything?'
                chat_comment_five = 'How you doing ' + name + ', can I help you with anything? Just doing the check-in.'
                chat_comment_six = 'Hi ' + name + ', how is everything going?'
                chat_comment_seven = 'Hi ' + name + ', just checking to see if everything is going ok?'
                chat_comment_eight = 'Hi ' + name + ', everything ok? Just checking in.'
                chat_comment_nine = 'Hi ' + name + ', just checking in to make sure you are doing ok and making progress?'
                chat_comment_ten = 'Hi ' + name + ', any questions so far? Just thought I would check-in.'

                # Try to send check-in and notification email
                try:

                    # Get the user chat
                    chat_instance = Chat.objects.get(user=member.user, client_update=False, coach_update=False)

                    # Create a message for the chat using one of the different responses
                    ChatMessage.objects.create(
                        chat=chat_instance,
                        profile_c=CoachProfile.objects.get(pk=2),
                        comment=random.choice(
                            [chat_comment_one, chat_comment_two, chat_comment_three, chat_comment_four,
                             chat_comment_five, chat_comment_six, chat_comment_seven, chat_comment_eight,
                             chat_comment_nine, chat_comment_ten])
                    )

                    # Mark the user chat as updated so their are notified in-site
                    chat_instance.other_update = True

                    # Save update chat
                    chat_instance.save()

                    # Delete their chat messages
                    cache.delete('chat_messages' + str(chat_instance.id))

                    # Send an email with SendGrid
                    if member.user.email:
                        try:
                            sg = sendgrid.SendGridAPIClient(
                                apikey='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')
                            data = {"personalizations": [
                                {"to": [{"email": member.user.email}], "subject": "JSA | Check-in Notification"}],
                                "from": {
                                    "email": "notifications@jamessmithacademy.com"}, "content": [
                                    {"type": "text/html", "value": render_to_string('progress/check_in.html')}]}
                            sg.client.mail.send.post(request_body=data)
                        except Exception as e:
                            pass

                # If there is an error, do not send check-in and move on to set current time as last check-in
                except Exception as e:
                    pass

                # Set last chat-in time as now  
                member_goal.last_check_in = timezone.now()

                # Save last check-in time
                member_goal.save()

        except Exception as e:
            pass

    print('Check-ins completed')
