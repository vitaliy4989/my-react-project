# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-06 02:43
from __future__ import unicode_literals

from django.db import migrations, models
import pyuploadcare.dj.models


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0011_auto_20170906_0108'),
    ]

    operations = [
        migrations.AddField(
            model_name='training',
            name='facility',
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name='training',
            name='frequency',
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name='training',
            name='medical',
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='training',
            name='file',
            field=pyuploadcare.dj.models.FileField(null=True),
        ),
    ]
