# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-22 09:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('progress', '0007_auto_20170822_0943'),
    ]

    operations = [
        migrations.AddField(
            model_name='weeklygrouptraining',
            name='name',
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name='weeklytraining',
            name='name',
            field=models.CharField(max_length=250, null=True),
        ),
    ]
