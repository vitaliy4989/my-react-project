from django.contrib import admin
from .models import Km, Hip, Images, Waist, Arm, Bench, Deadlift, Squat, Calf, Thigh, Chest, Goal, WeeklyGoal
from import_export.admin import ImportExportModelAdmin


class ImagesAdmin(admin.ModelAdmin):

    def complete_delete(modeladmin, request, queryset):
        for obj in queryset:
            obj.photo.delete()
            obj.delete()

    search_fields = ['user__username']

    actions = [complete_delete]

    complete_delete.short_description = "Delete - Including image on Uploadcare"


@admin.register(Goal)
class GoalAdmin(ImportExportModelAdmin):
    pass


admin.site.register(WeeklyGoal)
admin.site.register(Arm)
admin.site.register(Hip)
admin.site.register(Images, ImagesAdmin)
admin.site.register(Waist)
admin.site.register(Km)
admin.site.register(Bench)
admin.site.register(Deadlift)
admin.site.register(Squat)
admin.site.register(Calf)
admin.site.register(Thigh)
admin.site.register(Chest)
