from rest_framework import serializers
from .models import Images, Waist, Hip, Arm, Km, Bench, Deadlift, Squat, Thigh, Chest, Calf, Goal, WeeklyGoal


class GoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Goal
        fields = '__all__'


class WeeklyGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = WeeklyGoal
        fields = '__all__'


class ImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Images
        fields = '__all__'


class WaistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waist
        fields = '__all__'


class HipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hip
        fields = '__all__'


class ArmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Arm
        fields = '__all__'


class KmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Km
        fields = '__all__'


class BenchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bench
        fields = '__all__'


class DeadliftSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deadlift
        fields = '__all__'


class SquatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Squat
        fields = '__all__'


class ThighSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thigh
        fields = '__all__'


class ChestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chest
        fields = '__all__'


class CalfSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calf
        fields = '__all__'
