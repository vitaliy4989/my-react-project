from django.db import models
from django.contrib.auth.models import User
from pyuploadcare.dj.models import ImageField


class Goal(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    goal = models.CharField(blank=True, max_length=250)
    goal_category = models.CharField(blank=True, null=True, max_length=250)
    check_ins = models.BooleanField(default=True)
    last_check_in = models.DateTimeField(null=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username


class WeeklyGoal(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    goal = models.CharField(blank=True, max_length=250)
    achieved = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)


class Images(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    photo = ImageField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username


class Waist(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)


class Hip(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)


class Arm(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)


class Km(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)


class Bench(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)


class Deadlift(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)


class Squat(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)


class Thigh(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)


class Chest(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)


class Calf(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    measurement = models.FloatField()
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + str(self.measurement)
