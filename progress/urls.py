from . import views
from rest_framework.routers import SimpleRouter

router = SimpleRouter()

router.register(r'goal', views.GoalViewSet, 'GoalViewSet')
router.register(r'weekly-goal', views.WeeklyGoalViewSet, 'WeeklyGoalViewSet')
router.register(r'image', views.ImagesViewSet, 'ImagesViewSet')
router.register(r'waist', views.WaistViewSet, 'WaistViewSet')
router.register(r'hip', views.HipViewSet, 'HipViewSet')
router.register(r'arm', views.ArmViewSet, 'ArmViewSet')
router.register(r'km', views.KmViewSet, 'KmViewSet')
router.register(r'bench', views.BenchViewSet, 'BenchViewSet')
router.register(r'deadlift', views.DeadliftViewSet, 'DeadliftViewSet')
router.register(r'squat', views.SquatViewSet, 'SquatViewSet')
router.register(r'thigh', views.ThighViewSet, 'ThighViewSet')
router.register(r'chest', views.ChestViewSet, 'ChestViewSet')
router.register(r'calf', views.CalfViewSet, 'CalfViewSet')

urlpatterns = router.urls

