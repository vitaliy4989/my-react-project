from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from .models import HomeStepOne, HomeStepTwo, HomeStepThree, HomeStepFour, HomeStepFive, HomeStepSix
from modules.models import Module
from progress.models import Goal
from training.models import Training
from nutrition_tracker.models import Macros
from recipes.models import Combination
from django.core.cache import cache


class HomeStepOneViewSet(ViewSet):

    def completed_starter_modules(self, request):
        training_modules = Module.objects.filter(sub_filter_id=1)
        nutrition_modules = Module.objects.filter(sub_filter_id=2)
        return {
            'training_total': training_modules.count(),
            'training_completed': training_modules.filter(moduleenrolled__user=request.user).count(),
            'nutrition_total': nutrition_modules.count(),
            'nutrition_completed': nutrition_modules.filter(moduleenrolled__user=request.user).count()
        }

    def get_step_one(self, request):
        try:
            return cache.get_or_set('home_step_one' + str(request.user.id), HomeStepOne.objects.get(user=request.user),
                                    2592000)
        except HomeStepOne.DoesNotExist:
            return None

    def list(self, request):
        step_one = self.get_step_one(request)
        if step_one is not None:
            return Response({'completed': True, 'training': 100, 'nutrition': 100})
        else:
            starters = self.completed_starter_modules(request)
            if starters['training_completed'] >= starters['training_total'] and starters['nutrition_completed'] >= \
                    starters['nutrition_total']:
                HomeStepOne.objects.create(user=request.user, completed=True)
                cache.delete('home_step_one' + str(request.user.id))
                return Response({
                    'completed': True,
                    'training_total': starters['training_total'],
                    'training_completed': starters['training_completed'],
                    'nutrition_total': starters['nutrition_total'],
                    'nutrition_completed': starters['nutrition_completed']
                })
            else:
                return Response({
                    'completed': False,
                    'training_total': starters['training_total'],
                    'training_completed': starters['training_completed'],
                    'nutrition_total': starters['nutrition_total'],
                    'nutrition_completed': starters['nutrition_completed']
                })


class HomeStepTwoViewSet(ViewSet):

    def get_goal(self, request):
        try:
            return Goal.objects.get(user=request.user)
        except Goal.DoesNotExist:
            return None
        except Goal.MultipleObjectsReturned:
            Goal.objects.filter(user=request.user).delete()
            return None

    def get_step_two(self, request):
        try:
            return cache.get_or_set('home_step_two' + str(request.user.id), HomeStepTwo.objects.get(user=request.user),
                                    2592000)
        except HomeStepTwo.DoesNotExist:
            return None

    def list(self, request):
        step_two = self.get_step_two(request)
        if step_two != None:
            return Response({'completed': True})
        else:
            goal = self.get_goal(request)
            if goal != None:
                HomeStepTwo.objects.create(user=request.user, completed=True)
                cache.delete('home_step_two' + str(request.user.id))
                return Response({'completed': True})
            else:
                return Response({'completed': False})


class HomeStepThreeViewSet(ViewSet):

    def get_step_three(self, request):
        try:
            return cache.get_or_set('home_step_three' + str(request.user.id),
                                    HomeStepThree.objects.get(user=request.user), 2592000)
        except HomeStepThree.DoesNotExist:
            return None

    def get_training_programs(self, request):
        if Training.objects.filter(user=request.user).exists():
            return True
        else:
            return None

    def list(self, request):
        step_three = self.get_step_three(request)
        if step_three != None:
            return Response({'completed': True})
        else:
            training_programs = self.get_training_programs(request)
            if training_programs != None:
                HomeStepThree.objects.create(user=request.user, completed=True)
                cache.delete('home_step_three' + str(request.user.id))
                return Response({'completed': True})
            else:
                return Response({'completed': False})

    def create(self, request):
        HomeStepThree.objects.create(user=request.user, completed=True)
        return Response({'completed': True})


class HomeStepFourViewSet(ViewSet):

    def get_step_four(self, request):
        try:
            return cache.get_or_set('home_step_four' + str(request.user.id),
                                    HomeStepFour.objects.get(user=request.user), 2592000)
        except:
            return None

    def get_macros(self, request):
        if Macros.objects.filter(user=request.user).exists():
            return True
        else:
            return None

    def list(self, request):
        step_four = self.get_step_four(request)
        if step_four != None:
            return Response({'completed': True})
        else:
            macros = self.get_macros(request)
            if macros != None:
                HomeStepFour.objects.create(user=request.user, completed=True)
                cache.delete('home_step_four' + str(request.user.id))
                return Response({'completed': True})
            else:
                return Response({'completed': False})


class HomeStepFiveViewSet(ViewSet):

    def get_step_five(self, request):
        try:
            return cache.get_or_set('home_step_five' + str(request.user.id),
                                    HomeStepFive.objects.get(user=request.user), 2592000)
        except HomeStepFive.DoesNotExist:
            return None

    def get_meals(self, request):
        if Combination.objects.filter(profile_u=request.user.profile).exists():
            return True
        else:
            return None

    def list(self, request):
        step_five = self.get_step_five(request)
        if step_five != None:
            return Response({'completed': True})
        else:
            meals = self.get_meals(request)
            if meals != None:
                HomeStepFive.objects.create(user=request.user, completed=True)
                cache.delete('home_step_five' + str(request.user.id))
                return Response({'completed': True})
            else:
                return Response({'completed': False})

    def create(self, request):
        HomeStepFive.objects.create(user=request.user, completed=True)
        return Response({'completed': True})


class HomeStepSixViewSet(ViewSet):

    def get_step_six(self, request):
        try:
            return cache.get_or_set('home_step_five' + str(request.user.id), HomeStepSix.objects.get(user=request.user),
                                    2592000)
        except HomeStepSix.DoesNotExist:
            return None

    def list(self, request):
        step_six = self.get_step_six(request)
        if step_six != None:
            return Response({'completed': True})
        else:
            return Response({'completed': False})

    def create(self, request):
        HomeStepSix.objects.create(user=request.user, completed=True)
        cache.delete('home_step_six' + str(request.user.id))
        return Response({'completed': True})
