from django.db import models
from django.contrib.auth.models import User


class HomeStepOne(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False, db_index=True)


class HomeStepTwo(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False, db_index=True)


class HomeStepThree(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False, db_index=True)


class HomeStepFour(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False, db_index=True)


class HomeStepFive(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False, db_index=True)


class HomeStepSix(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False, db_index=True)

