from django.apps import AppConfig


class HomeStepsConfig(AppConfig):
    name = 'home_steps'
