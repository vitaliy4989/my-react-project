from . import views
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register(r'one', views.HomeStepOneViewSet, 'HomeStepOne')
router.register(r'two', views.HomeStepTwoViewSet, 'HomeStepTwo')
router.register(r'three', views.HomeStepThreeViewSet, 'HomeStepThree')
router.register(r'four', views.HomeStepFourViewSet, 'HomeStepFour')
router.register(r'five', views.HomeStepFiveViewSet, 'HomeStepFive')
router.register(r'six', views.HomeStepSixViewSet, 'HomeStepSix')

urlpatterns = router.urls