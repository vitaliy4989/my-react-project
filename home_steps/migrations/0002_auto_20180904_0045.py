# Generated by Django 2.0.7 on 2018-09-04 00:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home_steps', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='homestepeight',
            name='user',
        ),
        migrations.RemoveField(
            model_name='homestepseven',
            name='user',
        ),
        migrations.DeleteModel(
            name='HomeStepEight',
        ),
        migrations.DeleteModel(
            name='HomeStepSeven',
        ),
    ]
