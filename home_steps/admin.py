from django.contrib import admin
from .models import HomeStepOne, HomeStepTwo, HomeStepThree, HomeStepFour, HomeStepFive, HomeStepSix

admin.site.register(HomeStepOne)
admin.site.register(HomeStepTwo)
admin.site.register(HomeStepThree)
admin.site.register(HomeStepFour)
admin.site.register(HomeStepFive)
admin.site.register(HomeStepSix)
