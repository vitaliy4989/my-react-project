from django.contrib import admin

from .models import CanceledOrPurchased

admin.site.register(CanceledOrPurchased)