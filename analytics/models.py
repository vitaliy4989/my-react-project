from django.db import models
from django.contrib.auth.models import User


class CanceledOrPurchased(models.Model):
	canceled = models.BooleanField(default=False)
	purchased = models.BooleanField(default=False)
	trialed = models.BooleanField(default=False)
	date = models.DateTimeField(auto_now_add=True)


class ProgramOrProgramChat(models.Model):
	coach = models.ForeignKey("users.CoachProfile", null=True, on_delete=models.CASCADE)
	program = models.BooleanField(default=False)
	program_chat = models.BooleanField(default=False)
	date = models.DateTimeField(auto_now_add=True)


class ChatAnswered(models.Model):
	coach = models.ForeignKey("users.CoachProfile", null=True, on_delete=models.CASCADE)
	answered = models.BooleanField(default=False)
	date = models.DateTimeField(auto_now_add=True)