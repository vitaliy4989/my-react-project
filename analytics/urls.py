from django.urls import path
from . import views


urlpatterns = [
	path('get-canceled-purchased/', views.get_canceled_purchased, name='get_canceled_purchased'),
]
