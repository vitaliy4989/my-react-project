from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from .models import CanceledOrPurchased
from transactions.models import MemberAccount
from challenge.models import Entry
from django.db.models import Count


@login_required
def get_canceled_purchased(request):
    if request.user.is_staff:
        purchased = CanceledOrPurchased.objects.filter(purchased=True).order_by('date').extra(
            {'date': "date(date)"}).values('date').annotate(date_count=Count('purchased'))
        canceled = CanceledOrPurchased.objects.filter(canceled=True).order_by('date').extra(
            {'date': "date(date)"}).values('date').annotate(date_count=Count('canceled'))
        trialed = CanceledOrPurchased.objects.filter(trialed=True).order_by('date').filter(trialed=True).extra(
            {'date': "date(date)"}).values('date').annotate(date_count=Count('trialed'))
        active = MemberAccount.objects.filter(status='active').count()
        trialing = MemberAccount.objects.filter(status='trialing').count()
        challenge = Entry.objects.filter(challenge=5, user__memberaccount__status='active').count()
        total_trialed = CanceledOrPurchased.objects.filter(trialed=True).count()
        total_purchased = CanceledOrPurchased.objects.filter(purchased=True).count()
        membership_types = MemberAccount.objects.filter(status='active').values('membership_name').annotate(
            dcount=Count('membership_name'))
        types_array = []
        for membership in membership_types:
            types_array.append({'name': membership['membership_name'], 'count': membership['dcount']})
        return JsonResponse({
            'purchased': list(purchased),
            'canceled': list(canceled),
            'trialed': list(trialed),
            'active': active,
            'trialing': trialing,
            'total_trialed': total_trialed,
            'total_purchased': total_purchased,
            'membership_types': types_array,
            'challenge': challenge
        }, safe=False)
