from django.db import models
from django.contrib.auth.models import User
from pyuploadcare.dj.models import ImageField


class Challenge(models.Model):
    name = models.CharField(null=True, max_length=250)
    start = models.DateField()
    end = models.DateField()
    close_enrollment = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Entry(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    challenge = models.ForeignKey(Challenge, null=True, on_delete=models.CASCADE)
    story = models.TextField(null=True)
    gender = models.CharField(null=True, max_length=250)
    goal = models.CharField(null=True, max_length=250)
    target = models.FloatField(null=True)
    target_units = models.CharField(null=True, max_length=250)
    age = models.IntegerField(null=True)
    country = models.CharField(null=True, max_length=250)
    agreed_to_terms = models.BooleanField(default=False)
    pass_judging = models.BooleanField(default=False)
    fail_judging = models.BooleanField(default=False, db_index=True)
    date = models.DateTimeField(null=True, auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + self.challenge.name


class ImageEntry(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    entry = models.ForeignKey(Entry, null=True, on_delete=models.CASCADE)
    image_entry = ImageField()
    week_number = models.IntegerField(null=True, blank=True)
    body_position = models.CharField(null=True, blank=True, max_length=250)
    date = models.DateTimeField(null=True, auto_now_add=True, db_index=True)


class ChartEntry(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    entry = models.ForeignKey(Entry, null=True, on_delete=models.CASCADE)
    chart_entry = models.FloatField(null=True)
    date = models.DateTimeField(null=True, auto_now_add=True, db_index=True)
