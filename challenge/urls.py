from . import views
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register(r'challenge', views.ChallengeViewSet, 'ChallengeViewSet')
router.register(r'entry', views.EntryViewSet, 'EntryViewSet')
router.register(r'judging-entry', views.JudgingEntryViewSet, 'JudgingEntryViewSet')
router.register(r'image-entry', views.ImageEntryViewSet, 'ImageEntryViewSet')
router.register(r'chart-entry', views.ChartEntryViewSet, 'ChartEntryViewSet')
router.register(r'entry-not-failed', views.EntryNotFailedViewSet, 'EntryNotFailedViewSet')


urlpatterns = router.urls

