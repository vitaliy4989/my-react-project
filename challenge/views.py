from django.shortcuts import get_object_or_404, get_list_or_404
from .serializers import ChallengeSerializer, EntrySerializer, ImageEntrySerializer, ChartEntrySerializer
from django.contrib.auth.models import User
from .models import Challenge, Entry, ImageEntry, ChartEntry
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from django.core.cache import cache
import sendgrid
from django.template.loader import render_to_string
from django.db.models import Count


class ChallengeViewSet(ViewSet):

    def list(self, request):
        queryset = cache.get_or_set('challenge_list', Challenge.objects.all(), 2592000)
        serializer = ChallengeSerializer(queryset.order_by('-end'), many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ChallengeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Challenge.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = ChallengeSerializer(item)
        return Response(serializer.data)

    def update(self, request, pk=None):
        try:
            item = Challenge.objects.get(pk=pk)
        except Challenge.DoesNotExist:
            return Response(status=404)
        serializer = CategorySerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Challenge.objects.get(pk=pk)
        except Challenge.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class EntryViewSet(ViewSet):

    def list(self, request):
        queryset = Entry.objects.all()
        item = get_object_or_404(queryset, challenge=request.GET.get('challenge_id'), user=request.user)
        serializer = EntrySerializer(item)
        return Response(serializer.data)

    def create(self, request):
        serializer = EntrySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            if request.user.email:
                email = request.user.email
            elif User.objects.filter(email__iexact=request.POST.get('email')):
                email = request.user.email
            else:
                email = request.POST.get('email')
            User.objects.filter(pk=request.user.id).update(
                email=email,
                first_name=request.POST.get('first_name'),
                last_name=request.POST.get('last_name')
            )
            try:
                sg = sendgrid.SendGridAPIClient(
                    apikey='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')
                data = {"personalizations": [
                    {"to": [{"email": request.POST.get('email')}], "subject": "JSA | Challenge Entry"}],
                    "from": {"email": "accounts@jamessmithacademy.com"}, "content": [{"type": "text/html",
                                                                                      "value": render_to_string(
                                                                                          'challenge/entry_email.html',
                                                                                          {'user': request.user})}]}
                sg.client.mail.send.post(request_body=data)
            except Exception as e:
                pass
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Entry.objects.all()
        item = get_object_or_404(queryset, challenge=pk, user=request.user)
        serializer = EntrySerializer(item)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = Entry.objects.get(pk=pk)
        except Entry.DoesNotExist:
            return Response(status=404)
        serializer = EntrySerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Entry.objects.get(pk=pk)
        except Entry.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class ImageEntryViewSet(ViewSet):

    def create(self, request):
        serializer = ImageEntrySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = ImageEntry.objects.all()
        items = get_list_or_404(queryset.order_by('date', 'week_number'), entry=pk)
        serializer = ImageEntrySerializer(items, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = ImageEntry.objects.get(pk=pk)
        except ImageEntry.DoesNotExist:
            return Response(status=404)
        serializer = ImageEntrySerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = ImageEntry.objects.get(pk=pk)
        except ImageEntry.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class ChartEntryViewSet(ViewSet):

    def create(self, request):
        serializer = ChartEntrySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = ChartEntry.objects.all()
        items = get_list_or_404(queryset.order_by('date'), entry=pk)
        serializer = ChartEntrySerializer(items, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = ChartEntry.objects.get(pk=pk)
        except ChartEntry.DoesNotExist:
            return Response(status=404)
        serializer = ChartEntrySerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = ChartEntry.objects.get(pk=pk)
        except ChartEntry.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class EntryNotFailedViewSet(ViewSet):

    def list(self, request):
        queryset = Entry.objects.annotate(
            num_imageentry=Count('imageentry'),
        ).filter(
            challenge=request.GET.get('challenge_id'),
            fail_judging=False,
            num_imageentry__gte=12,
        ).distinct()
        serializer = EntrySerializer(queryset, many=True)
        return Response(serializer.data)


class JudgingEntryViewSet(ViewSet):

    def retrieve(self, request, pk=None):
        queryset = Entry.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = EntrySerializer(item)
        return Response(serializer.data)
