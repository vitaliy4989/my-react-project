from django.contrib import admin
from .models import Challenge, Entry, ImageEntry, ChartEntry

class ImageEntryAdmin(admin.ModelAdmin):

    def complete_delete(modeladmin, request, queryset):
    	for obj in queryset:
    		obj.image_entry.delete()
    		obj.delete()

    search_fields = ['user__username']

    actions = [complete_delete]

    complete_delete.short_description = "Delete - Including image on Uploadcare"


class EntryAdmin(admin.ModelAdmin):
	readonly_fields = (
        'user',
        'challenge'
    )

admin.site.register(Entry, EntryAdmin)


admin.site.register(Challenge)
admin.site.register(ImageEntry, ImageEntryAdmin)
admin.site.register(ChartEntry)