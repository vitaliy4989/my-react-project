from rest_framework import serializers
from .models import Challenge, Entry, ImageEntry, ChartEntry
from users.serializers import UserSerializer


class ChallengeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Challenge
        fields = '__all__'


class EntrySerializer(serializers.ModelSerializer):

    challenge = Challenge.objects.all()

    user = UserSerializer(read_only=True)

    class Meta:
        model = Entry
        fields = '__all__'


class ImageEntrySerializer(serializers.ModelSerializer):
	
	class Meta:
		model = ImageEntry
		fields = '__all__'


class ChartEntrySerializer(serializers.ModelSerializer):
	
	class Meta:
		model = ChartEntry
		fields = '__all__'