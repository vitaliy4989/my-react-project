# Generated by Django 2.1.4 on 2019-01-02 21:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('challenge', '0017_auto_20190102_2035'),
    ]

    operations = [
        migrations.RenameField(
            model_name='imageentry',
            old_name='position',
            new_name='body_position',
        ),
    ]
