# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-02-14 21:27
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('challenge', '0013_auto_20180214_0326'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='CheckinChart',
            new_name='ChartEntry',
        ),
        migrations.RenameModel(
            old_name='CheckinImage',
            new_name='ImageEntry',
        ),
    ]
