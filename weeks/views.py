from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from .models import Week, SubFilter, SubFilterCompleted
from modules.models import Module, ModuleEnrolled
from django.core.cache import cache
from .serializers import MainSerializer, SubSerializer
from django.views.decorators.cache import cache_page


@login_required
@cache_page(60 * 1440)
def main_filters(request):
    main_filters_serialised = MainSerializer(
        cache.get_or_set('main_filters', Week.objects.order_by('order')[:25], 2592000), many=True)
    return JsonResponse(main_filters_serialised.data, safe=False)


@login_required
def sub_filters(request, id):
    subfilters_list = []
    for subfilter in cache.get_or_set('subfilter' + str(id),
                                      SubFilter.objects.filter(main=int(id)).order_by('order')[:15], 2592000):
        if subfilter.required_to_complete == True:
            if SubFilterCompleted.objects.filter(user=request.user.id, sub_filter=subfilter.id).exists():
                completed = True
            else:
                modules_count = cache.get_or_set('subfilter_modules_count' + str(subfilter.id),
                                                 Module.objects.filter(sub_filter=subfilter.id).count(), 86400)
                enrolled_count = ModuleEnrolled.objects.filter(user=request.user,
                                                               module__sub_filter=subfilter.id).count()
                if enrolled_count == modules_count:
                    SubFilterCompleted.objects.create(user=request.user, sub_filter=subfilter, completed=True)
                    completed = True
                else:
                    completed = False
        else:
            completed = "not required"
        subfilter_json = SubSerializer(subfilter, many=False)
        subfilters_list.append({'subfilter': subfilter_json.data, 'completed': completed})
        if completed == False:
            break
    return JsonResponse(subfilters_list, safe=False)


@login_required
def starter_videos(request):
    training_modules = Module.objects.filter(sub_filter_id=1)
    nutrition_modules = Module.objects.filter(sub_filter_id=2)
    return JsonResponse({
        'training_total': training_modules.count(),
        'training_completed': training_modules.filter(moduleenrolled__user=request.user).count(),
        'nutrition_total': nutrition_modules.count(),
        'nutrition_completed': nutrition_modules.filter(moduleenrolled__user=request.user).count()
    }, safe=False)


@login_required
def sub_filter(request, id):
    sub_filter = cache.get_or_set('single_subfilter' + str(id), SubFilter.objects.get(pk=id), 2592000)
    serializer = SubSerializer(sub_filter, many=False)
    return JsonResponse(serializer.data, safe=False)


@login_required
def introduction_sub_filters(request):
    try:
        introduction_filter = Week.objects.get(order=1)
    except Week.DoesNotExist:
        return HttpResponse(status=400)
    items = SubFilter.objects.filter(main=introduction_filter).order_by('order')
    serializer = SubSerializer(items, many=True)
    return JsonResponse(serializer.data, safe=False)
