from django.db import models
from django.contrib.auth.models import User


class Week(models.Model):
    name = models.CharField(max_length=250)
    order = models.IntegerField(null=True, db_index=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class SubFilter(models.Model):
    name = models.CharField(max_length=250)
    main = models.ForeignKey(Week, null=True, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    order = models.IntegerField(null=True, db_index=True)
    required_to_complete = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class SubFilterCompleted(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    sub_filter = models.ForeignKey(SubFilter, null=True, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False)


