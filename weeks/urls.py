from django.urls import path
from . import views

urlpatterns = [
    path('main-filters/', views.main_filters, name='main_filters'),
    path('sub-filters/<int:id>/', views.sub_filters, name='sub_filters'),
    path('starters/', views.starter_videos, name='starter_videos'),
    path('sub-filter/<int:id>/', views.sub_filter, name='sub_filter'),
    path('introduction-sub-filters/', views.introduction_sub_filters, name='introduction_sub_filters'),
]
