from django.contrib import admin
from .models import Week, SubFilter, SubFilterCompleted

admin.site.register(Week)
admin.site.register(SubFilter)
admin.site.register(SubFilterCompleted)