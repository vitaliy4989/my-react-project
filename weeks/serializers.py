from rest_framework import serializers
from .models import Week, SubFilter, SubFilterCompleted


class MainSerializer(serializers.ModelSerializer):
    class Meta:
        model = Week
        fields = '__all__'


class SubSerializer(serializers.ModelSerializer):
	class Meta:
		model = SubFilter
		fields = '__all__'


class SubFilterCompletedSerializer(serializers.ModelSerializer):
	class Meta:
		model = SubFilterCompleted
		fields = '__all__'