# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-05 15:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('weeks', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='week',
            name='content',
        ),
        migrations.AlterField(
            model_name='weeksenrolled',
            name='week',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='weeks.Week'),
        ),
    ]
