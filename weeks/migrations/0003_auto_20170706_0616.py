# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-06 06:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('weeks', '0002_auto_20170705_1504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='week',
            name='tags',
            field=models.CharField(blank=True, max_length=250),
        ),
        migrations.AlterField(
            model_name='week',
            name='video',
            field=models.TextField(blank=True),
        ),
    ]
