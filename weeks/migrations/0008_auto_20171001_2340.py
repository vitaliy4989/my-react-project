# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-10-01 23:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('weeks', '0007_auto_20170926_0254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='week',
            name='date',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='week',
            name='order',
            field=models.IntegerField(db_index=True, null=True),
        ),
    ]
