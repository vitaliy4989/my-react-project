from django.db import models
from django.contrib.auth.models import User


class Chat(models.Model):
	user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now_add=True)
	client_update = models.BooleanField(default=False)
	coach_update = models.BooleanField(default=False)
	other_update = models.BooleanField(default=False)
	date = models.DateTimeField(auto_now_add=True)


class ChatMessage(models.Model):
	chat = models.ForeignKey(Chat, null=True, on_delete=models.CASCADE)
	profile_u = models.ForeignKey("users.Profile", null=True, on_delete=models.CASCADE)
	profile_c = models.ForeignKey("users.CoachProfile", null=True, on_delete=models.CASCADE)
	comment = models.TextField(null=True)
	date = models.DateTimeField(auto_now_add=True)


class FAQTopic(models.Model):
	name = models.CharField(max_length=250)

	def __str__(self):
		return self.name

class FAQQuestion(models.Model):
	topic = models.ForeignKey(FAQTopic, null=True, on_delete=models.CASCADE)
	key_words = models.CharField(max_length=250, db_index=True)
	question = models.TextField(null=True)
	answer = models.TextField(null=True)

	def __str__(self):
		return self.question