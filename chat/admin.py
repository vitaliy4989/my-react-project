from django.contrib import admin
from .models import Chat, ChatMessage, FAQTopic, FAQQuestion


class ChatMessageAdmin(admin.ModelAdmin):
	readonly_fields = (
        'profile_u','profile_c', 'chat',
    )

admin.site.register(Chat)
admin.site.register(FAQTopic)
admin.site.register(FAQQuestion)
admin.site.register(ChatMessage, ChatMessageAdmin)