from rest_framework import serializers
from users.serializers import ProfileSerializer, UserSerializer,  CoachProfileSerializer
from .models import ChatMessage, Chat, FAQTopic, FAQQuestion


class ChatSerializer(serializers.ModelSerializer):

	user = UserSerializer(read_only=True)
	
	class Meta:
		model = Chat
		fields = '__all__'


class ChatMessageSerializer(serializers.ModelSerializer):

	profile_c = CoachProfileSerializer(read_only=True)
	profile_u = ProfileSerializer(read_only=True)
	
	class Meta:
		model = ChatMessage
		fields = '__all__'


class FAQTopicSerializer(serializers.ModelSerializer):

	class Meta:
		model = FAQTopic
		fields = '__all__'


class FAQQuestionSerializer(serializers.ModelSerializer):

	class Meta:
		model = FAQQuestion
		fields = '__all__'