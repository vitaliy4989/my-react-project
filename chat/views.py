from django.shortcuts import redirect, get_list_or_404
from users.models import Profile, CoachProfile
from .models import ChatMessage, Chat, FAQTopic, FAQQuestion
from users.serializers import ProfileSerializer
from .serializers import ChatMessageSerializer, ChatSerializer, FAQTopicSerializer, FAQQuestionSerializer
from django.utils import timezone
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from rest_framework.response import Response
from django.http import Http404
from rest_framework import permissions
from rest_framework.viewsets import ViewSet
from analytics.models import ChatAnswered
from django.db.models import Count


def welcome_chat_message(request):
    if request.user.first_name:
        name = request.user.first_name
    else:
        name = request.user.username
    try:
        if Chat.objects.filter(user=request.user).exists():
            pass
        else:
            chat = Chat.objects.create(user=request.user, coach_update=True)
            ChatMessage.objects.create(
                chat=chat,
                profile_c=CoachProfile.objects.get(pk=9),
                comment='Hi ' + name + ', welcome to the JSA. This is your trainer support chat, where you can ask us questions about your fitness! This chat will become available when you upgrade from your trial. To get things started, please complete the steps on your home tab.',
            )
    except Exception as e:
        pass
    return


class ChatAnsweredViewSet(ViewSet):

    def list(self, request):
        completed_by_coach = []
        coaches = cache.get_or_set('coaches_list', CoachProfile.objects.all()[:10], 2592000)
        for coach in coaches:
            chats_answered = ChatAnswered.objects.filter(coach=coach).order_by('-date').extra(
                {'date': "date(date)"}).values('date').annotate(date_count=Count('answered'))
            completed_chats = {
                'username': coach.user.first_name, 'image': coach.image.cdn_url,
                'completed_today': list(chats_answered)
            }
            completed_by_coach.append(completed_chats)
        return Response(completed_by_coach)


class ChatViewSet(ViewSet):

    def get_chat(self, request):
        try:
            return Chat.objects.get(user=request.user)
        except ObjectDoesNotExist:
            return Chat.objects.create(user=request.user)
        except MultipleObjectsReturned:
            chat = Chat.objects.filter(user=request.user).latest('date')
            chat.delete()
            return redirect('/chat/user-chat')

    def list(self, request):
        chat = self.get_chat(request)
        chat_messages = cache.get_or_set(
            'chat_messages' + str(chat.id), ChatMessage.objects.filter(chat=chat.id).order_by('-date')[:50], 2592000)
        chat_serialized = ChatSerializer(chat, many=False)
        chat_messages_serialized = ChatMessageSerializer(
            list(reversed(chat_messages)), many=True)
        return Response({'chat': chat_serialized.data, 'messages': chat_messages_serialized.data})

    def create(self, request):
        chat_instance = self.get_chat(request)
        chat_instance.coach_update = False
        chat_instance.client_update = True
        chat_instance.date = timezone.now()
        chat_instance.save()
        serializer = ChatMessageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(chat=chat_instance, profile_u=request.user.profile)
            cache.delete('chat_messages' + str(chat_instance.id))
            return Response(serializer.data, status=201)
        return redirect('/chat/user-chat')

    def partial_update(self, request, pk=None):
        try:
            item = Chat.objects.get(pk=pk)
        except Chat.DoesNotExist:
            return Response(status=404)
        serializer = ChatSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save(coach_update=False, client_update=False, date=timezone.now())
            return Response(serializer.data)
        return Response(serializer.errors, status=400)


class CoachChatViewSet(ViewSet):
    permission_classes = (permissions.IsAdminUser,)

    def get_chat(self, pk):
        try:
            return Chat.objects.get(pk=pk)
        except Chat.DoesNotExist:
            raise Http404

    def get_profile(self, pk):
        try:
            return Profile.objects.get(user=pk)
        except  Profile.DoesNotExist:
            raise Http404

    def list(self, request):
        queryset = Chat.objects.all()
        if request.GET.get('search'):
            items = get_list_or_404(queryset, user__username__icontains=request.GET.get('search'))
            serializer = ChatSerializer(items, many=True)
            return Response(serializer.data)
        else:
            items = get_list_or_404(queryset.order_by('date'), client_update=True)
            serializer = ChatSerializer(items, many=True)
            return Response({'chats': serializer.data})

    def create(self, request):
        chat_instance = self.get_chat(int(request.POST.get('id')))
        chat_instance.coach_update = True
        chat_instance.client_update = False
        chat_instance.date = timezone.now()
        chat_instance.save()
        serializer = ChatMessageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(chat=chat_instance, profile_c=request.user.coachprofile)
            cache.delete('chat_messages' + str(chat_instance.id))
            ChatAnswered.objects.create(
                coach=request.user.coachprofile,
                answered=True
            )
            return Response(serializer.data, status=201)
        return Response(status=200)

    def retrieve(self, request, pk=None):
        chat_instance = self.get_chat(pk)
        chat_serialized = ChatSerializer(chat_instance, many=False)
        chat_messages = cache.get_or_set('chat_messages' + str(chat_instance.id),
                                         ChatMessage.objects.filter(chat=chat_instance.id).order_by('-date')[:50],
                                         2592000)
        profile_serialized = ProfileSerializer(self.get_profile(chat_instance.user.id), many=False)
        messages_serialized = ChatMessageSerializer(list(reversed(chat_messages)), many=True)
        return Response({
            'chat': chat_serialized.data,
            'profile': profile_serialized.data,
            'comments': messages_serialized.data
        })

    def destroy(self, request, pk=None):
        try:
            item = Chat.objects.get(pk=pk)
        except Chat.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class ChatOtherUpdateViewSet(ViewSet):

    def partial_update(self, request, pk=None):
        try:
            item = Chat.objects.get(pk=pk)
        except Chat.DoesNotExist:
            return Response(status=404)
        serializer = ChatSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save(other_update=False)
            return Response(serializer.data)
        return Response(serializer.errors, status=400)


class FAQTopicViewSet(ViewSet):

    def list(self, request):
        queryset = FAQTopic.objects.all()
        items = cache.get_or_set('topics', get_list_or_404(queryset), 2592000)
        serializer = FAQTopicSerializer(items, many=True)
        return Response(serializer.data)


class FAQQuestionViewSet(ViewSet):

    def list(self, request):
        queryset = FAQQuestion.objects.filter(topic=request.GET.get('id'))
        items = cache.get_or_set('questions' + str(request.GET.get('id')), get_list_or_404(queryset), 2592000)
        serializer = FAQQuestionSerializer(items, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = FAQQuestion.objects.all()
        item = get_list_or_404(queryset, key_words__icontains=pk)
        serializer = FAQQuestionSerializer(item, many=True)
        return Response(serializer.data)
