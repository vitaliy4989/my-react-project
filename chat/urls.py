from django.urls import path
from . import views
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register(r'user-chat', views.ChatViewSet, 'ChatViewSet')
router.register(r'chats-answered', views.ChatAnsweredViewSet, 'ChatAnsweredViewSet')
router.register(r'coach-chat', views.CoachChatViewSet, 'CoachChatViewSet')
router.register(r'other-update', views.ChatOtherUpdateViewSet, 'ChatOtherUpdateViewSet')
router.register(r'faq-topics', views.FAQTopicViewSet, 'FAQTopicViewSet')
router.register(r'faq-questions', views.FAQQuestionViewSet, 'FAQQuestionViewSet')

urlpatterns = router.urls