# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-10-23 02:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0002_auto_20171022_1125'),
    ]

    operations = [
        migrations.AddField(
            model_name='chat',
            name='client_update',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='chat',
            name='coach_update',
            field=models.BooleanField(default=False),
        ),
    ]
