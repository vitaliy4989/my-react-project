from django.contrib import admin
from .models import Module, ModuleEnrolled, FavModules
from import_export.admin import ImportExportModelAdmin
from django import forms


class ModuleEquipmentForm(forms.ModelForm):
    MY_CHOICES = (
        ('N/A', 'N/A'),
        ('Barbell', 'Barbell'),
        ('Rings', 'Rings'),
        ('TRX', 'TRX'),
        ('Dumbbell', 'Dumbbell'),
        ('Machine', 'Machine'),
        ('Body Weight', 'Body Weight'),
        ('Cardio', 'Cardio'),
        ('Cables', 'Cables'),
        ('Other', 'Other'),
        ('Kettlebell', 'Kettlebell'),
        ('Bands', 'Bands'),
    )

    equipment = forms.ChoiceField(choices=MY_CHOICES)


class ModuleAdmin(admin.ModelAdmin):
    search_fields = ['name', 'tags', ]

    form = ModuleEquipmentForm

    def complete_delete(modeladmin, request, queryset):
        for obj in queryset:
            obj.delete()

    actions = [complete_delete]

    complete_delete.short_description = "Delete - USE THIS TO DELETE"


class ModuleEnrolledAdmin(admin.ModelAdmin):
    readonly_fields = (
        'user',
        'module'
    )


@admin.register(FavModules)
class FavModulesAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Module, ModuleAdmin)
admin.site.register(ModuleEnrolled, ModuleEnrolledAdmin)
