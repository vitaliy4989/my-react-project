from rest_framework import serializers
from .models import Module, ModuleEnrolled, FavModules
from weeks.serializers import SubSerializer


class ModuleEnrolledSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuleEnrolled
        fields = "__all__"


class ModuleSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ('id', 'name')


class ModuleSerializer(serializers.ModelSerializer):
    sub_filter = SubSerializer(read_only=True)

    class Meta:
        model = Module
        fields = "__all__"


class FavModulesSerializer(serializers.ModelSerializer):
    module = ModuleSerializer(read_only=True)

    class Meta:
        model = FavModules
        fields = "__all__"
