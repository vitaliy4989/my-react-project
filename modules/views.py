from django.shortcuts import get_object_or_404, get_list_or_404
from .models import Module, ModuleEnrolled, FavModules
from .serializers import ModuleSerializer, ModuleEnrolledSerializer, FavModulesSerializer, ModuleSearchSerializer
from django.core.cache import cache
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from django.http import Http404


class FavouritesModulesViewSet(ViewSet):

    def get_module(self, pk):
        try:
            return Module.objects.get(pk=pk)
        except Module.DoesNotExist:
            raise Http404

    def list(self, request):
        queryset = FavModules.objects.all()
        items = get_list_or_404(queryset, user=request.user)
        serializer = FavModulesSerializer(items, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = FavModulesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user, module=self.get_module(request.POST.get('module')))
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = FavModules.objects.get(module=pk, user=request.user)
        except FavModules.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class ModuleViewSet(ViewSet):

    def list(self, request):
        queryset = cache.get_or_set('all_modules', Module.objects.all(), 2592000)
        items = get_list_or_404(queryset, tags__icontains=request.GET.get('search'))
        module_serializer = ModuleSearchSerializer(items, many=True)
        return Response(module_serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Module.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = ModuleSerializer(item)
        module_enrolled = cache.get_or_set('module_enrolled' + str(str(pk) + str(request.user.id)),
                                           ModuleEnrolled.objects.filter(user=request.user, module=pk).exists(),
                                           2592000)
        return Response(
            {'favourite': FavModules.objects.filter(user=request.user, module=item).exists(), 'module': serializer.data,
             'module_enrolled': module_enrolled})


class UncompletedModuleViewSet(ViewSet):

    def list(self, request):
        queryset = Module.objects.all()
        items = queryset.exclude(moduleenrolled__isnull=False, moduleenrolled__user=request.user)[:25]
        serializer = ModuleSerializer(items, many=True)
        return Response(serializer.data)


class ModuleEnrolledViewSet(ViewSet):

    def get_module_enrolled(self, pk, request):
        try:
            ModuleEnrolled.objects.get(user=request.user, module=pk)
            return True
        except ModuleEnrolled.DoesNotExist:
            return False
        except ModuleEnrolled.MultipleObjectsReturned:
            ModuleEnrolled.objects.filter(user=request.user, module=pk).delete()
            return False

    def create(self, request):
        if self.get_module_enrolled(request.POST.get('module'), request) == False:
            cache.delete('module_enrolled' + str(str(request.POST.get('module')) + str(request.user.id)))
            serializer = ModuleEnrolledSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save(user=request.user, score=100)
                return Response({'completed': True}, status=201)
        else:
            return Response({'completed': True}, status=201)


class RecentModuleViewSet(ViewSet):

    def list(self, request):
        modules_list = []
        for module in cache.get_or_set('recent_modules',
                                       Module.objects.order_by('-date').values('id', 'name', 'video', 'vimeo_url',
                                                                               'available_on_trial')[:15],
                                       86400):
            module_serializer = ModuleSerializer(module, many=False)
            module_enrolled = cache.get_or_set('module_enrolled' + str(str(module['id']) + str(request.user.id)),
                                               ModuleEnrolled.objects.filter(user=request.user,
                                                                             module=module['id']).exists(), 2592000)
            modules_list.append({'module': module_serializer.data, 'completed': module_enrolled})
        return Response(modules_list)


class CategoryModulesViewSet(ViewSet):

    def retrieve(self, request, pk=None):
        modules_list = []
        for module in cache.get_or_set('subfilter_modules' + str(pk),
                                       Module.objects.filter(sub_filter=pk).order_by('order').values('id', 'name',
                                                                                                     'video',
                                                                                                     'vimeo_url',
                                                                                                     'available_on_trial')[
                                       :100],
                                       86400):
            module_serializer = ModuleSerializer(module, many=False)
            module_enrolled = cache.get_or_set('module_enrolled' + str(str(module['id']) + str(request.user.id)),
                                               ModuleEnrolled.objects.filter(user=request.user,
                                                                             module=module['id']).exists(), 2592000)
            modules_list.append({'module': module_serializer.data, 'completed': module_enrolled})
        return Response(modules_list)


class OtherRelatedVideos(ViewSet):

    def list(self, request):
        modules_list = []
        for module in cache.get_or_set(
                'other_related_videos' + str(request.GET.get('subfilter_id')) + str(request.GET.get('module_id')),
                Module.objects.filter(sub_filter=request.GET.get('subfilter_id')).order_by('order').values('id', 'name',
                                                                                                           'video')[
                :50], 86400):
            module_serializer = ModuleSerializer(module, many=False)
            module_enrolled = cache.get_or_set('module_enrolled' + str(str(module['id']) + str(request.user.id)),
                                               ModuleEnrolled.objects.filter(user=request.user,
                                                                             module=module['id']).exists(), 2592000)
            modules_list.append({'module': module_serializer.data, 'completed': module_enrolled})
        return Response(modules_list)


class NextModuleViewSet(ViewSet):

    def list(self, request):
        sub_filter = int(request.GET.get('sub_filter'))
        if request.GET.get('current_order'):
            next_in_order = int(request.GET.get('current_order')) + 1
            try:
                module = Module.objects.get(order=next_in_order, sub_filter=sub_filter)
            except Module.DoesNotExist:
                another_module = Module.objects.filter(sub_filter=sub_filter).order_by('order').exclude(
                    pk=request.GET.get('module_id'))[:1]
                module = Module.objects.get(pk=another_module[0].id)
            except Exception as e:
                another_module = Module.objects.filter(sub_filter=sub_filter).order_by('order').exclude(
                    pk=request.GET.get('module_id'))[:1]
                module = Module.objects.get(pk=another_module[0].id)
        else:
            another_module = Module.objects.filter(sub_filter=sub_filter).order_by('order').exclude(
                pk=request.GET.get('module_id'))[:1]
            module = Module.objects.get(pk=another_module[0].id)
        module_serializer = ModuleSerializer(module, many=False)
        return Response({'favourite': FavModules.objects.filter(user=request.user, module=module).exists(),
                         'module': module_serializer.data,
                         'module_enrolled': ModuleEnrolled.objects.filter(module=module, user=request.user).exists()})
