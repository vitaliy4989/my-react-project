from . import views
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register(r'favourites', views.FavouritesModulesViewSet, 'FavouritesModulesViewSet')
router.register(r'module', views.ModuleViewSet, 'ModuleViewSet')
router.register(r'uncompleted', views.UncompletedModuleViewSet, 'UncompletedModuleViewSet')
router.register(r'enrolled', views.ModuleEnrolledViewSet, 'ModuleEnrolledViewSet')
router.register(r'recent', views.RecentModuleViewSet, 'RecentModuleViewSet')
router.register(r'next', views.NextModuleViewSet, 'NextModuleViewSet')
router.register(r'category', views.CategoryModulesViewSet, 'CategoryModulesViewSet')
router.register(r'other-related-videos', views.OtherRelatedVideos, 'OtherRelatedVideos')

urlpatterns = router.urls