# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-02-04 21:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modules', '0025_auto_20171104_0129'),
    ]

    operations = [
        migrations.AddField(
            model_name='module',
            name='available_for_program',
            field=models.BooleanField(default=False),
        ),
    ]
