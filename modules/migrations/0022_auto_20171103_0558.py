# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-03 05:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('modules', '0021_remove_module_week'),
    ]

    operations = [
        migrations.AlterField(
            model_name='moduleenrolled',
            name='module',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='enrolled_modules', to='modules.Module'),
        ),
    ]
