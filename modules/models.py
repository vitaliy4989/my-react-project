from django.db import models
from django.contrib.auth.models import User


class Module(models.Model):
    name = models.CharField(max_length=250)
    video = models.TextField(blank=True)
    vimeo_url = models.URLField(blank=True, null=True)
    equipment = models.CharField(max_length=250, null=True, db_index=True)
    tags = models.CharField(max_length=250, null=True, db_index=True)
    sub_filter = models.ForeignKey("weeks.SubFilter", null=True, on_delete=models.CASCADE)
    order = models.IntegerField(null=True, db_index=True)
    available_on_trial = models.BooleanField(default=False)
    available_for_program = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.name


class ModuleEnrolled(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, null=True, on_delete=models.CASCADE)
    score = models.IntegerField(null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.user.username + "-" + self.module.name


class FavModules(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username + "-" + self.module.name