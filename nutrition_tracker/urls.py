from django.urls import path
from . import views
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register(r'food-log-day', views.DayViewSet, 'DayViewSet')
router.register(r'food-log-meal', views.MealViewSet, 'MealViewSet')
router.register(r'macros', views.MacrosViewSet, 'MacrosViewSet')
router.register(r'clone-food-log-day', views.CloneDayViewSet, 'CloneDayViewSet')
router.register(r'nutritionix-meal', views.NutritionixMealViewSet, 'NutritionixMealViewSet')

urlpatterns = router.urls
