from rest_framework import serializers
from .models import Day, Meal, Macros
from users.serializers import UserSerializer
from recipes.serializers import RecipeSerializer
from foods.serializers import FoodSerializer


class MacrosSerializer(serializers.ModelSerializer):
	class Meta:
		model = Macros
		fields = '__all__'


class DaySerializer(serializers.ModelSerializer):
	user = UserSerializer(read_only=True)
	class Meta:
		model = Day
		fields = '__all__'


class MealSerializer(serializers.ModelSerializer):
	user = UserSerializer(read_only=True)
	food = FoodSerializer(read_only=True)
	recipe = RecipeSerializer(read_only=True)
	class Meta:
		model = Meal
		fields = '__all__'


