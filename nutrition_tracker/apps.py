from django.apps import AppConfig


class NutritionTrackerConfig(AppConfig):
    name = 'nutrition_tracker'
