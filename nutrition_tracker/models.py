from django.db import models
from django.contrib.auth.models import User



class Day(models.Model):
	name = models.CharField(max_length=250, null=True)
	user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
	order = models.IntegerField(null=True, default=0, db_index=True)
	remaining_protein = models.FloatField(null=True, default=0)
	remaining_carbs = models.FloatField(null=True, default=0)
	remaining_fat = models.FloatField(null=True, default=0)
	remaining_calories = models.FloatField(null=True, default=0)
	date = models.DateTimeField(auto_now_add=True, db_index=True, null=True)

	def __str__(self):
		return self.user.username


class Meal(models.Model):
	name = models.CharField(max_length=250, null=True)
	day = models.ForeignKey(Day, null=True, on_delete=models.CASCADE)
	user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
	protein = models.FloatField(null=True, blank=True)
	calories = models.FloatField(null=True, blank=True)
	carbs = models.FloatField(null=True, blank=True)
	fat = models.FloatField(null=True, blank=True)
	servings = models.FloatField(null=True, default=1)
	recipe = models.ForeignKey('recipes.Recipe', null=True, on_delete=models.CASCADE)
	food = models.ForeignKey('foods.Food', null=True, on_delete=models.CASCADE)
	order = models.IntegerField(null=True, default=0, db_index=True)
	date = models.DateTimeField(auto_now_add=True, db_index=True, null=True)

	def __str__(self):
		return self.user.username


class Macros(models.Model):
	user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
	protein = models.FloatField(null=True, default=0)
	carbs = models.FloatField(null=True, default=0)
	fat = models.FloatField(null=True, default=0)
	calories = models.FloatField(null=True, default=0)
	date = models.DateTimeField(auto_now_add=True, db_index=True, null=True)

	def __str__(self):
		return self.user.username



