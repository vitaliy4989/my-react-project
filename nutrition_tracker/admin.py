from django.contrib import admin
from .models import Day, Meal, Macros


class DayAdmin(admin.ModelAdmin):
    readonly_fields = (
        'user',
    )


class MacrosAdmin(admin.ModelAdmin):
    readonly_fields = (
        'user',
    )


admin.site.register(Day, DayAdmin)
admin.site.register(Meal)
admin.site.register(Macros, MacrosAdmin)
