from django.shortcuts import get_list_or_404, get_object_or_404
from django.http import HttpResponse
from django.utils.html import strip_tags
from .models import Day, Meal, Macros
from django.core.cache import cache
from .serializers import DaySerializer, MealSerializer
from recipes.models import Recipe
from foods.models import Food
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response


class DayViewSet(ViewSet):

    def list(self, request):
        days = cache.get_or_set('days' + str(request.user.id),
                                Day.objects.filter(user=request.user).order_by('-date')[:50], 2592000)
        days_serialized = DaySerializer(days, many=True)
        return Response(days_serialized.data)

    def create(self, request):
        serializer = DaySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(
                user=request.user,
            )
            cache.delete('days' + str(request.user.id))
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Day.objects.all()
        item = cache.get_or_set('day' + str(pk), get_object_or_404(queryset, pk=pk), 2592000)
        serializer = DaySerializer(item)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        try:
            item = Day.objects.get(pk=pk)
        except Day.DoesNotExist:
            return Response(status=404)
        item.delete()
        cache.delete('days' + str(request.user.id))
        cache.delete('day' + str(pk))
        cache.delete('meals' + str(pk))
        return Response(status=204)


class MealViewSet(ViewSet):

    def list(self, request):
        queryset = Meal.objects.all()
        items = get_list_or_404(queryset.order_by('-date'), user=request.user)
        serializer = MealSerializer(items[:20], many=True)
        return Response(serializer.data)

    def create(self, request):
        day = Day.objects.get(pk=request.POST.get('day_id'))
        if request.POST.get('recipe_id'):
            recipe = Recipe.objects.get(pk=request.POST.get('recipe_id'))
        else:
            recipe = None
        if request.POST.get('food_id'):
            food = Food.objects.get(pk=request.POST.get('food_id'))
        else:
            food = None
        Meal.objects.create(
            name=strip_tags(request.POST.get('name')),
            day=day,
            user=request.user,
            recipe=recipe,
            food=food,
        )
        day.remaining_protein = day.remaining_protein - float(request.POST.get('protein'))
        day.remaining_calories = day.remaining_calories - float(request.POST.get('calories'))
        day.save()
        cache.delete('day' + str(request.POST.get('day_id')))
        cache.delete('meals' + str(request.POST.get('day_id')))
        return HttpResponse(status=200)

    def retrieve(self, request, pk=None):
        meals = cache.get_or_set(
            'meals' + str(pk), Meal.objects.filter(day=pk).order_by('-id')[:50], 2592000)
        meals_serialized = MealSerializer(meals, many=True)
        return Response(meals_serialized.data)

    def partial_update(self, request, pk=None):
        if pk and request.POST.get('day_id'):
            day = Day.objects.get(pk=request.POST.get('day_id'))
            meal = Meal.objects.get(pk=pk)
            if day and meal:
                # Get the current remaining protein and calories.
                current_remaining_calories = day.remaining_calories
                current_remaining_protein = day.remaining_protein

                # Determine meals impact on day macros and calories

                meal_current_calories = float(request.POST.get('starting_calories'))
                meal_current_protein = float(request.POST.get('starting_protein'))

                # First add the current meal protein and calorie content back to the day.
                starting_remaining_calories = current_remaining_calories + meal_current_calories
                starting_remaining_protein = current_remaining_protein + meal_current_protein

                # Now subtract the updated servings from the starting remaining calories and protein.
                day.remaining_calories = starting_remaining_calories - float(request.POST.get('calories'))
                day.remaining_protein = starting_remaining_protein - float(request.POST.get('protein'))
                day.save()

                # Save meal new macros
                meal.servings = float(request.POST.get('servings'))
                meal.protein = float(request.POST.get('protein'))
                meal.calories = float(request.POST.get('calories'))
                meal.fat = float(request.POST.get('fat'))
                meal.carbs = float(request.POST.get('carbs'))
                meal.save()

                cache.delete('day' + str(request.POST.get('day_id')))
                cache.delete('meals' + str(request.POST.get('day_id')))
            return HttpResponse(status=200)

    def destroy(self, request, pk=None):
        if pk and request.POST.get('day_id'):
            Meal.objects.filter(pk=pk).delete()
            day = Day.objects.get(pk=request.POST.get('day_id'))
            remaining_calories = day.remaining_calories
            remaining_protein = day.remaining_protein
            day.remaining_calories = float(remaining_calories) + float(request.POST.get('calories'))
            day.remaining_protein = float(remaining_protein) + float(request.POST.get('protein'))
            day.save()
            cache.delete('day' + str(request.POST.get('day_id')))
            cache.delete('meals' + str(request.POST.get('day_id')))
        return HttpResponse(status=200)


class MacrosViewSet(ViewSet):

    def list(self, request):
        try:
            macros = cache.get_or_set('macros' + str(request.user.id),
                                      Macros.objects.filter(user=request.user).latest('date'), 2592000)
            return Response({'protein': macros.protein, 'calories': macros.calories})
        except Exception as e:
            return Response({'protein': 0, 'calories': 0})

    def create(self, request):
        if request.POST.get('protein') is not None and request.POST.get('calories') is not None:
            macros = Macros.objects.create(
                user=request.user,
                protein=float(request.POST.get('protein')),
                calories=float(request.POST.get('calories')),
            )
            cache.delete('macros' + str(request.user.id))
            return Response({'protein': macros.protein, 'calories': macros.calories})
        else:
            return HttpResponse(status=400)


class CloneDayViewSet(ViewSet):

    def create(self, request):
        day = Day.objects.get(pk=request.POST.get('log_id'))
        cloned_day = Day.objects.create(
            name=day.name,
            user=day.user,
            remaining_protein=day.remaining_protein,
            remaining_carbs=day.remaining_carbs,
            remaining_fat=day.remaining_fat,
            remaining_calories=day.remaining_calories
        )
        day_meals = Meal.objects.filter(day=day.id).order_by('id')
        for meal in day_meals:
            Meal.objects.create(
                name=meal.name,
                day=cloned_day,
                user=meal.user,
                servings=meal.servings,
                recipe=meal.recipe,
                food=meal.food,
                order=meal.order
            )
        cache.delete('days' + str(request.user.id))
        cache.delete('day' + str(request.POST.get('log_id')))
        cache.delete('meals' + str(request.POST.get('log_id')))
        return HttpResponse(status=200)


class NutritionixMealViewSet(ViewSet):

    def create(self, request):
        food = Food.objects.create(
            name=request.POST.get('name'),
            source='Nutritionix',
            protein=request.POST.get('protein'),
            carbs=request.POST.get('carbs'),
            fat=request.POST.get('fat'),
            calories=request.POST.get('calories'),
            serving_size=request.POST.get('serving_unit')
        )
        day = Day.objects.get(pk=request.POST.get('day_id'))
        Meal.objects.create(
            name=strip_tags(request.POST.get('name')),
            day=day,
            user=request.user,
            recipe=None,
            food=food,
            servings=request.POST.get('serving_qty')
        )
        day.remaining_protein = day.remaining_protein - float(request.POST.get('protein'))
        day.remaining_calories = day.remaining_calories - float(request.POST.get('calories'))
        day.save()
        cache.delete('day' + str(request.POST.get('day_id')))
        cache.delete('meals' + str(request.POST.get('day_id')))
        return HttpResponse(status=200)
