from django.db import models
from django.urls import reverse
from pyuploadcare.dj.models import ImageField


class Blog(models.Model):
    title = models.CharField(max_length=100, unique=True)
    photo = ImageField(null=True)
    slug = models.SlugField(max_length=100, unique=True)
    body = models.TextField()
    posted = models.DateTimeField(db_index=True, auto_now_add=True)
    category = models.ForeignKey('blog.Category', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % self.title

    def get_absolute_url(self):
        return reverse('blog:view_blog_post', args=[str(self.slug)])



class Category(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, db_index=True)

    def __str__(self):
        return '%s' % self.title

    def get_absolute_url(self):
        return reverse('blog:view_blog_category', args=[str(self.slug)])
