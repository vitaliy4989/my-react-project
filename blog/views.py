from .models import Blog, Category
from django.shortcuts import render, get_object_or_404
from django.core.cache import cache


def index(request):
    return render(request, 'blog/index.html', {
        'categories': cache.get_or_set('categories', Category.objects.all(), 2592000),
        'posts': cache.get_or_set('posts', Blog.objects.all().order_by('-posted')[:50], 2592000)
    })


def view_post(request, slug):
    return render(request, 'blog/view_post.html', {
        'post': cache.get_or_set("post" + slug, Blog.objects.get(slug=slug), 2592000)
    })


def view_category(request, slug):
    category = get_object_or_404(Category, slug=slug)
    return render(request, 'blog/view_category.html', {
        'category': category,
        'posts': Blog.objects.filter(category=category)[:50]
    })
