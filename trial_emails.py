import os
from django.core.wsgi import get_wsgi_application
from django.core.cache.backends.memcached import BaseMemcachedCache

BaseMemcachedCache.close = lambda self, **kwargs: None
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "academy.settings")
application = get_wsgi_application()

from emails.views import trial_email_followup

trial_email_followup()