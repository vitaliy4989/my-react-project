from django.db import models
from django.contrib.auth.models import User
from pyuploadcare.dj.models import FileField


class Training(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    coach = models.ForeignKey("users.CoachProfile", null=True, on_delete=models.SET_NULL)
    age = models.IntegerField(null=True)
    training_age = models.CharField(max_length=250, null=True)
    gender = models.CharField(max_length=250, null=True)
    goal = models.CharField(max_length=250, null=True)
    frequency = models.CharField(max_length=250, null=True)
    facility = models.CharField(max_length=250, null=True)
    medical = models.CharField(max_length=250, null=True)
    file = FileField(null=True)
    user_request = models.BooleanField(default=False, db_index=True)
    coach_completed = models.BooleanField(default=False, db_index=True)
    message_update = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True, db_index=True)


class SavedExercise(models.Model):
    name = models.CharField(max_length=250, blank=True, db_index=True)
    body_part = models.CharField(max_length=250, blank=True, null=True, db_index=True)
    link = models.CharField(max_length=250, blank=True)

    def __str__(self):
        return self.name


class TrainingDay(models.Model):
    training_programme = models.ForeignKey(Training, null=True, on_delete=models.CASCADE)
    type = models.CharField(max_length=250, blank=True, null=True)
    rounds = models.FloatField(blank=True, null=True)
    time_completed_in = models.FloatField(blank=True, null=True)
    name = models.CharField(max_length=250, blank=True)
    saved = models.BooleanField(default=False, db_index=True)
    notes = models.TextField(null=True, blank=True)
    user_change_log = models.TextField(null=True, blank=True)
    order = models.IntegerField(null=True, default=0, db_index=True)

    def __str__(self):
        return self.name


class Superset(models.Model):
    training_day = models.ForeignKey(TrainingDay, null=True, on_delete=models.CASCADE)
    training_log_day = models.ForeignKey("training.TrainingLogDay", null=True, on_delete=models.CASCADE)
    color = models.CharField(max_length=250, blank=True)


class TrainingExercise(models.Model):
    training_day = models.ForeignKey(TrainingDay, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=250, blank=True, db_index=True)
    link = models.CharField(max_length=250, blank=True)
    superset = models.BooleanField(default=False)
    superset_group = models.ForeignKey(Superset, null=True, on_delete=models.SET_NULL)
    order = models.IntegerField(null=True, default=0, db_index=True)


class TrainingExerciseSet(models.Model):
    t_p_exercise = models.ForeignKey(TrainingExercise, null=True, on_delete=models.CASCADE)
    weight = models.CharField(max_length=250, blank=True)
    reps = models.CharField(max_length=250, blank=True)
    tempo = models.CharField(max_length=250, blank=True)
    rest = models.CharField(max_length=250, blank=True)


class TrainingComment(models.Model):
    training = models.ForeignKey(Training, null=True, on_delete=models.CASCADE)
    comment = models.TextField(null=True)
    user = models.ForeignKey("users.Profile", null=True, on_delete=models.CASCADE)
    coach = models.ForeignKey("users.CoachProfile", null=True, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True, db_index=True, null=True)


class TrainingLogDay(models.Model):
    name = models.CharField(max_length=250, null=True)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    type = models.CharField(max_length=250, blank=True, null=True)
    focus = models.CharField(max_length=250, blank=True, null=True)
    rounds = models.FloatField(blank=True, null=True)
    experience = models.CharField(max_length=250, blank=True, null=True)
    time_completed_in = models.FloatField(blank=True, null=True)
    goal = models.CharField(max_length=250, null=True, blank=True)
    facility = models.CharField(max_length=250, null=True, blank=True)
    for_program_templates = models.BooleanField(default=False, db_index=True)
    notes = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, db_index=True)


class Exercise(models.Model):
    training_log_day = models.ForeignKey(TrainingLogDay, null=True, on_delete=models.CASCADE)
    video = models.ForeignKey("modules.Module", null=True, on_delete=models.CASCADE)
    alternative_name = models.CharField(max_length=250, null=True, blank=True)
    superset_group = models.ForeignKey(Superset, null=True, on_delete=models.SET_NULL)
    order = models.IntegerField(null=True, default=0, db_index=True)


class Set(models.Model):
    exercise = models.ForeignKey(Exercise, null=True, on_delete=models.CASCADE)
    reps = models.CharField(max_length=250, null=True)
    rest = models.CharField(max_length=250, null=True)
    weight = models.CharField(max_length=250, null=True)


class StepsLeaderboard(models.Model):
    profile = models.ForeignKey("users.Profile", null=True, on_delete=models.CASCADE)
    steps = models.IntegerField(null=True, default=0, db_index=True)
    date = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return str(self.steps) + '-' + self.profile.user.first_name
