from django.contrib import admin
from .models import Training, TrainingComment, TrainingLogDay, Exercise, Set, TrainingExercise, TrainingExerciseSet, \
    TrainingDay, SavedExercise, StepsLeaderboard, Superset
from import_export.admin import ImportExportModelAdmin

admin.site.register(Training)
admin.site.register(TrainingLogDay)
admin.site.register(Superset)
admin.site.register(Exercise)
admin.site.register(Set)
admin.site.register(TrainingExercise)
admin.site.register(TrainingExerciseSet)
admin.site.register(TrainingDay)
admin.site.register(StepsLeaderboard)


class TrainingCommentAdmin(admin.ModelAdmin):
    readonly_fields = (
        'user', 'coach', 'training',
    )


admin.site.register(TrainingComment, TrainingCommentAdmin)


@admin.register(SavedExercise)
class SavedExerciseAdmin(ImportExportModelAdmin):
    pass
