# Generated by Django 2.0.3 on 2018-04-27 12:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0022_trainingprogrammeexercise'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingprogrammeexercise',
            name='link',
            field=models.CharField(blank=True, max_length=250),
        ),
        migrations.AddField(
            model_name='trainingprogrammeexercise',
            name='name',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
