# Generated by Django 2.0.3 on 2018-04-27 11:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0019_auto_20180427_1154'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingcomment',
            name='training_programme',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='training.TrainingProgramme'),
        ),
    ]
