# Generated by Django 2.0.3 on 2018-04-30 06:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0035_auto_20180430_0647'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingday',
            name='order',
            field=models.IntegerField(db_index=True, default=0, null=True),
        ),
    ]
