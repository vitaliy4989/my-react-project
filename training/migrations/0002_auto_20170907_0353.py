# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-07 03:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='training',
            name='age',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='training',
            name='gender',
            field=models.CharField(max_length=250, null=True),
        ),
    ]
