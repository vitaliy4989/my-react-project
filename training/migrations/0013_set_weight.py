# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-02-06 04:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0012_exercise_video'),
    ]

    operations = [
        migrations.AddField(
            model_name='set',
            name='weight',
            field=models.FloatField(null=True),
        ),
    ]
