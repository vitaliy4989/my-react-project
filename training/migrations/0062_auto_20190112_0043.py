# Generated by Django 2.1.4 on 2019-01-12 00:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0061_auto_20190112_0042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trainingexercise',
            name='training_exercises',
            field=models.ManyToManyField(to='training.Superset'),
        ),
    ]
