# Generated by Django 2.0.3 on 2018-04-30 06:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0034_auto_20180430_0444'),
    ]

    operations = [
        migrations.CreateModel(
            name='TrainingDay',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=250)),
                ('training_programme', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='training.Training')),
            ],
        ),
        migrations.RemoveField(
            model_name='trainingexercise',
            name='training_programme',
        ),
        migrations.AddField(
            model_name='trainingexercise',
            name='training_day',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='training.TrainingDay'),
        ),
    ]
