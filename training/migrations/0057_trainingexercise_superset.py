# Generated by Django 2.1.3 on 2018-11-26 21:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0056_trainingday_user_change_log'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingexercise',
            name='superset',
            field=models.BooleanField(default=False),
        ),
    ]
