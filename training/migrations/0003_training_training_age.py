# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-22 00:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0002_auto_20170907_0353'),
    ]

    operations = [
        migrations.AddField(
            model_name='training',
            name='training_age',
            field=models.IntegerField(null=True),
        ),
    ]
