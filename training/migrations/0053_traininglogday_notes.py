# Generated by Django 2.0.7 on 2018-09-26 11:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0052_set_rest'),
    ]

    operations = [
        migrations.AddField(
            model_name='traininglogday',
            name='notes',
            field=models.TextField(blank=True, null=True),
        ),
    ]
