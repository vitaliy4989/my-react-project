# Generated by Django 2.0.3 on 2018-04-30 04:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0030_auto_20180430_0442'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tpexercise',
            name='training_programme',
        ),
        migrations.DeleteModel(
            name='TPExercise',
        ),
    ]
