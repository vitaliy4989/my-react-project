from django.urls import path
from . import views
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register(r'training', views.TrainingViewSet, 'TrainingViewSet')
router.register(r'user-training', views.UserTrainingViewSet, 'UserTraining')
router.register(r'superset', views.SupersetViewSet, 'SupersetViewSet')
router.register(r'coach-training', views.CoachTrainingViewSet, 'CoachTraining')
router.register(r'training-requests-search', views.TrainingRequestsSearchViewSet, 'TrainingRequestsSearch')
router.register(r'saved-exercise', views.SavedExerciseViewSet, 'SavedExercise')
router.register(r'training-day', views.TrainingDayViewSet, 'TrainingDay')
router.register(r'coach-training-day', views.CoachTrainingDayViewSet, 'CoachTrainingDay')
router.register(r'training-exercise', views.TrainingExerciseViewSet, 'TrainingExercise')
router.register(r'training-exercise-set', views.TrainingExerciseSetViewSet, 'TrainingExerciseSet')
router.register(r'training-comment', views.TrainingCommentViewSet, 'TrainingComment')
router.register(r'training-comment-update', views.TrainingCommentMessageUpdateViewSet, 'TrainingCommentMessageUpdateViewSet')
router.register(r'coach-training-comment', views.CoachTrainingCommentViewSet, 'CoachTrainingComment')
router.register(r'training-log-day', views.TrainingLogDayViewSet, 'TrainingLogDay')
router.register(r'exercise', views.ExerciseViewSet, 'Exercise')
router.register(r'set', views.SetViewSet, 'Set')
router.register(r'assign-training', views.AssignTrainingViewSet, 'AssignTrainingViewSet')
router.register(r'steps', views.StepsLeaderboardViewSet, 'StepsLeaderboard')
router.register(r'exercise-videos', views.SearchExerciseVideosViewSet, 'SearchExerciseVideos')
router.register(r'exercise-videos-by-equipment', views.SearchExerciseVideosByEquipmentViewSet, 'SearchExerciseVideosByEquipment')
router.register(r'search-previous-requests', views.SearchPreviousRequestsViewSet, 'SearchPreviousRequests')
router.register(r'program-credit', views.ProgramCreditViewSet, 'ProgramCreditViewSet')

urlpatterns = [
    path('copy-training-day/', views.CopyTrainingDay.as_view(), name='CopyTrainingDay'),
    path('copy-training-log-day/', views.CopyTrainingLogDay.as_view(), name='CopyTrainingLogDay'),
    path('client-duplicate-training-day/', views.ClientCloneTrainingDay.as_view(), name='ClientCloneTrainingDay'),
    path('copy-saved-exercise-to-log/', views.CopySavedExerciseToLogList.as_view(), name='CopySavedExerciseToLog'),
    path('saved-training-day/', views.SavedTrainingDayList.as_view(), name='SaveTrainingDay'),
    path('coach-finished-training/', views.CoachFinishedTrainingList.as_view(), name='CoachFinishedTraining'),
    path('user-training-pdf/<int:pk>/', views.user_training_pdf, name='user_training_pdf'),
    path('training-log-day-pdf/<int:pk>/', views.training_log_day_pdf, name='training_log_day_pdf'),
]

urlpatterns += router.urls


