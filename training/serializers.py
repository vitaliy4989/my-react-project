from rest_framework import serializers
from .models import Training, TrainingComment, TrainingLogDay, Exercise, Set, TrainingExercise, TrainingExerciseSet, \
    SavedExercise, TrainingDay, StepsLeaderboard, Superset
from users.serializers import UserSerializer, ProfileSerializer, CoachProfileSerializer
from modules.serializers import ModuleSerializer


class TrainingExerciseSetSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingExerciseSet
        fields = '__all__'


class SupersetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Superset
        fields = '__all__'


class TrainingExerciseSerializer(serializers.ModelSerializer):
    superset_group = SupersetSerializer(read_only=True)

    class Meta:
        model = TrainingExercise
        fields = '__all__'


class TrainingDaySerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingDay
        fields = '__all__'


class TrainingCommentSerializer(serializers.ModelSerializer):
    training = serializers.PrimaryKeyRelatedField(queryset=Training.objects.all())
    user = ProfileSerializer(read_only=True)
    coach = CoachProfileSerializer(read_only=True)

    class Meta:
        model = TrainingComment
        fields = '__all__'


class TrainingSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    coach = CoachProfileSerializer(read_only=True)

    class Meta:
        model = Training
        fields = '__all__'

    def create(self, validated_data):
        training = Training(
            user=self.context['request'].user,
            age=validated_data['age'],
            training_age=validated_data['training_age'],
            gender=validated_data['gender'],
            goal=validated_data['goal'],
            frequency=validated_data['frequency'],
            facility=validated_data['facility'],
            medical=validated_data['medical'],
            user_request=self.context['user_update'],
            coach_completed=self.context['coach_update'],
        )
        training.save()
        TrainingComment.objects.create(
            training=training,
            user=self.context['user_profile'],
            comment=self.context['comment']
        )
        return training


class SavedExerciseSerializer(serializers.ModelSerializer):
    class Meta:
        model = SavedExercise
        fields = '__all__'


class TrainingLogDaySerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingLogDay
        fields = '__all__'


class ExerciseSerializer(serializers.ModelSerializer):
    video = ModuleSerializer(read_only=True)
    superset_group = SupersetSerializer(read_only=True)

    class Meta:
        model = Exercise
        fields = '__all__'


class SetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Set
        fields = '__all__'


class StepsLeaderboardSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = StepsLeaderboard
        fields = '__all__'
