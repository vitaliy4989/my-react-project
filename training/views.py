from django.shortcuts import get_object_or_404, get_list_or_404
from django.template.loader import render_to_string
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from .models import Training, TrainingComment, TrainingLogDay, Exercise, Set, TrainingExercise, TrainingExerciseSet, \
    TrainingDay, SavedExercise, StepsLeaderboard, Superset
from users.models import CoachProfile
import datetime
from django.core.cache import cache
from django.utils import timezone
from .serializers import TrainingSerializer, TrainingCommentSerializer, TrainingLogDaySerializer, ExerciseSerializer, \
    SetSerializer, TrainingExerciseSerializer, TrainingExerciseSetSerializer, TrainingDaySerializer, \
    SavedExerciseSerializer, StepsLeaderboardSerializer, SupersetSerializer
from modules.models import Module
from modules.serializers import ModuleSerializer, ModuleSearchSerializer
from chat.models import ChatMessage, Chat
import sendgrid
from xhtml2pdf import pisa
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from rest_framework import permissions
from rest_framework.viewsets import ViewSet
import random
from analytics.models import ProgramOrProgramChat
from django.db.models import Count


class TrainingViewSet(ViewSet):
    permission_classes = (permissions.IsAdminUser,)

    def list(self, request):
        queryset = Training.objects.filter(
            user_request=True).order_by('date')[:50]
        serializer = TrainingSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        training = Training.objects.create(
            training_age=request.POST.get('training_age'),
            gender=request.POST.get('gender'),
            goal=request.POST.get('goal'),
            frequency=request.POST.get('frequency'),
            facility=request.POST.get('facility')
        )
        serializer = TrainingSerializer(training, many=False)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Training.objects.all()
        item = get_list_or_404(queryset, goal=pk, user=None)
        serializer = TrainingSerializer(item, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = Training.objects.get(pk=pk)
        except Training.DoesNotExist:
            return Response(status=404)
        serializer = TrainingSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Training.objects.get(pk=pk)
        except Training.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class UserTrainingViewSet(ViewSet):

    def list(self, request):
        queryset = Training.objects.filter(user=request.user)
        serializer = TrainingSerializer(queryset.order_by('-date'), many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = TrainingSerializer(
            data=request.data,
            context={
                "request": request,
                "user_profile": request.user.profile,
                "coach_update": False,
                "user_update": True,
                "comment": request.POST.get('comment')
            }
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Training.objects.all()
        item = get_object_or_404(queryset, pk=pk, user=request.user)
        serializer = TrainingSerializer(item)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = Training.objects.get(pk=pk)
        except Training.DoesNotExist:
            return Response(status=404)
        serializer = TrainingSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Training.objects.get(pk=pk)
        except Training.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


@login_required
def user_training_pdf(request, pk):
    training_day = TrainingDay.objects.get(pk=pk)
    if training_day.training_programme.user.username == request.user.username or request.user.is_staff:
        try:
            day_name = training_day.name.split('-', 1)[0]
        except Exception as e:
            day_name = training_day.name
        training_exercises = TrainingExercise.objects.filter(
            training_day=training_day).order_by('order')
        exercises_sets = []
        for exercise in training_exercises:
            sets = TrainingExerciseSet.objects.filter(
                t_p_exercise=exercise.id).order_by('id')
            exercises_sets.append({'exercise': exercise, 'sets': sets})
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=' + \
                                          '"' + day_name + '.pdf' + '"'
        pisaStatus = pisa.CreatePDF(render_to_string('training/program.html', {
            'day_name': day_name, 'training_day': training_day, 'training_exercises': exercises_sets}), dest=response,
                                    link_callback=None)
        return response


class CoachTrainingViewSet(ViewSet):
    permission_classes = (permissions.IsAdminUser,)

    def list(self, request):
        queryset = Training.objects.filter(
            user_request=True).order_by('date')[:50]
        serializer = TrainingSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = TrainingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Training.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = TrainingSerializer(item)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = Training.objects.get(pk=pk)
        except Training.DoesNotExist:
            return Response(status=404)
        serializer = TrainingSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Training.objects.get(pk=pk)
        except Training.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class TrainingRequestsSearchViewSet(ViewSet):
    permission_classes = (permissions.IsAdminUser,)

    def list(self, request):
        requests = Training.objects.filter(user_request=True).order_by('-date').extra({'date': "date(date)"}).values(
            'date').annotate(date_count=Count('id'))
        coaches = cache.get_or_set('coaches_list', CoachProfile.objects.all()[:10], 2592000)
        completed_by_coach = []
        for coach in coaches:
            programs_answered = ProgramOrProgramChat.objects.filter(coach=coach).order_by('-date').extra(
                {'date': "date(date)"}).values('date').annotate(date_count=Count('program'))
            completed_programs = {'image': coach.image.cdn_url, 'completed_today': list(programs_answered)}
            completed_by_coach.append(completed_programs)
        return JsonResponse({'requests': list(requests), 'completed_by_coach': completed_by_coach}, safe=False)

    def retrieve(self, request, pk=None):
        queryset = Training.objects.all()
        item = get_list_or_404(queryset, user__username__icontains=pk)
        serializer = TrainingSerializer(item, many=True)
        return Response(serializer.data)


class CoachFinishedTrainingList(APIView):
    permission_classes = (permissions.IsAdminUser,)

    def post(self, request, format=None):
        training = Training.objects.get(pk=request.POST.get('id'))
        chat_comment = "NOTIFICATION: Please visit your personalised exercise program. If you have any questions or would like changes to the program, please let me know on the designated program messages."
        training.coach = request.user.coachprofile
        training.user_request = False
        training.coach_completed = True
        training.date = timezone.now()
        training.save()
        try:
            chat = Chat.objects.filter(user=training.user.id)
            if chat.exists():
                chat_instance = Chat.objects.get(user=training.user.id)
                ChatMessage.objects.create(
                    chat=chat_instance,
                    profile_c=request.user.coachprofile,
                    comment=chat_comment
                )
                chat.update(
                    other_update=True
                )
                cache.delete('chat_messages' + str(chat_instance.id))
            else:
                pass
            if training.user.email:
                link = 'https://www.jamessmithacademy.com/users/#/training/requested/' + \
                       str(request.POST.get('id'))
                sg = sendgrid.SendGridAPIClient(
                    apikey='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')
                data = {
                    "personalizations": [{"to": [{"email": training.user.email}], "subject": "Training Notification"}],
                    "from": {
                        "email": "notifications@jamessmithacademy.com"}, "content": [{"type": "text/html",
                                                                                 "value": render_to_string(
                                                                                     'training/training_notification.html',
                                                                                     {'link': link})}]}
                sg.client.mail.send.post(request_body=data)
        except Exception as e:
            pass
        cache.delete('training_messages' + str(training.id))
        return HttpResponse(status=200)


class CoachTrainingDayViewSet(ViewSet):
    permission_classes = (permissions.IsAdminUser,)

    def list(self, request):
        queryset = TrainingDay.objects.filter(
            name__icontains=request.GET.get('name'), saved=True)
        serializer = TrainingDaySerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = TrainingDaySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = TrainingDay.objects.all()
        item = get_list_or_404(queryset.order_by(
            'order'), training_programme=pk)
        serializer = TrainingDaySerializer(item, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = TrainingDay.objects.get(pk=pk)
        except TrainingDay.DoesNotExist:
            return Response(status=404)
        serializer = TrainingDaySerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = TrainingDay.objects.get(pk=pk)
        except TrainingDay.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class TrainingDayViewSet(ViewSet):

    def list(self, request):
        queryset = TrainingDay.objects.filter(training_programme=request.GET.get('id'))
        serializer = TrainingDaySerializer(queryset.order_by('order'), many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = TrainingDaySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = TrainingDay.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = TrainingDaySerializer(item)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = TrainingDay.objects.get(pk=pk)
        except TrainingDay.DoesNotExist:
            return Response(status=404)
        serializer = TrainingDaySerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = TrainingDay.objects.get(pk=pk)
        except TrainingDay.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class SavedTrainingDayList(APIView):
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request, format=None):
        queryset = TrainingDay.objects.filter(name__icontains=request.GET.get('name'), saved=True)[:100]
        serializer = TrainingDaySerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        training_day = TrainingDay.objects.get(pk=request.POST.get('id'))
        copied_training_day = TrainingDay.objects.create(
            name=training_day.name,
            saved=True,
            order=training_day.order,
            notes=training_day.notes,
            type=training_day.type,
            rounds=training_day.rounds,
            time_completed_in=training_day.time_completed_in
        )
        training_exercises = TrainingExercise.objects.filter(
            training_day=training_day.id)
        supersets = []
        for exercise in training_exercises:
            if exercise.superset_group is not None:
                if len(supersets) > 0:
                    cloned_superset = None
                    for superset in supersets:
                        if exercise.superset_group.pk == superset['original_superset'].pk:
                            cloned_superset = superset['cloned_superset']
                            break
                    if cloned_superset is None:
                        cloned_superset = Superset.objects.create(
                            training_day=copied_training_day,
                            color=exercise.superset_group.color
                        )
                        supersets.append({
                            'original_superset': exercise.superset_group,
                            'cloned_superset': cloned_superset
                        })
                else:
                    cloned_superset = Superset.objects.create(
                        training_day=copied_training_day,
                        color=exercise.superset_group.color
                    )
                    supersets.append({
                        'original_superset': exercise.superset_group,
                        'cloned_superset': cloned_superset
                    })
            else:
                cloned_superset = None
            copied_training_exercise = TrainingExercise.objects.create(
                training_day=copied_training_day,
                name=exercise.name,
                link=exercise.link,
                order=exercise.order,
                superset=exercise.superset,
                superset_group=cloned_superset
            )
            exercise_sets = TrainingExerciseSet.objects.filter(
                t_p_exercise=exercise.id)
            for set in exercise_sets:
                TrainingExerciseSet.objects.create(
                    t_p_exercise=copied_training_exercise,
                    weight='',
                    reps=set.reps,
                    tempo=set.tempo,
                    rest=set.rest,
                )
        return HttpResponse(status=200)


class CopyTrainingDay(APIView):
    permission_classes = (permissions.IsAdminUser,)

    def post(self, request, format=None):
        training_programme = Training.objects.get(
            pk=request.POST.get('program_id'))
        training_day = TrainingDay.objects.get(pk=request.POST.get('day_id'))
        copied_training_day = TrainingDay.objects.create(
            training_programme=training_programme,
            name=training_day.name,
            order=training_day.order,
            notes=training_day.notes,
            type=training_day.type,
            rounds=training_day.rounds,
            time_completed_in=training_day.time_completed_in
        )
        training_exercises = TrainingExercise.objects.filter(
            training_day=training_day.id)
        supersets = []
        for exercise in training_exercises:
            if exercise.superset_group is not None:
                if len(supersets) > 0:
                    cloned_superset = None
                    for superset in supersets:
                        if exercise.superset_group.pk == superset['original_superset'].pk:
                            cloned_superset = superset['cloned_superset']
                            break
                    if cloned_superset is None:
                        cloned_superset = Superset.objects.create(
                            training_day=copied_training_day,
                            color=exercise.superset_group.color
                        )
                        supersets.append({
                            'original_superset': exercise.superset_group,
                            'cloned_superset': cloned_superset
                        })
                else:
                    cloned_superset = Superset.objects.create(
                        training_day=copied_training_day,
                        color=exercise.superset_group.color
                    )
                    supersets.append({
                        'original_superset': exercise.superset_group,
                        'cloned_superset': cloned_superset
                    })
            else:
                cloned_superset = None
            copied_training_exercise = TrainingExercise.objects.create(
                training_day=copied_training_day,
                name=exercise.name,
                link=exercise.link,
                order=exercise.order,
                superset=exercise.superset,
                superset_group=cloned_superset
            )
            exercise_sets = TrainingExerciseSet.objects.filter(
                t_p_exercise=exercise.id).order_by('id')
            for set in exercise_sets:
                TrainingExerciseSet.objects.create(
                    t_p_exercise=copied_training_exercise,
                    weight='',
                    reps=set.reps,
                    tempo=set.tempo,
                    rest=set.rest,
                )
        return HttpResponse(status=200)


class CopyTrainingLogDay(APIView):

    def get(self, request):
        queryset = TrainingLogDay.objects.filter(for_program_templates=True)
        item = cache.get_or_set('for_program_templates', get_list_or_404(queryset.order_by('-date')), 2592000)
        serializer = TrainingLogDaySerializer(item, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        log_day = TrainingLogDay.objects.get(pk=request.POST.get('day_log_id'))
        copied_log_day = TrainingLogDay.objects.create(
            name=log_day.name,
            user=request.user,
            type=log_day.type,
            rounds=log_day.rounds,
            time_completed_in=log_day.time_completed_in,
            notes=log_day.notes,
            goal=log_day.goal,
            facility=log_day.facility
        )
        log_exercises = Exercise.objects.filter(
            training_log_day=log_day.id).order_by('id')
        supersets = []
        for exercise in log_exercises:
            if exercise.superset_group is not None:
                if len(supersets) > 0:
                    cloned_superset = None
                    for superset in supersets:
                        if exercise.superset_group.pk == superset['original_superset'].pk:
                            cloned_superset = superset['cloned_superset']
                            break
                    if cloned_superset is None:
                        cloned_superset = Superset.objects.create(
                            training_log_day=copied_log_day,
                            color=exercise.superset_group.color
                        )
                        supersets.append({
                            'original_superset': exercise.superset_group,
                            'cloned_superset': cloned_superset
                        })
                else:
                    cloned_superset = Superset.objects.create(
                        training_log_day=copied_log_day,
                        color=exercise.superset_group.color
                    )
                    supersets.append({
                        'original_superset': exercise.superset_group,
                        'cloned_superset': cloned_superset
                    })
            else:
                cloned_superset = None
            copied_log_exercise = Exercise.objects.create(
                training_log_day=copied_log_day,
                video=exercise.video,
                alternative_name=exercise.alternative_name,
                order=exercise.order,
                superset_group=cloned_superset
            )
            exercise_sets = Set.objects.filter(
                exercise=exercise.id).order_by('id')
            for set in exercise_sets:
                Set.objects.create(
                    exercise=copied_log_exercise,
                    weight=set.weight,
                    reps=set.reps,
                    rest=set.rest,
                )
        return HttpResponse(status=200)


class ClientCloneTrainingDay(APIView):

    def post(self, request, format=None):
        training_programme = Training.objects.get(
            pk=request.POST.get('training_id'))
        training_day = TrainingDay.objects.get(pk=request.POST.get('day_id'))
        copied_training_day = TrainingDay.objects.create(
            training_programme=training_programme,
            name=training_day.name + ' Clone',
            order=training_day.order,
            notes=training_day.notes,
            type=training_day.type,
            rounds=training_day.rounds,
            time_completed_in=training_day.time_completed_in
        )
        training_exercises = TrainingExercise.objects.filter(
            training_day=training_day.id)
        supersets = []
        for exercise in training_exercises:
            if exercise.superset_group is not None:
                if len(supersets) > 0:
                    cloned_superset = None
                    for superset in supersets:
                        if exercise.superset_group.pk == superset['original_superset'].pk:
                            cloned_superset = superset['cloned_superset']
                            break
                    if cloned_superset is None:
                        cloned_superset = Superset.objects.create(
                            training_day=copied_training_day,
                            color=exercise.superset_group.color
                        )
                        supersets.append({
                            'original_superset': exercise.superset_group,
                            'cloned_superset': cloned_superset
                        })
                else:
                    cloned_superset = Superset.objects.create(
                        training_day=copied_training_day,
                        color=exercise.superset_group.color
                    )
                    supersets.append({
                        'original_superset': exercise.superset_group,
                        'cloned_superset': cloned_superset
                    })
            else:
                cloned_superset = None
            copied_training_exercise = TrainingExercise.objects.create(
                training_day=copied_training_day,
                name=exercise.name,
                link=exercise.link,
                order=exercise.order,
                superset=exercise.superset,
                superset_group=cloned_superset
            )
            exercise_sets = TrainingExerciseSet.objects.filter(
                t_p_exercise=exercise.id).order_by('id')
            for set in exercise_sets:
                TrainingExerciseSet.objects.create(
                    t_p_exercise=copied_training_exercise,
                    weight=set.weight,
                    reps=set.reps,
                    tempo=set.tempo,
                    rest=set.rest,
                )
        return HttpResponse(status=200)


class SupersetViewSet(ViewSet):

    def list(self, request):
        queryset = Superset.objects.all()
        if request.GET.get('training_day_id') is not None:
            items = get_list_or_404(queryset, training_day=request.GET.get('training_day_id'))
        if request.GET.get('training_log_day_id') is not None:
            items = get_list_or_404(queryset, training_log_day=request.GET.get('training_log_day_id'))
        serializer = SupersetSerializer(items, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = SupersetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Superset.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = SupersetSerializer(item)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = Superset.objects.get(pk=pk)
        except Superset.DoesNotExist:
            return Response(status=404)
        serializer = SupersetSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Superset.objects.get(pk=pk)
        except Superset.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class SavedExerciseViewSet(ViewSet):

    def list(self, request):
        queryset = cache.get_or_set('saved_exercises', SavedExercise.objects.all(), 2592000)
        items = get_list_or_404(queryset, body_part__icontains=request.GET.get('name'))
        serializer = SavedExerciseSerializer(items, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = SavedExerciseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

        training_exercise_serializer = TrainingExerciseSerializer(
            data=request.data)
        if training_exercise_serializer.is_valid():
            training_exercise_serializer.save()
            cache.delete('saved_exercises')
            return Response(status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = SavedExercise.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = SavedExerciseSerializer(item)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = SavedExercise.objects.get(pk=pk)
        except SavedExercise.DoesNotExist:
            return Response(status=404)
        serializer = SavedExerciseSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            cache.delete('saved_exercises')
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = SavedExercise.objects.get(pk=pk)
        except SavedExercise.DoesNotExist:
            return Response(status=404)
        item.delete()
        cache.delete('saved_exercises')
        return Response(status=204)


class CopySavedExerciseToLogList(APIView):

    def post(self, request, format=None):
        training_log_day = TrainingLogDay.objects.get(
            pk=request.POST.get('log_id'))
        Exercise.objects.create(
            training_log_day=training_log_day,
            alternative_name=request.POST.get('exercise_name'),
            order=request.POST.get('order')
        )
        return HttpResponse(status=200)


class TrainingExerciseViewSet(ViewSet):

    def list(self, request):
        queryset = TrainingExercise.objects.filter(
            name__icontains=request.GET.get('name'))
        serializer = TrainingExerciseSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = TrainingExerciseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = TrainingExercise.objects.all()
        item = get_list_or_404(queryset.order_by('order'), training_day=pk)
        serializer = TrainingExerciseSerializer(item, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = TrainingExercise.objects.get(pk=pk)
        except TrainingExercise.DoesNotExist:
            return Response(status=404)
        serializer = TrainingExerciseSerializer(item, data=request.data)
        if serializer.is_valid():
            if request.POST.get('superset_group') is not None:
                if request.POST.get('superset_group') == '':
                    serializer.save(superset_group=None)
                else:
                    superset = Superset.objects.get(pk=request.POST.get('superset_group'))
                    serializer.save(superset_group=superset)
            else:
                serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = TrainingExercise.objects.get(pk=pk)
        except TrainingExercise.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class TrainingExerciseSetViewSet(ViewSet):

    def get_training_exercise(self, pk):
        try:
            return TrainingExercise.objects.get(pk=pk)
        except TrainingExercise.DoesNotExist:
            raise Http404

    def list(self, request):
        queryset = TrainingExerciseSet.objects.all()
        serializer = TrainingExerciseSetSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = TrainingExerciseSetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(t_p_exercise=self.get_training_exercise(
                request.POST.get('t_p_exercise')))
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = TrainingExerciseSet.objects.all()
        item = get_list_or_404(queryset.order_by('id'), t_p_exercise=pk)
        serializer = TrainingExerciseSetSerializer(item, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = TrainingExerciseSet.objects.get(pk=pk)
        except TrainingExerciseSet.DoesNotExist:
            return Response(status=404)
        serializer = TrainingExerciseSetSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = TrainingExerciseSet.objects.get(pk=pk)
        except TrainingExerciseSet.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class TrainingCommentViewSet(ViewSet):

    def get_training(self, request, pk):
        try:
            return Training.objects.get(pk=pk, user=request.user)
        except Training.DoesNotExist:
            raise Http404

    def create(self, request):
        serializer = TrainingCommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user.profile)
            training_instance = self.get_training(request, request.POST.get('training'))
            training_instance.user_request = True
            training_instance.coach_completed = False
            training_instance.date = timezone.now()
            training_instance.save()
            cache.delete('training_messages' +
                         str(request.POST.get('training')))
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        training_program = self.get_training(request, pk)
        queryset = TrainingComment.objects.all()
        item = cache.get_or_set('training_messages' + str(pk),
                                get_list_or_404(queryset.order_by('-date'), training=training_program.pk), 2592000)
        serializer = TrainingCommentSerializer(list(reversed(item)), many=True)
        return Response(serializer.data)


class TrainingCommentMessageUpdateViewSet(ViewSet):

    def create(self, request):
        training = Training.objects.get(pk=request.POST.get('id'))
        training.message_update = False
        training.save()
        serializer = TrainingSerializer(training, many=False)
        return Response(serializer.data)


class CoachTrainingCommentViewSet(ViewSet):
    permission_classes = (permissions.IsAdminUser,)

    def get_training(self, pk):
        try:
            return Training.objects.get(pk=pk)
        except Training.DoesNotExist:
            raise Http404

    def email_notification(self, training, request):
        if training.user.email:
            try:
                link = 'https://www.jamessmithacademy.com/users/#/training/requested/' + \
                       str(training.id)
                sg = sendgrid.SendGridAPIClient(
                    apikey='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')
                data = {
                    "personalizations": [{"to": [{"email": training.user.email}], "subject": "Training Notification"}],
                    "from": {
                        "email": "notifications@jamessmithacademy.com"}, "content": [{"type": "text/html",
                                                                                 "value": render_to_string(
                                                                                     'training/training_notification.html',
                                                                                     {'link': link})}]}
                sg.client.mail.send.post(request_body=data)
            except Exception as e:
                pass
        return

    def chat_notification(self, training, coach, request):
        try:
            chat_instance = Chat.objects.get(user=training.user.id)
            chat_comment = "NOTIFICATION: Please check your personalised exercise program messages."
            ChatMessage.objects.create(
                chat=chat_instance,
                profile_c=coach,
                comment=chat_comment
            )
            chat_instance.other_update = True
            chat_instance.save()
            cache.delete('chat_messages' + str(chat_instance.id))
        except Exception as e:
            pass
        return

    def list(self, request):
        queryset = TrainingComment.objects.all()
        serializer = TrainingCommentSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = TrainingCommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(coach=request.user.coachprofile)
            training_instance = self.get_training(request.POST.get('training'))
            self.email_notification(training_instance, request)
            self.chat_notification(training_instance, request.user.coachprofile, request)
            training_instance.user_request = False
            training_instance.coach_completed = True
            training_instance.message_update = True
            training_instance.date = timezone.now()
            training_instance.save()
            cache.delete('training_messages' + str(request.POST.get('training')))
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = TrainingComment.objects.all()
        item = cache.get_or_set('training_messages' + str(pk), get_list_or_404(queryset.order_by('-date'), training=pk),
                                2592000)
        serializer = TrainingCommentSerializer(list(reversed(item)), many=True)
        return Response(serializer.data)


class TrainingLogDayViewSet(ViewSet):

    def list(self, request):
        queryset = TrainingLogDay.objects.filter(user=request.user)
        serializer = TrainingLogDaySerializer(
            queryset.order_by('-date'), many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = TrainingLogDaySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = TrainingLogDay.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = TrainingLogDaySerializer(item)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = TrainingLogDay.objects.get(pk=pk)
        except TrainingLogDay.DoesNotExist:
            return Response(status=404)
        serializer = TrainingLogDaySerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = TrainingLogDay.objects.get(pk=pk)
        except TrainingLogDay.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


@login_required
def training_log_day_pdf(request, pk):
    training_log = TrainingLogDay.objects.get(pk=pk)
    if training_log.user.username == request.user.username or request.user.is_staff:
        training_log_exercises = Exercise.objects.filter(
            training_log_day=training_log).order_by('order')
        exercises_sets = []
        for exercise in training_log_exercises:
            sets = Set.objects.filter(exercise=exercise.id).order_by('id')
            exercises_sets.append({'exercise': exercise, 'sets': sets})
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=' + \
                                          '"' + training_log.name + '.pdf' + '"'
        pisaStatus = pisa.CreatePDF(render_to_string(
            'training/log.html', {'day_name': training_log.name, 'training_exercises': exercises_sets}), dest=response,
            link_callback=None)
        return response


class ExerciseViewSet(ViewSet):

    def get_training_day_log(self, pk):
        try:
            return TrainingLogDay.objects.get(pk=pk)
        except TrainingLogDay.DoesNotExist:
            pass

    def get_video(self, pk):
        try:
            return Module.objects.get(pk=pk)
        except Module.DoesNotExist:
            pass

    def list(self, request):
        queryset = Exercise.objects.all()
        serializer = ExerciseSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ExerciseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(video=self.get_video(request.POST.get(
                'video')), training_log_day=self.get_training_day_log(request.POST.get('training_log_day')))
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Exercise.objects.all()
        item = get_list_or_404(queryset.order_by('order', 'id'), training_log_day=pk)
        serializer = ExerciseSerializer(item, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = Exercise.objects.get(pk=pk)
        except Exercise.DoesNotExist:
            return Response(status=404)
        serializer = ExerciseSerializer(item, data=request.data)
        if serializer.is_valid():
            if request.POST.get('superset_group') is not None:
                if request.POST.get('superset_group') == '':
                    serializer.save(superset_group=None)
                else:
                    superset = Superset.objects.get(pk=request.POST.get('superset_group'))
                    serializer.save(superset_group=superset)
            else:
                serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Exercise.objects.get(pk=pk)
        except Exercise.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class SetViewSet(ViewSet):

    def list(self, request):
        queryset = Set.objects.all()
        serializer = SetSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = SetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Set.objects.all()
        item = get_list_or_404(queryset.order_by('id'), exercise=pk)
        serializer = SetSerializer(item, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            item = Set.objects.get(pk=pk)
        except Set.DoesNotExist:
            return Response(status=404)
        serializer = SetSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Set.objects.get(pk=pk)
        except Set.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class AssignTrainingViewSet(ViewSet):
    permission_classes = (permissions.IsAdminUser,)

    def match_accuracy(self, item, match):

        # Find the percentage match percentage to show to coach
        matching_statement = ''
        percentage_match = 0
        if item.goal == match.goal:
            percentage_match = percentage_match + 25
        else:
            matching_statement = matching_statement + ' Program does not match the clients goal.'

        if item.facility == match.facility:
            percentage_match = percentage_match + 25
        else:
            matching_statement = matching_statement + ' Program does not match requested training facility.'

        if item.training_age == match.training_age:
            percentage_match = percentage_match + 25
        else:
            matching_statement = matching_statement + ' Program does not match clients experience, you may need to adjust sets and reps'

        if item.frequency == match.frequency:
            percentage_match = percentage_match + 25
        else:
            matching_statement = matching_statement + ' Program does not match frequency, you will need to adjust the number of days,'
        matching_statement = matching_statement + ' Generated program match accuracy: ' + str(percentage_match) + '%'
        return matching_statement

    def copy_days(self, item, queryresult):

        # Choose a random result.
        training = random.choice(queryresult)

        # Select the training request to copy to.
        training_days = TrainingDay.objects.filter(training_programme=training)

        # Copy randomly selected training day to the training request. 
        for day in training_days:
            copied_training_day = TrainingDay.objects.create(
                training_programme=item,
                name=day.name,
                order=day.order,
                notes=day.notes
            )
            training_exercises = TrainingExercise.objects.filter(
                training_day=day.id)
            for exercise in training_exercises:
                copied_training_exercise = TrainingExercise.objects.create(
                    training_day=copied_training_day,
                    name=exercise.name,
                    link=exercise.link,
                    order=exercise.order
                )
                exercise_sets = TrainingExerciseSet.objects.filter(
                    t_p_exercise=exercise.id).order_by('id')
                for set in exercise_sets:
                    TrainingExerciseSet.objects.create(
                        t_p_exercise=copied_training_exercise,
                        weight='',
                        reps=set.reps,
                        tempo=set.tempo,
                        rest=set.rest,
                    )
        return training

    def retrieve(self, request, pk=None):

        # Get all training programs
        queryset = cache.get_or_set('all_training_request', Training.objects.all(), 2592000)

        # Get current training request
        item = get_object_or_404(queryset, pk=pk)

        # If they have a medical condition, warn coach that they need to talk to the client and stop auto generate.
        if item.medical == "A medical professional HAS NOT given me permission to train" or item.medical == "A medical professional HAS given me permission to train":

            return Response({'message': 'They have a medical condition, please check with them.'})

        else:

            # Get training progams and exclude this request, any requests made by this user and where there are no training days. 
            filter_existing_training = queryset.filter(user=None)

            # Of that list, filter programs which match the goal. 
            filter_by_goal = cache.get_or_set('filter_by_goal' + str(item.goal),
                                              filter_existing_training.filter(goal=item.goal), 2592000)

            # Filter programs which match the facility.
            filter_by_facility = cache.get_or_set('filter_by_facility' + str(item.facility + item.goal),
                                                  filter_by_goal.filter(facility=item.facility), 2592000)

            # Filter programs which match the frequency.
            filter_by_frequency = cache.get_or_set(
                'filter_by_frequency' + str(item.frequency + item.facility + item.goal),
                filter_by_facility.filter(frequency=item.frequency), 2592000)

            # Filter programs which match the training age.
            filter_by_experience = cache.get_or_set(
                'filter_by_frequency' + str(item.training_age + item.frequency + item.facility + item.goal),
                filter_by_frequency.filter(training_age=item.training_age), 2592000)

            # If there are programs which match by experience (the most accurate match, return a random result).
            if len(filter_by_experience) > 0:
                match = self.copy_days(item, filter_by_experience[:50])
                return Response({'message': self.match_accuracy(item, match)})

            # There are no programs which match
            else:
                return Response({'message': 'There are no existing programs which match this request.'})


class StepsLeaderboardViewSet(ViewSet):

    def list(self, request):
        today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
        today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)
        steps_leaderboard = StepsLeaderboard.objects.filter(date__range=(today_min, today_max)).order_by('-steps')

        steps_leaderboard_serializer = StepsLeaderboardSerializer(steps_leaderboard[:40], many=True)
        try:
            my_steps = steps_leaderboard.filter(profile=request.user.profile).latest('date')
            my_steps_serializer = StepsLeaderboardSerializer(my_steps, many=False).data
        except:
            my_steps_serializer = None
        return Response({'steps_leaderboard': steps_leaderboard_serializer.data, 'my_steps': my_steps_serializer})

    def create(self, request):
        serializer = StepsLeaderboardSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(profile=request.user.profile)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)


class SearchExerciseVideosViewSet(ViewSet):

    def list(self, request):
        queryset = cache.get_or_set('all_modules', Module.objects.all(), 2592000)
        items = get_list_or_404(queryset, tags__icontains=request.GET.get('name'), available_for_program=True)
        serializer = ModuleSearchSerializer(items, many=True)
        return Response(serializer.data)


class SearchExerciseVideosByEquipmentViewSet(ViewSet):

    def list(self, request):
        queryset = cache.get_or_set('all_modules', Module.objects.all(), 2592000)
        items = get_list_or_404(queryset, equipment=request.GET.get('equipment'), available_for_program=True)
        serializer = ModuleSearchSerializer(items, many=True)
        return Response(serializer.data)


class SearchPreviousRequestsViewSet(ViewSet):

    def retrieve(self, request, pk=None):
        training = Training.objects.get(pk=pk)
        training_programs = Training.objects.filter(user=training.user).exclude(pk=pk)
        serializer = TrainingSerializer(training_programs, many=True)
        return Response({'training_programs': serializer.data, 'program_count': training_programs.count()})


class ProgramCreditViewSet(ViewSet):

    def create(self, request):
        ProgramOrProgramChat.objects.create(
            coach=request.user.coachprofile,
            program=True
        )
        return HttpResponse(status=200)
