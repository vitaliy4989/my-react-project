from django.contrib import admin
from .models import  MemberAccount, GiftCoupon

class MemberAdmin(admin.ModelAdmin):
	readonly_fields = (
        'user',
    )
	search_fields = ['user__username', 'user__email']


class GiftCouponAdmin(admin.ModelAdmin):
	readonly_fields = (
        'purchased_by',
    )
	search_fields = ['purchased_by__username', 'purchased_by__email', 'coupon_value']


admin.site.register(MemberAccount, MemberAdmin)
admin.site.register(GiftCoupon, GiftCouponAdmin)

