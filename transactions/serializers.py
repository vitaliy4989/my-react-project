from rest_framework import serializers
from .models import MemberAccount
from users.serializers import UserSerializer


class MemberAccountSerializer(serializers.ModelSerializer):

	user = UserSerializer(read_only=True)

	class Meta:
		model = MemberAccount
		fields = '__all__'

