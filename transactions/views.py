from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.http import HttpResponse, JsonResponse
from django.utils.html import strip_tags
from django.contrib.auth.decorators import login_required
from .models import MemberAccount, GiftCoupon
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from datetime import date, datetime
from django.utils import timezone
from dateutil.relativedelta import relativedelta
from .serializers import MemberAccountSerializer
from chat.views import welcome_chat_message
from analytics.models import CanceledOrPurchased
import sendgrid
import stripe
import json
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from emails.views import delete_from_trailnopurchase_list, started_trial_email
from modules.models import ModuleEnrolled
from progress.models import Goal
from nutrition_tracker.models import Macros, Day
from recipes.models import Combination
from home_steps.models import HomeStepOne, HomeStepTwo, HomeStepThree, HomeStepFour, HomeStepFive, HomeStepSix
import uuid
import random
from django.core.cache import cache


@require_POST
@csrf_exempt
def stripe_status(request):
    endpoint_secret = "whsec_QJv0jsUD5teDq4NyyLigD7t7uIIXLpRs"
    stripe.api_key = "sk_live_iayvQaH5IaeEOhkbkLt7vA2z"
    payload = request.body.decode('utf-8')
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None
    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, endpoint_secret
        )
    except ValueError as e:
        print(e)
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:
        print(e)
        return HttpResponse(status=400)
    new_event = json.loads(request.body)
    if new_event['type'] == "customer.subscription.deleted" or new_event['type'] == "customer.subscription.updated":
        data = new_event['data']
        data_object = data['object']
        try:
            stripe_account = MemberAccount.objects.filter(sub_id=data_object.get('id'))
            if stripe_account.exists():
                stripe_account.update(
                    status=data_object.get('status'),
                    at_period_end=data_object.get('cancel_at_period_end'),
                    period_end=data_object.get('current_period_end')
                )
                if data_object.get('status') == 'canceled' and stripe_account[0].user.email:
                    try:
                        sg = sendgrid.SendGridAPIClient(
                            apikey='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')
                        data = {
                            "personalizations": [
                                {
                                    "to": [
                                        {
                                            "email": stripe_account[0].user.email
                                        }
                                    ],
                                    "subject": "Subscription Canceled"
                                }
                            ],
                            "from": {
                                "email": "accounts@jamessmithacademy.com"
                            },
                            "content": [
                                {
                                    "type": "text/html",
                                    "value": render_to_string('transactions/subscription_canceled_offer.html',
                                                              {'user': stripe_account[0].user})
                                }
                            ]
                        }
                        sg.client.mail.send.post(request_body=data)
                    except Exception as e:
                        pass
        except Exception as e:
            pass
        if new_event['type'] == "customer.subscription.deleted":
            CanceledOrPurchased.objects.create(
                canceled=True
            )
    return HttpResponse(status=200)


@login_required
def subscribe_monthly(request):
    if request.method == 'POST' and request.is_secure():

        # Define plan based on currency
        if request.POST.get('currency') == 'aud':
            plan = "plan_Cjhzd0ksX02MeC"
        elif request.POST.get('currency') == 'gbp':
            plan = "plan_Cjhxk6zWcFmSIx"
        elif request.POST.get('currency') == 'usd':
            plan = "plan_Cji0gT4DsYLt6M"
        else:
            return HttpResponse('There has been an error, please contact support.', status=400)

        # Subscribe user
        return subscribe(request, plan, 'Premium Plan - Subscription')

    else:

        return HttpResponse('Your connection is not secure.', status=400)


@login_required
def subscribe_monthly_standard(request):
    if request.method == 'POST' and request.is_secure():

        # Define plan based on currency
        if request.POST.get('currency') == 'aud':
            plan = "plan_ERKwviL2kRYN0y"
        elif request.POST.get('currency') == 'gbp':
            plan = "plan_ERKyytWmyMq1sM"
        elif request.POST.get('currency') == 'usd':
            plan = "plan_ERKxNj24QEroFD"
        else:
            return HttpResponse('There has been an error, please contact support.', status=400)

        # Subscribe user
        return subscribe(request, plan, 'Standard Plan')

    else:

        return HttpResponse('Your connection is not secure.', status=400)


@login_required
def subscribe_jsa_core(request):
    if request.method == 'POST' and request.is_secure():

        # Define plan based on currency

        if request.POST.get('currency') == 'aud':
            plan = "jsa-core-aud-3"
        elif request.POST.get('currency') == 'gbp':
            plan = "jsa-core-gbp-3"
        elif request.POST.get('currency') == 'usd':
            plan = "jsa-core-usd-3"
        else:
            return HttpResponse('There has been an error, please contact support.', status=400)

        # Subscribe user
        return subscribe(request, plan, 'JSA Core')

    else:

        return HttpResponse('Your connection is not secure.', status=400)


@login_required
def stripe_block_purchase(request):
    if request.method == 'POST' and request.is_secure():

        return stripe_charge(request)

    else:

        return HttpResponse('Your connection is not secure.', status=400)


@login_required
def create_paypal_three_month(request):
    if request.method == 'POST':

        # Get member account
        member_account = get_member_account(request)

        # Determine membership end date
        if member_account.membership_end_date is not None:

            if member_account.status == 'active' and member_account.membership_end_date > timezone.now():
                # Convert months into membership end date and add to current membership end date
                end_date = member_account.membership_end_date + relativedelta(months=+int(3))

            else:
                # Convert months into membership end date
                end_date = date.today() + relativedelta(months=+int(3))
        else:
            # Convert months into membership end date
            end_date = date.today() + relativedelta(months=+int(3))

        # if the purchase is a gift, then create a coupon, or else credit the users account.
        if request.POST.get('gift') is not None:

            gift_coupon = GiftCoupon.objects.create(
                purchased_by=request.user,
                recipient_email="",
                recipient_message="",
                coupon_value=uuid.uuid4().hex[:6].upper() + str(random.choice('String'))
            )

            # Track purchase for analytics
            CanceledOrPurchased.objects.create(
                purchased=True
            )

            # Remove from trialing email list
            delete_from_trailnopurchase_list(request.user.email)

            return JsonResponse({'giftCouponId': gift_coupon.id, 'coupon': gift_coupon.coupon_value}, safe=False)

        else:

            # Check whether a refund is necessary and if so process
            check_refund = check_for_refund(request, member_account)

            # Update member account
            update_member_account(member_account,
                                  membership_name='Premium Plan - 3 Month Block',
                                  customer_id='None',
                                  card_id='None',
                                  status='active',
                                  sub_id='None',
                                  membership_end_date=end_date,
                                  trial_end_date=None
                                  )

            # Track payment for analytics
            CanceledOrPurchased.objects.create(
                purchased=True
            )

            # Remove from trialing email list
            delete_from_trailnopurchase_list(request.user.email)

            return HttpResponse(
                'Your payment was successful and membership is active, please visit account info to view your details. Thank you.')


def subscribe(request, plan, plan_name):
    # Get the user member account
    member_account = get_member_account(request)

    if member_account:
        # Check whether a refund is necessary and if so process
        check_refund = check_for_refund(request, member_account)

        if check_refund == True:
            return create_subscription(request, member_account, plan, plan_name)
        else:
            return HttpResponse('There has been an error, please contact support.', status=400)


def stripe_charge(request):
    stripe.api_key = "sk_live_iayvQaH5IaeEOhkbkLt7vA2z"

    # Get the user member account
    member_account = get_member_account(request)

    # Check whether a refund is necessary and if so process
    if request.POST.get('gift') is None:
        check_refund = check_for_refund(request, member_account)

    # Attempt subscription
    try:

        # Determine membership end date
        if member_account.membership_end_date is not None:

            if member_account.status == 'active' and member_account.membership_end_date > timezone.now():
                # Convert months into membership end date and add to current membership end date
                end_date = member_account.membership_end_date + relativedelta(months=+int(request.POST.get('months')))

            else:
                # Convert months into membership end date
                end_date = date.today() + relativedelta(months=+int(request.POST.get('months')))
        else:
            # Convert months into membership end date
            end_date = date.today() + relativedelta(months=+int(request.POST.get('months')))

        # Create new Stripe customer
        stripe_customer = stripe.Customer.create(
            description="Account for " + request.user.username,
            source=request.POST.get('tokenId')
        )

        # Get charge amount
        amount = int(request.POST.get('amount')) / 100

        # Create Stripe charge
        stripe_charge = stripe.Charge.create(
            amount=int(request.POST.get('amount')),
            currency=request.POST.get('currency'),
            receipt_email=request.POST.get('tokenEmail'),
            customer=stripe_customer.id,
            description="James Smith Academy Pty Ltd (ABN:72621129511). Charge of " + str(amount) + request.POST.get(
                'currency')
        )

        # If charge succeeded
        if stripe_charge.status == 'succeeded':

            # Get card information
            sources = stripe_customer.sources['data']
            card = sources[0]

            # if the purchase is a gift, then create a coupon, or else credit the users account.
            if request.POST.get('gift') is not None:

                gift_coupon = GiftCoupon.objects.create(
                    purchased_by=request.user,
                    recipient_email="",
                    recipient_message="",
                    coupon_value=uuid.uuid4().hex[:6].upper() + str(random.choice('String'))
                )

                # Track purchase for analytics
                CanceledOrPurchased.objects.create(
                    purchased=True
                )

                # Remove from trialing email list
                delete_from_trailnopurchase_list(request.user.email)

                return JsonResponse({'giftCouponId': gift_coupon.id, 'coupon': gift_coupon.coupon_value}, safe=False)

            else:

                update_member_account(member_account,
                                      membership_name=strip_tags(request.POST.get('membership')),
                                      customer_id=stripe_customer.id,
                                      card_id=card.id,
                                      status='active',
                                      sub_id='None',
                                      membership_end_date=end_date,
                                      trial_end_date=None
                                      )

                # Track purchase for analytics
                CanceledOrPurchased.objects.create(
                    purchased=True
                )

                # Remove from trialing email list
                delete_from_trailnopurchase_list(request.user.email)

                return HttpResponse(
                    'Your payment was successful and membership is active, please visit account info to view your details. Thank you.',
                    status=200)

        else:

            return HttpResponse('Charge failed, please try again or contact support.', status=400)

    # Throw an exception
    except stripe.error.CardError as e:
        body = e.json_body
        err = body.get('error', {})
        return HttpResponse(
            "Status is: " + str(e.http_status) + ", Type is: " + str(err.get('type')) + ", Code is: "
            + str(err.get('code')) + ", Param is: " + str(err.get('param')) + ", Message is: " + str(err.get('message'))
            , status=400)
    except stripe.error.RateLimitError as e:
        return HttpResponse(e, status=400)
    except stripe.error.InvalidRequestError as e:
        return HttpResponse(e, status=400)
    except stripe.error.AuthenticationError as e:
        return HttpResponse(e, status=400)
    except stripe.error.APIConnectionError as e:
        return HttpResponse(e, status=400)
    except stripe.error.StripeError as e:
        return HttpResponse(e, status=400)
    except Exception as e:
        return HttpResponse(e, status=400)


def get_member_account(request):
    try:
        member_account = MemberAccount.objects.get(user=request.user)
        return member_account
    except MemberAccount.DoesNotExist:
        member_account = MemberAccount.objects.create(
            user=request.user
        )
        return member_account


def update_member_account(member_account, **kwargs):
    member_account.membership_name = kwargs['membership_name']
    member_account.customer_id = kwargs['customer_id']
    member_account.card_id = kwargs['card_id']
    member_account.status = kwargs['status']
    member_account.sub_id = kwargs['sub_id']
    member_account.membership_end_date = kwargs['membership_end_date']
    member_account.trial_end_date = kwargs['trial_end_date']
    member_account.save()
    return


def check_for_refund(request, member_account):
    stripe.api_key = "sk_live_iayvQaH5IaeEOhkbkLt7vA2z"

    # If Stripe subscription is active and subscription id exists, attempt to refund
    if member_account.status == 'active' and member_account.sub_id != None:

        try:
            # Retrieve subscription from Stripe
            stripe_subscription = stripe.Subscription.retrieve(member_account.sub_id)

            # Check again that subscription is active
            if stripe_subscription.status == 'active':

                # If subscription is active, get most recent customer charge from Stripe
                charge = stripe.Charge.list(limit=1, customer=member_account.customer_id)

                # Get the most recent charge Id
                charge_id = charge.data[0].id

                # Calculate remaining time left on subscription
                end_date = datetime.fromtimestamp(float(stripe_subscription.current_period_end))
                days_remaining = (end_date - datetime.now()).days

                # If the time left is greater than 0
                if days_remaining > 0:
                    # Calculate the amount to be refunded
                    daily_sub_amount = stripe_subscription.plan.amount / 30
                    refund_amount = daily_sub_amount * days_remaining

                    # Process the refund
                    stripe.Refund.create(
                        charge=charge_id,
                        amount=round(refund_amount)
                    )

                    # Delete the refunded subscripton
                    stripe_subscription.delete()

        except Exception as e:
            pass

    return True


def create_subscription(request, member_account, plan, plan_name):
    stripe.api_key = "sk_live_iayvQaH5IaeEOhkbkLt7vA2z"

    # Attempt subscription
    try:
        # Create new customer incase customer has changed currency
        new_stripe_customer = stripe.Customer.create(
            email=request.POST.get('tokenEmail'),
            description="Account for " + request.user.username,
            source=request.POST.get('tokenId')
        )

        # Create new subscription
        stripe_subscription = stripe.Subscription.create(
            customer=new_stripe_customer.id,
            items=[
                {
                    "plan": plan,
                },
            ]
        )

        # Get card information from new customer
        sources = new_stripe_customer.sources['data']
        card = sources[0]

        # Update member account with new customer and subscription information
        update_member_account(member_account,
                              membership_name=plan_name,
                              customer_id=new_stripe_customer.id,
                              card_id=card.id,
                              status=stripe_subscription.status,
                              sub_id=stripe_subscription.id,
                              membership_end_date=None,
                              trial_end_date=None
                              )

        # Track purchase for analytics
        CanceledOrPurchased.objects.create(
            purchased=True
        )

        # Remove from trialing email list
        delete_from_trailnopurchase_list(request.user.email)

        return HttpResponse('Thank you for subscribing! Please visit account info to view your details.', status=200)

    # Throw an exception
    except stripe.error.CardError as e:
        body = e.json_body
        err = body.get('error', {})
        return HttpResponse(
            "Status is: " + str(e.http_status) + ", Type is: " + str(err.get('type')) + ", Code is: "
            + str(err.get('code')) + ", Param is: " + str(err.get('param')) + ", Message is: " + str(err.get('message'))
            , status=400)
    except stripe.error.RateLimitError as e:
        return HttpResponse(e, status=400)
    except stripe.error.InvalidRequestError as e:
        return HttpResponse(e, status=400)
    except stripe.error.AuthenticationError as e:
        return HttpResponse(e, status=400)
    except stripe.error.APIConnectionError as e:
        return HttpResponse(e, status=400)
    except stripe.error.StripeError as e:
        return HttpResponse(e, status=400)
    except Exception as e:
        return HttpResponse(e, status=400)


@login_required
def continue_sub(request):
    stripe.api_key = "sk_live_iayvQaH5IaeEOhkbkLt7vA2z"
    if request.method == 'POST' and request.is_secure():
        if MemberAccount.objects.filter(user=request.user.id).exists():
            stripe_account = MemberAccount.objects.get(user=request.user.id)
            if stripe_account:
                subscription = stripe.Subscription.retrieve(stripe_account.sub_id)
                if subscription.plan.id:
                    subscription.plan = subscription.plan.id
                    subscription.cancel_at_period_end = False
                subscription.save()
                stripe_account.at_period_end = False
                stripe_account.period_end = subscription.current_period_end
                stripe_account.status = subscription.status
                stripe_account.trial_end_date = None
                stripe_account.save()
            else:
                messages.error(request, 'Something went wrong, please contact account support.')
                return redirect('users:account')
        else:
            pass
        messages.success(request, 'Subscription continued.')
        return redirect('users:account')
    else:
        messages.error(request, 'Your connection is not secure.')
        return redirect('users:account')


@login_required
def unsubscribe(request):
    stripe.api_key = "sk_live_iayvQaH5IaeEOhkbkLt7vA2z"
    if request.method == 'POST' and request.is_secure():
        if MemberAccount.objects.filter(user=request.user.id).exists():
            stripe_account = MemberAccount.objects.get(user=request.user.id)
            if stripe_account:
                sub = stripe.Subscription.retrieve(stripe_account.sub_id)
                sub.cancel_at_period_end = True
                sub.save()
                stripe_account.status = sub.status
                stripe_account.at_period_end = True
                stripe_account.period_end = sub.current_period_end
                stripe_account.trial_end_date = None
                stripe_account.save()
                messages.success(request, 'Subscription cancelled.')
                return redirect('users:account')
            else:
                messages.error(request, 'Something went wrong, please contact account support.')
                return redirect('users:account')
        else:
            messages.error(request, 'Something went wrong, please contact account support.')
            return redirect('users:account')
    else:
        messages.error(request, 'Your connection is not secure.')
        return redirect('users:account')


@login_required
def update_card(request):
    stripe.api_key = "sk_live_iayvQaH5IaeEOhkbkLt7vA2z"
    if request.method == 'GET':
        if MemberAccount.objects.filter(user=request.user.id).exists():
            stripe_account = MemberAccount.objects.get(user=request.user.id)
            if stripe_account.card_id:
                try:
                    customer = stripe.Customer.retrieve(stripe_account.customer_id)
                    card = customer.sources.retrieve(stripe_account.card_id)
                except:
                    card = None
            else:
                card = None
        else:
            card = None
        return JsonResponse({'card': card}, safe=False)
    elif request.method == 'POST' and request.is_secure():
        try:
            stripe_account = MemberAccount.objects.get(user=request.user.id)
            customer = stripe.Customer.retrieve(stripe_account.customer_id)
            customer.source = request.POST.get('tokenId')
            customer.save()
            sources = customer.sources['data']
            card = sources[0]
            stripe_account.card_id = card.id
            stripe_account.save()
            return HttpResponse('Card update, thanks!')
        except stripe.error.CardError as e:
            body = e.json_body
            err = body.get('error', {})
            error_message = ("Status is: " + e.http_status + ", Type is: " + err.get('type') + ", Code is: "
                             + err.get('code') + ", Param is: " + err.get('param') + ", Message is: " + err.get(
                        'message')
                             )
            return HttpResponse(error_message)
        except stripe.error.RateLimitError as e:
            return HttpResponse(e)
        except stripe.error.InvalidRequestError as e:
            return HttpResponse(e)
        except stripe.error.AuthenticationError as e:
            return HttpResponse(e)
        except stripe.error.APIConnectionError as e:
            return HttpResponse(e)
        except stripe.error.StripeError as e:
            return HttpResponse(e)
        except Exception as e:
            return HttpResponse(e)
    else:
        messages.error(request, 'Your connection is not secure.')
        return redirect('users:account')


@login_required
def special_offer(request):
    stripe.api_key = "sk_live_iayvQaH5IaeEOhkbkLt7vA2z"
    if request.method == 'POST' and request.is_secure():
        member_account = MemberAccount.objects.get(user=request.user)
        if member_account:
            try:
                subscription = stripe.Subscription.retrieve(member_account.sub_id)
                if subscription:
                    charge = stripe.Charge.list(limit=1, customer=member_account.customer_id)
                else:
                    messages.error(request, 'We cannot find your current subscription, please contact support.')
                    return redirect('users:account')
                try:
                    charge_id = charge.data[0].id
                    end_date = datetime.fromtimestamp(float(subscription.current_period_end))
                    days_left = (end_date - datetime.now()).days
                    if days_left > 0:
                        daily_sub_amount = subscription.plan.amount / 30
                        refund_amount = daily_sub_amount * days_left
                        refund = stripe.Refund.create(
                            charge=charge_id,
                            amount=round(refund_amount)
                        )
                    pass
                except IndexError:
                    charge_id = None
                    pass

                subscription.delete()

                if request.POST.get('currency') == 'aud':
                    plan = "jsa-core-aud-3"
                elif request.POST.get('currency') == 'gbp':
                    plan = "jsa-core-gbp-3"
                elif request.POST.get('currency') == 'usd':
                    plan = "jsa-core-usd-3"
                else:
                    messages.error(request, 'There has been an error, please contact support.')
                    return redirect('users:account')

                new_subscription = stripe.Subscription.create(
                    customer=member_account.customer_id,
                    items=[
                        {
                            "plan": plan,
                        },
                    ]
                )
                if new_subscription:
                    member_account.membership_name = 'JSA Core'
                    member_account.customer_id = new_subscription.customer
                    member_account.status = 'active'
                    member_account.sub_id = new_subscription.id
                    member_account.membership_end_date = None
                    member_account.save()
                    messages.success(request,
                                     'You have successfully subscribed to JSA Core. If you had any time remaining on your previous subscription, we have refunded the difference. Thanks!')
                    return redirect('users:account')
                else:
                    messages.error(request, 'Something went wrong, please contact support.')
                    return redirect('users:account')
            except stripe.error.CardError as e:
                body = e.json_body
                err = body.get('error', {})
                messages.error(request, "Status is: " + e.http_status + ", Type is: " + err.get('type') + ", Code is: "
                               + err.get('code') + ", Param is: " + err.get('param') + ", Message is: " + err.get(
                    'message'))
                return redirect('users:account')
            except stripe.error.RateLimitError as e:
                messages.error(request, e)
                return redirect('users:account')
            except stripe.error.InvalidRequestError as e:
                messages.error(request, e)
                return redirect('users:account')
            except stripe.error.AuthenticationError as e:
                messages.error(request, e)
                return redirect('users:account')
            except stripe.error.APIConnectionError as e:
                messages.error(request, e)
                return redirect('users:account')
            except stripe.error.StripeError as e:
                messages.error(request, e)
                return redirect('users:account')
            except Exception as e:
                messages.error(request, e)
                return redirect('users:account')
        else:
            messages.error(request, 'Could not get your stripe account.')
            return redirect('users:account')
    else:
        messages.error(request, 'Your connection is not secure.')
        return redirect('users:account')


@login_required
def email_gift_coupon(request):
    if request.method == 'POST':
        if request.POST.get('recipientEmail'):
            gift_coupon = GiftCoupon.objects.get(pk=int(request.POST.get('couponId')))
            gift_coupon.recipient_email = strip_tags(request.POST.get('recipientEmail'))
            gift_coupon.recipient_message = strip_tags(request.POST.get('recipientMessage'))
            gift_coupon.save()
            try:
                sg = sendgrid.SendGridAPIClient(
                    apikey='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')
                data = {
                    "personalizations": [
                        {
                            "to": [
                                {
                                    "email": gift_coupon.recipient_email
                                }
                            ],
                            "subject": "JSA | Gift Coupon"
                        }
                    ],
                    "from": {
                        "email": "accounts@jamessmithacademy.com"
                    },
                    "content": [
                        {
                            "type": "text/html",
                            "value": render_to_string('transactions/gift_coupon.html',
                                                      {'message': gift_coupon.recipient_message,
                                                       'coupon': gift_coupon.coupon_value})
                        }
                    ]
                }
                sg.client.mail.send.post(request_body=data)
                messages.success(request, 'Your email has been sent! Thanks for your purchase!')
                return redirect('users:gift_coupon')
            except Exception as e:
                messages.error(request,
                               'There was an error. We could not send them the email. Your coupons can be found in your account area.')
                return redirect('users:gift_coupon')


@login_required
def redeem_gift_coupon(request):
    if request.method == 'POST':

        gift_coupon = GiftCoupon.objects.get(coupon_value=request.POST.get('coupon'))

        if gift_coupon.redeemed == False:

            # Get member account
            member_account = get_member_account(request)

            # Determine membership end date
            if member_account.membership_end_date is not None:

                if member_account.status == 'active' and member_account.membership_end_date > timezone.now():
                    # Convert months into membership end date and add to current membership end date
                    end_date = member_account.membership_end_date + relativedelta(months=+int(3))

                else:
                    # Convert months into membership end date
                    end_date = date.today() + relativedelta(months=+int(3))
            else:
                # Convert months into membership end date
                end_date = date.today() + relativedelta(months=+int(3))

            # Update member account
            update_member_account(member_account,
                                  membership_name='Premium Plan - 3 Month Block',
                                  customer_id='None',
                                  card_id='None',
                                  status='active',
                                  sub_id='None',
                                  membership_end_date=end_date,
                                  trial_end_date=None
                                  )

            gift_coupon.redeemed = True
            gift_coupon.date_redeemed = timezone.now()
            gift_coupon.save()

            messages.success(request,
                             'You have redeemed the coupon. Your account has been credited and is now active. Enjoy!')
            return redirect('users:account')

        else:

            messages.error(request, 'This coupon has already been redeemed.')
            return redirect('users:account')

    else:

        messages.error(request, 'There was an error, please try again.')
        return redirect('users:account')


def plan_ending():
    members = MemberAccount.objects.filter(status='active', membership_end_date__isnull=False)
    for member in members:
        if member.membership_end_date is not None:
            today = timezone.now()
            membership_end = member.membership_end_date
            time_remaining = membership_end - today
            if time_remaining.days == 3:
                if member.user.email:
                    try:
                        sg = sendgrid.SendGridAPIClient(
                            apikey='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')
                        data = {
                            "personalizations": [
                                {
                                    "to": [
                                        {
                                            "email": member.user.email
                                        }
                                    ],
                                    "subject": "Plan Ending Soon"
                                }
                            ],
                            "from": {
                                "email": "accounts@jamessmithacademy.com"
                            },
                            "content": [
                                {
                                    "type": "text/html",
                                    "value": render_to_string('transactions/plan_ending.html', {'user': member.user})
                                }
                            ]
                        }
                        sg.client.mail.send.post(request_body=data)
                    except Exception as e:
                        pass
            if today > member.membership_end_date:
                member_account = MemberAccount.objects.get(pk=member.id)
                member_account.status = 'canceled'
                member_account.save()
                CanceledOrPurchased.objects.create(
                    canceled=True
                )
                if member.user.email:
                    try:
                        sg = sendgrid.SendGridAPIClient(
                            apikey='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')
                        data = {
                            "personalizations": [
                                {
                                    "to": [
                                        {
                                            "email": member.user.email
                                        }
                                    ],
                                    "subject": "Plan Ended"
                                }
                            ],
                            "from": {
                                "email": "accounts@jamessmithacademy.com"
                            },
                            "content": [
                                {
                                    "type": "text/html",
                                    "value": render_to_string('transactions/plan_ended.html', {'user': member.user})
                                }
                            ]
                        }
                        sg.client.mail.send.post(request_body=data)
                    except Exception as e:
                        pass
    print('Sent Plan Ended Emails')


@login_required
def restart_free_trial(request):
    if request.method == 'POST':
        MemberAccount.objects.filter(user=request.user).delete()
        modules_enrolled = ModuleEnrolled.objects.filter(user=request.user)
        for module in modules_enrolled:
            cache.delete('module_enrolled' + str(module.id) + str(request.user.id))
            module.delete()
        Goal.objects.filter(user=request.user).delete()
        Macros.objects.filter(user=request.user).delete()
        Day.objects.filter(user=request.user).delete()
        Combination.objects.filter(profile_u=request.user.profile).delete()
        HomeStepOne.objects.filter(user=request.user).delete()
        HomeStepTwo.objects.filter(user=request.user).delete()
        HomeStepThree.objects.filter(user=request.user).delete()
        HomeStepFour.objects.filter(user=request.user).delete()
        HomeStepFive.objects.filter(user=request.user).delete()
        HomeStepSix.objects.filter(user=request.user).delete()
        cache.delete('home_step_one' + str(request.user.id))
        cache.delete('home_step_two' + str(request.user.id))
        cache.delete('home_step_three' + str(request.user.id))
        cache.delete('home_step_four' + str(request.user.id))
        cache.delete('home_step_five' + str(request.user.id))
        cache.delete('home_step_six' + str(request.user.id))
        return redirect('users:home')


class RestartTrialViewSet(ViewSet):

    def create(self, request):
        MemberAccount.objects.filter(user=request.user).delete()
        modules_enrolled = ModuleEnrolled.objects.filter(user=request.user)
        for module in modules_enrolled:
            cache.delete('module_enrolled' + str(module.id) + str(request.user.id))
            module.delete()
        Goal.objects.filter(user=request.user).delete()
        Macros.objects.filter(user=request.user).delete()
        Day.objects.filter(user=request.user).delete()
        Combination.objects.filter(profile_u=request.user.profile).delete()
        HomeStepOne.objects.filter(user=request.user).delete()
        HomeStepTwo.objects.filter(user=request.user).delete()
        HomeStepThree.objects.filter(user=request.user).delete()
        HomeStepFour.objects.filter(user=request.user).delete()
        HomeStepFive.objects.filter(user=request.user).delete()
        HomeStepSix.objects.filter(user=request.user).delete()
        cache.delete('home_step_one' + str(request.user.id))
        cache.delete('home_step_two' + str(request.user.id))
        cache.delete('home_step_three' + str(request.user.id))
        cache.delete('home_step_four' + str(request.user.id))
        cache.delete('home_step_five' + str(request.user.id))
        cache.delete('home_step_six' + str(request.user.id))
        return HttpResponse(status=200)


class MemberAccountViewSet(ViewSet):
    def get_or_start_trial(self, request):
        try:
            member_account = MemberAccount.objects.get(user=request.user)
            if 'started_trial' in request.session:
                del request.session['started_trial']
        except MemberAccount.DoesNotExist:
            end_date = date.today() + relativedelta(days=5)
            member_account = MemberAccount.objects.create(
                user=request.user,
                membership_name="Trialing Plan",
                sub_id='None',
                status="trialing",
                trial_end_date=end_date
            )
            try:
                welcome_chat_message(request)
                started_trial_email(request)
                CanceledOrPurchased.objects.create(
                    trialed=True
                )
                if 'started_trial' in request.session:
                    del request.session['started_trial']
                request.session['started_trial'] = 'started_trial'
            except Exception as e:
                print(e)
                pass
        return member_account

    def check_membership_expired(self, member_account):
        try:
            if member_account.trial_end_date is None and member_account.membership_end_date is None:
                pass
            elif member_account.membership_end_date is not None:
                if timezone.now() > member_account.membership_end_date:
                    member_account.status = 'canceled'
                    member_account.membership_end_date = None
                    member_account.save()
            elif member_account.trial_end_date is not None:
                if timezone.now() > member_account.trial_end_date:
                    member_account.status = 'canceled'
                    member_account.trial_end_date = None
                    member_account.save()
        except Exception as e:
            print(e)
        return member_account

    def list(self, request):
        member_account = MemberAccount.objects.get(user=request.user)
        member_account = self.check_membership_expired(member_account)
        serializer = MemberAccountSerializer(member_account, many=False)
        return Response(serializer.data)
