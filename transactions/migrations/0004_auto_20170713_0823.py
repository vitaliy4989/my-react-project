# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-13 08:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transactions', '0003_auto_20170707_0915'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stripeaccount',
            name='customer_id',
            field=models.CharField(blank=True, db_index=True, max_length=250),
        ),
    ]
