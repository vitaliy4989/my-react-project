# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-10-27 23:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transactions', '0011_auto_20170822_0031'),
    ]

    operations = [
        migrations.AddField(
            model_name='stripeaccount',
            name='sub_type',
            field=models.CharField(blank=True, db_index=True, max_length=250),
        ),
    ]
