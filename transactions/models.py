from django.db import models
from django.contrib.auth.models import User


class MemberAccount(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    membership_name = models.CharField(max_length=250, null=True)
    customer_id = models.CharField(max_length=250, blank=True, db_index=True)
    sub_id = models.CharField(max_length=250, blank=True, db_index=True)
    card_id = models.CharField(max_length=250, blank=True, db_index=True)
    status = models.CharField(max_length=250, blank=True, db_index=True)
    trial_end = models.CharField(max_length=250, null=True, blank=True, db_index=True)
    at_period_end = models.BooleanField(default=False)
    period_end = models.IntegerField(null=True, blank=True)
    membership_end_date = models.DateTimeField(null=True, blank=True)
    trial_end_date = models.DateTimeField(null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username


class GiftCoupon(models.Model):
    purchased_by = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    recipient_email = models.CharField(max_length=250, blank=True, null=True)
    recipient_message = models.TextField(null=True)
    coupon_value = models.CharField(max_length=250, unique=True, db_index=True)
    redeemed = models.BooleanField(default=False)
    date_redeemed = models.DateTimeField(null=True, blank=True)
    date_purchased = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.purchased_by.username
