from django.urls import path
from . import views
from rest_framework.routers import SimpleRouter

router = SimpleRouter()

router.register(r'member-account', views.MemberAccountViewSet, 'MemberAccountViewSet')
router.register(r'restart-trial', views.RestartTrialViewSet, 'RestartTrialViewSet')

urlpatterns = [
    path('status/', views.stripe_status, name='stripe_status'),
    path('restart-free-trial/', views.restart_free_trial, name='restart_free_trial'),
    path('unsubscribe/', views.unsubscribe, name='unsubscribe'),
    path('continue/', views.continue_sub, name='continue'),
    path('update-card/', views.update_card, name='update_card'),
    path('create-paypal-three-month/', views.create_paypal_three_month, name='create_paypal_three_month'),
    path('special-offer/', views.special_offer, name='special_offer'),
    path('stripe-block-purchase/', views.stripe_block_purchase, name='stripe_block_purchase'),
    path('subscribe-monthly/', views.subscribe_monthly, name='subscribe_monthly'),
    path('subscribe-monthly-standard/', views.subscribe_monthly_standard, name='standard_subscribe_monthly'),
    path('subscribe-jsa-core/', views.subscribe_jsa_core, name='subscribe_jsa_core'),
    path('email-gift-coupon/', views.email_gift_coupon, name='email_gift_coupon'),
    path('redeem-gift-coupon/', views.redeem_gift_coupon, name='redeem_gift_coupon'),
]

urlpatterns += router.urls
