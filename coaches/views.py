from django.shortcuts import render
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.decorators import login_required
from transactions.models import MemberAccount


@login_required
@user_passes_test(lambda u: u.is_staff)
def index(request):
    member_account = MemberAccount.objects.get(user=request.user)
    request.session['member_account_status'] = member_account.status
    return render(request, 'coaches/index.html')
