from django.http import HttpResponse, JsonResponse
from django.utils.html import strip_tags
from django.contrib.auth.decorators import login_required
from .models import Recipe, Favourite, Combination, CombinationRecipe
from itertools import product
from django.core.cache import cache
from .serializers import RecipeSerializer, FavouriteSerializer, CombinationRecipeSerializer, CombinationSerializer
from django.template.loader import render_to_string
from xhtml2pdf import pisa
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
import random


class RecipeViewSet(ViewSet):

    def create(self, request):
        serializer = RecipeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(
                user=request.user.profile,
                username=request.user.username,
                calories=(float(request.POST.get('protein')) * 4) + (float(request.POST.get('carbs')) * 4) + (
                        float(request.POST.get('fat')) * 9)
            )
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        recipe = cache.get_or_set('recipe' + str(pk), Recipe.objects.get(pk=pk), 2592000)
        recipe_serializer = RecipeSerializer(recipe, many=False)
        return JsonResponse({'recipe': recipe_serializer.data,
                             'favourite': Favourite.objects.filter(recipe=pk, user=request.user).exists()}, safe=False)

    def partial_update(self, request, pk=None):
        try:
            item = Recipe.objects.get(pk=pk)
        except Recipe.DoesNotExist:
            return Response(status=404)
        serializer = RecipeSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Recipe.objects.get(pk=pk)
        except Recipe.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class FavouriteViewSet(ViewSet):

    def get_recipe(self, pk):
        try:
            return Recipe.objects.get(pk=pk)
        except:
            raise Http404

    def list(self, request):
        queryset = cache.get_or_set('favourite_recipes' + str(request.user.id),
                                    Favourite.objects.filter(user=request.user), 2592000)
        serializer = FavouriteSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = FavouriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user, recipe=self.get_recipe(request.POST.get('recipe')))
            cache.delete('favourite_recipes' + str(request.user.id))
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            items = Favourite.objects.filter(recipe=pk, user=request.user)
        except Favourite.DoesNotExist:
            return Response(status=404)
        items.delete()
        cache.delete('favourite_recipes' + str(request.user.id))
        return Response(status=204)


class SearchRecipesViewSet(ViewSet):

    def list(self, request):

        recipes = cache.get_or_set('all_recipes', Recipe.objects.all(), 86400)

        if request.GET.get('recipe_name') is not None:
            recipes = recipes.filter(name__icontains=request.GET.get('recipe_name'))

        if request.GET.get('recipe_type') is not None:
            recipes = recipes.filter(type_meal=request.GET.get('recipe_type'))

        if request.GET.get('recipe_protein') is not None:
            recipes = recipes.filter(protein__gte=int(request.GET.get('recipe_protein')))

        if request.GET.get('recipe_calories') is not None:
            recipes = recipes.filter(calories__gte=int(request.GET.get('recipe_calories')))

        recipes = recipes.order_by('-date').values('id', 'name', 'photo')
        return Response(recipes, content_type='application/json')


class TopRecipesViewSet(ViewSet):

    def list(self, request):
        recipes = cache.get_or_set('top_recipes',
                                   Recipe.objects.filter(approved=True).order_by('-date').values('id', 'name', 'photo')[
                                   :100], 86400)
        return Response(recipes, content_type='application/json')


class NewRecipesViewSet(ViewSet):

    def list(self, request):
        recipes = cache.get_or_set('new_recipes', Recipe.objects.order_by('-date').values('id', 'name', 'photo')[:100],
                                   86400)
        return Response(recipes, content_type='application/json')


class RecipesByMealViewSet(ViewSet):

    def list(self, request):
        recipes = cache.get_or_set('get_by_meal' + str(request.GET.get('meal')),
                                   Recipe.objects.filter(type_meal=request.GET.get('meal')).values('id', 'name',
                                                                                                   'photo')[:100],
                                   86400)
        return Response(recipes, content_type='application/json')


class RecipesByProteinViewSet(ViewSet):

    def list(self, request):
        recipes = Recipe.objects.filter(protein__gte=strip_tags(int(request.GET.get('protein')))).order_by(
            '-average').values('id', 'name', 'photo')[:100]
        return Response(recipes, content_type='application/json')


class RecipesByCaloriesViewSet(ViewSet):

    def list(self, request):
        recipes = Recipe.objects.filter(calories__gte=strip_tags(int(request.GET.get('calories')))).order_by(
            '-average').values('id', 'name', 'photo')[:100]
        return Response(recipes, content_type='application/json')


class RecipesByNameViewSet(ViewSet):

    def list(self, request):
        recipes = Recipe.objects.filter(name__icontains=request.GET.get('name')).order_by('-average').values('id',
                                                                                                             'name',
                                                                                                             'photo')[
                  :100]
        return Response(recipes, content_type='application/json')


class MyRecipesViewSet(ViewSet):

    def list(self, request):
        recipe_serializer = RecipeSerializer(Recipe.objects.filter(username=request.user.username), many=True)
        return JsonResponse(recipe_serializer.data, safe=False)


@login_required
def recipe_pdf(request, pk):
    recipe = cache.get_or_set('recipe' + str(pk), Recipe.objects.get(pk=pk), 2592000)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=' + '"' + recipe.name + '.pdf' + '"'
    pisaStatus = pisa.CreatePDF(render_to_string('recipes/recipe.html', {'recipe': recipe}), dest=response,
                                link_callback=None)
    if pisaStatus.err:
        return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response


class CombinationPdfViewSet(ViewSet):

    def retrieve(self, request, pk=None):
        combination = Combination.objects.get(pk=pk)
        combination_recipes = CombinationRecipe.objects.filter(combination=pk)

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=' + '"' + combination.name + '.pdf' + '"'
        pisaStatus = pisa.CreatePDF(render_to_string('recipes/combination.html',
                                                     {'combination_recipes': combination_recipes,
                                                      'combination': combination}), dest=response, link_callback=None)
        if pisaStatus.err:
            return HttpResponse('We had some errors <pre>' + html + '</pre>')
        return response


class RateRecipeViewSet(ViewSet):

    def create(self, request):
        recipe_id = request.POST.get('recipe_id')
        recipe_rating = request.POST.get('rating')
        recipe = Recipe.objects.get(pk=recipe_id)
        current_rating = recipe.rating
        number_of_votes = recipe.votes
        new_number_of_votes = number_of_votes + 1
        new_rating = float(current_rating) + float(recipe_rating)
        recipe.rating = new_rating
        recipe.votes = new_number_of_votes
        recipe.average = float(new_rating) / float(new_number_of_votes)
        recipe.save()
        cache.delete('recipe' + str(request.POST.get('recipe_id')))
        return Response({'average': recipe.average})


class CombinationSearchViewSet(ViewSet):

    def list(self, request):
        # Query the different meals
        breakfast = Recipe.objects.raw(
            "SELECT id, name, photo, type_meal, protein, calories FROM recipes_recipe WHERE approved=True AND type_meal='breakfast' ORDER BY random() LIMIT 75")
        lunch = Recipe.objects.raw(
            "SELECT id, name, photo, type_meal, protein, calories FROM recipes_recipe WHERE approved=True AND type_meal='lunch' ORDER BY random() LIMIT 75")
        dinner = Recipe.objects.raw(
            "SELECT id, name, photo, type_meal, protein, calories FROM recipes_recipe WHERE approved=True AND type_meal='dinner' ORDER BY random() LIMIT 75")
        snack = Recipe.objects.raw(
            "SELECT id, name, photo, type_meal, protein, calories FROM recipes_recipe WHERE approved=True AND type_meal='snack' ORDER BY random() LIMIT 75")

        # Make combinations based on the number of required meals
        if int(request.GET.get('meals')) == 3:
            meals_list = [breakfast, lunch, dinner]
            meal_combinations = list(product(*meals_list))
        elif int(request.GET.get('meals')) == 4:
            meals_list = [breakfast, snack, lunch, dinner]
            meal_combinations = list(product(*meals_list))

        # Loop through combinations to find meal combinations which fit their macros
        combination_list = []

        meal_combinations = random.sample(meal_combinations, len(meal_combinations))

        for combination in meal_combinations:
            calorie_allowance = float(request.GET.get('calories'))
            protein_allowance = float(request.GET.get('protein'))
            combination_protein = 0
            combination_calories = 0
            for recipe in combination:
                combination_calories = combination_calories + recipe.calories
                combination_protein = combination_protein + recipe.protein
            calorie_difference = float(
                calorie_allowance) - float(combination_calories)
            protein_difference = float(
                protein_allowance) - float(combination_protein)
            if -300 <= calorie_difference <= 300 and -20 <= protein_difference <= 20:
                recipe_combination_serializer = RecipeSerializer(
                    combination, many=True)
                combination_list.append({
                    'protein': round(combination_protein, 2),
                    'calories': round(combination_calories, 2),
                    'recipes': recipe_combination_serializer.data
                })
            if len(combination_list) > 15:
                return JsonResponse(combination_list, safe=False)
        return JsonResponse(combination_list, safe=False)


class SaveGeneratedCombinationViewSet(ViewSet):

    def create(self, request):
        recipe_ids = request.POST.get('recipe_ids').split(",")
        combination = Combination.objects.create(
            name=request.POST.get('name'),
            profile_u=request.user.profile,
            protein=request.POST.get('protein'),
            calories=request.POST.get('calories')
        )
        for recipe in recipe_ids:
            recipe = Recipe.objects.get(pk=int(recipe))
            CombinationRecipe.objects.create(
                combination=combination,
                recipe=recipe,
            )
        cache.delete('combinations' + str(request.user.id))
        return HttpResponse(status=200)


class CombinationViewSet(ViewSet):

    def list(self, request):
        combinations = cache.get_or_set('combinations' + str(request.user.id),
                                        Combination.objects.filter(profile_u=request.user.profile).order_by('-id'),
                                        2592000)
        combinations_serialized = CombinationSerializer(combinations, many=True)
        return JsonResponse(combinations_serialized.data, safe=False)

    def create(self, request):
        combination = Combination.objects.create(
            profile_u=request.user.profile,
            name=request.POST.get('name'),
            protein=request.POST.get('protein'),
            calories=request.POST.get('calories')
        )
        cache.delete('combinations' + str(request.user.id))
        return JsonResponse({'id': combination.id}, safe=False)

    def retrieve(self, request, pk=None):
        combination = Combination.objects.get(pk=int(pk))
        combination_recipes = CombinationRecipe.objects.filter(combination=combination).order_by('id')
        combination_recipes_serialized = CombinationRecipeSerializer(combination_recipes, many=True)
        combination_serialized = CombinationSerializer(combination, many=False)
        return JsonResponse({'day': combination_serialized.data, 'meals': combination_recipes_serialized.data},
                            safe=False)

    def partial_update(self, request, pk=None):
        try:
            item = Combination.objects.get(pk=pk)
        except Combination.DoesNotExist:
            return Response(status=404)
        serializer = CombinationSerializer(item, data=request.data)
        cache.delete('combinations' + str(request.user.id))
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Combination.objects.get(pk=pk)
        except Combination.DoesNotExist:
            return Response(status=404)
        item.delete()
        cache.delete('combinations' + str(request.user.id))
        return Response(status=204)


class CombinationRecipeViewSet(ViewSet):

    def list(self, request):
        queryset = CombinationRecipe.objects.all()
        serializer = CombinationRecipeSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        combination = Combination.objects.get(pk=request.POST.get('combination_id'))
        recipe = Recipe.objects.get(pk=request.POST.get('recipe_id'))
        combination_recipe = CombinationRecipe.objects.create(
            combination=combination,
            recipe=recipe,
        )
        combination.calories = combination.calories + (recipe.calories * combination_recipe.servings)
        combination.protein = combination.protein + (recipe.protein * combination_recipe.servings)
        combination.save()
        return JsonResponse({'id': combination_recipe.id}, safe=False)

    def retrieve(self, request, pk=None):
        queryset = CombinationRecipe.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = CombinationRecipeSerializer(item)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        # Get the combinatiorecipe (meal) and the combination (meal plan)
        combination = Combination.objects.get(pk=request.POST.get('combination_id'))
        combination_recipe = CombinationRecipe.objects.get(pk=pk)

        # Remove the existing meals macros from the day combination
        combination_recipe_calories = combination_recipe.recipe.calories * combination_recipe.servings
        combination_recipe_protein = combination_recipe.recipe.protein * combination_recipe.servings
        combination.calories = combination.calories - combination_recipe_calories
        combination.protein = combination.protein - combination_recipe_protein
        combination.save()

        # Calculate the new meals macros and add to the day combination.
        updated_combination_recipe_calories = combination_recipe.recipe.calories * float(request.POST.get('servings'))
        updated_combination_recipe_protein = combination_recipe.recipe.protein * float(request.POST.get('servings'))
        combination.calories = combination.calories + updated_combination_recipe_calories
        combination.protein = combination.protein + updated_combination_recipe_protein
        combination_recipe.servings = float(request.POST.get('servings'))
        combination.save()
        combination_recipe.save()
        return HttpResponse(status=200)

    def destroy(self, request, pk=None):
        combination = Combination.objects.get(pk=request.POST.get('combination_id'))
        recipe = Recipe.objects.get(pk=request.POST.get('recipe_id'))
        combination_recipe = CombinationRecipe.objects.get(pk=pk)
        combination.calories = combination.calories - \
                               (recipe.calories * combination_recipe.servings)
        combination.protein = combination.protein - \
                              (recipe.protein * combination_recipe.servings)
        combination_recipe.delete()
        combination.save()
        return HttpResponse(status=200)


class RecipesByMealApprovedViewSet(ViewSet):

    def list(self, request):
        recipes = cache.get_or_set('search_meal_for_plan' + str(request.GET.get('type')),
                                   Recipe.objects.filter(type_meal=request.GET.get('type'), approved=True).order_by(
                                       '-date').values('id', 'servings', 'protein', 'calories', 'name', 'photo'), 86400)
        return Response(recipes, content_type='application/json')
