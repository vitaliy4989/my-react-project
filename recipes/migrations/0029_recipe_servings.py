# Generated by Django 2.0.7 on 2018-09-03 02:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0028_auto_20180902_2346'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='servings',
            field=models.FloatField(default=1),
        ),
    ]
