# Generated by Django 2.0.3 on 2018-04-17 01:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0018_favourite'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='ingredients',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='recipe',
            name='method',
            field=models.TextField(null=True),
        ),
    ]
