# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-14 22:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_coachprofile'),
        ('recipes', '0011_remove_recipe_food_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='RecipeCombinations',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('agreed', models.BooleanField(default=False)),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='users.Profile')),
            ],
        ),
    ]
