from django.contrib import admin
from .models import Recipe, Favourite, Combination, CombinationRecipe
from import_export.admin import ImportExportModelAdmin


@admin.register(Favourite)
class FavouriteAdmin(ImportExportModelAdmin):
    pass


class RecipeAdmin(admin.ModelAdmin):
	readonly_fields = (
        'user',
    )
    
	search_fields = ['user__username']

admin.site.register(Recipe, RecipeAdmin)
admin.site.register(Combination)
admin.site.register(CombinationRecipe)