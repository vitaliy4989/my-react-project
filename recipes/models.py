from django.db import models
from django.contrib.auth.models import User
from pyuploadcare.dj.models import FileField, ImageField


class Recipe(models.Model):
	name = models.CharField(max_length=250, null=True)
	user = models.ForeignKey('users.Profile', null=True, on_delete=models.CASCADE)
	username = models.CharField(max_length=250, null=True)
	type_meal = models.CharField(max_length=250, null=True)
	description = models.TextField(null=True)
	servings = models.FloatField(default=1)
	ingredients = models.TextField(null=True)
	method = models.TextField(null=True)
	photo = ImageField(null=True)
	protein = models.FloatField(null=True)
	carbs = models.FloatField(null=True)
	fat = models.FloatField(null=True)
	rating = models.FloatField(null=True, default=0)
	votes = models.IntegerField(null=True, default=0)
	average = models.FloatField(null=True, default=0)
	calories = models.FloatField(null=True)
	approved = models.BooleanField(default=False)
	date = models.DateTimeField(auto_now_add=True, db_index=True)

	def __str__(self):
		return self.name


class Favourite(models.Model):
	user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
	recipe = models.ForeignKey(Recipe, null=True, on_delete=models.CASCADE)
	notes = models.TextField(null=True)

	def __str__(self):
		return self.recipe.name


class Combination(models.Model):
	name = models.CharField(max_length=250, blank=True)
	calories = models.FloatField(null=True)
	protein = models.FloatField(null=True)
	profile_u = models.ForeignKey('users.Profile', null=True, on_delete=models.CASCADE)


class CombinationRecipe(models.Model):
	combination = models.ForeignKey(Combination, null=True, on_delete=models.CASCADE)
	recipe = models.ForeignKey(Recipe, null=True, on_delete=models.CASCADE)
	servings = models.FloatField(default=1)

	def __str__(self):
		return self.recipe.name