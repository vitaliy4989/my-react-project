from django.urls import path
from . import views
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register(r'recipe', views.RecipeViewSet, 'RecipeViewSet')
router.register(r'favourite', views.FavouriteViewSet, 'FavouriteViewSet')
router.register(r'search', views.SearchRecipesViewSet, 'SearchRecipesViewSet')
router.register(r'combination', views.CombinationViewSet, 'CombinationViewSet')
router.register(r'combination-recipe', views.CombinationRecipeViewSet, 'CombinationRecipeViewSet')
router.register(r'top', views.TopRecipesViewSet, 'TopRecipesViewSet')
router.register(r'new', views.NewRecipesViewSet, 'NewRecipesViewSet')
router.register(r'by-name', views.RecipesByNameViewSet, 'RecipesByNameViewSet')
router.register(r'by-calories', views.RecipesByCaloriesViewSet, 'RecipesByCaloriesViewSet')
router.register(r'by-protein', views.RecipesByProteinViewSet, 'RecipesByProteinViewSet')
router.register(r'by-meal', views.RecipesByMealViewSet, 'RecipesByMealViewSet')
router.register(r'my-recipes', views.MyRecipesViewSet, 'MyRecipesViewSet')
router.register(r'combinations-pdf', views.CombinationPdfViewSet, 'CombinationPdfViewSet')
router.register(r'rate', views.RateRecipeViewSet, 'RateRecipeViewSet')
router.register(r'combination-search', views.CombinationSearchViewSet, 'CombinationSearchViewSet')
router.register(r'save-generated-combination', views.SaveGeneratedCombinationViewSet, 'SaveGeneratedCombinationViewSet')
router.register(r'by-meal-approved', views.RecipesByMealApprovedViewSet, 'RecipesByMealApprovedViewSet')

urlpatterns = [
    path('recipe-pdf/<int:pk>/', views.recipe_pdf, name='recipe_pdf'),
]

urlpatterns += router.urls
