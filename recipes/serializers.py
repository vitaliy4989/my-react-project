from rest_framework import serializers
from .models import Recipe, Favourite, Combination, CombinationRecipe


class RecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recipe
        fields = '__all__'


class FavouriteSerializer(serializers.ModelSerializer):
	recipe = RecipeSerializer(read_only=True)
	class Meta:
		model = Favourite
		fields = '__all__'


class CombinationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Combination
		fields = '__all__'


class CombinationRecipeSerializer(serializers.ModelSerializer):
	recipe = RecipeSerializer(read_only=True)
	class Meta:
		model = CombinationRecipe
		fields = '__all__'