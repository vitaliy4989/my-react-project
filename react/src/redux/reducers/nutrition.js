const defaultState = {
  protein: undefined,
  calories: undefined,
  days: undefined,
  day: undefined,
  meals: undefined,
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case "UPDATEPROTEINCALORIES":
      return Object.assign({}, state, {
        protein: action.protein,
        calories: action.calories,
      });
    case "UPDATEDAYS":
      return Object.assign({}, state, {
        days: action.days,
      });
    case "UPDATEDAY":
      return Object.assign({}, state, {
        day: action.day,
      });
    case "UPDATEMEALS":
      return Object.assign({}, state, {
        meals: action.meals,
      });
    default:
      return state;
  }
}
