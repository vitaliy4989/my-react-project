import { combineReducers } from "redux";
import videos from "./videos";
import recipes from "./recipes";
import nutrition from "./nutrition";
import training from "./training";
import chat from "./chat";
import progress from "./progress";
import user from "./user";
import meals from "./meals";
import home from "./home";

const rootReducer = combineReducers({
  videos,
  recipes,
  nutrition,
  training,
  chat,
  progress,
  user,
  meals,
  home,
});

export default rootReducer;
