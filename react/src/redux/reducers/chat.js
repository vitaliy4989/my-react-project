const defaultState = {
  comments: undefined,
  chatId: undefined,
  coachUpdate: "",
  otherUpdate: "",
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case "UPDATECHAT":
      return Object.assign({}, state, {
        comments: action.comments,
        chatId: action.chatId,
        coachUpdate: action.coachUpdate,
        otherUpdate: action.otherUpdate,
      });
    case "UPDATECHATREAD":
      return Object.assign({}, state, {
        coachUpdate: action.coachUpdate,
        otherUpdate: action.otherUpdate,
      });
    default:
      return state;
  }
}
