const defaultState = {
  goal: undefined,
  checkIns: undefined,
  goalCategory: undefined,
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case "UPDATEGOAL":
      return Object.assign({}, state, {
        goal: action.goal,
        checkIns: action.checkIns,
        goalCategory: action.goalCategory,
      });
    default:
      return state;
  }
}
