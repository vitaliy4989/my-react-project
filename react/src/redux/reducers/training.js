const defaultState = {
  programs: undefined,
  program: undefined,
  exercises: undefined,
  requestedPrograms: undefined,
  requestedProgram: undefined,
  trainingDays: undefined,
  trainingDayExercises: undefined,
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case "UPDATEPROGRAMS":
      return Object.assign({}, state, {
        programs: action.programs,
      });
    case "UPDATEPROGRAM":
      return Object.assign({}, state, {
        program: action.program,
      });
    case "UPDATEEXERCISES":
      return Object.assign({}, state, {
        exercises: action.exercises,
      });
    case "UPDATEREQUESTEDPROGRAMS":
      return Object.assign({}, state, {
        requestedPrograms: action.requestedPrograms,
      });
    case "UPDATEREQUESTEDPROGRAM":
      return Object.assign({}, state, {
        requestedProgram: action.requestedProgram,
      });
    case "UPDATETRAININGDAYS":
      return Object.assign({}, state, {
        trainingDays: action.trainingDays,
      });
    case "UPDATETRAININGDAYEXERCISES":
      return Object.assign({}, state, {
        trainingDayExercises: action.trainingDayExercises,
      });
    default:
      return state;
  }
}
