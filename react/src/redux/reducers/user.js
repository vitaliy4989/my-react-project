const defaultState = {
  image: sessionStorage.profileImage,
  membershipStatus: sessionStorage.membershipStatus,
  membershipType: sessionStorage.membershipType,
  username: sessionStorage.username,
  firstName: sessionStorage.firstName,
  email: sessionStorage.email,
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case "UPDATEIMAGE":
      return Object.assign({}, state, {
        image: action.image,
      });
    case "UPDATEUSER":
      return Object.assign({}, state, {
        membershipStatus: action.membershipStatus,
        membershipType: action.membershipType,
        username: action.username,
        firstName: action.firstName,
        email: action.email,
      });
    default:
      return state;
  }
}
