const defaultState = {
  recipes: [],
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case "UPDATENEWFIVERECIPES":
      return Object.assign({}, state, {
        recipes: action.recipes,
      });
    default:
      return state;
  }
}
