const defaultState = {
  day: undefined,
  dayId: undefined,
  dayCalories: undefined,
  dayProtein: undefined,
  meals: undefined,
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case "UPDATEDAYMEALS":
      return Object.assign({}, state, {
        day: action.day,
        dayId: action.dayId,
        meals: action.meals,
        dayProtein: action.dayProtein,
        dayCalories: action.dayCalories,
      });
    default:
      return state;
  }
}
