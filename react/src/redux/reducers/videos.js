const defaultState = {
  favouriteVideos: null,
  currentVideoMainFilterId: undefined,
  updateMainFilter: false,
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case "UPDATERECENTVIDEOS":
      return Object.assign({}, state, {
        recentVideos: action.recentVideos,
      });
    case "UPDATEFAVOURITEVIDEOS":
      return Object.assign({}, state, {
        favouriteVideos: action.favouriteVideos,
      });
    case "UPDATEMAINFILTER":
      return Object.assign({}, state, {
        updateMainFilter: action.updateMainFilter,
      });
    case "UPDATECURRENTNMAINFILTERID":
      return Object.assign({}, state, {
        currentVideoMainFilterId: action.currentVideoMainFilterId,
      });
    default:
      return state;
  }
}
