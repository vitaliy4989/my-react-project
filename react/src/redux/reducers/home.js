const defaultState = {
  stepOne: {
    nutrition_total: 0,
    training_total: 0,
    nutrition_completed: 0,
    training_completed: 0,
    training: 0,
    nutrition: 0,
    completed: false,
  },
  stepTwo: undefined,
  stepThree: undefined,
  stepFour: undefined,
  stepFive: undefined,
  stepSix: undefined,
  stepOneCompleted: undefined,
  stepTwoCompleted: undefined,
  stepThreeCompleted: undefined,
  stepFourCompleted: undefined,
  stepFiveCompleted: undefined,
  stepSixCompleted: undefined,
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case "UPDATESTEPONE":
      return {
        ...state,
        stepOne: {
          ...state.stepOne,
          ...action.stepOne,
        },
        stepOneCompleted: action.stepOneCompleted,
      };
    case "UPDATESTEPTWO":
      return Object.assign({}, state, {
        stepTwo: action.stepTwo,
        stepTwoCompleted: action.stepTwoCompleted,
      });
    case "UPDATESTEPTHREE":
      return Object.assign({}, state, {
        stepThree: action.stepThree,
        stepThreeCompleted: action.stepThreeCompleted,
      });
    case "UPDATESTEPFOUR":
      return Object.assign({}, state, {
        stepFour: action.stepFour,
        stepFourCompleted: action.stepFourCompleted,
      });
    case "UPDATESTEPFIVE":
      return Object.assign({}, state, {
        stepFive: action.stepFive,
        stepFiveCompleted: action.stepFiveCompleted,
      });
    case "UPDATESTEPSIX":
      return Object.assign({}, state, {
        stepSix: action.stepSix,
        stepSixCompleted: action.stepSixCompleted,
      });
    default:
      return state;
  }
}
