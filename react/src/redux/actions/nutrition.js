import axiosAjax from "../../config/axiosAjax";

export const updateCaloriesProtein = (protein, calories) => {
  return {
    type: "UPDATEPROTEINCALORIES",
    protein: protein,
    calories: calories,
  };
};

export const updateDays = days => {
  return {
    type: "UPDATEDAYS",
    days: days,
  };
};

export const updateDay = day => {
  return {
    type: "UPDATEDAY",
    day: day,
  };
};

export const updateMeals = meals => {
  return {
    type: "UPDATEMEALS",
    meals: meals,
  };
};

export function getCaloriesProtein() {
  return function(dispatch) {
    return axiosAjax
      .get("/nutrition-tracker/macros/")
      .then(response => {
        dispatch(
          updateCaloriesProtein(response.data.protein, response.data.calories),
        );
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getDays() {
  return function(dispatch) {
    return axiosAjax
      .get("/nutrition-tracker/food-log-day/")
      .then(response => {
        dispatch(updateDays(response.data));
      })
      .catch(error => {
        dispatch(updateDays([]));
      });
  };
}

export function getDay(dayId) {
  return function(dispatch) {
    return axiosAjax
      .get("/nutrition-tracker/food-log-day/" + dayId)
      .then(response => {
        dispatch(updateDay(response.data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getMeals(dayId) {
  return function(dispatch) {
    return axiosAjax
      .get("/nutrition-tracker/food-log-meal/" + dayId)
      .then(response => {
        dispatch(updateMeals(response.data));
      })
      .catch(error => {
        dispatch(updateMeals([]));
      });
  };
}
