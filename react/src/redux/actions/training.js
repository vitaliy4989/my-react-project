import axiosAjax from "../../config/axiosAjax";

export const updatePrograms = programs => {
  return {
    type: "UPDATEPROGRAMS",
    programs: programs,
  };
};

export const updateProgram = program => {
  return {
    type: "UPDATEPROGRAM",
    program: program,
  };
};

export const updateExercises = exercises => {
  return {
    type: "UPDATEEXERCISES",
    exercises: exercises,
  };
};

export const updateRequestedPrograms = requestedPrograms => {
  return {
    type: "UPDATEREQUESTEDPROGRAMS",
    requestedPrograms: requestedPrograms,
  };
};

export const updateRequestedProgram = requestedProgram => {
  return {
    type: "UPDATEREQUESTEDPROGRAM",
    requestedProgram: requestedProgram,
  };
};

export const updateTrainingDays = trainingDays => {
  return {
    type: "UPDATETRAININGDAYS",
    trainingDays: trainingDays,
  };
};

export const updateTrainingDayExercises = trainingDayExercises => {
  return {
    type: "UPDATETRAININGDAYEXERCISES",
    trainingDayExercises: trainingDayExercises,
  };
};

export function getPrograms() {
  return function(dispatch) {
    return axiosAjax
      .get("/training/training-log-day/")
      .then(response => {
        dispatch(updatePrograms(response.data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getProgram(programId) {
  return function(dispatch) {
    dispatch(updateProgram(undefined));
    return axiosAjax
      .get("/training/training-log-day/" + programId)
      .then(response => {
        dispatch(updateProgram(response.data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getExercises(programId) {
  return function(dispatch) {
    dispatch(updateExercises(undefined));
    return axiosAjax
      .get("/training/exercise/" + programId)
      .then(response => {
        dispatch(updateExercises(response.data));
      })
      .catch(error => {
        dispatch(updateExercises([]));
      });
  };
}

export function getRequestedPrograms() {
  return function(dispatch) {
    return axiosAjax
      .get("/training/user-training/")
      .then(response => {
        dispatch(updateRequestedPrograms(response.data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getRequestedProgram(programId) {
  return function(dispatch) {
    return axiosAjax
      .get("/training/user-training/" + programId)
      .then(response => {
        dispatch(updateRequestedProgram(response.data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getTrainingDays(trainingId) {
  return function(dispatch) {
    dispatch(updateTrainingDays(undefined));
    return axiosAjax
      .get("/training/training-day/", {
        params: {
          id: trainingId,
        },
      })
      .then(response => {
        dispatch(updateTrainingDays(response.data));
      })
      .catch(error => {
        dispatch(updateTrainingDays([]));
      });
  };
}

export function getTrainingDayExercises(trainingDayId) {
  return function(dispatch) {
    dispatch(updateTrainingDayExercises(undefined));
    return axiosAjax
      .get("/training/training-exercise/" + trainingDayId)
      .then(response => {
        dispatch(updateTrainingDayExercises(response.data));
      })
      .catch(error => {
        dispatch(updateTrainingDayExercises([]));
      });
  };
}
