import axiosAjax from "../../config/axiosAjax";

export const updateDayMeals = (day, dayId, dayCalories, dayProtein, meals) => {
  return {
    type: "UPDATEDAYMEALS",
    day: day,
    dayId: dayId,
    dayCalories: dayCalories,
    dayProtein: dayProtein,
    meals: meals,
  };
};

export function getDayMeals(id) {
  return function(dispatch) {
    return axiosAjax
      .get("/recipes/combination/" + id)
      .then(response => {
        dispatch(
          updateDayMeals(
            response.data.day,
            response.data.day.id,
            response.data.day.calories,
            response.data.day.protein,
            response.data.meals,
          ),
        );
      })
      .catch(error => {
        console.log(error);
      });
  };
}
