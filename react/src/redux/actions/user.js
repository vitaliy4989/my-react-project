import axiosAjax from "../../config/axiosAjax";

export const updateImage = image => {
  return {
    type: "UPDATEIMAGE",
    image: image,
  };
};

export const updateUser = (
  membershipStatus,
  membershipType,
  username,
  firstName,
  email,
) => {
  return {
    type: "UPDATEUSER",
    membershipStatus: membershipStatus,
    membershipType: membershipType,
    username: username,
    firstName: firstName,
    email: email,
  };
};

export function getImage() {
  return function(dispatch) {
    return (window.onload = () => {
      if (sessionStorage.profileImage) {
        dispatch(updateImage(sessionStorage.profileImage));
      }
    });
  };
}

export function getUser() {
  return function(dispatch) {
    return axiosAjax
      .get("/transactions/member-account/")
      .then(response => {
        dispatch(
          updateUser(
            response.data.status,
            response.data.membership_name,
            response.data.user.username,
            response.data.user.first_name,
            response.data.user.email,
          ),
        );
      })
      .catch(error => {
        console.log(error);
      });
  };
}
