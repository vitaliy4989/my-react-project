import axiosAjax from "../../config/axiosAjax";

export const updateNewFiveRecipes = recipes => {
  return {
    type: "UPDATENEWFIVERECIPES",
    recipes: recipes,
  };
};

export function getNewFiveRecipes() {
  return function(dispatch) {
    return axiosAjax
      .get("/recipes/get-all/")
      .then(response => {
        dispatch(updateNewFiveRecipes(response.data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}
