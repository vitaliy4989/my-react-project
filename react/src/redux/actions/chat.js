import axiosAjax from "../../config/axiosAjax";

export const updateChat = (chatId, comments, coachUpdate, otherUpdate) => {
  return {
    type: "UPDATECHAT",
    chatId: chatId,
    comments: comments,
    coachUpdate: coachUpdate,
    otherUpdate: otherUpdate,
  };
};

export const markRead = () => {
  return {
    type: "UPDATECHATREAD",
    coachUpdate: false,
    otherUpdate: false,
  };
};

export function getChat() {
  return function(dispatch) {
    return axiosAjax
      .get("/chat/user-chat/")
      .then(response => {
        dispatch(
          updateChat(
            response.data.chat.id,
            response.data.messages,
            response.data.chat.coach_update,
            response.data.chat.other_update,
          ),
        );
      })
      .catch(error => {
        console.log(error);
      });
  };
}
