import axiosAjax from "../../config/axiosAjax";

export const updateFavouriteVideos = favouriteVideos => {
  return {
    type: "UPDATEFAVOURITEVIDEOS",
    favouriteVideos: favouriteVideos,
  };
};

export const updateCurrentMainFilterId = currentVideoMainFilterId => {
  return {
    type: "UPDATECURRENTNMAINFILTERID",
    currentVideoMainFilterId: currentVideoMainFilterId,
  };
};

export const updateMainFilter = updateMainFilter => {
  return {
    type: "UPDATEMAINFILTER",
    updateMainFilter: updateMainFilter,
  };
};

export function getFavouriteVideos() {
  return function(dispatch) {
    return axiosAjax
      .get("/modules/favourites")
      .then(response => {
        dispatch(updateFavouriteVideos(response.data));
      })
      .catch(error => {
        dispatch(updateFavouriteVideos([]));
      });
  };
}
