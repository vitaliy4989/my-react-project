import axiosAjax from "../../config/axiosAjax";

export const updateGoal = (goal, checkIns, goalCategory) => {
  return {
    type: "UPDATEGOAL",
    goal: goal,
    checkIns: checkIns,
    goalCategory: goalCategory,
  };
};

export function getGoal() {
  return function(dispatch) {
    return axiosAjax
      .get("/progress/goal/")
      .then(response => {
        dispatch(
          updateGoal(
            response.data.goal,
            response.data.check_ins,
            response.data.goal_category,
          ),
        );
      })
      .catch(error => {
        dispatch(updateGoal("", undefined, undefined));
      });
  };
}
