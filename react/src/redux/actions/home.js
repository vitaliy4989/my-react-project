import axiosAjax from "../../config/axiosAjax";
import localforage from "localforage";

export const updateStepOne = (stepOne, stepOneCompleted) => {
  return {
    type: "UPDATESTEPONE",
    stepOne: stepOne,
    stepOneCompleted: stepOneCompleted,
  };
};

export const updateStepTwo = (stepTwo, stepTwoCompleted) => {
  return {
    type: "UPDATESTEPTWO",
    stepTwo: stepTwo,
    stepTwoCompleted: stepTwoCompleted,
  };
};

export const updateStepThree = (stepThree, stepThreeCompleted) => {
  return {
    type: "UPDATESTEPTHREE",
    stepThree: stepThree,
    stepThreeCompleted: stepThreeCompleted,
  };
};

export const updateStepFour = (stepFour, stepFourCompleted) => {
  return {
    type: "UPDATESTEPFOUR",
    stepFour: stepFour,
    stepFourCompleted: stepFourCompleted,
  };
};

export const updateStepFive = (stepFive, stepFiveCompleted) => {
  return {
    type: "UPDATESTEPFIVE",
    stepFive: stepFive,
    stepFiveCompleted: stepFiveCompleted,
  };
};

export const updateStepSix = (stepSix, stepSixCompleted) => {
  return {
    type: "UPDATESTEPSIX",
    stepSix: stepSix,
    stepSixCompleted: stepSixCompleted,
  };
};

export function getStepOne() {
  return function(dispatch) {
    return axiosAjax
      .get("/home-steps/one")
      .then(response => {
        localforage.setItem(
          sessionStorage.username + "stepOne",
          response.data.completed,
        );
        dispatch(updateStepOne(response.data, response.data.completed));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getStepTwo() {
  return function(dispatch) {
    return axiosAjax
      .get("/home-steps/two")
      .then(response => {
        localforage.setItem(
          sessionStorage.username + "stepTwo",
          response.data.completed,
        );
        dispatch(updateStepTwo(response.data, response.data.completed));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getStepThree() {
  return function(dispatch) {
    return axiosAjax
      .get("/home-steps/three")
      .then(response => {
        localforage.setItem(
          sessionStorage.username + "stepThree",
          response.data.completed,
        );
        dispatch(updateStepThree(response.data, response.data.completed));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getStepFour() {
  return function(dispatch) {
    return axiosAjax
      .get("/home-steps/four")
      .then(response => {
        localforage.setItem(
          sessionStorage.username + "stepFour",
          response.data.completed,
        );
        dispatch(updateStepFour(response.data, response.data.completed));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getStepFive() {
  return function(dispatch) {
    return axiosAjax
      .get("/home-steps/five")
      .then(response => {
        localforage.setItem(
          sessionStorage.username + "stepFive",
          response.data.completed,
        );
        dispatch(updateStepFive(response.data, response.data.completed));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getStepSix() {
  return function(dispatch) {
    return axiosAjax
      .get("/home-steps/six")
      .then(response => {
        localforage.setItem(
          sessionStorage.username + "stepSix",
          response.data.completed,
        );
        dispatch(updateStepSix(response.data, response.data.completed));
      })
      .catch(error => {
        console.log(error);
      });
  };
}
