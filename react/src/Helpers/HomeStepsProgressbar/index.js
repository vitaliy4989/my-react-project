// @flow

type Type = {
  nutrition_total?: number,
  training_total?: number,
  nutrition_completed?: number,
  training_completed?: number,
  completed?: boolean,
  training?: 0,
  nutrition?: 0,
};

export const stepOneProgress = ({
  nutrition_total,
  training_total,
  nutrition_completed,
  training_completed,
  completed,
  training,
  nutrition,
}: Type): number => {
  if (training === 100 && nutrition !== 100) return 50;
  if (completed) return 100;
  if ((training && nutrition) === 0)
    return parseInt(
      ((nutrition_completed + training_completed) /
        (nutrition_total + training_total)) *
        100,
    );
};
