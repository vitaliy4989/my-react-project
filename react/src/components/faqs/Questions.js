import React from "react";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";

export default class Questions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: null,
    };
  }

  componentDidMount() {
    axiosAjax
      .get("/chat/faq-questions/", {
        params: {
          id: this.props.topicId,
        },
      })
      .then(response => {
        this.setState({
          questions: response.data,
        });
      })
      .catch(error => {
        this.setState({
          questions: [],
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        {this.state.questions !== null ? (
          this.state.questions.map((question, key) => (
            <div key={key} className='content-container'>
              <h6>{question.question}</h6>
              <p style={{ whiteSpace: "pre-line" }}>{question.answer}</p>
            </div>
          ))
        ) : (
          <div
            style={{ paddingTop: "5%", paddingBottom: "5%" }}
            className='text-center'
          >
            <MDSpinner
              color1='#4a4090'
              color2='#4a4090'
              color3='#4a4090'
              color4='#4a4090'
            />
          </div>
        )}
      </React.Fragment>
    );
  }
}
