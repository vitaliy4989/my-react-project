import React from "react";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import Questions from "./Questions";

export default class Topics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      topics: null,
      search: "",
      questions: null,
      searchIcon: <i className='far dark-purp fa-search' />,
      noResult: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    axiosAjax
      .get("/chat/faq-topics/")
      .then(response => {
        this.setState({
          topics: response.data,
        });
      })
      .catch(error => {
        this.setState({
          topics: [],
        });
      });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    axiosAjax
      .get("/chat/faq-questions/" + this.state.search + "/")
      .then(response => {
        this.setState({
          questions: response.data,
          noResult: false,
        });
      })
      .catch(error => {
        this.setState({
          questions: null,
          noResult: true,
        });
      });
    this.setState({
      searchIcon: (
        <i
          className='far fa-times-circle'
          style={crossIcon}
          onClick={() =>
            this.setState({
              questions: null,
              noResult: false,
              search: "",
              searchIcon: <i className='far dark-purp fa-search' />,
            })
          }
        />
      ),
    });
  }

  render() {
    return (
      <React.Fragment>
        <form
          onChange={this.handleSubmit}
          style={{ marginBottom: "1rem", marginTop: "1.5rem" }}
        >
          <div className='input-group faq-search'>
            <input
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.search}
              name='search'
              className='form-control'
              placeholder='Type question key words...'
              style={{
                borderRadius: 20,
                borderTopRightRadius: 0,
                borderColor: "#CED4DA",
                borderBottomRightRadius: 0,
                borderRight: 0,
              }}
              aria-describedby='faq-search-icon'
            />
            <div className='input-group-append'>
              <span
                className='input-group-text faq-search-icon'
                id='faq-search-icon'
              >
                {this.state.searchIcon}
              </span>
            </div>
          </div>
        </form>
        {this.state.questions !== null
          ? this.state.questions.map((question, key) => (
              <div
                key={key}
                className='animated-2 fadeIn'
                className='content-container'
              >
                <h6>{question.question}</h6>
                <p>{question.answer}</p>
              </div>
            ))
          : this.state.noResult === true && (
              <div className='text-center alert alert-light'>
                <p>
                  No questions match your search, maybe try different key words.
                </p>
              </div>
            )}
        {this.state.topics !== null ? (
          this.state.topics.map((topic, key) => (
            <div
              key={key}
              className='animated-2 fadeIn'
              style={{ marginTop: 25 }}
            >
              <h4>{topic.name}</h4>
              <Questions topicId={topic.id} />
            </div>
          ))
        ) : (
          <div
            style={{ paddingTop: "10%", paddingBottom: "10%" }}
            className='text-center'
          >
            <MDSpinner
              color1='#4a4090'
              color2='#4a4090'
              color3='#4a4090'
              color4='#4a4090'
            />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const crossIcon = {
  cursor: "pointer",
};

const searchIcon = {
  border: "none",
};
