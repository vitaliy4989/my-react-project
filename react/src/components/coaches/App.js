import React, { Component } from "react";
import { Route, HashRouter, Redirect, Switch } from "react-router-dom";
import CoachChat from "../chat/CoachChat";
import Conversation from "../chat/Conversation";
import CanceledPurchased from "../analytics/CanceledPurchased";
import Home from "./Home";
import Judging from "../challenge/Judging";
import JudgeEntry from "../challenge/JudgeEntry";
import Requests from "../exercise/coaches/Requests";
import Request from "../exercise/coaches/Request";
import ManageDays from "../exercise/coaches/ManageDays";
import ManageExercises from "../exercise/coaches/manage/exercises/index";
import ManagePrograms from "../exercise/coaches/manage/programs/index";

export default class App extends React.Component {
  render() {
    return (
      <HashRouter basename='/'>
        <React.Fragment>
          <Route exact path='/' component={Home} />
          <Route exact path='/chat' component={CoachChat} />
          <Route exact path='/conversation/:id' component={Conversation} />
          <Route exact path='/judging' component={Judging} />
          <Route exact path='/judge/:id' component={JudgeEntry} />
          <Route exact path='/analytics' component={CanceledPurchased} />
          <Route exact path='/requests' component={Requests} />
          <Route exact path='/request/:id' component={Request} />
          <Route exact path='/manage/days' component={ManageDays} />
          <Route exact path='/manage/exercises' component={ManageExercises} />
          <Route exact path='/manage/programs' component={ManagePrograms} />
        </React.Fragment>
      </HashRouter>
    );
  }
}
