import React from "react";
import { NavLink } from "react-router-dom";
import Profile from "./Profile";

export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Coaches</h4>
              </div>
            </div>
          </div>
        </div>

        <div className='container'>
          <Profile />
        </div>

        <div className='container'>
          <div className='list-group list-group-flush list-wrapper animated fadeIn'>
            <NavLink
              to='/requests'
              className='waves-effect list-group-item list-group-item-action d-flex justify-content-between align-items-center animated fadeIn'
            >
              <span>Training Requests</span>
              <span>
                <i className='far fa-chevron-right' />
              </span>
            </NavLink>
            <NavLink
              to='/chat'
              className='waves-effect list-group-item list-group-item-action d-flex justify-content-between align-items-center animated fadeIn'
            >
              <span>Chats</span>
              <span>
                <i className='far fa-chevron-right' />
              </span>
            </NavLink>
            <NavLink
              to='/judging'
              className='waves-effect list-group-item list-group-item-action d-flex justify-content-between align-items-center animated fadeIn'
            >
              <span>Challenge Judging</span>
              <span>
                <i className='far fa-chevron-right' />
              </span>
            </NavLink>
            {sessionStorage.username === "jamesshaw" ? (
              <NavLink
                to='/analytics'
                className='waves-effect list-group-item list-group-item-action d-flex justify-content-between align-items-center animated fadeIn'
              >
                <span>Business Analytics</span>
                <span>
                  <i className='far fa-chevron-right' />
                </span>
              </NavLink>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
