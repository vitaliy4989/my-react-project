import React from "react";
import axiosAjax from "../../config/axiosAjax";
import Uploader from "../shared/Uploader";

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      coachProfile: undefined,
      coachImage: undefined,
      image: "",
      updating: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._createCoachProfile = this._createCoachProfile.bind(this);
    this._updateCoachProfile = this._updateCoachProfile.bind(this);
  }

  componentDidMount() {
    axiosAjax
      .get("/users/coach-profile")
      .then(response => {
        this.setState({
          coachProfile: true,
          coachImage: response.data.image,
        });
      })
      .catch(error => {
        this.setState({
          coachProfile: false,
          coachImage: undefined,
        });
      });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _createCoachProfile(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("image", this.state.image.uuid);
    axiosAjax({
      method: "post",
      url: "/users/coach-profile/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          coachProfile: true,
          coachImage: response.data.image,
        });
      })
      .catch(error => {
        this.setState({
          coachProfile: false,
          coachImage: undefined,
        });
      });
  }

  _updateCoachProfile(event) {
    event.preventDefault();
    this.setState({ updating: true });
    const formData = new FormData();
    formData.append("image", this.state.image.uuid);
    axiosAjax({
      method: "patch",
      url: "/users/coach-profile/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          coachProfile: true,
          coachImage: response.data.image,
          updating: false,
        });
      })
      .catch(error => {
        this.setState({
          coachProfile: false,
          coachImage: undefined,
          updating: false,
        });
      });
  }

  render() {
    return (
      <div className='content-container text-center'>
        {this.state.coachProfile !== undefined ? (
          this.state.coachProfile === true ? (
            <React.Fragment>
              <img
                className='rounded-circle'
                style={{ border: "solid 2px #4a4090" }}
                src={this.state.coachImage + "-/autorotate/yes/"}
                height='100'
                width='100'
              />
              <form onSubmit={this._updateCoachProfile}>
                <div className='form-group text-center'>
                  <label>Coach Profile Image</label>
                  <br />
                  <Uploader
                    id='file'
                    name='file'
                    imageOnly={true}
                    dataCrop='400x400'
                    onChange={file => {
                      if (file) {
                        file.progress(info => console.log("uploaded"));
                        file.done(info => console.log("done"));
                      }
                    }}
                    onUploadComplete={info => this.setState({ image: info })}
                  />
                </div>
                <div className='form-group text-center'>
                  <button className='btn btn-primary btn-rounded'>
                    {this.state.updating === true
                      ? "Please wait..."
                      : "Update profile image"}
                  </button>
                </div>
              </form>
            </React.Fragment>
          ) : (
            <form onSubmit={this._createCoachProfile}>
              <div className='form-group text-center'>
                <label>Coach Profile Image</label>
                <br />
                <Uploader
                  id='file'
                  name='file'
                  imageOnly={true}
                  dataCrop='400x400'
                  onChange={file => {
                    if (file) {
                      file.progress(info => console.log("uploaded"));
                      file.done(info => console.log("done"));
                    }
                  }}
                  onUploadComplete={info => this.setState({ image: info })}
                />
              </div>
              <div className='form-group text-center'>
                <button className='btn btn-primary btn-rounded'>
                  Create coach profile
                </button>
              </div>
            </form>
          )
        ) : (
          <div>Loading coach profile...</div>
        )}
      </div>
    );
  }
}
