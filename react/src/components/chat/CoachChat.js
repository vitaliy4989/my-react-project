import React from "react";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import Conversation from "./Conversation";

export default class CoachChat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      search: "",
      chatId: undefined,
      coaches: [],
      completedByCoach: [],
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this._loadChats = this._loadChats.bind(this);
  }

  _loadChats(event) {
    axiosAjax
      .get("/chat/coach-chat")
      .then(response => {
        this.setState({
          data: response.data.chats,
          chatId: response.data.chats[0].id,
        });
      })
      .catch(error => {
        this.setState({
          data: [],
        });
      });
  }

  _loadChatsAnswered() {
    axiosAjax
      .get("/chat/chats-answered")
      .then(response => {
        this.setState({
          completedByCoach: response.data,
        });
      })
      .catch(error => {
        this.setState({
          completedByCoach: [],
        });
      });
  }

  componentDidMount() {
    this._loadChats();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  deleteChat(id) {
    var r = confirm("Do you want to delete this clients program?!");
    if (r == true) {
      axiosAjax.delete("/chat/coach-chat/" + id + "/").then(response => {
        this.reloadChats();
      });
    } else {
      console.log("not deleted");
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    axiosAjax
      .get("/chat/coach-chat", {
        params: {
          search: this.state.search,
        },
      })
      .then(response => {
        this.setState({
          data: response.data,
        });
      })
      .catch(error => {
        this.setState({
          data: [],
        });
      });
  }

  render() {
    return (
      <div>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Coach Chat</h4>
              </div>
            </div>
          </div>
        </div>
        <div className='container'>
          <div className='content-container'>
            <div className='row'>
              <div className='col'>
                <h6># of chats answered</h6>
              </div>
              <div className='col'>
                <button
                  className='btn btn-sm btn-light btn-rounded float-right'
                  onClick={() => this._loadChatsAnswered()}
                >
                  Load Table
                </button>
              </div>
            </div>
            <table className='table'>
              <thead>
                <tr>
                  <th scope='col'>Profile</th>
                  <th scope='col'>Name</th>
                  <th scope='col'>Chats completed by date</th>
                </tr>
              </thead>
              <tbody>
                {this.state.completedByCoach.map((coach, key) =>
                  coach.completed_today.length > 0 ? (
                    <tr key={key}>
                      <td>
                        <img
                          className='mr-3 small-profile-wrapper rounded-circle'
                          src={coach.image + "-/autorotate/yes/"}
                        />
                      </td>
                      <td>{coach.username}</td>
                      <td>
                        <div
                          style={{
                            maxHeight: 75,
                            overflowX: "hidden",
                            overflowY: "scroll",
                          }}
                        >
                          <table className='table table-sm'>
                            <thead className='thead-light'>
                              <tr>
                                <th scope='col'>Date</th>
                                <th scope='col'>Chats</th>
                              </tr>
                            </thead>
                            <tbody>
                              {coach.completed_today.map((day, key) => (
                                <tr key={key}>
                                  <td>{day.date}</td>
                                  <td>{day.date_count}</td>
                                </tr>
                              ))}
                            </tbody>
                          </table>
                        </div>
                      </td>
                    </tr>
                  ) : null,
                )}
              </tbody>
            </table>
          </div>
          <div className='container'>
            <div className='row'>
              <div
                className='col text-center'
                style={{
                  marginTop: 30,
                  border: "1px solid #E2E6EA",
                  borderRadius: 8,
                  padding: 5,
                }}
              >
                <h6 className='m-0'>Server date</h6>
                <p className='m-0'>
                  <u>{sessionStorage.serverTime}</u>
                </p>
                <p className='m-0 p-smaller'>
                  Chats before this date are overdue.
                </p>
              </div>
            </div>
          </div>
          <div className='content-container' style={{ padding: 0 }}>
            <div className='row no-gutters animated fadeIn'>
              <div className='col-lg-4 col-md-5'>
                <div
                  className='container'
                  style={{
                    paddingTop: 15,
                    paddingBottom: 15,
                    borderTopLeftRadius: 6,
                  }}
                >
                  <div className='row'>
                    <div className='col'>
                      <form onSubmit={this.handleSubmit} className='form-row'>
                        <div className='col-10'>
                          <div className='chat-search'>
                            <input
                              autoComplete='off'
                              onChange={this.handleInputChange}
                              value={this.state.search}
                              type='text'
                              className='form-control form-control-rounded'
                              name='search'
                              type='text'
                              placeholder='Search by username....'
                              aria-label='Search'
                              aria-describedby='basic-addon2'
                            />
                          </div>
                        </div>
                        <div className='col-2'>
                          <button className='btn btn-block btn-rounded btn-light'>
                            <i className='fas fa-search' />
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div className='row' style={{ marginTop: "1rem" }}>
                    <div className='col'>
                      <button
                        className='btn btn-block btn-rounded btn-light'
                        onClick={() => this._loadChats()}
                      >
                        Reload Chats <i className='fas fa-sync-alt' />
                      </button>
                    </div>
                  </div>
                  <div className='row' style={{ marginTop: "1rem" }}>
                    <div className='col text-center'>
                      <i className='fas fa-lg fa-inbox-in' /> &nbsp;{" "}
                      {this.state.data !== undefined && this.state.data.length}
                    </div>
                  </div>
                </div>
                <div
                  className='list-group list-group-flush'
                  style={{
                    maxHeight: 300,
                    minHeight: 200,
                    overflowX: "hidden",
                    overflowY: "scroll",
                  }}
                >
                  {this.state.data !== undefined ? (
                    this.state.data.length > 0 ? (
                      this.state.data.map((chat, key) => (
                        <div
                          key={key}
                          className='list-group-item animated fadeIn'
                          onClick={() => this.setState({ chatId: chat.id })}
                          style={
                            this.state.chatId === chat.id
                              ? {
                                  backgroundColor: "#e9ecef",
                                  cursor: "pointer",
                                  borderRadius: 0,
                                }
                              : {
                                  backgroundColor: "#DDE1E7",
                                  cursor: "pointer",
                                  borderRadius: 0,
                                }
                          }
                        >
                          <div className='row d-flex justify-content-between align-items-center'>
                            <div className='col'>
                              {chat.user.name
                                ? chat.user.name
                                : chat.user.username}
                              <br />
                              <span
                                style={{ fontSize: 10 }}
                                className='text-muted'
                              >
                                {chat.date.substring(0, chat.date.indexOf("T"))}
                              </span>
                            </div>
                            {chat.client_update === true && (
                              <div className='col-auto'>
                                <span className='badge badge-primary badge-pill'>
                                  1
                                </span>
                              </div>
                            )}
                          </div>
                        </div>
                      ))
                    ) : (
                      <div className='list-group-item text-center'>
                        No Chats
                      </div>
                    )
                  ) : (
                    <div
                      style={{ paddingTop: "10%", paddingBottom: "10%" }}
                      className='text-center'
                    >
                      <MDSpinner
                        color1='#4a4090'
                        color2='#4a4090'
                        color3='#4a4090'
                        color4='#4a4090'
                      />
                    </div>
                  )}
                </div>
              </div>
              <div
                className='col-lg-8 col-md-7'
                style={{ background: "#F8F8FB" }}
              >
                {this.state.chatId !== undefined && (
                  <Conversation
                    loadChats={this._loadChats}
                    chatId={this.state.chatId}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const respond = {
  color: "#84e184",
};
