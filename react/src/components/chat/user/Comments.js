import React, { PureComponent } from "react";
import axiosAjax from "../../../config/axiosAjax";
import Comment from "../../shared/Comment";
import CreateComment from "./CreateComment";
import { connect } from "react-redux";
import { getChat, markRead } from "../../../redux/actions/chat";
import MDSpinner from "react-md-spinner";

const scrollToBottom = () => {
  const messagesDiv = document.getElementById("commentArea");
  if (messagesDiv) messagesDiv.scrollTop = messagesDiv.scrollHeight;
};

class Comments extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      comments: undefined,
      coachUpdate: this.props.coachUpdate,
      trialAlert: false,
    };
    this._pushNewComment = this._pushNewComment.bind(this);
  }

  _pushNewComment(comment) {
    this.props.comments.push({
      profile_c: null,
      comment: comment,
      id: null,
      profile_u: true,
      date: "Now",
    });
    this.forceUpdate();
  }

  componentDidMount() {
    if (!this.props.chatId) {
      this.props.onGetChat();
    }
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "none";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "none";
    }
    this._removeBadge();
    scrollToBottom();
  }

  componentDidUpdate() {
    scrollToBottom();
  }

  _removeBadge() {
    if (this.props.coachUpdate === true) {
      axiosAjax({
        method: "patch",
        url: "/chat/user-chat/" + this.props.chatId + "/",
        headers: { "Content-Type": "multipart/form-data" },
      })
        .then(response => {
          this.props.onMarkRead();
        })
        .catch(error => {
          console.log(error);
        });
    }
    if (this.props.otherUpdate === true) {
      axiosAjax({
        method: "patch",
        url: "/chat/other-update/" + this.props.chatId + "/",
        headers: { "Content-Type": "multipart/form-data" },
      })
        .then(response => {
          this.props.onMarkRead();
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  componentWillUnmount() {
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "block";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "block";
    }
  }

  render() {
    return (
      <div id='trainer-support-messages' className='trainer-support-messages'>
        <div
          style={{
            border: "1px solid #E8ECEF",
            paddingTop: 5,
            paddingBottom: 5,
            borderTop: "none",
            background: "white",
          }}
        >
          <div className='container text-center'>
            <strong>Trainer Support</strong>
            {this.props.membershipStatus === "trialing" ? (
              <p className='p-smaller text-muted m-0'>
                If you would like support, please{" "}
                <a href='/users/membership-options/'>
                  <u>upgrade your account</u>
                </a>
                .
              </p>
            ) : (
              <p className='p-smaller text-muted m-0'>
                Also check out our{" "}
                <a href='/users/#/frequently-asked-questions'>
                  <u>F.A.Qs</u>
                </a>
                . We aim to reply in 24h.
              </p>
            )}
          </div>
        </div>
        {this.props.comments !== undefined ? (
          this.props.comments.length > 0 ? (
            this.props.comments.length >= 1 ? (
              <div
                style={{
                  height: "100%",
                  overflowX: "hidden",
                  overflowY: "scroll",
                  border: "1px solid #E8ECEF",
                  background: "white",
                }}
                id='commentArea'
              >
                <div className='container'>
                  {this.props.comments.map((comment, key) =>
                    comment.profile_u ? (
                      <div key={key} className='row'>
                        <div className='col'>
                          {Comment({
                            comment: comment.comment,
                            noImage: true,
                            class: "comment-right animated fadeIn",
                            date: comment.date.substring(
                              0,
                              comment.date.indexOf("T"),
                            ),
                          })}
                        </div>
                      </div>
                    ) : (
                      <div key={key} className='row'>
                        <div className='col'>
                          {Comment({
                            comment: comment.comment,
                            image: comment.profile_c.image,
                            heading: comment.profile_c.user.first_name,
                            class: "comment-left animated fadeIn",
                            textClass: "comment-left-text",
                            date: comment.date.substring(
                              0,
                              comment.date.indexOf("T"),
                            ),
                          })}
                        </div>
                      </div>
                    ),
                  )}
                </div>
              </div>
            ) : (
              <div className='h-100 d-flex flex-column justify-content-center'>
                <div className='text-center'>
                  <MDSpinner singleColor='#4a4090' />
                </div>
              </div>
            )
          ) : (
            <div className='h-100 d-flex flex-column justify-content-center' />
          )
        ) : (
          <div className='h-100 d-flex flex-column justify-content-center'>
            <div className='text-center'>
              <MDSpinner singleColor='#4a4090' />
            </div>
          </div>
        )}
        <CreateComment
          pushNewComment={this._pushNewComment}
          id={this.props.chatId}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    chatId: state.chat.chatId,
    comments: state.chat.comments,
    coachUpdate: state.chat.coachUpdate,
    otherUpdate: state.chat.otherUpdate,
    membershipStatus: state.user.membershipStatus,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetChat: () => {
      dispatch(getChat());
    },
    onMarkRead: () => {
      dispatch(markRead());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Comments);
