import React from "react";
import { connect } from "react-redux";
import { getChat } from "../../redux/actions/chat";
import { NavLink } from "react-router-dom";

class ChatNotifications extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onGetChat();
  }

  render() {
    return $(window).width() > 768 ? (
      <NavLink
        to='/trainer-support'
        className='trainer-support-button'
        style={{ margin: "1rem", position: "fixed", bottom: 0, right: 0 }}
      >
        <button
          className='btn btn-primary float-right btn-rounded'
          style={{
            boxShadow:
              "3px 3px 29px  rgba(74, 64, 144, .2), 2px 2px 29px  rgba(74, 64, 144, .2)",
          }}
        >
          {this.props.coachUpdate === true ||
          this.props.otherUpdate === true ? (
            <span className='badge badge-pill badge-danger'>1</span>
          ) : null}
          &nbsp; Support &nbsp;
          <i className='far fa-comments-alt' />
        </button>
      </NavLink>
    ) : this.props.coachUpdate === true || this.props.otherUpdate === true ? (
      <span className='badge badge-pill badge-danger'>1</span>
    ) : null;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    coachUpdate: state.chat.coachUpdate,
    otherUpdate: state.chat.otherUpdate,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetChat: () => {
      dispatch(getChat());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatNotifications);
