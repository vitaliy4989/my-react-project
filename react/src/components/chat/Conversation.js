import React from "react";
import axiosAjax from "../../config/axiosAjax";
import Comment from "../shared/Comment";
import MDSpinner from "react-md-spinner";

const scrollToBottom = () => {
  const messagesDiv = document.getElementById("commentArea");
  if (messagesDiv) messagesDiv.scrollTop = messagesDiv.scrollHeight;
};

export default class Conversation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      comment: "",
      profile: "",
      markRead: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  _loadChatMessages(id) {
    axiosAjax
      .get("/chat/coach-chat/" + id)
      .then(response => {
        this.setState({
          data: response.data,
          profile: response.data.profile,
        });
      })
      .catch(error => {
        this.setState({
          data: [],
          profile: "",
        });
      });
    scrollToBottom();
  }

  componentDidMount() {
    this._loadChatMessages(this.props.chatId);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.chatId !== this.props.chatId) {
      this._loadChatMessages(this.props.chatId);
      this.setState({
        markRead: false,
      });
    }
    scrollToBottom();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  markAsRead() {
    axiosAjax({
      method: "patch",
      url: "/chat/user-chat/" + this.props.chatId + "/",
      headers: { "Content-Type": "multipart/form-data" },
    })
      .then(response => {
        this.setState({
          markRead: true,
        });
        this.props.loadChats();
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleSubmit(event) {
    event.preventDefault();

    document.getElementById("formButton").disabled = true;

    const formData = new FormData();
    formData.append("id", this.state.data.chat.id);
    formData.append("comment", this.state.comment);
    axiosAjax({
      method: "post",
      url: "/chat/coach-chat/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          comment: "",
        });
        this._loadChatMessages(this.props.chatId);
        this.props.loadChats();
        document.getElementById("formButton").disabled = false;
      })
      .catch(function(error) {
        console.log(error);
        document.getElementById("formButton").disabled = false;
      });
  }

  render() {
    return (
      <div style={{ background: "white" }}>
        <div
          className='container'
          style={{
            paddingTop: 0,
            background: "white",
            borderTopLeftRadius: 0,
            borderBottomLeftRadius: 0,
          }}
        >
          <div
            className='row'
            style={{
              paddingTop: 15,
              paddingBottom: 15,
              borderTopRightRadius: 6,
            }}
          >
            <div className='col d-flex flex-column justify-content-center'>
              {this.state.profile !== "" && (
                <h6 className='m-0 text-muted'>
                  {this.state.profile.user.first_name} &nbsp;{" "}
                  {this.state.profile.user.username}
                </h6>
              )}
            </div>
            <div className='col-auto d-flex flex-column justify-content-center'>
              <button
                className='btn btn-rounded btn-primary'
                onClick={() => this.markAsRead()}
              >
                {this.state.markRead === true ? (
                  <i className='far green fa-check' />
                ) : (
                  <div>No comment</div>
                )}
              </button>
            </div>
          </div>
        </div>
        {this.state.data !== undefined ? (
          this.state.data.comments.length > 0 && (
            <div
              style={{
                maxHeight: 400,
                minHeight: 400,
                overflowX: "hidden",
                overflowY: "scroll",
                padding: 10,
                border: "solid 1px #E8ECEF",
                borderRight: "none",
                boxShadow: "inset 0 0 10px rgba(0, 0, 0, .25)",
              }}
              id='commentArea'
            >
              {this.state.data.comments.map((comment, key) =>
                comment.profile_c !== null ? (
                  <div key={key} className='row'>
                    <div className='col'>
                      <Comment
                        class='animated fadeIn'
                        comment={comment.comment}
                        noImage={true}
                        class='comment-right animated fadeIn'
                      />
                    </div>
                  </div>
                ) : (
                  <div key={key} className='row'>
                    <div className='col'>
                      <Comment
                        class='animated fadeIn'
                        image={comment.profile_u.image}
                        comment={comment.comment}
                        class='comment-left animated fadeIn'
                        textClass='comment-left-text'
                      />
                    </div>
                  </div>
                ),
              )}
            </div>
          )
        ) : (
          <div
            style={{ paddingTop: "10%", paddingBottom: "10%" }}
            className='text-center'
          >
            <MDSpinner
              color1='#4a4090'
              color2='#4a4090'
              color3='#4a4090'
              color4='#4a4090'
            />
          </div>
        )}
        <div style={{ padding: 10 }}>
          <form onSubmit={this.handleSubmit}>
            <div className='form-row'>
              <div className='col-10'>
                <textarea
                  className='form-control form-control-rounded'
                  onChange={this.handleInputChange}
                  value={this.state.comment}
                  name='comment'
                  placeholder='Type a message...'
                  required
                />
              </div>
              <div className='col-2'>
                <button
                  id='formButton'
                  className='btn btn-block btn-primary btn-rounded'
                >
                  {this.state.sending === true ? (
                    <i className='fas fa-circle-notch fa-spin' />
                  ) : (
                    <i className='fab fa-lg fa-telegram-plane' />
                  )}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const file = {
  fontSize: 12,
  marginTop: 10,
};
