import React, { Component } from "react";
import axiosAjax from "../../config/axiosAjax";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Legend,
  ResponsiveContainer,
  BarChart,
  Bar,
  Tooltip,
} from "recharts";

class CanceledPurchased extends Component {
  constructor(props) {
    super(props);
    this.state = {
      canceled: [],
      trialed: [],
      purchased: [],
      active: 0,
      trialing: 0,
      totalTrialed: 0,
      totalPurchased: 0,
      membershipTypes: [],
      challenge: 0,
    };
  }

  componentDidMount() {
    axiosAjax.get("/analytics/get-canceled-purchased/").then(response => {
      this.setState({
        purchased: response.data.purchased,
        canceled: response.data.canceled,
        trialed: response.data.trialed,
        active: response.data.active,
        trialing: response.data.trialing,
        totalTrialed: response.data.total_trialed,
        totalPurchased: response.data.total_purchased,
        membershipTypes: response.data.membership_types,
        challenge: response.data.challenge,
      });
    });
  }

  render() {
    return (
      <React.Fragment>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Business Analytics</h4>
              </div>
            </div>
          </div>
        </div>
        <div className='container'>
          <div className='row'>
            <div className='col'>
              <div className='content-container'>
                <h5>Active | Trials | Challenge</h5>
                <ResponsiveContainer width='100%' height={250}>
                  <BarChart
                    data={[
                      {
                        status: "Status",
                        Active: this.state.active,
                        Trialing: this.state.trialing,
                        Challenge: this.state.challenge,
                      },
                    ]}
                    margin={{ top: 5, bottom: 5 }}
                  >
                    <XAxis dataKey='status' />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey='Active' fill='#4c9064' />
                    <Bar dataKey='Trialing' fill='#5E4BDD' />
                    <Bar dataKey='Challenge' fill='#7b74ae' />
                  </BarChart>
                </ResponsiveContainer>
              </div>
              <div className='content-container'>
                <h5>Membership Types</h5>
                <ResponsiveContainer width='100%' height={250}>
                  <BarChart
                    data={this.state.membershipTypes}
                    margin={{ top: 5, bottom: 5 }}
                  >
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <XAxis dataKey='name' />
                    <Bar dataKey='count' label='name' fill='#4A4090' />
                  </BarChart>
                </ResponsiveContainer>
              </div>
              <div className='content-container' style={{ marginTop: "1rem" }}>
                <h5>Trialled</h5>
                <ResponsiveContainer width='100%' height={250}>
                  <LineChart
                    data={this.state.trialed}
                    margin={{ top: 5, bottom: 5 }}
                  >
                    <XAxis dataKey='date' />
                    <YAxis domain={["dataMin", "dataMax"]} />
                    <Tooltip />
                    <Line
                      type='monotone'
                      dataKey='date_count'
                      name='Trialled'
                      stroke='#5E4BDD'
                      strokeWidth={2}
                      dot={{ strokeWidth: 1 }}
                    />
                  </LineChart>
                </ResponsiveContainer>
              </div>
              <div className='content-container text-center'>
                <h5 className='text-muted'>
                  {Math.round(
                    (this.state.totalPurchased / this.state.totalTrialed) * 100,
                  )}
                  % conversion
                </h5>
                <i className='fal fa-lg fa-arrow-alt-down' />
              </div>
              <div className='content-container'>
                <h5>Purchased</h5>
                <ResponsiveContainer width='100%' height={250}>
                  <LineChart
                    data={this.state.purchased}
                    margin={{ top: 5, bottom: 5 }}
                  >
                    <XAxis dataKey='date' />
                    <YAxis domain={["dataMin", "dataMax"]} />
                    <Tooltip />
                    <Line
                      type='monotone'
                      dataKey='date_count'
                      name='Purchased'
                      stroke='#4A4090'
                      strokeWidth={2}
                      dot={{ strokeWidth: 1 }}
                    />
                  </LineChart>
                </ResponsiveContainer>
              </div>
              <div className='content-container' style={{ marginTop: "1rem" }}>
                <h5>Cancelled</h5>
                <ResponsiveContainer width='100%' height={250}>
                  <LineChart
                    data={this.state.canceled}
                    margin={{ top: 5, bottom: 5 }}
                  >
                    <XAxis dataKey='date' />
                    <YAxis domain={["dataMin", "dataMax"]} />
                    <Tooltip />
                    <Line
                      type='monotone'
                      dataKey='date_count'
                      name='Cancelled'
                      stroke='#f1a6a0'
                      strokeWidth={2}
                      dot={{ strokeWidth: 1 }}
                    />
                  </LineChart>
                </ResponsiveContainer>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CanceledPurchased;
