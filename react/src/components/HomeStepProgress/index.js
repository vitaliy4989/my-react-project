/**
 * @flow
 * @format
 */
import React, { PureComponent } from "react";

import "./styles.css";

type Props = {
  value: number,
};

class HomeStepProgress extends PureComponent<Props> {
  state = {
    progressValue: 1,
  };

  componentDidMount() {
    setTimeout(() => this.setState({ progressValue: this.props.value }), 100);
  }

  render() {
    const { progressValue } = this.state;

    if (progressValue === 0) return null;

    return (
      <div
        style={{ marginTop: 10 }}
        className='col-xs-12 d-flex flex-row align-items-center'
      >
        <div className='progress-container'>
          <div
            className='progress progress_animated'
            style={{
              width: `${progressValue}%`,
            }}
          />
        </div>
        <span className='progress-value'>{`${progressValue}%`}</span>
      </div>
    );
  }
}

export default HomeStepProgress;
