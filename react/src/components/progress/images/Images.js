import React from "react";
import axiosAjax from "../../../config/axiosAjax";
import Uploader from "../../shared/Uploader";
import MDSpinner from "react-md-spinner";
import LightAlert from "../../shared/LightAlert";
import "./styles.css";
import { NavLink } from "react-router-dom";

export default class Images extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      file: "",
      uploaded: false,
      uploadAgain: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    window.scrollTo(0, 0);
  }

  _loadImages() {
    axiosAjax
      .get("/progress/image/")
      .then(response => {
        this.setState({
          data: response.data,
        });
      })
      .catch(error => {
        this.setState({
          data: [],
        });
      });
  }

  componentDidMount() {
    this._loadImages();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("photo", this.state.file.uuid);
    axiosAjax({
      method: "post",
      url: "/progress/image/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this._loadImages();
        this.setState({
          uploaded: false,
          uploadAgain: true,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  deletePhoto(id) {
    var r = confirm("Are you sure you want to delete this image?!");
    if (r == true) {
      axiosAjax.delete("/progress/image/" + id).then(response => {
        this._loadImages();
      });
    }
  }

  render() {
    return this.props.progressTab === true ? (
      <div className='content-container animated fadeIn max-width-700'>
        <div className='row'>
          <div className='col d-flex justify-content-between align-items-center'>
            <h6 className='m-0 section-heading'>Recent Images</h6>
          </div>
          <div className='col-auto d-flex justify-content-between align-items-center'>
            <NavLink to='/progress/images'>
              All Images &nbsp; <i className='far fa-angle-right' />
            </NavLink>
          </div>
        </div>
        <div className='images-scroll'>
          {this.state.data !== undefined ? (
            this.state.data.length > 0 ? (
              this.state.data.slice(0, 3).map((image, key) => (
                <div className='item' key={key}>
                  <img
                    className='img-fluid rounded'
                    src={image.photo + "-/autorotate/yes/"}
                  />
                </div>
              ))
            ) : null
          ) : (
            <div className='text-center' style={{ padding: "5%" }}>
              <MDSpinner singleColor='#4a4090' />
            </div>
          )}
        </div>
        <form onSubmit={this.handleSubmit} style={{ marginTop: "1rem" }}>
          <div className='form-row'>
            <div className='col-12 text-center'>
              <Uploader
                id='file'
                name='file'
                imageOnly={true}
                onChange={file => {
                  if (file) {
                    file.progress(info => console.log("uploaded"));
                    file.done(info => this.setState({ uploaded: true }));
                  }
                }}
                onUploadComplete={info =>
                  this.setState({ file: info, uploadAgain: false })
                }
              />
            </div>
            <div className='col-12 text-center'>
              {this.state.uploaded === true && (
                <button className='waves-effect btn enter-button-centered btn-rounded btn-primary'>
                  Click to upload
                </button>
              )}
              {this.state.uploadAgain === true && (
                <p className='m-0'>Click link to upload another image.</p>
              )}
            </div>
          </div>
        </form>
      </div>
    ) : (
      <React.Fragment>
        <div className='container' style={{ marginBottom: "1rem" }}>
          <div className='content-container max-width-500 text-center'>
            <form onSubmit={this.handleSubmit}>
              <div className='form-row'>
                <div className='col-12 text-center'>
                  <Uploader
                    id='file'
                    name='file'
                    imageOnly={true}
                    onChange={file => {
                      if (file) {
                        file.progress(info => console.log("uploaded"));
                        file.done(info => this.setState({ uploaded: true }));
                      }
                    }}
                    onUploadComplete={info =>
                      this.setState({ file: info, uploadAgain: false })
                    }
                  />
                </div>
                <div className='col-12 text-center'>
                  {this.state.uploaded === true && (
                    <button className='waves-effect btn enter-button-centered btn-rounded btn-primary'>
                      Click to upload
                    </button>
                  )}
                  {this.state.uploadAgain === true && (
                    <p className='m-0'>Click link to upload another image.</p>
                  )}
                </div>
              </div>
            </form>
          </div>
        </div>
        {this.state.data !== undefined ? (
          this.state.data.length > 0 ? (
            <div className='container'>
              <div className='card-columns' style={{ marginTop: 30 }}>
                {this.state.data.map((data, key) => (
                  <div key={"image" + key} className='card'>
                    <img
                      className='card-img-top'
                      src={data.photo + "-/autorotate/yes/"}
                    />
                    <div className='card-body'>
                      <div className='row'>
                        <div className='col'>
                          <button
                            className='btn btn-light'
                            onClick={() => this.deletePhoto(data.id)}
                          >
                            <i className='fas fa-trash red' />
                          </button>
                        </div>
                        <div className='col'>
                          <small className='text-muted'>
                            {data.date.substring(0, data.date.indexOf("T"))}
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          ) : (
            <div className='h-100 d-flex flex-column justify-content-center text-center'>
              <LightAlert
                container={true}
                message='Upload your first image above.'
              />
            </div>
          )
        ) : (
          <div className='h-100 d-flex flex-column justify-content-center'>
            <div className='text-center'>
              <MDSpinner singleColor='#4a4090' />
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}
