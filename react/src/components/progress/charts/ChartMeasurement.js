import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";

class ChartMeasurement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      measurement: this.props.measurement,
      date: this.props.date,
      showUpdateButton: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._updateMeasurement = this._updateMeasurement.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _updateMeasurement() {
    if (parseFloat(this.state.measurement) > 0) {
      this.setState({ showUpdateButton: false });
      const formData = new FormData();
      formData.append("measurement", parseFloat(this.state.measurement));
      axiosAjax({
        method: "patch",
        url:
          "/progress/" + this.props.measurementName + "/" + this.state.id + "/",
        headers: { "Content-Type": "multipart/form-data" },
        data: formData,
      })
        .then(response => {
          this.setState({
            showUpdateButton: false,
          });
          this.props.loadChart(this.props.measurementName);
        })
        .catch(error => {
          this.setState({ showUpdateButton: true });
        });
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        id: this.props.id,
        date: this.props.date,
        measurement: this.props.measurement,
        showUpdateButton: false,
      });
    }
    if (this.state.measurement !== prevState.measurement) {
      this.setState({
        showUpdateButton: true,
      });
    }
    if (this.state.id !== prevState.id) {
      this.setState({
        showUpdateButton: false,
      });
    }
  }

  render() {
    return (
      <div
        className='row shadow-sm mb-2 p-2 bg-white'
        style={{ borderRadius: 10, margin: 15 }}
      >
        <div className='col d-flex flex-column justify-content-center text-center'>
          <input
            step='0.00000001'
            type='number'
            name='measurement'
            className='form-control'
            value={this.state.measurement}
            onChange={this.handleInputChange}
          />
        </div>
        {this.state.showUpdateButton === true && (
          <div className='col d-flex flex-column justify-content-center'>
            <button
              onClick={() => this._updateMeasurement()}
              className='btn btn-outline-success'
            >
              <i className='far fa-check' />
            </button>
          </div>
        )}
        <div className='col d-flex flex-column justify-content-center p-xs text-center'>
          <button
            className='btn btn-light'
            onClick={() => this.props.deleteMeasurement(this.state.id)}
          >
            <i className='far fa-times' />
          </button>
        </div>
        <div className='col d-flex flex-column justify-content-center text-center p-xs'>
          {this.state.date}
        </div>
      </div>
    );
  }
}

export default ChartMeasurement;
