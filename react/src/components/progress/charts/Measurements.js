import React from "react";
import { NavLink } from "react-router-dom";

export default class Measurements extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      exercise: false,
      circumference: false,
    };
  }

  render() {
    return (
      <div className='max-width-700'>
        <div className='mb-1 mt-4 d-flex justify-content-between align-items-center'>
          <h6 className='m-0 section-heading'>Measurements</h6>
        </div>
        <div className='animated fadeIn'>
          <div className='rounded-corners bg-white shadow-sm p-3 mt-3'>
            <button
              className='btn btn-link'
              style={{ textDecoration: "none", paddingLeft: 0 }}
              onClick={() => this.setState({ exercise: !this.state.exercise })}
              type='button'
              data-toggle='collapse'
              data-target='#exercise'
              aria-expanded='false'
              aria-controls='exercise'
            >
              {this.state.exercise === true ? (
                <i className='far fa-angle-down' />
              ) : (
                <i className='far fa-angle-right' />
              )}{" "}
              &nbsp; Exercise
            </button>
            <div className='collapse' id='exercise'>
              <div className='list-group list-group-flush'>
                <NavLink
                  className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                  to='/progress/chart/bench'
                >
                  Bench <i className='far fa-chevron-right' />
                </NavLink>
                <NavLink
                  className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                  to='/progress/chart/deadlift'
                >
                  Deadlift <i className='far fa-chevron-right' />
                </NavLink>
                <NavLink
                  className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                  to='/progress/chart/squat'
                >
                  Squat <i className='far fa-chevron-right' />
                </NavLink>
                <NavLink
                  className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                  to='/progress/chart/km'
                >
                  1km <i className='far fa-chevron-right' />
                </NavLink>
              </div>
            </div>
          </div>
          <div className='rounded-corners bg-white shadow-sm p-3 mt-3'>
            <button
              className='btn btn-link'
              style={{ textDecoration: "none", paddingLeft: 0 }}
              onClick={() =>
                this.setState({ circumference: !this.state.circumference })
              }
              type='button'
              data-toggle='collapse'
              data-target='#circumference'
              aria-expanded='false'
              aria-controls='circumference'
            >
              {this.state.circumference === true ? (
                <i className='far fa-angle-down' />
              ) : (
                <i className='far fa-angle-right' />
              )}{" "}
              &nbsp; Circumference
            </button>
            <div className='collapse' id='circumference'>
              <div className='list-group list-group-flush'>
                <NavLink
                  className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                  to='/progress/chart/arm'
                >
                  Arm <i className='far fa-chevron-right' />
                </NavLink>
                <NavLink
                  className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                  to='/progress/chart/chest'
                >
                  Chest <i className='far fa-chevron-right' />
                </NavLink>
                <NavLink
                  className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                  to='/progress/chart/waist'
                >
                  Waist <i className='far fa-chevron-right' />
                </NavLink>
                <NavLink
                  className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                  to='/progress/chart/hip'
                >
                  Hip <i className='far fa-chevron-right' />
                </NavLink>
                <NavLink
                  className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                  to='/progress/chart/thigh'
                >
                  Thigh <i className='far fa-chevron-right' />
                </NavLink>
                <NavLink
                  className='list-group-item list-group-item-action  d-flex justify-content-between align-items-center'
                  to='/progress/chart/calf'
                >
                  Calf <i className='far fa-chevron-right' />
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
