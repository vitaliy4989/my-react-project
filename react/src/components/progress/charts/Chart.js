import React from "react";
import axiosAjax from "../../../config/axiosAjax";
import LineChart from "recharts/es6/chart/LineChart";
import Line from "recharts/es6/cartesian/Line";
import Tooltip from "recharts/es6/component/Tooltip";
import YAxis from "recharts/es6/cartesian/YAxis";
import XAxis from "recharts/es6/cartesian/XAxis";
import ResponsiveContainer from "recharts/es6/component/ResponsiveContainer";
import MDSpinner from "react-md-spinner";
import ChartMeasurement from "./ChartMeasurement";

export default class Chart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name,
      data: undefined,
      addMetric: true,
      showTable: false,
      measurement: "",
      sending: false,
      invalidMeasurement: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleMeasurementChange = this.handleMeasurementChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this._deleteMeasurement = this._deleteMeasurement.bind(this);
    this._loadChart = this._loadChart.bind(this);
    window.scrollTo(0, 0);
  }

  _loadChart(name) {
    axiosAjax
      .get("/progress/" + name)
      .then(response => {
        this.setState({
          data: response.data,
        });
      })
      .catch(error => {
        this.setState({
          data: [],
        });
      });
  }

  componentDidMount() {
    this._loadChart(this.state.name);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ name: nextProps.name });
    this._loadChart(nextProps.name);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleMeasurementChange(evt) {
    if (evt.target.validity.valid !== true) {
      this.setState({
        measurement: "",
        invalidMeasurement: true,
      });
    } else {
      const measurement = evt.target.validity.valid
        ? evt.target.value
        : this.state.measurement;
      this.setState({
        measurement: measurement,
        invalidMeasurement: false,
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.state.invalidMeasurement === true) {
      alert("Please enter only numbers.");
    } else {
      if (parseFloat(this.state.measurement) > 0) {
        this.setState({ sending: true });
        const formData = new FormData();
        formData.append("measurement", parseFloat(this.state.measurement));
        axiosAjax({
          method: "post",
          url: "/progress/" + this.state.name + "/",
          headers: { "Content-Type": "multipart/form-data" },
          data: formData,
        })
          .then(response => {
            this._loadChart(this.state.name);
            this.setState({
              measurement: "",
              sending: false,
            });
          })
          .catch(error => {
            console.log(error);
            this.setState({ sending: false });
          });
      }
    }
  }

  _deleteMeasurement(id) {
    if (confirm("Click OK to delete this measurement.")) {
      axiosAjax
        .delete("/progress/" + this.state.name + "/" + id + "/")
        .then(response => {
          this._loadChart(this.state.name);
        });
    }
  }

  render() {
    if (typeof this.state.data !== "undefined" && this.state.data.length >= 1) {
      var chart_data = [];

      const dataJson = this.state.data.map(data =>
        chart_data.push({
          measurement: data.measurement,
          date: data.date.substring(0, data.date.indexOf("T")),
        }),
      );
    } else {
      var chart_data = [];
    }

    return (
      <React.Fragment>
        {this.state.data !== undefined ? (
          <div className={$(window).width() > 768 ? "container" : ""}>
            {this.state.data.length > 0 ? (
              <div className='content-container-no-shadow'>
                <ResponsiveContainer width='100%' height={250}>
                  <LineChart data={chart_data} margin={{ top: 5, bottom: 5 }}>
                    <YAxis domain={["dataMin", "dataMax"]} />
                    <Tooltip />
                    <XAxis dataKey='date' />
                    <Line
                      type='monotone'
                      dataKey='measurement'
                      name={this.state.name}
                      stroke='#5F47FF'
                      strokeWidth={2}
                      dot={{ strokeWidth: 1 }}
                    />
                  </LineChart>
                </ResponsiveContainer>
              </div>
            ) : null}
            <div className='row'>
              <div className='col-lg-4'>
                <div className='content-container-no-shadow'>
                  <div className='text-center'>
                    <h6 className='m-0'>New Measurement</h6>
                    <p className='p-xs text-muted'>
                      Please enter a number, no units.
                    </p>
                    <form onSubmit={this.handleSubmit} className='form-row'>
                      <div className='col-12'>
                        <input
                          style={
                            this.state.invalidMeasurement === true
                              ? { borderColor: "red" }
                              : {}
                          }
                          autoComplete='off'
                          type='number'
                          step='any'
                          onChange={this.handleMeasurementChange}
                          value={this.state.measurement}
                          className='form-control form-control-single-border text-center'
                          name='measurement'
                          placeholder={
                            "Enter " + this.state.name + " measurement"
                          }
                          required
                        />
                      </div>
                      <div className='col-12'>
                        <button
                          id='formButton'
                          className='waves-effect btn enter-button-centered btn-rounded btn-primary'
                        >
                          {this.state.sending === true ? (
                            <i className='fas fa-circle-notch fa-spin' />
                          ) : (
                            "Enter"
                          )}
                        </button>
                      </div>
                    </form>
                    <br />
                    <div className='row'>
                      <div className='col-12'>
                        <button
                          className='btn btn-link btn-xs'
                          type='button'
                          data-toggle='collapse'
                          data-target='#chartHelp'
                          aria-expanded='false'
                          aria-controls='chartHelp'
                        >
                          How do I do this?
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className='collapse' id='chartHelp'>
                    <p className='text-muted'>
                      <strong>Circumference</strong>
                    </p>
                    <p>
                      For circumference based measurements, you just need a tape
                      measure. You can measure in either centimeters or inches,
                      just be consistent. The sites we have chosen for you are
                      very common sites to measure. An example of how to measure
                      can be found
                      <a
                        href='https://www.dummies.com/health/how-to-get-your-body-measurements/'
                        target='_blank'
                      >
                        {" "}
                        here
                      </a>
                      .
                    </p>
                    <p className='text-muted'>
                      <strong>Exercise</strong>
                    </p>
                    <p>
                      For the lifts, pick a rep range to consistently measure.
                      Each time you measure, measure the weight you can complete
                      for this rep range. For example, on the squat, you may
                      decide to measure your 8 rep performance. Week 1, you may
                      complete 40kg for 8 reps. The next month, you may complete
                      42.5kg for 8 reps.
                    </p>
                  </div>
                </div>
              </div>
              <div className='col-lg-8'>
                <div style={{ marginTop: 30 }}>
                  {this.state.data.length > 0
                    ? this.state.data.map((data, key) => (
                        <ChartMeasurement
                          key={key}
                          id={data.id}
                          loadChart={this._loadChart}
                          measurement={data.measurement}
                          date={data.date.substring(0, data.date.indexOf("T"))}
                          deleteMeasurement={this._deleteMeasurement}
                          measurementName={this.state.name}
                        />
                      ))
                    : null}
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className='h-100 d-flex flex-column justify-content-center'>
            <div className='text-center'>
              <MDSpinner singleColor='#4a4090' />
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}
