import React, { Component } from "react";
import axiosAjax from "../../../../config/axiosAjax";
import { NotificationManager } from "react-notifications";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      achieved: this.props.achieved,
      goal: this.props.goal,
      key: this.props.setKey,
      saveButton: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._updateWeeklyGoal = this._updateWeeklyGoal.bind(this);
  }

  componentDidMount() {
    if (this.state.id === undefined) {
      this._createWeeklyGoal();
    }
  }

  _createWeeklyGoal() {
    const formData = new FormData();
    formData.append("goal", "");
    axiosAjax({
      method: "post",
      url: "/progress/weekly-goal/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          id: response.data.id,
        });
      })
      .catch(error => {
        alert("There was an error, please add the goal again.");
      });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _deleteGoal() {
    if (confirm("Click OK to delete this weekly goal.")) {
      this.props.removeGoal(this.state.key);
      if (this.state.id) {
        axiosAjax
          .delete("/progress/weekly-goal/" + this.state.id + "/")
          .then(response => {
            console.log("goal deleted");
          });
      }
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        id: this.props.id,
        achieved: this.props.achieved,
        goal: this.props.goal,
        key: this.props.setKey,
      });
    }
    if (this.state.goal !== prevState.goal) {
      this.setState({
        saveButton: true,
      });
    }
  }

  _updateWeeklyGoal() {
    if (this.state.id !== undefined) {
      this.setState({ saveButton: false });
      event.preventDefault();
      this.setState({ updated: true });
      const formData = new FormData();
      formData.append("goal", this.state.goal);
      axiosAjax({
        method: "patch",
        url: "/progress/weekly-goal/" + this.state.id + "/",
        headers: { "Content-Type": "multipart/form-data" },
        data: formData,
      })
        .then(response => {
          this.setState({
            goal: response.data.goal,
            achieved: response.data.achieved,
            saveButton: false,
          });
        })
        .catch(error => {
          alert("There was an error updating your goal.");
          this.setState({ saveButton: true });
        });
    }
  }

  render() {
    return (
      <div className='row'>
        <div className='col p-3 mt-3 bg-white shadow-sm rounded d-flex flex-column justify-content-center'>
          <form onSubmit={this._updateWeeklyGoal}>
            <div className='form-row'>
              <div className='col'>
                <input
                  type='text'
                  className='form-control'
                  value={this.state.goal}
                  onChange={this.handleInputChange}
                  name='goal'
                  placeholder='Enter weekly goal...'
                  maxLength='100'
                />
              </div>
              <div className='col-auto d-flex flex-column justify-content-center align-items-end'>
                <button
                  type='button'
                  className='btn btn-light'
                  onClick={() => this._deleteGoal()}
                >
                  <i className='far fa-times' />
                </button>
              </div>
            </div>
            {this.state.saveButton === true && (
              <div className='form-row'>
                <div className='col'>
                  <button className='btn btn-sm btn-outline-success mt-2'>
                    Save
                  </button>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    );
  }
}

export default Index;
