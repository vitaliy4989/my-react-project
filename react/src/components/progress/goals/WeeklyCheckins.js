import React, { Component } from "react";
import Switch from "react-toggle-switch";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import { connect } from "react-redux";
import { getGoal } from "../../../redux/actions/progress";

class WeeklyCheckins extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkIns: undefined,
      enabled: true,
    };
    this._toggleCheckIns = this._toggleCheckIns.bind(this);
  }

  componentDidMount() {
    this.props.onGetGoal();
  }

  static getDerivedStateFromProps(props, state) {
    if (state.checkIns !== props.checkIns) {
      return {
        checkIns: props.checkIns,
      };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.checkIns !== this.props.checkIns) {
      this.setState({
        checkIns: this.props.checkIns,
      });
    }
  }

  _toggleCheckIns(event) {
    const checkIns = !this.state.checkIns;
    this.setState({
      enabled: false,
    });
    event.preventDefault();
    const formData = new FormData();
    formData.append("check_ins", checkIns);
    axiosAjax({
      method: "patch",
      url: "/progress/goal/" + undefined + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    }).then(response => {
      this.props.onGetGoal();
      this.setState({
        enabled: true,
      });
    });
  }

  render() {
    return (
      <div className='content-container animated fadeIn max-width-700'>
        {this.props.checkIns !== undefined || this.props.goal !== undefined ? (
          <React.Fragment>
            <div className='row'>
              <div className='col-auto'>
                <h6 className='section-heading'>Check-Ins</h6>
              </div>
              {this.props.goal !== "" && (
                <div className='col'>
                  <Switch
                    className='float-right'
                    onClick={this._toggleCheckIns}
                    on={this.state.checkIns}
                    enabled={this.state.enabled}
                  />
                </div>
              )}
            </div>
            {this.props.goal === "" ? (
              <p className='m-0'>Set your goal to receive check-ins.</p>
            ) : (
              <p className='m-0 text-muted'>
                We {this.state.checkIns === true ? "will" : "will not"} check-in
                with you if we do not hear from you in a week.
              </p>
            )}
          </React.Fragment>
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "10%", paddingBottom: "10%" }}
          >
            <MDSpinner
              color1='#4a4090'
              color2='#4a4090'
              color3='#4a4090'
              color4='#4a4090'
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    checkIns: state.progress.checkIns,
    goal: state.progress.goal,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetGoal: () => {
      dispatch(getGoal());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WeeklyCheckins);
