import React, { Component } from "react";
import axiosAjax from "../../../../config/axiosAjax";
import WeeklyGoal from "../WeeklyGoal/index";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weeklyGoals: undefined,
    };
    this._removeGoal = this._removeGoal.bind(this);
  }

  componentDidMount() {
    this._getWeeklyGoals();
  }

  _getWeeklyGoals() {
    axiosAjax
      .get("/progress/weekly-goal/")
      .then(response => {
        this.setState({
          weeklyGoals: response.data,
        });
      })
      .catch(error => {
        this.setState({
          weeklyGoals: [],
        });
      });
  }

  _newGoal() {
    console.log("hello");
    var key = this.state.weeklyGoals.length - 1;
    this.state.weeklyGoals.push(
      <WeeklyGoal removeGoal={this._removeGoal} setKey={key} key={key} />,
    );
    this.setState({
      weeklyGoals: this.state.weeklyGoals,
    });
  }

  _removeGoal(id) {
    delete this.state.weeklyGoals[id];
    this.setState({
      sets: this.state.weeklyGoals,
    });
  }

  render() {
    return (
      <div>
        <button
          className='btn btn-link btn-block'
          type='button'
          data-toggle='collapse'
          data-target='#weeklyGoals'
          aria-expanded='false'
          aria-controls='collapseExample'
        >
          Break your goal into weekly goals.
        </button>
        <div className='collapse' id='weeklyGoals'>
          <div className='row'>
            <div
              className='col p-3 mt-3 bg-white rounded d-flex flex-column justify-content-center text-center'
              onClick={() => this._newGoal()}
            >
              <button className='btn btn-primary btn-sm btn-rounded'>
                New Weekly Goal
              </button>
            </div>
          </div>
          {this.state.weeklyGoals !== undefined
            ? this.state.weeklyGoals.length > 0
              ? this.state.weeklyGoals.map((goal, key) => (
                  <WeeklyGoal
                    key={key}
                    setKey={key}
                    id={goal.id}
                    goal={goal.goal}
                    achieved={goal.achieved}
                    date={goal.date}
                    removeGoal={this._removeGoal}
                  />
                ))
              : null
            : null}
        </div>
      </div>
    );
  }
}

export default Index;
