import React, { Component } from "react";
import { connect } from "react-redux";
import { getGoal, updateGoal } from "../../../redux/actions/progress";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import { NotificationManager } from "react-notifications";
import WeeklyGoals from "./WeeklyGoals/index";

class MainGoal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      goal: this.props.goal,
      checkIn: undefined,
      goalCategory: this.props.goalCategory,
      updated: false,
      updateButton: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._createGoal = this._createGoal.bind(this);
    this._updateGoal = this._updateGoal.bind(this);
    window.scrollTo(0, 0);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  componentDidMount() {
    this.props.onGetGoal();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.goal !== this.props.goal) {
      this.setState(
        {
          goal: this.props.goal,
        },
        () =>
          this.setState({
            updateButton: false,
          }),
      );
    }
    if (
      prevState.goal !== this.state.goal ||
      prevState.goalCategory !== this.state.goalCategory
    ) {
      this.setState({
        updateButton: true,
      });
    }
    if (prevProps.checkIn !== this.props.checkIn) {
      this.setState({
        checkIn: this.props.checkIn,
      });
    }
    if (prevProps.goalCategory !== this.props.goalCategory) {
      this.setState(
        {
          goalCategory: this.props.goalCategory,
        },
        () =>
          this.setState({
            updateButton: false,
          }),
      );
    }
  }

  _updateGoal(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("goal", this.state.goal);
    if (this.props.checkIns !== undefined) {
      formData.append("check_ins", this.props.checkIns);
    }
    formData.append("goal_category", this.state.goalCategory);
    axiosAjax({
      method: "patch",
      url: "/progress/goal/" + undefined + "/",
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Goal updated.");
        //this.props.onUpdateGoal(response.data.goal, response.data.check_ins, response.data.goal_category);
        this.setState({ updated: true });
      })
      .catch(error => {
        console.log(error);
      });
  }

  _createGoal(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("goal", this.state.goal);
    if (this.props.checkIns !== undefined) {
      formData.append("check_ins", this.props.checkIns);
    }
    formData.append("goal_category", this.state.goalCategory);
    axiosAjax({
      method: "post",
      url: "/progress/goal/",
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Goal set.");
        this.props.onUpdateGoal(
          response.data.goal,
          response.data.check_ins,
          response.data.goal_category,
        );
        this.setState({ updated: true });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className='content-container animated fadeIn max-width-700'>
        <h5 className='text-center'>Your Goal</h5>
        {this.props.goal !== undefined ? (
          <React.Fragment>
            <div className='row'>
              <div className='col-12 text-center'>
                <form
                  onSubmit={
                    this.props.goal !== "" ? this._updateGoal : this._createGoal
                  }
                >
                  <div className='form-group'>
                    <label htmlFor='goal'>In General</label>
                    <br />
                    <select
                      name='goalCategory'
                      id='goalCategory'
                      value={this.state.goalCategory}
                      onChange={this.handleInputChange}
                      className='custom-select form-control-single-border text-center'
                      style={{ maxWidth: 300 }}
                      required
                    >
                      <option value='' disabled selected>
                        Select General Goal
                      </option>
                      <option value='Build Muscle'>Build Muscle</option>
                      <option value='Lose Fat'>Lose Fat</option>
                      <option value='Improve Strength'>Improve Strength</option>
                      <option value='Improve CV'>Improve CV</option>
                      <option value='General Health'>
                        Improve Overall Health
                      </option>
                      <option value='Other'>Other</option>
                    </select>
                  </div>
                  <div className='form-group'>
                    <label htmlFor='goal'>
                      In Detail <br />
                      <small>(250 characters limit)</small>
                    </label>
                    <textarea
                      maxLength='250'
                      autoComplete='off'
                      onChange={this.handleInputChange}
                      value={this.state.goal}
                      name='goal'
                      className='form-control form-control-single-border text-center'
                      placeholder='Write your general goal here in more detail.'
                      required
                    />
                  </div>
                  {this.state.updateButton === true && (
                    <div className='form-group'>
                      <button
                        id='formButton'
                        className='enter-button-centered btn-rounded waves-effect btn btn-primary'
                      >
                        {this.props.goal === "" ? "Enter" : "Update Goal"}
                      </button>
                    </div>
                  )}
                </form>
              </div>
            </div>
            {this.props.goal !== "" && <WeeklyGoals />}
          </React.Fragment>
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "10%", paddingBottom: "10%" }}
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    goal: state.progress.goal,
    checkIns: state.progress.checkIns,
    goalCategory: state.progress.goalCategory,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetGoal: () => {
      dispatch(getGoal());
    },
    onUpdateGoal: () => {
      dispatch(updateGoal(goal, checkIns, goalCategory));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainGoal);
