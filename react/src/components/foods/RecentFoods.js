import React from "react";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import CreateMeal from "../nutrition/logs/CreateMeal";

export default class RecentFoods extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      foods: undefined,
    };
  }

  componentDidMount() {
    axiosAjax
      .get("/nutrition-tracker/food-log-meal")
      .then(response => {
        this.setState({
          foods: response.data,
        });
      })
      .catch(error => {
        this.setState({
          foods: [],
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        {this.state.foods !== undefined ? (
          this.state.foods.length > 0 ? (
            <div style={searchBox} className='list-group list-group-flush'>
              {this.state.foods.map((food, key) => (
                <div
                  key={key}
                  className='list-group-item d-flex justify-content-between align-items-center'
                >
                  <div>{food.name}</div>
                  {food.food !== null && (
                    <CreateMeal
                      dayId={this.props.dayId}
                      foodId={food.food.id}
                      name={food.food.name}
                      protein={food.food.protein}
                      calories={food.food.calories}
                    />
                  )}
                  {food.recipe !== null && (
                    <CreateMeal
                      dayId={this.props.dayId}
                      recipeId={food.recipe.id}
                      name={food.recipe.name}
                      protein={food.recipe.protein}
                      calories={food.recipe.calories}
                    />
                  )}
                </div>
              ))}
            </div>
          ) : (
            <div className='alert alert-light text-center'>
              You have no recently added foods.
            </div>
          )
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "5%", paddingBottom: "5%" }}
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const searchBox = {
  maxHeight: "100vh",
  overflowX: "hidden",
  overflowY: "scroll",
};
