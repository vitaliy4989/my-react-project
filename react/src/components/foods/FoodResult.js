import React, { Component } from "react";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import SaveToDay from "./SaveToDay";

class FoodResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      moreInfo: false,
      food: undefined,
      protein: "",
      calories: "",
      carbs: "",
      fat: "",
      calories: "",
      altMeasurements: [],
      servingQty: 0,
      servingUnit: "",
      servingWeightGrams: 0,
      proteinPerGram: 0,
      caloriesPerGram: 0,
      fatPerGram: 0,
      carbsPerGram: 0,
      gramsPerServing: 0,
      photo: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleQtyChange = this.handleQtyChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleQtyChange(evt) {
    const servingQty = evt.target.validity.valid
      ? evt.target.value
      : this.state.servingQty;
    this.setState({ servingQty: servingQty });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.servingUnit !== this.state.servingUnit) {
      this.state.altMeasurements.map(measurement => {
        if (measurement.measure === this.state.servingUnit) {
          var gramsPerServing = measurement.serving_weight / measurement.qty;
          this.setState({
            servingQty: measurement.qty,
            gramsPerServing: gramsPerServing,
            protein: Math.round(
              gramsPerServing * measurement.qty * this.state.proteinPerGram,
            ),
            calories: Math.round(
              gramsPerServing * measurement.qty * this.state.caloriesPerGram,
            ),
            carbs: Math.round(
              gramsPerServing * measurement.qty * this.state.carbsPerGram,
            ),
            fat: Math.round(
              gramsPerServing * measurement.qty * this.state.fatPerGram,
            ),
          });
        }
      });
    }
    if (prevState.servingQty !== this.state.servingQty) {
      this.setState({
        protein: Math.round(
          this.state.gramsPerServing *
            this.state.servingQty *
            this.state.proteinPerGram,
        ),
        calories: Math.round(
          this.state.gramsPerServing *
            this.state.servingQty *
            this.state.caloriesPerGram,
        ),
        carbs: Math.round(
          this.state.gramsPerServing *
            this.state.servingQty *
            this.state.carbsPerGram,
        ),
        fat: Math.round(
          this.state.gramsPerServing *
            this.state.servingQty *
            this.state.fatPerGram,
        ),
      });
    }
  }

  _searchNutriItem() {
    this.setState({ moreInfo: true });
    axiosAjax({
      method: "post",
      url: "https://trackapi.nutritionix.com/v2/natural/nutrients",
      headers: {
        "Content-Type": "application/json",
        "x-app-id": "af45f585",
        "x-app-key": "2bbde1f34284274382b24d9b5aa6ccf9",
        "x-remote-user-id": sessionStorage.username,
      },
      data: {
        query: this.props.food.food_name,
      },
    })
      .then(response => {
        this.setState({
          name: response.data.foods[0].food_name,
          food: response.data.foods[0],
          proteinPerGram:
            response.data.foods[0].nf_protein /
            response.data.foods[0].serving_weight_grams,
          caloriesPerGram:
            response.data.foods[0].nf_calories /
            response.data.foods[0].serving_weight_grams,
          carbsPerGram:
            response.data.foods[0].nf_total_carbohydrate /
            response.data.foods[0].serving_weight_grams,
          fatPerGram:
            response.data.foods[0].nf_total_fat /
            response.data.foods[0].serving_weight_grams,
          gramsPerServing:
            response.data.foods[0].serving_weight_grams /
            response.data.foods[0].serving_qty,
          altMeasurements: response.data.foods[0].alt_measures
            ? response.data.foods[0].alt_measures
            : [],
          servingQty: response.data.foods[0].serving_qty,
          servingUnit: response.data.foods[0].serving_unit,
          servingWeightGrams: response.data.foods[0].serving_weight_grams,
          photo: response.data.foods[0].photo.thumb,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className='list-group-item'>
        <div className='row d-flex justify-content-between align-items-center'>
          {this.state.moreInfo === true ? (
            <React.Fragment>
              <div
                className='col'
                style={{ cursor: "pointer" }}
                onClick={() => this.setState({ moreInfo: false })}
              >
                <div
                  className='row d-flex justify-content-between align-items-center'
                  style={{ overflow: "hidden" }}
                >
                  <div className='col-auto'>
                    <img
                      src={this.props.food.photo.thumb}
                      height='25'
                      width='25'
                      className='rounded'
                    />
                  </div>
                  <div className='col'>
                    <p className='text-capitalize m-0 p-0'>
                      {this.props.food.food_name}
                    </p>
                  </div>
                </div>
              </div>
              <div className='col-auto d-flex justify-content-between align-items-center'>
                <button
                  onClick={() => this.setState({ moreInfo: false })}
                  className='btn btn-sm btn-rounded btn-light'
                >
                  <i className='fas fa-angle-down' />
                </button>
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div
                className='col'
                style={{ cursor: "pointer" }}
                onClick={() => this._searchNutriItem()}
              >
                <div
                  className='row d-flex justify-content-between align-items-center'
                  style={{ overflow: "hidden" }}
                >
                  <div className='col-auto'>
                    <img
                      src={this.props.food.photo.thumb}
                      height='25'
                      width='25'
                      className='rounded'
                    />
                  </div>
                  <div className='col'>
                    <p className='text-capitalize m-0 p-0'>
                      {this.props.food.food_name}
                    </p>
                  </div>
                </div>
              </div>
              <div className='col-auto d-flex justify-content-between align-items-center'>
                <button
                  onClick={() => this._searchNutriItem()}
                  className='btn btn-sm btn-rounded btn-light'
                >
                  <i className='fas fa-angle-left' />
                </button>
              </div>
            </React.Fragment>
          )}
        </div>
        {this.state.moreInfo === true && (
          <React.Fragment>
            {this.state.food !== undefined ? (
              <div style={moreInfo}>
                <div className='row'>
                  <div className='col-12'>
                    <div className='row' style={nutritionalInfo}>
                      <div className='col d-flex justify-content-between align-items-center'>
                        <p className='lead m-0'>Total Calories</p>
                        <p className='green lead m-0'>{this.state.calories}</p>
                      </div>
                    </div>
                    <div className='row d-flex justify-content-between align-items-center'>
                      <div className='col-4 text-center' style={macros}>
                        <p className='m-0'>
                          <span className='green'>{this.state.protein}g</span>{" "}
                          Pro
                        </p>
                      </div>
                      <div className='col-4 text-center' style={macros}>
                        <p className='m-0'>
                          <span className='green'>{this.state.carbs}g</span>{" "}
                          Carb
                        </p>
                      </div>
                      <div className='col-4 text-center' style={macros}>
                        <p className='m-0'>
                          <span className='green'>{this.state.fat}g</span> Fat
                        </p>
                      </div>
                    </div>
                    <div className='row d-flex justify-content-between align-items-center'>
                      <div className='col'>
                        <p className='m-0 text-muted' style={{ fontSize: 10 }}>
                          {Math.round(
                            this.state.gramsPerServing * this.state.servingQty,
                          )}{" "}
                          grams
                        </p>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-6'>
                        <label htmlFor='servingUnit'>Change units</label>
                        <select
                          className='custom-select form-control-sm'
                          onChange={this.handleInputChange}
                          name='servingUnit'
                        >
                          {this.state.altMeasurements.length > 0
                            ? this.state.altMeasurements.map(
                                (measurement, key) => (
                                  <option value={measurement.measure} key={key}>
                                    {measurement.measure}
                                  </option>
                                ),
                              )
                            : null}
                        </select>
                      </div>
                      <div className='col-6'>
                        <label htmlFor='servingQty'>Change quantity</label>
                        <input
                          className='form-control  form-control-sm'
                          type='number'
                          step='any'
                          value={this.state.servingQty}
                          onChange={this.handleQtyChange}
                          name='servingQty'
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col'>
                    <SaveToDay
                      foodResult={true}
                      dayId={this.props.dayId}
                      name={this.state.name}
                      calories={this.state.calories}
                      protein={this.state.protein}
                      fat={this.state.fat}
                      carbs={this.state.carbs}
                      serving={{
                        qty: this.state.servingQty,
                        unit: this.state.servingUnit,
                        grams: Math.round(
                          this.state.gramsPerServing * this.state.servingQty,
                        ),
                      }}
                    />
                  </div>
                </div>
              </div>
            ) : (
              <div
                className='text-center'
                style={{ paddingTop: "10%", paddingBottom: "10%" }}
              >
                <MDSpinner singleColor='#4a4090' />
              </div>
            )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const macros = {
  fontSize: 10,
  margin: 0,
  border: "solid 1px #EFF0F2",
  padding: 5,
};

const nutritionalInfo = {
  border: "solid 1px #EFF0F2",
  padding: 5,
};

const moreInfo = {
  marginBottom: "1rem",
  marginTop: "1rem",
};

export default FoodResult;
