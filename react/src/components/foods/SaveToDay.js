import React, { Component } from "react";
import axiosAjax from "../../config/axiosAjax";
import { connect } from "react-redux";
import { getDay, getMeals } from "../../redux/actions/nutrition";

class SaveToDay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      added: false,
      sending: false,
    };
  }

  addMealToDay() {
    const formData = new FormData();
    this.setState({ sending: true, added: true });
    formData.append("day_id", this.props.dayId);
    formData.append("name", this.props.name);
    formData.append("protein", this.props.protein);
    formData.append("carbs", this.props.carbs);
    formData.append("fat", this.props.fat);
    formData.append("serving_qty", this.props.serving.qty);
    formData.append("serving_unit", this.props.serving.unit);
    formData.append("serving_grams", this.props.serving.grams);
    formData.append("calories", this.props.calories);
    axiosAjax({
      method: "post",
      url: "/nutrition-tracker/nutritionix-meal/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          added: true,
          sending: false,
        });
        this.props.onGetDay(this.props.dayId);
        this.props.onGetMeals(this.props.dayId);
      })
      .catch(error => {
        console.log(error);
        this.setState({ sending: false, added: false });
      });
  }

  render() {
    return this.props.foodResult === true ? (
      this.state.added === true ? (
        <div style={{ marginTop: "1rem" }}>
          <p className='green'>Added! You can edit this food on the log. </p>
          <button
            onClick={() => this.addMealToDay()}
            className='btn btn-secondary'
          >
            {this.state.sending === true ? (
              <i className='fas fa-circle-notch fa-spin' />
            ) : (
              "Add again?"
            )}
          </button>
        </div>
      ) : (
        <div style={{ marginTop: "1rem" }}>
          <button
            onClick={() => this.addMealToDay()}
            className='btn btn-secondary'
          >
            {this.state.sending === true ? (
              <i className='fas fa-circle-notch fa-spin' />
            ) : (
              "Add to log"
            )}
          </button>
        </div>
      )
    ) : this.state.added === true ? (
      <button className='btn float-right btn-round btn-active animated pulse'>
        <i className='fas fa-check white' />
      </button>
    ) : (
      <button
        onClick={() => this.addMealToDay()}
        className='btn btn-light float-right btn-round'
      >
        {this.state.sending === true ? (
          <i className='fas fa-circle-notch fa-spin' />
        ) : (
          <i className='fas fa-plus' />
        )}
      </button>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    day: state.nutrition.day,
    meals: state.nutrition.meals,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetDay: dayId => {
      dispatch(getDay(dayId));
    },
    onGetMeals: dayId => {
      dispatch(getMeals(dayId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SaveToDay);
