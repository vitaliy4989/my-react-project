import React from "react";
import CreateFood from "./CreateFood";
import axiosAjax from "../../config/axiosAjax";
import Food from "./Food";
import FoodResult from "./FoodResult";
import MDSpinner from "react-md-spinner";
import RecentFoods from "./RecentFoods";

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      foods: undefined,
      branded: undefined,
      common: undefined,
      create: false,
      searching: false,
      startSearch: false,
      searchForFoods: true,
      recentlyAddedFoods: false,
      searchIcon: <i className='fas dark-purp fa-search' />,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._searchNutri = this._searchNutri.bind(this);
    this._hideCreateFood = this._hideCreateFood.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _searchNutri(event) {
    event.preventDefault();
    this.setState({ searching: true, startSearch: true });
    axiosAjax({
      method: "get",
      url: "https://trackapi.nutritionix.com/v2/search/instant",
      headers: {
        "Content-Type": "application/json",
        "x-app-id": "af45f585",
        "x-app-key": "2bbde1f34284274382b24d9b5aa6ccf9",
        "x-remote-user-id": sessionStorage.username,
      },
      params: {
        query: this.state.search,
        self: false,
      },
    })
      .then(response => {
        this.setState({
          searching: false,
          branded: response.data.branded,
          common: response.data.common,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({ searching: false });
      });

    this.setState({ searching: true });
    axiosAjax
      .get("/foods/food", {
        params: {
          search: this.state.search,
        },
      })
      .then(response => {
        this.setState({
          foods: response.data,
          searching: false,
        });
      })
      .catch(error => {
        this.setState({
          searching: false,
        });
      });
  }

  _hideCreateFood(event) {
    this.setState({
      create: false,
    });
  }

  render() {
    return (
      <div
        className='modal fade'
        id='selectFoodModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='selectFoodModal'
        aria-hidden='true'
      >
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <button
                type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Close'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <div
                className='option-header-wrapper'
                style={{ marginBottom: "1rem" }}
              >
                <div
                  className={
                    this.state.searchForFoods === true
                      ? "option-header option-header-active"
                      : "option-header"
                  }
                  onClick={() =>
                    this.setState({
                      searchForFoods: true,
                      recentlyAddedFoods: false,
                      create: false,
                    })
                  }
                >
                  Search
                </div>
                <div
                  className={
                    this.state.recentlyAddedFoods === true
                      ? "option-header option-header-active"
                      : "option-header"
                  }
                  onClick={() =>
                    this.setState({
                      recentlyAddedFoods: true,
                      searchForFoods: false,
                      create: false,
                    })
                  }
                >
                  Recent
                </div>
              </div>
              {this.state.searchForFoods === true && (
                <div className='row'>
                  <div className='col'>
                    <form onSubmit={this._searchNutri}>
                      <div className='form-row'>
                        <div className='col-8'>
                          <input
                            autoFocus='autofocus'
                            onChange={this.handleInputChange}
                            value={this.state.search}
                            name='search'
                            type='text'
                            className='form-control'
                            placeholder='Search foods...'
                          />
                        </div>
                        <div className='col-4'>
                          <button className='btn btn-block btn-primary'>
                            <i className='fas fa-search' />
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              )}
              {this.state.recentlyAddedFoods === true && (
                <RecentFoods dayId={this.props.id} />
              )}
              {this.state.recentlyAddedFoods === false && (
                <div
                  className='text-center'
                  style={{ marginTop: "1rem", marginBottom: "1rem" }}
                >
                  <button
                    className='waves-effect btn btn-rounded btn-sm btn-light'
                    onClick={() =>
                      this.setState({
                        create: !this.state.create,
                        search: "",
                        startSearch: false,
                      })
                    }
                  >
                    {this.state.create === true ? "Cancel" : "Food not there?"}
                  </button>
                </div>
              )}
              {this.state.create === true && (
                <CreateFood
                  dayId={this.props.id}
                  hideCreateFood={this._hideCreateFood}
                />
              )}
              {this.state.startSearch === true &&
              this.state.recentlyAddedFoods === false ? (
                this.state.searching === true ? (
                  <div
                    className='text-center'
                    style={{ paddingTop: "5%", paddingBottom: "5%" }}
                  >
                    <MDSpinner singleColor='#4a4090' />
                  </div>
                ) : (
                  <div
                    style={searchBox}
                    className='list-group list-group-flush'
                  >
                    {this.state.common !== undefined && (
                      <React.Fragment>
                        <div className='list-group-item'>
                          <div className='d-flex justify-content-between align-items-center'>
                            <div>
                              <h6 className='m-0'>Common Foods</h6>
                              <p
                                style={{ fontSize: 10 }}
                                className='text-muted p-smaller m-0'
                              >
                                Powered by Nutritionix{" "}
                                <i className='fab fa-nutritionix' />
                              </p>
                            </div>
                          </div>
                        </div>
                        {this.state.common.map((food, key) => (
                          <FoodResult
                            dayId={this.props.id}
                            key={key}
                            food={food}
                          />
                        ))}
                      </React.Fragment>
                    )}
                    {this.state.branded !== undefined && (
                      <React.Fragment>
                        <div className='list-group-item'>
                          <div className='d-flex justify-content-between align-items-center'>
                            <div>
                              <h6 className='m-0'>Branded Foods</h6>
                              <p
                                style={{ fontSize: 10 }}
                                className='text-muted p-smaller m-0'
                              >
                                Powered by Nutritionix{" "}
                                <i className='fab fa-nutritionix' />
                              </p>
                            </div>
                          </div>
                        </div>
                        {this.state.branded.map((food, key) => (
                          <FoodResult
                            dayId={this.props.id}
                            key={key}
                            food={food}
                          />
                        ))}
                      </React.Fragment>
                    )}
                    {this.state.foods !== undefined && (
                      <React.Fragment>
                        <div className='list-group-item'>
                          <div className='d-flex justify-content-between align-items-center'>
                            <div>
                              <h6 className='m-0'>Member Added</h6>
                              <p
                                style={{ fontSize: 10 }}
                                className='text-muted p-smaller m-0'
                              >
                                Added by members
                              </p>
                            </div>
                          </div>
                        </div>
                        {this.state.foods.map((food, key) => (
                          <Food dayId={this.props.id} key={key} food={food} />
                        ))}
                      </React.Fragment>
                    )}
                  </div>
                )
              ) : null}
            </div>
            <div className='modal-footer'>
              <button
                type='button'
                className='btn btn-light'
                data-dismiss='modal'
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const searchBox = {
  minHeight: "100vh",
  overflowX: "hidden",
  overflowY: "scroll",
};

const crossIcon = {
  cursor: "pointer",
};

const searchIcon = {
  border: "none",
};
