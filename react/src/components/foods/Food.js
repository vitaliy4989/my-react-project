import React from "react";
import CreateMeal from "../nutrition/logs/CreateMeal";

class Food extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showFood: false,
      protein: this.props.food.protein,
      calories: this.props.food.calories,
      carbs: this.props.food.carbs,
      fat: this.props.food.fat,
      name: "",
      servingQty: 1,
      servingSize: this.props.food.serving_size,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.food.id !== this.props.food.id) {
      this.setState({
        protein: this.props.food.protein,
        carbs: this.props.food.carbs,
        calories: this.props.food.calories,
        fat: this.props.food.fat,
        servingQty: 1,
        servingSize: this.props.food.serving_size,
      });
    }
  }

  render() {
    return (
      <div className='list-group-item'>
        <div
          onClick={() => this.setState({ showFood: !this.state.showFood })}
          style={{ cursor: "pointer" }}
          className='row d-flex justify-content-between align-items-center'
        >
          <div className='col' style={{ overflow: "hidden" }}>
            <p
              className='text-lowercase m-0'
              style={{ wordBreak: "break-all" }}
            >
              {this.props.food.name.replace(/,/g, ", ")}
            </p>
          </div>
          <div className='col-auto'>
            <button className='btn btn-sm btn-rounded btn-light'>
              {this.state.showFood === true ? (
                <i className='fas fa-angle-down' />
              ) : (
                <i className='fas fa-angle-left' />
              )}
            </button>
          </div>
        </div>
        {this.state.showFood === true && (
          <div style={moreInfo}>
            <div className='row' style={nutritionalInfo}>
              <div className='col d-flex justify-content-between align-items-center'>
                <p className='lead m-0'>Total Calories</p>
                <p className='green lead m-0'>{this.state.calories}</p>
              </div>
            </div>
            <div className='row d-flex justify-content-between align-items-center'>
              <div className='col-4 text-center' style={macros}>
                <p className='m-0'>
                  <span className='green'>{this.state.protein}g</span> Pro
                </p>
              </div>
              <div className='col-4 text-center' style={macros}>
                <p className='m-0'>
                  <span className='green'>{this.state.carbs}g</span> Carb
                </p>
              </div>
              <div className='col-4 text-center' style={macros}>
                <p className='m-0'>
                  <span className='green'>{this.state.fat}g</span> Fat
                </p>
              </div>
            </div>
            <div className='row' style={{ marginTop: "1rem" }}>
              <div className='col-auto'>
                <p className='text-muted p-smaller m-0'>
                  <strong>{this.state.servingSize}</strong> serving size
                </p>
              </div>
              <div className='col-auto'>
                <p
                  className='text-muted p-smaller m-0'
                  style={{ wordBreak: "break-word" }}
                >
                  Source: {this.props.food.source}
                </p>
              </div>
            </div>
            <div className='row' style={{ marginTop: "1rem" }}>
              <div className='col'>
                <p style={{ fontSize: 10 }} className='text-muted'>
                  ** You can edit 'member added' food servings after adding to
                  your log.
                </p>
              </div>
            </div>
            <CreateMeal
              foodResult={true}
              dayId={this.props.dayId}
              foodId={this.props.food.id}
              name={this.props.food.name}
              protein={this.props.food.protein}
              calories={this.props.food.calories}
            />
          </div>
        )}
      </div>
    );
  }
}

const macros = {
  fontSize: 10,
  margin: 0,
  border: "solid 1px #EFF0F2",
  padding: 5,
};

const nutritionalInfo = {
  border: "solid 1px #EFF0F2",
  padding: 5,
};

const moreInfo = {
  marginBottom: "1rem",
  marginTop: "1rem",
};

const food = {
  padding: 10,
};

export default Food;
