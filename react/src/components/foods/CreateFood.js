import React from "react";
import axiosAjax from "../../config/axiosAjax";
import { NotificationManager } from "react-notifications";
import { connect } from "react-redux";
import { getDay, getMeals } from "../../redux/actions/nutrition";

class CreateFood extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      calories: "",
      protein: "",
      carbs: "",
      fat: "",
      foodType: "",
      serving: "",
      source: "",
      message: "",
      sending: false,
      foodId: undefined,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createFoodItem = this.createFoodItem.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _addFoodToLog() {
    const formData = new FormData();
    formData.append("day_id", this.props.dayId);
    formData.append("food_id", this.state.foodId);
    formData.append("name", this.state.name);
    formData.append("protein", this.state.protein);
    formData.append("calories", this.state.calories);
    axiosAjax({
      method: "post",
      url: "/nutrition-tracker/food-log-meal/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetDay(this.props.dayId);
        this.props.onGetMeals(this.props.dayId);
        this.props.hideCreateFood();
      })
      .catch(error => {
        console.log(error);
      });
  }

  createFoodItem(event) {
    event.preventDefault();
    this.setState({ sending: true });
    const formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("calories", this.state.calories);
    formData.append("protein", this.state.protein);
    formData.append("carbs", this.state.carbs);
    formData.append("fat", this.state.fat);
    formData.append("food_type", this.state.foodType);
    formData.append("serving_size", this.state.serving);
    formData.append("source", this.state.source);
    axiosAjax({
      method: "post",
      url: "/foods/food/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        NotificationManager.success(
          "Food item saved to your log and saved to our records.",
          "",
          10000,
        );
        this.setState(
          {
            foodId: response.data.id,
          },
          () => {
            this._addFoodToLog();
          },
        );
      })
      .catch(error => {
        console.log(error);
      });
    this.setState({ sending: false });
  }

  render() {
    return (
      <div
        className='row'
        style={{
          marginBottom: "1rem",
          borderBottom: "1px solid rgb(238, 241, 242)",
          borderTop: "1px solid rgb(238, 241, 242)",
          paddingTop: 15,
          paddingBottom: 25,
        }}
      >
        <div className='col-12'>
          <h6>Create food</h6>
          <form onSubmit={this.createFoodItem}>
            <div className='form-row'>
              <div className='col-12'>
                <label>Food/drink name</label>
                <input
                  type='text'
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  className='form-control '
                  id='name'
                  name='name'
                  value={this.state.name}
                  placeholder='Name'
                  required
                />
              </div>
              <div className='col-12'>
                <label>Serving size</label>
                <input
                  type='text'
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  className='form-control '
                  id='serving'
                  name='serving'
                  value={this.state.serving}
                  placeholder='Example: 100g, 1 cup, 1 tbs'
                  required
                />
              </div>
              <div className='col-12'>
                <label>Food type</label>
                <input
                  type='text'
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  className='form-control '
                  id='foodType'
                  name='foodType'
                  value={this.state.foodType}
                  placeholder='Example: Meat'
                  required
                />
              </div>
              <div className='col-12'>
                <label>
                  Protein per{" "}
                  {this.state.serving ? this.state.serving : "serving"}
                </label>
                <input
                  type='number'
                  step='0.00000001'
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  className='form-control '
                  id='protein'
                  name='protein'
                  value={this.state.protein}
                  placeholder='Protein'
                  required
                />
              </div>
              <div className='col-12'>
                <label>
                  Carbs per{" "}
                  {this.state.serving ? this.state.serving : "serving"}
                </label>
                <input
                  type='number'
                  step='0.00000001'
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  className='form-control '
                  id='carbs'
                  name='carbs'
                  value={this.state.carbs}
                  placeholder='Carbs'
                  required
                />
              </div>
              <div className='col-12'>
                <label>
                  Fat per {this.state.serving ? this.state.serving : "serving"}
                </label>
                <input
                  type='number'
                  step='0.00000001'
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  className='form-control '
                  id='fat'
                  name='fat'
                  value={this.state.fat}
                  placeholder='Fat'
                  required
                />
              </div>
              <div className='col-12'>
                <label>
                  Calories per{" "}
                  {this.state.serving ? this.state.serving : "serving"}
                </label>
                <input
                  type='number'
                  step='0.00000001'
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  className='form-control '
                  id='calories'
                  name='calories'
                  value={this.state.calories}
                  placeholder='Calories'
                  required
                />
              </div>
              <div className='col-12'>
                <label>Source (yourself, website, etc)</label>
                <input
                  type='text'
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  className='form-control '
                  id='source'
                  name='source'
                  value={this.state.source}
                  placeholder='Source'
                  required
                />
              </div>
            </div>
            <br />
            <div className='form-row'>
              <div className='col-12'>
                <button
                  type='submit'
                  id='formButton'
                  className='waves-effect btn btn-block btn-primary'
                >
                  {this.state.sending === true ? (
                    <i className='fas fa-circle-notch fa-spin' />
                  ) : (
                    "Enter"
                  )}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    day: state.nutrition.day,
    meals: state.nutrition.meals,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetDay: dayId => {
      dispatch(getDay(dayId));
    },
    onGetMeals: dayId => {
      dispatch(getMeals(dayId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateFood);
