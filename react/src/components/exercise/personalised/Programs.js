import React, { Component } from "react";
import { connect } from "react-redux";
import { getRequestedPrograms } from "../../../redux/actions/training";
import { NavLink } from "react-router-dom";
import MDSpinner from "react-md-spinner";

class Programs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayLimit: 2,
    };
    this._upgrade = this._upgrade.bind(this);
  }

  componentDidMount() {
    this.props.onGetRequestedPrograms();
  }

  _upgrade() {
    window.location.href =
      window.location.protocol +
      "//" +
      window.location.host +
      "/users/membership-options/";
  }

  render() {
    return (
      <React.Fragment>
        <div className='mb-1 mt-4 d-flex justify-content-between align-items-center'>
          <span>
            <h6 className='p-0 m-0 section-heading'>Personalised </h6>
            <p className='text-muted p-xs m-0'>Exercise Programs</p>
          </span>
          {this.props.membershipStatus !== "trialing" ? (
            <NavLink to='/training/request' className='black'>
              <button className='btn btn-sm btn-rounded btn-primary'>
                Request New
              </button>
            </NavLink>
          ) : (
            <button
              type='button'
              class='btn btn-sm btn-rounded btn-primary'
              data-toggle='modal'
              data-target='#trainingProgramExample'
            >
              Example
            </button>
          )}
        </div>
        {this.props.membershipStatus === "trialing" ? (
          <div
            onClick={this._upgrade}
            className='rounded-corners shadow-sm p-3 mt-3'
            style={{
              background:
                'url("https://d3qqgqjfawam42.cloudfront.net/images/app/rocket.svg"), linear-gradient(#5EB9D0, #5ED4A9)',
              backgroundSize: "contain",
              backgroundPosition: "right center",
              backgroundRepeat: "no-repeat",
              cursor: "pointer",
            }}
          >
            <div className='row'>
              <div className='col-9 d-flex justify-content-between align-items-center'>
                <span>
                  <p className='font-weight-bold white m-0'>
                    Upgrade your membership
                  </p>
                  <p className='white p-xs m-0'>
                    Request and access your personalised exercise program.
                  </p>
                </span>
              </div>
              <div className='col-3 d-flex justify-content-end align-items-center'>
                <i className='far fa-lg fa-chevron-right white' />
              </div>
            </div>
          </div>
        ) : this.props.requestedPrograms !== undefined ? (
          Array.isArray(this.props.requestedPrograms) &&
          this.props.requestedPrograms.length > 0 ? (
            <React.Fragment>
              {this.props.requestedPrograms
                .slice(0, this.state.displayLimit)
                .map((program, key) => (
                  <NavLink
                    to={"/training/requested/" + program.id}
                    className='rounded-corners bg-white shadow-sm p-3 mt-3 d-flex justify-content-between align-items-center'
                    key={key}
                  >
                    <span>
                      <p className='text-capitalize m-0'>{program.goal}</p>
                      <p className='p-smaller m-0 text-muted'>
                        {program.date.substring(0, program.date.indexOf("T"))}
                      </p>
                    </span>
                    <span>
                      <i className='far fa-chevron-right' />
                    </span>
                  </NavLink>
                ))}
              {this.props.requestedPrograms.length > 2 &&
                this.props.requestedPrograms.length !==
                  this.state.displayLimit && (
                  <div className='mt-1 d-flex justify-content-end align-items-center'>
                    <button
                      className='btn btn-sm btn-rounded btn-link'
                      onClick={() =>
                        this.setState({
                          displayLimit: this.props.requestedPrograms.length,
                        })
                      }
                    >
                      Show more
                    </button>
                  </div>
                )}
              {this.props.requestedPrograms.length > 2 &&
                this.state.displayLimit > 2 && (
                  <div className='mt-1 d-flex justify-content-end align-items-center'>
                    <button
                      className='btn btn-sm btn-rounded btn-link'
                      onClick={() => this.setState({ displayLimit: 2 })}
                    >
                      Show less
                    </button>
                  </div>
                )}
            </React.Fragment>
          ) : (
            <div className='rounded-corners bg-white shadow-sm p-3 mt-3'>
              <p className='m-0'>
                {this.props.home === true
                  ? "Request a personalised exercise program in your exercise tab."
                  : "Request a personalised exercise program by clicking Request Program above."}
              </p>
            </div>
          )
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "5%", paddingBottom: "5%" }}
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    requestedPrograms: state.training.requestedPrograms,
    membershipStatus: state.user.membershipStatus,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetRequestedPrograms: () => {
      dispatch(getRequestedPrograms());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Programs);
