import React, { Component } from "react";
import { getTrainingDayExercises } from "../../../../redux/actions/training";
import connect from "react-redux/es/connect/connect";
import axiosAjax from "../../../../config/axiosAjax";
import Exercise from "./Exercise";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      supersets: undefined,
      newSetId: undefined,
      generateNewSuperset: false,
      newSetColor: undefined,
    };
  }

  componentDidMount() {
    this._getSupersets();
  }

  _getSupersets() {
    axiosAjax
      .get("/training/superset", {
        params: {
          training_day_id: this.props.dayId,
        },
      })
      .then(response => {
        this.setState({
          supersets: response.data,
        });
      })
      .catch(error => {
        this.setState({
          supersets: [],
        });
      });
  }

  _generateRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  _deleteSuperset(id) {
    if (confirm("Click OK to delete this superset")) {
      axiosAjax.delete("/training/superset/" + id + "/").then(response => {
        this.props.onGetTrainingDayExercises(this.props.dayId);
        this._getSupersets();
      });
    }
  }

  _createSuperset() {
    this.setState({ generateNewSuperset: true });
    const formData = new FormData();
    formData.append("training_day", this.props.dayId);
    formData.append("color", this._generateRandomColor());
    axiosAjax({
      method: "post",
      url: "/training/superset/",
      data: formData,
    })
      .then(response => {
        this.setState({
          newSetId: response.data.id,
          newSetColor: response.data.color,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  _closeModal() {
    this.props.onGetTrainingDayExercises(this.props.dayId);
    this._getSupersets();
    this.setState({
      generateNewSuperset: false,
    });
  }

  render() {
    return (
      <div
        className='modal fade'
        id='supersets'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='supersetsLabel'
        aria-hidden='true'
        data-backdrop='static'
      >
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title' id='supersetsLabel'>
                Manage Supersets
              </h5>
              <button
                type='button'
                onClick={() => this._closeModal()}
                className='close'
                data-dismiss='modal'
                aria-label='Close'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              {this.state.generateNewSuperset === false && (
                <button
                  onClick={() => this._createSuperset()}
                  className='btn btn-light btn-sm btn-block'
                >
                  New Superset
                </button>
              )}
              {this.state.generateNewSuperset === true && (
                <div className='list-group list-group-flush mt-3 mb-5'>
                  <p className='text-muted p-xs'>
                    PICK EXERCISES FOR NEW{" "}
                    <span
                      style={{
                        height: 10,
                        width: 50,
                        borderRadius: 5,
                        padding: 5,
                        color: "white",
                        backgroundColor: this.state.newSetColor,
                      }}
                    >
                      SUPERSET
                    </span>
                  </p>
                  {this.props.trainingDayExercises !== undefined ? (
                    this.props.trainingDayExercises.length > 0 ? (
                      this.props.trainingDayExercises.map((exercise, key) =>
                        !exercise.superset_group ? (
                          <Exercise
                            key={key}
                            name={exercise.name}
                            id={exercise.id}
                            setColor={this.state.newSetColor}
                            setId={this.state.newSetId}
                            dayId={this.props.dayId}
                          />
                        ) : null,
                      )
                    ) : (
                      <p>Please first add exercises to create supersets.</p>
                    )
                  ) : (
                    <p>Loading exercises...</p>
                  )}
                  <div className='list-group-item d-flex justify-content-between align-items-center'>
                    <button
                      type='button'
                      onClick={() => this._closeModal()}
                      className='close'
                      data-dismiss='modal'
                      aria-label='Close'
                      className='btn btn-link btn-block btn-sm'
                    >
                      Finished
                    </button>
                  </div>
                </div>
              )}

              {this.state.supersets !== undefined ? (
                this.state.supersets.length > 0 ? (
                  <div className='list-group list-group-flush mt-3'>
                    <p className='text-muted p-xs'>CURRENT SUPERSETS</p>
                    {this.state.supersets.map((superset, key) => (
                      <div
                        key={key}
                        className='list-group-item d-flex justify-content-between align-items-center'
                      >
                        <span
                          style={{
                            height: 10,
                            width: 50,
                            borderRadius: 5,
                            backgroundColor: superset.color,
                          }}
                        />
                        <button
                          onClick={() => this._deleteSuperset(superset.id)}
                          className='btn btn-sm btn-light'
                        >
                          <i className='far fa-times' />
                        </button>
                      </div>
                    ))}
                  </div>
                ) : null
              ) : (
                <p>Loading....</p>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    trainingDayExercises: state.training.trainingDayExercises,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetTrainingDayExercises: trainingDayId => {
      dispatch(getTrainingDayExercises(trainingDayId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Index);
