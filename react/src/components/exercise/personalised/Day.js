import React, { PureComponent } from "react";
import { NavLink } from "react-router-dom";
import axiosAjax from "../../../config/axiosAjax";
import { connect } from "react-redux";
import {
  getTrainingDays,
  updateTrainingDays,
} from "../../../redux/actions/training";

class Day extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      sending: false,
      changeOrder: false,
      order: this.props.day.order,
      dayArrayIndex: this.props.dayArrayIndex,
      toMove: 0,
    };
  }

  componentDidMount() {
    if (this.state.order !== this.state.dayArrayIndex) {
      this._saveDayOrder();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.day.id !== this.props.day.id) {
      this.setState(
        {
          sending: false,
          changeOrder: false,
          order: this.props.day.order,
          dayArrayIndex: this.props.dayArrayIndex,
          toMove: 0,
        },
        () => {
          this._saveDayOrder();
        },
      );
    }
    if (prevState.toMove !== this.state.toMove) {
      this._reOrderDays();
    }
  }

  cleanDayName(dayName) {
    var s = dayName;
    var n = s.indexOf("-");
    return s.substring(0, n != -1 ? n : s.length);
  }

  _duplicateDay() {
    if (
      confirm(
        "Click OK to clone this day. This will create an identical copy of this training day.",
      )
    ) {
      this.setState({ sending: true });
      const formData = new FormData();
      formData.append("day_id", this.props.day.id);
      formData.append("training_id", this.props.trainingId);
      axiosAjax({
        method: "post",
        url: "/training/client-duplicate-training-day/",
        headers: { "Content-Type": "multipart/form-data" },
        data: formData,
      })
        .then(response => {
          this.setState({ sending: false });
          this.props.onGetTrainingDays(this.props.trainingId);
        })
        .catch(error => {
          this.setState({ sending: false });
        });
    }
  }

  _saveDayOrder() {
    const formData = new FormData();
    formData.append("order", this.state.dayArrayIndex);
    axiosAjax({
      method: "patch",
      url: "/training/training-day/" + this.props.day.id + "/",
      data: formData,
    }).then(response => {
      this.setState({
        order: response.data.order,
      });
    });
  }

  _reOrderDays() {
    var days = this.props.trainingDays;
    var dayRemoved = days.splice(this.state.dayArrayIndex, 1)[0];
    var newIndex = this.state.dayArrayIndex + this.state.toMove;
    days.splice(newIndex, 0, dayRemoved);
    this.props.updateDays(this.props.trainingDays);
    this.props.reRenderDays();
  }

  _deleteDay() {
    if (
      confirm(
        "Warning! By clicking OK, you will permanently delete this training day.",
      )
    ) {
      this.setState({ sending: true });
      axiosAjax
        .delete("/training/training-day/" + this.props.day.id + "/")
        .then(response => {
          this.setState({ sending: false });
          this.props.onGetTrainingDays(this.props.trainingId);
        })
        .catch(error => {
          this.setState({ sending: false });
        });
    }
  }

  render() {
    return (
      <div className='rounded-corners bg-white shadow-sm p-3 mt-3'>
        {this.state.changeOrder === true
          ? this.state.dayArrayIndex !== 0 && (
              <div className='text-center m-2'>
                <button
                  onClick={() => this.setState({ toMove: -1 })}
                  className='btn btn-secondary btn-xs btn-rounded'
                >
                  <i className='fas fa-arrow-alt-up' />
                </button>
              </div>
            )
          : null}
        <div className='row d-flex align-items-center'>
          <NavLink
            className='col'
            to={"/training/requested/program/" + this.props.day.id}
          >
            {this.cleanDayName(this.props.day.name)}
          </NavLink>
          <div className='col-auto'>
            <div className='dropdown float-right'>
              <button
                className='btn btn-light btn-sm'
                type='button'
                id={"dayOptions" + this.props.day.id}
                data-toggle='dropdown'
                aria-haspopup='true'
                aria-expanded='false'
              >
                <i className='far fa-ellipsis-v' />
              </button>
              <div
                className='dropdown-menu dropdown-menu-right'
                aria-labelledby={"dayOptions" + this.props.day.id}
              >
                <button
                  onClick={() => this._duplicateDay()}
                  className='dropdown-item'
                >
                  <i className='far fa-clone' /> &nbsp; Clone Day
                </button>
                <button
                  onClick={() =>
                    this.setState({
                      changeOrder: !this.state.changeOrder,
                    })
                  }
                  className='dropdown-item'
                >
                  <i className='far fa-sort' /> &nbsp; Change Order
                </button>
                <div className='dropdown-divider' />
                <button
                  onClick={() => this._deleteDay()}
                  className='dropdown-item'
                >
                  <i className='far red fa-times' /> &nbsp; Delete Day
                </button>
              </div>
            </div>
          </div>
        </div>
        {this.state.changeOrder === true
          ? this.state.dayArrayIndex !== this.props.trainingDays.length - 1 && (
              <div className='text-center m-2'>
                <button
                  onClick={() => this.setState({ toMove: 1 })}
                  className='btn btn-secondary btn-xs btn-rounded'
                >
                  <i className='fas fa-arrow-alt-down' />
                </button>
              </div>
            )
          : null}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    trainingDays: state.training.trainingDays,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetTrainingDays: trainingId => {
      dispatch(getTrainingDays(trainingId));
    },
    updateDays: days => {
      dispatch(updateTrainingDays(days));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Day);
