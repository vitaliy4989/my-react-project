import React, { PureComponent } from "react";
import axiosAjax from "../../../config/axiosAjax";

class CreateComment extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      comment: "",
      sending: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._createComment = this._createComment.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _createComment(event) {
    event.preventDefault();
    this.props.pushNewComment(this.state.comment);
    const formData = new FormData();
    formData.append("training", this.props.programId);
    formData.append("comment", this.state.comment);
    axiosAjax({
      method: "post",
      url: "/training/training-comment/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          comment: "",
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <form
        onSubmit={this._createComment}
        style={{
          border: "1px solid #E8ECEF",
          paddingTop: 10,
          paddingBottom: 20,
          background: "white",
        }}
      >
        <div className='container'>
          <div className='form-row'>
            <div className='col-9'>
              <textarea
                style={{ height: 41 }}
                className='form-control form-control-rounded'
                onChange={this.handleInputChange}
                value={this.state.comment}
                name='comment'
                placeholder='Type a message...'
                required
              />
            </div>
            <div className='col-3'>
              <button
                id='formButton'
                className='btn btn-block btn-light btn-rounded'
              >
                <i className='fab fa-lg fa-telegram-plane' />
              </button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default CreateComment;
