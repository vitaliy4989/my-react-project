import React, { PureComponent } from "react";
import axiosAjax from "../../../config/axiosAjax";
import Comment from "../../shared/Comment";
import CreateComment from "./CreateComment";
import { connect } from "react-redux";
import { updateRequestedProgram } from "../../../redux/actions/training";
import MDSpinner from "react-md-spinner";

const scrollToBottom = () => {
  const messagesDiv = document.getElementById("commentArea");
  if (messagesDiv) messagesDiv.scrollTop = messagesDiv.scrollHeight;
};

class Comments extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      comments: undefined,
    };
    this._loadTrainingComments = this._loadTrainingComments.bind(this);
    this._pushNewComment = this._pushNewComment.bind(this);
  }

  _loadTrainingComments(event) {
    axiosAjax
      .get("/training/training-comment/" + this.props.programId)
      .then(response => {
        this.setState({
          comments: response.data,
        });
      })
      .catch(error => {
        this.setState({
          comments: [],
        });
      });
    scrollToBottom();
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "none";
    }
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "none";
    }
  }

  _updateMessage() {
    const formData = new FormData();
    formData.append("id", this.props.programId);
    axiosAjax({
      method: "post",
      url: "/training/training-comment-update/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    }).then(response => {
      this.props.onUpdateRequestedProgram(response.data);
    });
  }

  _pushNewComment(comment) {
    this.state.comments.push({
      coach: null,
      comment: comment,
      date: null,
      id: null,
      training: null,
      user: true,
      date: "Now",
    });
    this.forceUpdate();
  }

  componentDidMount() {
    this._loadTrainingComments();
    if (this.props.messageUpdate === true) {
      this._updateMessage();
    }
  }

  componentDidUpdate() {
    scrollToBottom();
  }

  componentWillReceiveProps(nextProps) {
    axiosAjax
      .get("/training/training-comment/" + nextProps.programId)
      .then(response => {
        this.setState({
          comments: response.data,
        });
      });
  }

  componentWillUnmount() {
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "block";
    }
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "block";
    }
  }

  render() {
    return (
      <div id='program-messages' className='program-messages'>
        <div
          style={{
            border: "1px solid #E8ECEF",
            paddingTop: 5,
            paddingBottom: 5,
            borderTop: "none",
            background: "white",
          }}
        >
          <div className='container text-center'>
            <p className='p-smaller text-muted m-0' style={{ fontSize: 10 }}>
              Response time here can be up to 72 hours. For general or
              non-program questions, please use the{" "}
              <a href='/users/#/trainer-support'>
                <u>Trainer Support chat</u>
              </a>
              .
            </p>
          </div>
        </div>
        {this.state.comments !== undefined ? (
          this.state.comments.length > 0 ? (
            this.state.comments.length >= 1 ? (
              <div
                style={{
                  height: "100%",
                  overflowX: "hidden",
                  overflowY: "scroll",
                  border: "1px solid #E8ECEF",
                  background: "white",
                }}
                id='commentArea'
              >
                <div className='container'>
                  {this.state.comments.map((comment, key) =>
                    comment.user ? (
                      <div key={key} className='row'>
                        <div className='col'>
                          {Comment({
                            comment: comment.comment,
                            noImage: true,
                            class: "comment-right animated fadeIn",
                            date: comment.date.substring(
                              0,
                              comment.date.indexOf("T"),
                            ),
                          })}
                        </div>
                      </div>
                    ) : (
                      <div key={key} className='row'>
                        <div className='col'>
                          {Comment({
                            comment: comment.comment,
                            image: comment.coach.image,
                            heading: comment.coach.user.first_name,
                            class: "comment-left animated fadeIn",
                            textClass: "comment-left-text",
                            date: comment.date.substring(
                              0,
                              comment.date.indexOf("T"),
                            ),
                          })}
                        </div>
                      </div>
                    ),
                  )}
                </div>
              </div>
            ) : (
              <div className='h-100 d-flex flex-column justify-content-center'>
                <div className='text-center'>
                  <MDSpinner singleColor='#4a4090' />
                </div>
              </div>
            )
          ) : (
            <p className='text-center m-2'>
              Leave a message here for your trainer.
            </p>
          )
        ) : (
          <div className='h-100 d-flex flex-column justify-content-center'>
            <div className='text-center'>
              <MDSpinner singleColor='#4a4090' />
            </div>
          </div>
        )}
        <CreateComment
          pushNewComment={this._pushNewComment}
          programId={this.props.programId}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUpdateRequestedProgram: program => {
      dispatch(updateRequestedProgram(program));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(Comments);
