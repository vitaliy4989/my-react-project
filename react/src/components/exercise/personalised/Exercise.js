import React, { PureComponent } from "react";
import Sets from "./Sets";
import { connect } from "react-redux";
import {
  getTrainingDayExercises,
  updateTrainingDayExercises,
} from "../../../redux/actions/training";
import axiosAjax from "../../../config/axiosAjax";
import { _userChangeLog } from "./UserChangeLog";

class Exercise extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      removeSets: false,
      superset: this.props.exercise.superset,
      order: this.props.exercise.order,
      supersetGroup: this.props.exercise.superset_group,
      exerciseArrayIndex: this.props.exerciseArrayIndex,
      toMove: 0,
      changeOrder: false,
      showExerciseDetail: false,
    };
  }

  _openLink() {
    if (this.props.exercise.link.indexOf("www.jamessmithacademy.com") !== -1) {
      var link = this.props.exercise.link.replace(
        "https://www.jamessmithacademy.com",
        "",
      );
      window.location.href =
        window.location.protocol + "//" + window.location.host + link;
    } else {
      window.open(this.props.exercise.link);
      return false;
    }
  }

  componentDidMount() {
    if (this.state.order !== this.state.exerciseArrayIndex) {
      this._saveExercseOrder();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.exercise.id !== this.props.exercise.id) {
      this.setState(
        {
          removeSets: false,
          superset: this.props.exercise.superset,
          order: this.props.exercise.order,
          exerciseArrayIndex: this.props.exerciseArrayIndex,
          supersetGroup: this.props.exercise.superset_group,
          toMove: 0,
          changeOrder: false,
          showExerciseDetail: false,
        },
        () => {
          this._saveExercseOrder();
        },
      );
    }
    if (prevState.toMove !== this.state.toMove) {
      this._reOrderExercises();
    }
  }

  _saveExercseOrder() {
    const formData = new FormData();
    formData.append("order", this.state.exerciseArrayIndex);
    formData.append("superset", this.state.superset);
    axiosAjax({
      method: "patch",
      url: "/training/training-exercise/" + this.props.exercise.id + "/",
      data: formData,
    }).then(response => {
      this.setState({
        order: response.data.order,
      });
    });
  }

  _deleteExercise() {
    if (confirm("Warning! By clicking OK, you will delete this exercise.")) {
      axiosAjax
        .delete("/training/training-exercise/" + this.props.exercise.id + "/")
        .then(response => {
          this.props.onGetTrainingDayExercises(this.props.dayId);
          _userChangeLog(
            this.props.dayId,
            this.props.dayChangeLogs +
              "    " +
              new Date() +
              " " +
              this.props.username +
              ' deleted exercise: "' +
              this.props.exercise.name +
              '"; ',
          );
        });
    }
  }

  _reOrderExercises() {
    var exercises = this.props.trainingDayExercises;
    var exerciseRemoved = exercises.splice(this.state.exerciseArrayIndex, 1)[0];
    var newIndex = this.state.exerciseArrayIndex + this.state.toMove;
    exercises.splice(newIndex, 0, exerciseRemoved);
    this.props.updateExercises(exercises);
    this.props.reRenderExercises();
  }

  render() {
    return (
      <React.Fragment>
        <div className='row no-gutters'>
          {this.state.supersetGroup || this.state.superset === true ? (
            <div
              className='col-auto'
              style={{
                width: 3,
                borderRadius: 5,
                marginRight: 10,
                marginTop: 7,
                marginBottom: 7,
                display: "flex",
                flexDirection: "column",
                backgroundColor: this.state.supersetGroup
                  ? this.state.supersetGroup.color
                  : "red",
              }}
            />
          ) : (
            <div
              className='col-auto'
              style={{
                width: 3,
                borderRadius: 5,
                marginRight: 10,
                marginTop: 7,
                marginBottom: 7,
                display: "flex",
                flexDirection: "column",
                backgroundColor: "transparent",
              }}
            />
          )}
          <div className='col'>
            {this.state.changeOrder === true
              ? this.state.exerciseArrayIndex !== 0 && (
                  <div className='text-center m-2'>
                    <button
                      onClick={() => this.setState({ toMove: -1 })}
                      className='btn btn-secondary btn-xs btn-rounded'
                    >
                      <i className='fas fa-arrow-alt-up' />
                    </button>
                  </div>
                )
              : null}
            <div
              className='row'
              style={
                this.state.supersetGroup
                  ? {
                      paddingTop: "1rem",
                      paddingBottom: "1rem",
                    }
                  : {
                      borderRadius: 0,
                      paddingTop: "1rem",
                      paddingBottom: "1rem",
                    }
              }
            >
              <div className='col'>
                <div
                  className='row d-flex justify-content-between align-items-center'
                  onClick={() =>
                    this.setState({
                      showExerciseDetail: !this.state.showExerciseDetail,
                    })
                  }
                  style={{ cursor: "pointer" }}
                >
                  <div className='col-10'>
                    <h6 className='text-capitalize p-smaller m-0'>
                      {this.props.exercise.name}
                      &nbsp;
                      {this.state.supersetGroup ||
                      this.state.superset === true ? (
                        <span className='text-muted' style={{ fontSize: 10 }}>
                          (SUPERSET)
                        </span>
                      ) : null}
                    </h6>
                  </div>
                  <div className='col-2 text-center'>
                    {this.state.showExerciseDetail === true ? (
                      <i className='far text-muted fa-chevron-up' />
                    ) : (
                      <i className='far text-muted fa-chevron-right' />
                    )}
                  </div>
                </div>
                {this.state.showExerciseDetail === true ? (
                  <React.Fragment>
                    <Sets
                      exerciseId={this.props.exercise.id}
                      removeSets={this.state.removeSets}
                      dayType={this.props.dayType}
                    />
                    <div className='row'>
                      <div className='col'>
                        <button
                          className='btn btn-sm btn-block btn-link'
                          onClick={() => this._deleteExercise()}
                        >
                          <i className='far fa-times' /> &nbsp; Delete
                        </button>
                      </div>
                      <div className='col'>
                        <button
                          className='btn btn-sm btn-block btn-link'
                          onClick={() =>
                            this.setState({
                              changeOrder: !this.state.changeOrder,
                            })
                          }
                          type='button'
                        >
                          <i className='far fa-sort' /> &nbsp;{" "}
                          {this.state.changeOrder === true
                            ? "Finished"
                            : "Order"}
                        </button>
                      </div>
                      {this.props.exercise.link ? (
                        <div className='col'>
                          <button
                            className='btn btn-sm btn-block btn-link'
                            onClick={() => this._openLink()}
                          >
                            <i className='far fa-video' /> &nbsp; Demo
                          </button>
                        </div>
                      ) : null}
                    </div>
                  </React.Fragment>
                ) : null}
              </div>
            </div>
            {this.state.changeOrder === true
              ? this.state.exerciseArrayIndex !==
                  this.props.trainingDayExercises.length - 1 && (
                  <div className='text-center m-2'>
                    <button
                      onClick={() => this.setState({ toMove: 1 })}
                      className='btn btn-secondary btn-xs btn-rounded'
                    >
                      <i className='fas fa-arrow-alt-down' />
                    </button>
                  </div>
                )
              : null}
          </div>
          <div
            className='col-auto'
            style={{
              width: 3,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 7,
              marginBottom: 7,
              display: "flex",
              flexDirection: "column",
              backgroundColor: "transparent",
            }}
          />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    trainingDayExercises: state.training.trainingDayExercises,
    username: state.user.username,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetTrainingDayExercises: trainingDayId => {
      dispatch(getTrainingDayExercises(trainingDayId));
    },
    updateExercises: exercises => {
      dispatch(updateTrainingDayExercises(exercises));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Exercise);
