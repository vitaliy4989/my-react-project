import axiosAjax from "../../../config/axiosAjax";

export function _userChangeLog(dayId, message) {
  const formData = new FormData();
  formData.append("user_change_log", message);
  axiosAjax({
    method: "patch",
    url: "/training/training-day/" + dayId + "/",
    data: formData,
  })
    .then(response => {
      console.log("Activity logged.");
    })
    .catch(error => {
      console.log("There has been an error.");
    });
}
