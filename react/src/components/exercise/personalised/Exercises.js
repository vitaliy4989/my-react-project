import React, { PureComponent } from "react";
import Exercise from "./Exercise";
import MDSpinner from "react-md-spinner";
import LightAlert from "../../shared/LightAlert";
import { connect } from "react-redux";
import { getTrainingDayExercises } from "../../../redux/actions/training";

class Exercises extends PureComponent {
  constructor(props) {
    super(props);
    this._reRenderExercises = this._reRenderExercises.bind(this);
  }

  componentDidMount() {
    this.props.onGetTrainingDayExercises(this.props.dayId);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.dayId !== this.props.dayId) {
      this.props.onGetTrainingDayExercises(this.props.dayId);
    }
  }

  _reRenderExercises(event) {
    this.forceUpdate();
  }

  render() {
    return (
      <React.Fragment>
        {this.props.trainingDayExercises !== undefined ? (
          this.props.trainingDayExercises.length > 0 ? (
            this.props.trainingDayExercises.map((exercise, key) => (
              <Exercise
                exercise={exercise}
                key={key}
                reRenderExercises={this._reRenderExercises}
                exerciseArrayIndex={key}
                dayId={this.props.dayId}
                dayChangeLogs={this.props.dayChangeLogs}
                dayType={this.props.dayType}
              />
            ))
          ) : (
            <div className='list-group-item'>
              <LightAlert
                class='text-center'
                message='There are no exercises listed, please check the notes.'
              />
            </div>
          )
        ) : (
          <div
            style={{ paddingTop: "5%", paddingBottom: "5%" }}
            className='text-center'
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    trainingDayExercises: state.training.trainingDayExercises,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetTrainingDayExercises: trainingDayId => {
      dispatch(getTrainingDayExercises(trainingDayId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Exercises);
