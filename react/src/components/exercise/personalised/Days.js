import React, { PureComponent } from "react";
import Day from "./Day";
import MDSpinner from "react-md-spinner";
import { connect } from "react-redux";
import { getTrainingDays } from "../../../redux/actions/training";

class Days extends PureComponent {
  constructor(props) {
    super(props);
    this._reRenderDays = this._reRenderDays.bind(this);
  }

  componentDidMount() {
    this.props.onGetTrainingDays(this.props.trainingId);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.trainingId !== this.props.trainingId) {
      this.props.onGetTrainingDays(this.props.trainingId);
    }
  }

  _reRenderDays(event) {
    this.forceUpdate();
  }

  render() {
    return (
      <React.Fragment>
        <div className='mb-1 mt-4 d-flex justify-content-between align-items-center'>
          <h6 className='p-0 m-0'>Training Days</h6>
        </div>
        {this.props.trainingDays !== undefined ? (
          this.props.trainingDays.length > 0 ? (
            this.props.trainingDays.map((day, key) => (
              <Day
                key={key}
                dayArrayIndex={key}
                trainingId={this.props.trainingId}
                day={day}
                reRenderDays={this._reRenderDays}
              />
            ))
          ) : (
            <div className='mt-3'>
              <strong>Important, please read</strong>
              <p>
                Programs normally take 3 days to complete, but will take longer
                if you did not provide us with enough information. You can add
                more information in the program messages.
              </p>
              <p>
                We will send you an email notification about any updates to your
                program.
              </p>
              <p>
                Program messages are for you to communicate with the coach about
                the program. This includes questions directly relating to the
                program and does not include questions about nutrition,
                memberships etc. It can take 2-3 days to receive a response on
                your Program Messages. Therefore, if you have any general or non
                program questions, please use the Trainer Chat, as the response
                time in about 24h.
              </p>
            </div>
          )
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "5%", paddingBottom: "5%" }}
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    trainingDays: state.training.trainingDays,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetTrainingDays: trainingId => {
      dispatch(getTrainingDays(trainingId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Days);
