import React from "react";
import axiosAjax from "../../../config/axiosAjax";
import { connect } from "react-redux";
import { getRequestedPrograms } from "../../../redux/actions/training";
import { getStepThree } from "../../../redux/actions/home";
import { NotificationManager } from "react-notifications";

class Request extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: "",
      gender: "",
      age: "",
      experience: "",
      goal: "",
      frequency: "",
      facility: "",
      medical: "",
      comment: "",
      message: false,
      sending: false,
      moreInfo: false,
      stepCompleted: false,
      requestSent: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    window.scrollTo(0, 0);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ requestSent: true });
    const formData = new FormData();
    formData.append("gender", this.state.gender);
    formData.append("age", Math.round(this.state.age, -1));
    formData.append("training_age", this.state.experience);
    formData.append("goal", this.state.goal);
    formData.append("frequency", this.state.frequency);
    formData.append("facility", this.state.facility);
    formData.append("medical", this.state.medical);
    formData.append("comment", this.state.comment);
    axiosAjax({
      method: "post",
      url: "/training/user-training/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        NotificationManager.success(
          "Training request sent, please read the instructions below.",
          "",
          5000,
        );
        this.setState({
          message: true,
          requestSent: true,
        });
        this.props.onGetStepThree();
      })
      .catch(error => {
        this.setState({
          requestSent: false,
        });
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        {this.props.membershipStatus === "trialing" ? (
          <React.Fragment>
            <strong className='home-step-container__description-title'>
              Explained
            </strong>
            <p className='home-step-container__description-content'>
              While on the trial, you will be able to access My Workouts and
              James' Workouts, but not personalised exercise programs. If you
              would like a personalised exercise program, upgrade your account{" "}
              <a href='/users/membership-options/'>
                <u>here</u>
              </a>
              .
            </p>
          </React.Fragment>
        ) : (
          <div
            className={
              this.props.noBorder === true
                ? "max-width-500"
                : "content-container max-width-500"
            }
          >
            <div>
              <button
                onClick={() =>
                  this.setState({ moreInfo: !this.state.moreInfo })
                }
                className='btn btn-block btn-link'
                style={{ paddingLeft: 0 }}
              >
                {this.state.moreInfo === true
                  ? "Ok, makes sense."
                  : "About Personalised Exercise Programs"}
              </button>
              {this.state.moreInfo === true && (
                <div className='animated-2 fadeIn mt-3'>
                  <p>
                    We offer personalised exercise programs to meet your unique
                    requirements and goals. To make sure the program is well
                    suited to your needs, we ask you to firstly complete the
                    questionnaire.
                  </p>
                  <p>
                    If we have any questions, we will contact you directly on
                    the program messages. Likewise, if you have any questions or
                    would like any changes, please send us a message on the
                    designated program messages.
                  </p>
                  <p>
                    You are not limited to one personalised exercise program, so
                    if you would like another, just submit another request.
                  </p>
                  <p>
                    When we update your program or respond to your message, you
                    will receive an in-site notification and an email.
                  </p>
                  <p>
                    We suggest requesting a new personalised exercise program
                    every 6 weeks.
                  </p>
                </div>
              )}
              <form onSubmit={this.handleSubmit}>
                <div className='form-group'>
                  <label htmlFor='age'>Age</label>
                  <input
                    type='number'
                    onChange={this.handleInputChange}
                    value={this.state.age}
                    name='age'
                    className={
                      this.state.age !== ""
                        ? "is-valid form-control form-control-single-border"
                        : "form-control form-control-single-border"
                    }
                    placeholder='Age'
                    required
                  />
                </div>
                <div className='form-group'>
                  <label htmlFor='gender'>Gender</label>
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.gender}
                    name='gender'
                    className={
                      this.state.gender !== ""
                        ? "is-valid custom-select form-control form-control-single-border"
                        : "custom-select form-control form-control-single-border"
                    }
                    required
                  >
                    <option value='' disabled selected>
                      Select Gender
                    </option>
                    <option>Male</option>
                    <option>Female</option>
                  </select>
                </div>
                <div className='form-group'>
                  <label htmlFor='training_age'>
                    Years Exercise Experience
                  </label>
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.experience}
                    name='experience'
                    className={
                      this.state.experience !== ""
                        ? "is-valid custom-select form-control form-control-single-border"
                        : "custom-select form-control form-control-single-border"
                    }
                    required
                  >
                    <option value='' disabled selected>
                      Select Experience Level
                    </option>
                    <option value='Less than 1'>Less than 1 year</option>
                    <option value='1-2'>1-2 years</option>
                    <option value='2-3'>2-3 years</option>
                    <option value='3-4'>3-4 years</option>
                    <option value='4+'>4+ years</option>
                  </select>
                </div>
                <div className='form-group'>
                  <label htmlFor='goal'>General Goal</label>
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.goal}
                    name='goal'
                    className={
                      this.state.goal !== ""
                        ? "is-valid custom-select form-control form-control-single-border"
                        : "custom-select form-control form-control-single-border"
                    }
                    required
                  >
                    <option value='' disabled selected>
                      Select General Goal
                    </option>
                    <option value='Build Muscle'>Build Muscle</option>
                    <option value='Lose Fat'>Lose Fat</option>
                    <option value='Other'>Other</option>
                  </select>
                </div>
                <div className='form-group'>
                  <label htmlFor='frequency'>Exercise Frequency</label>
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.frequency}
                    name='frequency'
                    className={
                      this.state.frequency !== ""
                        ? "is-valid custom-select form-control form-control-single-border"
                        : "custom-select form-control form-control-single-border"
                    }
                    required
                  >
                    <option value='' disabled selected>
                      Select Exercise Frequency
                    </option>
                    <option value='1 - week'>1 - week</option>
                    <option value='2 - week'>2 - week</option>
                    <option value='3 - week'>3 - week</option>
                    <option value='+3 - week'>+3 - week</option>
                  </select>
                </div>
                <div className='form-group'>
                  <label htmlFor='facility'>Exercise Facility</label>
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.facility}
                    name='facility'
                    className={
                      this.state.facility !== ""
                        ? "is-valid custom-select form-control form-control-single-border"
                        : "custom-select form-control form-control-single-border"
                    }
                    required
                  >
                    <option value='' disabled selected>
                      Select Exercise Facility
                    </option>
                    <option value='home'>Home</option>
                    <option value='gym'>Gym</option>
                    <option value='outside'>Outside</option>
                  </select>
                </div>
                <div className='form-group'>
                  <label htmlFor='medical'>Medical Issues</label>
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.medical}
                    name='medical'
                    className={
                      this.state.medical !== ""
                        ? "is-valid custom-select form-control form-control-single-border"
                        : "custom-select form-control form-control-single-border"
                    }
                    required
                  >
                    <option value='' disabled selected>
                      Select Medical Issues
                    </option>
                    <option value='None'>None</option>
                    <option value='A medical professional HAS given me permission to train'>
                      Yes - A medical professional HAS given me permission to
                      exercise
                    </option>
                    <option value='A medical professional HAS NOT given me permission to train'>
                      Yes - A medical professional HAS NOT given me permission
                      to exercise
                    </option>
                  </select>
                </div>
                <div className='form-group'>
                  <label htmlFor='comment'>More Information</label>
                  {this.state.medical ===
                    "A medical professional HAS NOT given me permission to train" && (
                    <div className='alert alert-danger animated-2 fadeIn'>
                      <i className='fas fa-exclamation-circle' /> You have
                      indicated that you have a medical condition and have not
                      been given permission to exercise. We please ask that if
                      you have a medical condition, you first receive permission
                      from a medical professional.
                    </div>
                  )}
                  {this.state.facility === "home" && (
                    <div className='alert alert-light animated-2 fadeIn'>
                      <i className='fas fa-exclamation-circle' /> Please make
                      sure to list the equipment you have at home to reduce the
                      amount of time it takes to write your program. If you have
                      a multi gym, please specify the available exercises on the
                      multi gym as they differ.
                    </div>
                  )}
                  <textarea
                    onChange={this.handleInputChange}
                    value={this.state.comment}
                    name='comment'
                    className={
                      this.state.comment !== ""
                        ? "is-valid form-control form-control-single-border"
                        : "form-control form-control-single-border"
                    }
                    placeholder='Leave us some more information which you feel is important to your training request...'
                    required
                  />
                </div>
                {this.state.requestSent === false && (
                  <div className='text-center'>
                    <button
                      id='formButton'
                      type='submit'
                      className='enter-button-centered btn-rounded btn btn-primary'
                    >
                      {this.state.sending === true ? (
                        <i className='fas fa-circle-notch fa-spin' />
                      ) : (
                        "Enter"
                      )}
                    </button>
                  </div>
                )}

                {this.state.message === true && (
                  <React.Fragment>
                    <div
                      style={{ marginTop: "1rem" }}
                      className='alert alert-success animated-2 fadeIn'
                      role='alert'
                    >
                      Success! Your request was sent, please read below.
                    </div>
                    <div
                      className='alert alert-light animated-2 fadeIn'
                      role='alert'
                    >
                      <h6>Important, please read</h6>
                      <p className='p-smaller'>
                        Personalised exercise programs normally take 3 days to
                        complete, but will take longer if you did not provide us
                        with enough information. You can add more information by
                        heading to your personalised exercise programs, clicking
                        on the request and heading to program messages to send a
                        new message to us.
                      </p>
                      <p className='p-smaller'>
                        If you are looking to exercise while we work on your
                        program, check out the 'Cheeky sessions' on your
                        exercise tab.
                      </p>
                      <p className='p-smaller'>
                        We will send you an email notification about any updates
                        to your program.
                      </p>
                      <p className='p-smaller'>
                        Program messages are for you to communicate with the
                        coach about the program. This includes questions
                        directly relating to the program and does not include
                        questions about nutrition, memberships etc. It can take
                        2-3 days to receive a response on your Program Messages.
                        Therefore, if you have any general or non program
                        questions, please use the Trainer Chat, as the response
                        time in about 24h.
                      </p>
                    </div>
                  </React.Fragment>
                )}
              </form>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const file = {
  fontSize: 12,
};

const overLay = {
  background: "rgba(255, 0, 0, 0.3)",
  zIndex: 1000,
  position: "absolute",
};

const mapStateToProps = (state, ownProps) => {
  return {
    membershipStatus: state.user.membershipStatus,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetRequestedPrograms: () => {
      dispatch(getRequestedPrograms());
    },
    onGetStepThree: () => {
      dispatch(getStepThree());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Request);
