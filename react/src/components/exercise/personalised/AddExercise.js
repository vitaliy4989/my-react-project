import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import AddExerciseSearchResult from "./AddExerciseSearchResult";
import MDSpinner from "react-md-spinner";
import { connect } from "react-redux";
import { getTrainingDayExercises } from "../../../redux/actions/training";
import { _userChangeLog } from "./UserChangeLog";

class AddExercise extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: "",
      startSearch: false,
      exercises: undefined,
      createNewExercise: false,
      noResult: false,
      name: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._createNewExercise = this._createNewExercise.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _createNewExercise(event) {
    event.preventDefault();
    this.setState({ added: true });
    const formData = new FormData();
    formData.append("training_day", this.props.dayId);
    formData.append("name", this.state.name);
    formData.append("link", "");
    formData.append("order", this.props.trainingDayExercises.length + 1);
    axiosAjax({
      method: "post",
      url: "/training/training-exercise/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetTrainingDayExercises(this.props.dayId);
        _userChangeLog(
          this.props.dayId,
          this.props.dayChangeLogs +
            "    " +
            new Date() +
            " " +
            this.props.username +
            ' added exercise: "' +
            this.state.name +
            '"; ',
        );
        this.setState({ added: true, name: "" });
      })
      .catch(error => {
        this.setState({ added: false });
      });
  }

  _searchExercises() {
    if (this.state.searchTerm !== "") {
      this.setState({
        startSearch: true,
      });
      axiosAjax
        .get("/training/saved-exercise", {
          params: {
            name: this.state.searchTerm,
          },
        })
        .then(response => {
          this.setState({
            exercises: response.data,
            noResults: false,
          });
        })
        .catch(error => {
          this.setState({
            exercises: [],
            noResults: true,
          });
        });
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.searchTerm !== this.state.searchTerm) {
      this._searchExercises();
    }
  }

  render() {
    return (
      <div
        className='modal fade'
        id='addExerciseModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='addExerciseModal'
        aria-hidden='true'
      >
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h6 className='modal-title'>Add Exercises</h6>
              <button
                type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Close'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <div className='row'>
                <div className='col'>
                  <button
                    className='btn btn-sm btn-block btn-light'
                    onClick={() =>
                      this.setState({
                        createNewExercise: !this.state.createNewExercise,
                        searchTerm: "",
                      })
                    }
                  >
                    {this.state.createNewExercise === true
                      ? "Search Exercises"
                      : "Add Custom Exercise"}
                  </button>
                </div>
              </div>
              {this.state.createNewExercise === true ? (
                <form
                  onSubmit={this._createNewExercise}
                  style={{ marginTop: "1rem" }}
                >
                  <div className='form-row'>
                    <div className='col-8'>
                      <input
                        autoFocus='autofocus'
                        autoComplete='off'
                        className='form-control '
                        onChange={this.handleInputChange}
                        value={this.state.name}
                        name='name'
                        type='text'
                        placeholder='Exercise name...'
                        required
                        autoComplete='off'
                      />
                    </div>
                    <div className='col-4'>
                      <button
                        id='formButton'
                        className='waves-effect btn btn-block btn-primary'
                        type='submit'
                      >
                        {this.state.sending === true ? (
                          <i className='fas fa-circle-notch fa-spin' />
                        ) : (
                          "Enter"
                        )}
                      </button>
                    </div>
                  </div>
                </form>
              ) : (
                <form style={{ marginTop: "1rem" }}>
                  <div className='form-row'>
                    <div className='col'>
                      <input
                        autoFocus='autofocus'
                        autoComplete='off'
                        onChange={this.handleInputChange}
                        value={this.state.searchTerm}
                        name='searchTerm'
                        type='text'
                        className='form-control'
                        placeholder='Enter name or body part ...'
                        aria-describedby='basic-addon1'
                        required
                      />
                    </div>
                  </div>
                </form>
              )}
              {this.state.startSearch === true && this.state.searchTerm !== "" && (
                <div
                  style={{
                    maxHeight: "100vh",
                    overflowY: "scroll",
                    overflowX: "hidden",
                  }}
                >
                  {this.state.exercises !== undefined ? (
                    <div
                      className='list-group-flush list-group list-wrapper-no-box'
                      style={{
                        marginBottom: 15,
                        marginTop: 15,
                      }}
                    >
                      {this.state.exercises.length > 0 ? (
                        this.state.exercises.map((exercise, key) => (
                          <AddExerciseSearchResult
                            key={key}
                            exercise={exercise}
                            dayChangeLogs={this.props.dayChangeLogs}
                            dayId={this.props.dayId}
                          />
                        ))
                      ) : this.state.noResults === true ? (
                        <div className='list-group-item animated fadeIn'>
                          No results...
                        </div>
                      ) : (
                        <div className='list-group-item animated fadeIn'>
                          Searching...
                        </div>
                      )}
                    </div>
                  ) : (
                    <div
                      className='text-center'
                      style={{
                        paddingTop: "10%",
                        paddingBottom: "10%",
                      }}
                    >
                      <MDSpinner singleColor='#4a4090' />
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    trainingDayExercises: state.training.trainingDayExercises,
    username: state.user.username,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetTrainingDayExercises: trainingDayId => {
      dispatch(getTrainingDayExercises(trainingDayId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddExercise);
