import React, { Component } from "react";
import { connect } from "react-redux";
import { getTrainingDayExercises } from "../../../redux/actions/training";
import axiosAjax from "../../../config/axiosAjax";
import { _userChangeLog } from "./UserChangeLog";

class AddExerciseSearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      added: false,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.exercise.id !== prevProps.exercise.id) {
      this.setState({
        added: false,
      });
    }
  }

  _createExercise() {
    this.setState({ added: true });
    const formData = new FormData();
    formData.append("training_day", this.props.dayId);
    formData.append("name", this.props.exercise.name);
    formData.append("link", this.props.exercise.link);
    formData.append("order", this.props.trainingDayExercises.length + 1);
    axiosAjax({
      method: "post",
      url: "/training/training-exercise/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetTrainingDayExercises(this.props.dayId);
        this.setState({ added: true });
        _userChangeLog(
          this.props.dayId,
          this.props.dayChangeLogs +
            "    " +
            new Date() +
            " " +
            this.props.username +
            ' added exercise: "' +
            this.props.exercise.name +
            '"; ',
        );
      })
      .catch(error => {
        this.setState({ added: false });
      });
  }

  render() {
    return (
      <div className='list-group-item d-flex justify-content-between align-items-center'>
        <div>{this.props.exercise.name}</div>
        <div>
          {this.state.added === true ? (
            <button className='btn btn-round btn-success float-right'>
              <i className='far fa-check white' />
            </button>
          ) : (
            <button
              onClick={() => this._createExercise()}
              className='btn btn-round btn-light float-right'
            >
              <i className='far fa-plus' />
            </button>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    trainingDayExercises: state.training.trainingDayExercises,
    username: state.user.username,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetTrainingDayExercises: trainingDayId => {
      dispatch(getTrainingDayExercises(trainingDayId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddExerciseSearchResult);
