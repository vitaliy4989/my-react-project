import React from "react";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import { NotificationManager } from "react-notifications";

class NeatUp247 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stepsEntries: undefined,
      todaysSteps: undefined,
      steps: "",
      displayLimit: 3,
      saved: false,
    };
    this._uploadSteps = this._uploadSteps.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  _loadSteps() {
    axiosAjax
      .get("/training/steps/")
      .then(response => {
        this.setState({
          stepsEntries: response.data.steps_leaderboard,
          steps: response.data.my_steps
            ? response.data.my_steps.steps
            : undefined,
          todaysSteps: response.data.my_steps
            ? response.data.my_steps.steps
            : undefined,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentDidMount() {
    this._loadSteps();
  }

  handleInputChange(event) {
    if (event.target.value === "") {
      this.setState({ steps: "" });
    } else {
      const steps = event.target.validity.valid
        ? event.target.value
        : this.state.steps;
      this.setState({ steps });
    }
  }

  _uploadSteps(event) {
    event.preventDefault();
    this.setState({ saved: true });
    const formData = new FormData();
    formData.append("steps", this.state.steps);
    axiosAjax({
      method: "post",
      url: "/training/steps/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Steps saved, keep it up!");
        this._loadSteps();
      })
      .catch(error => {
        this.setState({ saved: false });
      });
  }

  render() {
    return (
      <React.Fragment>
        <div className='mb-4 mt-4 d-flex justify-content-between align-items-center'>
          <span>
            <h6 className='m-0 section-heading'>NeatUp247</h6>
            <p className='m-0 text-muted p-xs'>
              Add your steps to the leaderboard.
            </p>
          </span>
        </div>
        <div className='list-group-wrapper'>
          <div className='mt-2 list-group list-group-flush rounded-corners bg-white shadow-sm p-3'>
            <div className='list-group-item'>
              <form onSubmit={this._uploadSteps}>
                <div className='form-row'>
                  <div className='col-8 text-center'>
                    <input
                      autoComplete='off'
                      pattern='\d*'
                      type='number'
                      step='any'
                      style={{
                        backgroundColor: "#F1F1F6",
                        borderColor: "#F1F1F6",
                      }}
                      onChange={this.handleInputChange}
                      value={this.state.steps}
                      name='steps'
                      className='form-control form-control-rounded text-center'
                      placeholder="Today's step count..."
                      required
                    />
                  </div>

                  <div className='col-4'>
                    {this.state.saved === true || this.state.todaysSteps ? (
                      <button
                        type='button'
                        id='formButton'
                        className='btn btn-block btn-rounded btn-success'
                      >
                        Added
                      </button>
                    ) : (
                      <button
                        type='submit'
                        id='formButton'
                        className='btn btn-block btn-rounded btn-primary'
                      >
                        Add
                      </button>
                    )}
                  </div>
                </div>
              </form>
            </div>
            {this.state.stepsEntries !== undefined ? (
              this.state.stepsEntries.length > 0 ? (
                this.state.stepsEntries
                  .slice(0, this.state.displayLimit)
                  .map((entry, key) => (
                    <div
                      key={key}
                      className='list-group-item d-flex justify-content-between align-items-center animated fadeIn'
                    >
                      {entry.profile.image ? (
                        <span>
                          <img
                            className='rounded-circle'
                            src={entry.profile.image}
                            height='40'
                            width='40'
                          />{" "}
                          &nbsp; <span>{entry.profile.user.first_name}</span>
                        </span>
                      ) : (
                        <span>
                          <img
                            className='rounded-circle'
                            src='https://d3qqgqjfawam42.cloudfront.net/profile-pic-black.svg'
                            height='40'
                            width='40'
                          />{" "}
                          &nbsp;
                          <span>{entry.profile.user.first_name}</span>
                        </span>
                      )}
                      <p className='m-0'>
                        {entry.steps} &nbsp;{" "}
                        <i className='fal fa-shoe-prints' />
                      </p>
                    </div>
                  ))
              ) : null
            ) : (
              <div
                className='text-center list-group-item'
                style={{ paddingTop: "5%", paddingBottom: "5%" }}
              >
                <MDSpinner singleColor='#4a4090' />
              </div>
            )}
            {this.state.stepsEntries !== undefined &&
              this.state.stepsEntries.length > 3 &&
              this.state.stepsEntries.length !== this.state.displayLimit && (
                <div className='list-group-item text-center'>
                  <button
                    className='btn btn-sm btn-rounded btn-link'
                    onClick={() =>
                      this.setState({
                        displayLimit: this.state.stepsEntries.length,
                      })
                    }
                  >
                    Show more
                  </button>
                </div>
              )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default NeatUp247;
