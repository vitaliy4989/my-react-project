import React from "react";
import axiosAjax from "../../../../../config/axiosAjax";

export default class Exercise extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.exercise.id,
      name: this.props.exercise.name ? this.props.exercise.name : "",
      bodyPart: this.props.exercise.body_part
        ? this.props.exercise.body_part
        : "",
      link: this.props.exercise.link ? this.props.exercise.link : "",
      updated: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._updateExercise = this._updateExercise.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.name !== this.state.name) {
      this.setState({ updated: false });
    }
    if (prevState.bodyPart !== this.state.bodyPart) {
      this.setState({ updated: false });
    }
    if (prevState.link !== this.state.link) {
      this.setState({ updated: false });
    }
    if (this.props.exercise.id !== prevProps.exercise.id) {
      this.setState({
        name: this.props.exercise.name ? this.props.exercise.name : "",
        bodyPart: this.props.exercise.body_part
          ? this.props.exercise.body_part
          : "",
        link: this.props.exercise.link ? this.props.exercise.link : "",
        updated: false,
      });
    }
  }

  _deleteExercise() {
    var r = confirm("Delete exercise?");
    if (r == true) {
      axiosAjax
        .delete("/training/saved-exercise/" + this.props.exercise.id + "/")
        .then(response => {
          this.props.updateExerciseList();
        });
    }
  }

  _updateExercise(event) {
    this.setState({ updated: true });
    event.preventDefault();
    const formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("body_part", this.state.bodyPart);
    formData.append("link", this.state.link);
    axiosAjax({
      method: "patch",
      url: "/training/saved-exercise/" + this.props.exercise.id + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({ updated: true });
      })
      .catch(error => {
        this.setState({ updated: false });
      });
  }

  render() {
    return (
      <div className='list-group-item'>
        <div className='row d-flex justify-content-between align-items-center'>
          <div className='col'>
            <input
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.name}
              name='name'
              type='text'
              className='form-control'
              placeholder='Name'
            />
          </div>
          <div className='col'>
            <input
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.bodyPart}
              name='bodyPart'
              type='text'
              className='form-control'
              placeholder='Body part'
            />
          </div>
          <div className='col'>
            <input
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.link}
              name='link'
              type='text'
              className='form-control'
              placeholder='Link'
            />
          </div>
          <div className='col'>
            {this.state.updated === true ? (
              <button
                onClick={this._updateExercise}
                style={{
                  background: "#4c9064",
                  border: "none",
                }}
                className='btn waves-effect btn-block btn-light'
              >
                <i className='fas white fa-check' />
              </button>
            ) : (
              <button
                onClick={this._updateExercise}
                className='btn waves-effect btn-light btn-block'
              >
                <i className='fas fa-check' />
              </button>
            )}
          </div>
          <div className='col'>
            <button
              onClick={() => this._deleteExercise()}
              className='btn btn-outline-danger btn-block'
            >
              <i className='fas fa-times' />
            </button>
          </div>
        </div>
      </div>
    );
  }
}
