import React from "react";
import axiosAjax from "../../../../../config/axiosAjax";
import Exercise from "./Exercise";
import MDSpinner from "react-md-spinner";
import { NotificationContainer } from "react-notifications";
import { NavLink } from "react-router-dom";

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      exercises: null,
      searching: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this._updateExerciseList = this._updateExerciseList.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ searching: true });
    axiosAjax
      .get("/training/saved-exercise/", {
        params: {
          name: this.state.search,
        },
      })
      .then(response => {
        this.setState({
          exercises: response.data,
        });
      })
      .catch(error => {
        this.setState({
          exercises: [],
        });
      });
  }

  _updateExerciseList() {
    this.handleSubmit(event);
  }

  render() {
    return (
      <React.Fragment>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col'>
                <h4>Exercise Program Requests</h4>
              </div>
            </div>
            <div className='row' style={{ marginTop: "1rem" }}>
              <div className='col-auto'>
                <NavLink
                  className='btn btn-link white text-monospace p-smaller'
                  to='/requests'
                >
                  REQUESTS
                </NavLink>
              </div>
              <div className='col-auto'>
                <NavLink
                  className='btn btn-link white text-monospace p-smaller'
                  to='/manage/days'
                >
                  MANAGE DAYS
                </NavLink>
              </div>
              <div className='col-auto'>
                <NavLink
                  className='btn btn-link white text-monospace p-smaller'
                  to='/manage/exercises'
                >
                  <u>MANAGE EXERCISES</u>
                </NavLink>
              </div>
            </div>
          </div>
        </div>
        <div className='container'>
          <div className='content-container'>
            <form onSubmit={this.handleSubmit}>
              <div className='form-row'>
                <div className='col-10'>
                  <input
                    autoComplete='off'
                    onChange={this.handleInputChange}
                    value={this.state.search}
                    name='search'
                    type='text'
                    className='form-control'
                    placeholder='Search exercises...'
                    aria-describedby='basic-addon1'
                  />
                </div>
                <div className='col-2'>
                  <button className='btn btn-block btn-primary'>
                    <i className='far fa-search' />
                  </button>
                </div>
              </div>
            </form>
          </div>
          {this.state.searching === true ? (
            this.state.exercises !== null ? (
              this.state.exercises.length > 0 ? (
                <div className='list-group list-group-flush list-wrapper animated fadeIn'>
                  <div className='list-group-item list-group-heading d-flex justify-content-between align-items-center'>
                    <h5 className='p-0 m-0'>Exercises</h5>
                  </div>
                  {this.state.exercises.map((exercise, key) => (
                    <Exercise
                      key={key}
                      exercise={exercise}
                      updateExerciseList={this._updateExerciseList}
                    />
                  ))}
                </div>
              ) : (
                <div className='alert alert-light text-center'>
                  No exercises match that search.
                </div>
              )
            ) : (
              <div
                className='text-center'
                style={{ paddingTop: "10%", paddingBottom: "10%" }}
              >
                <MDSpinner
                  color1='#4a4090'
                  color2='#4a4090'
                  color3='#4a4090'
                  color4='#4a4090'
                />
              </div>
            )
          ) : null}
        </div>
      </React.Fragment>
    );
  }
}
