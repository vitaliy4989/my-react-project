import React from "react";
import axiosAjax from "../../../../../config/axiosAjax";
import SavedDays from "../../SavedDays";
import Day from "../../Day";
import MDSpinner from "react-md-spinner";

export default class Program extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      days: null,
    };
    this.updateDays = this.updateDays.bind(this);
  }

  componentDidMount() {
    this._loadTrainingDays();
  }

  _loadTrainingDays() {
    axiosAjax
      .get("/training/coach-training-day/" + this.props.id)
      .then(response => {
        this.setState({
          days: response.data,
        });
      })
      .catch(error => {
        this.setState({
          days: [],
        });
      });
  }

  updateDays(event) {
    this._loadTrainingDays();
  }

  _deleteProgram() {
    var r = confirm("Do you want to delete this program?!");
    if (r == true) {
      axiosAjax.delete("/training/training/" + this.props.id).then(response => {
        this.props.reloadPrograms(event);
      });
    } else {
      console.log("not deleted");
    }
  }

  render() {
    return (
      <div className='content-container'>
        <div className='d-flex justify-content-between align-items-center'>
          <div style={greyPill}>
            <h6 className='m-0'>Program {this.props.number + 1} for: </h6>
          </div>
          <div style={greyPill}>
            <p className='m-0 text-muted'>
              <strong>Gender</strong> {this.props.program.gender}
            </p>
          </div>
          <div style={greyPill}>
            <p className='m-0 text-muted'>
              <strong>Years Experience</strong>{" "}
              {this.props.program.training_age}
            </p>
          </div>
          <div style={greyPill}>
            <p className='m-0 text-muted'>
              <strong>Frequency</strong> {this.props.program.frequency}
            </p>
          </div>
          <div style={greyPill}>
            <p className='m-0 text-muted'>
              <strong>Facility</strong> {this.props.program.facility}
            </p>
          </div>
          <div>
            <button
              onClick={() => this._deleteProgram()}
              className='btn btn-sm btn-rounded btn-danger'
            >
              Delete
            </button>
          </div>
        </div>
        <hr />
        <SavedDays updateDays={this.updateDays} programId={this.props.id} />
        <br />
        {this.state.days !== null ? (
          this.state.days.length > 0 ? (
            this.state.days.map((day, key) => (
              <Day
                updateDays={this.updateDays}
                key={key}
                day={day}
                dayManager={false}
              />
            ))
          ) : (
            <div className='alert alert-light text-center'>
              Add a training day by search days above or by creating a new day.
            </div>
          )
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "10%", paddingBottom: "10%" }}
          >
            <MDSpinner
              color1='#4a4090'
              color2='#4a4090'
              color3='#4a4090'
              color4='#4a4090'
            />
          </div>
        )}
      </div>
    );
  }
}

const greyPill = {
  fontSize: ".871rem",
  padding: "3px 15px",
  textAlign: "center",
  borderRadius: "20px",
  background: "#f8f8f8",
};
