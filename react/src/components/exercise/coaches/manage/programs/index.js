import React from "react";
import CreateProgram from "./CreateProgram";
import axiosAjax from "../../../../../config/axiosAjax";
import Program from "./Program";
import MDSpinner from "react-md-spinner";

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      createProgram: false,
      goal: "",
      programs: null,
      search: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    window.scrollTo(0, 0);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    this.setState({
      search: true,
    });
    event.preventDefault();
    axiosAjax
      .get("/training/training/" + this.state.goal + "/")
      .then(response => {
        this.setState({
          programs: response.data,
        });
      })
      .catch(error => {
        this.setState({
          programs: [],
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Auto Generate Programs</h4>
              </div>
              <div className='col-auto'>
                <button
                  onClick={() =>
                    this.setState({ createProgram: !this.state.createProgram })
                  }
                  className='btn btn-rounded btn-white-menu'
                >
                  {this.state.createProgram === true
                    ? "Done"
                    : "Create Program"}
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className='container'>
          {this.state.createProgram === true && <CreateProgram />}
          {this.state.createProgram === false && (
            <div>
              <form onSubmit={this.handleSubmit} className='content-container'>
                <div className='form-row'>
                  <div className='col-10'>
                    <select
                      onChange={this.handleInputChange}
                      value={this.state.goal}
                      name='goal'
                      className=' custom-select form-control form-control-single-border'
                      required
                    >
                      <option>--Search By Goal--</option>
                      <option value='Fat loss'>Fat loss</option>
                      <option value='Muscle gain'>Muscle gain</option>
                      <option value='Body recomp'>Body recomp</option>
                      <option value='Other'>Other</option>
                    </select>
                  </div>
                  <div className='col-2'>
                    <button className='btn btn-block btn-primary'>
                      <i className='far fa-search' />
                    </button>
                  </div>
                </div>
              </form>
              {this.state.search === true ? (
                this.state.programs !== null ? (
                  this.state.programs.length > 0 ? (
                    this.state.programs.map((program, key) => (
                      <Program
                        reloadPrograms={this.handleSubmit}
                        key={key}
                        program={program}
                        id={program.id}
                        number={key}
                      />
                    ))
                  ) : (
                    <div
                      className='alert alert-light text-center'
                      style={{ marginTop: 25 }}
                    >
                      There are no programs under this goal, create one above.
                    </div>
                  )
                ) : (
                  <div
                    className='text-center'
                    style={{ paddingTop: "10%", paddingBottom: "10%" }}
                  >
                    <MDSpinner
                      color1='#4a4090'
                      color2='#4a4090'
                      color3='#4a4090'
                      color4='#4a4090'
                    />
                  </div>
                )
              ) : (
                <div
                  className='alert alert-light text-center'
                  style={{ marginTop: 25 }}
                >
                  Search programs above by goal.
                </div>
              )}
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}
