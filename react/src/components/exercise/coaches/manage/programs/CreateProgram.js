import React from "react";
import axiosAjax from "../../../../../config/axiosAjax";
import SavedDays from "../../SavedDays";
import Day from "../../Day";

export default class CreateProgram extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: "",
      age: "",
      experience: "",
      goal: "",
      frequency: "",
      facility: "",
      programId: null,
      days: [],
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateDays = this.updateDays.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _loadTrainingDays() {
    axiosAjax
      .get("/training/coach-training-day/" + this.state.programId)
      .then(response => {
        this.setState({
          days: response.data,
        });
      })
      .catch(error => {
        this.setState({
          days: [],
        });
      });
  }

  updateDays(event) {
    this._loadTrainingDays();
  }

  handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("gender", this.state.gender);
    formData.append("training_age", this.state.experience);
    formData.append("goal", this.state.goal);
    formData.append("frequency", this.state.frequency);
    formData.append("facility", this.state.facility);
    axiosAjax({
      method: "post",
      url: "/training/training/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          programId: response.data.id,
        });
      })
      .catch(error => {});
  }

  render() {
    return (
      <div className='content-container'>
        <form onSubmit={this.handleSubmit}>
          <div className='form-row'>
            <div className='col-lg form-group'>
              <label htmlFor='gender'>Gender</label>
              <select
                onChange={this.handleInputChange}
                value={this.state.gender}
                name='gender'
                className='custom-select form-control form-control-single-border'
                required
              >
                <option>--Gender--</option>
                <option>Male</option>
                <option>Female</option>
              </select>
            </div>
            <div className='col-lg form-group'>
              <label htmlFor='training_age'>Years training experience</label>
              <select
                onChange={this.handleInputChange}
                value={this.state.experience}
                name='experience'
                className=' custom-select form-control form-control-single-border'
                required
              >
                <option>--Experience--</option>
                <option value='Less than 1'>Less than 1</option>
                <option value='1-2'>1-2</option>
                <option value='2-3'>2-3</option>
                <option value='3-4'>3-4</option>
                <option value='4+'>4+</option>
              </select>
            </div>
          </div>

          <div className='form-row'>
            <div className='col-lg form-group'>
              <label htmlFor='goal'>General goal</label>
              <select
                onChange={this.handleInputChange}
                value={this.state.goal}
                name='goal'
                className=' custom-select form-control form-control-single-border'
                required
              >
                <option>--General goal--</option>
                <option value='Fat loss'>Fat loss</option>
                <option value='Muscle gain'>Muscle gain</option>
                <option value='Body recomp'>Body recomp</option>
                <option value='Other'>Other</option>
              </select>
            </div>
            <div className='col-lg form-group'>
              <label htmlFor='frequency'>Training frequency</label>
              <select
                onChange={this.handleInputChange}
                value={this.state.frequency}
                name='frequency'
                className=' custom-select form-control form-control-single-border'
                required
              >
                <option>--Training frequency--</option>
                <option value='1 - week'>1 - week</option>
                <option value='2 - week'>2 - week</option>
                <option value='3 - week'>3 - week</option>
                <option value='+3 - week'>+3 - week</option>
              </select>
            </div>
          </div>

          <div className='form-row'>
            <div className='col-lg form-group'>
              <label htmlFor='facility'>Training facility</label>
              <select
                onChange={this.handleInputChange}
                value={this.state.facility}
                name='facility'
                className=' custom-select form-control form-control-single-border'
                required
              >
                <option>--Training facility--</option>
                <option value='home'>Home</option>
                <option value='gym'>Gym</option>
                <option value='outside'>Outside</option>
              </select>
            </div>
          </div>
          {this.state.programId === null && (
            <div className='text-center'>
              <button className='btn btn-primary btn-rounded enter-button-centered'>
                Enter
              </button>
            </div>
          )}
        </form>
        {this.state.programId !== null && (
          <div className='animated-2 fadeIn' style={{ marginTop: "1rem" }}>
            <h6>Now add training days</h6>
            <SavedDays
              updateDays={this.updateDays}
              programId={this.state.programId}
            />
            <br />
            {this.state.days.length > 0 ? (
              this.state.days.map((day, key) => (
                <Day
                  updateDays={this.updateDays}
                  key={key}
                  day={day}
                  dayManager={false}
                />
              ))
            ) : (
              <div className='alert alert-light text-center'>
                Add a training day by search days above or by creating a new
                day.
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}
