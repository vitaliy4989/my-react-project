import React, { PureComponent } from "react";
import axiosAjax from "../../../config/axiosAjax";

class DayNotes extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      day: this.props.day,
      notes: this.props.day.notes,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createNotes = this.createNotes.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      day: nextProps.day,
      notes: nextProps.day.notes,
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  createNotes(event) {
    event.preventDefault();
    this.setState({ sending: true });
    document.getElementById("formButton").disabled = true;
    const formData = new FormData();
    formData.append("notes", this.state.notes);
    axiosAjax({
      method: "patch",
      url: "/training/training-day/" + this.state.day.id + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        alert("Notes updated!");
      })
      .catch(function(error) {
        console.log(error);
      });
    document.getElementById("formButton").disabled = false;
    this.setState({ sending: false });
  }

  render() {
    return (
      <div style={dayNotesContainer}>
        <form onSubmit={this.createNotes}>
          <div className='form-group'>
            <textarea
              className='form-control'
              placeholder='Write any session notes here...'
              style={textArea}
              onChange={this.handleInputChange}
              value={this.state.notes}
              name='notes'
            />
          </div>
          <div className='form-group'>
            <button
              id='formButton'
              className='btn btn-sm btn-primary btn-rounded'
              type='submit'
            >
              Update Notes
            </button>
          </div>
        </form>
      </div>
    );
  }
}

const dayNotesContainer = {
  marginTop: 15,
};

const textArea = {
  height: 100,
  whiteSpace: "pre-wrap",
};

export default DayNotes;
