import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import Messages from "./Messages";
import Program from "./Program";

class Request extends Component {
  constructor(props) {
    super(props);
    this.state = {
      program: [],
      user: "",
      showMessages: false,
      showProgram: true,
      showForm: false,
      updated: false,
      previousRequests: 0,
      otherPrograms: [],
      getCredit: false,
    };
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    axiosAjax
      .get("/training/coach-training/" + this.props.match.params.id)
      .then(response => {
        this.setState({
          program: response.data,
          user: response.data.user,
        });
      });
    axiosAjax
      .get("/training/search-previous-requests/" + this.props.match.params.id)
      .then(response => {
        this.setState({
          previousRequests: response.data.program_count,
          otherPrograms: response.data.training_programs,
        });
      });
  }

  markUpdated() {
    this.setState({
      updated: true,
    });
    const formData = new FormData();
    formData.append("id", this.props.match.params.id);
    axiosAjax({
      method: "post",
      url: "/training/coach-finished-training/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          updated: true,
        });
      })
      .catch(error => {
        this.setState({
          updated: false,
        });
      });
  }

  _createProgramCredit() {
    this.setState({
      getCredit: true,
    });
    const formData = new FormData();
    formData.append("id", this.props.match.params.id);
    axiosAjax({
      method: "post",
      url: "/training/program-credit/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          getCredit: true,
        });
      })
      .catch(error => {
        this.setState({
          getCredit: false,
        });
      });
  }

  deleteOldProgram() {
    var r = confirm("Delete old file based program?");
    if (r == true) {
      const formData = new FormData();
      formData.append("file", "");
      axiosAjax({
        method: "patch",
        url: "/training/coach-training/" + this.props.match.params.id + "/",
        headers: { "Content-Type": "multipart/form-data" },
        data: formData,
      })
        .then(response => {
          location.reload();
        })
        .catch(function(error) {
          console.log(error);
        });
    }
  }

  _autoGeneratePrograms() {
    axiosAjax
      .get("/training/assign-training/" + this.props.match.params.id + "/")
      .then(response => {
        alert(response.data.message);
        location.reload();
      });
  }

  render() {
    return (
      <React.Fragment>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row d-flex justify-content-between align-items-center'>
              <div className='col'>
                <h4 className='m-0'>{this.state.user.username}</h4>
                <p className='p-smaller'>{this.state.user.first_name}</p>
              </div>
              <div className='col-auto'>
                {this.state.getCredit === true ? (
                  <button
                    style={{ background: "#4c9064", color: "white" }}
                    className='btn btn-xs btn-rounded'
                  >
                    <i className='fas white fa-dollar-sign' /> &nbsp; 2.3
                  </button>
                ) : (
                  <button
                    onClick={() => this._createProgramCredit()}
                    className='btn btn-xs btn-rounded btn-white-menu'
                  >
                    <i className='fas fa-dollar-sign' /> &nbsp; Get Credit
                  </button>
                )}
              </div>
              <div className='col-auto'>
                {this.state.updated === true ? (
                  <button
                    style={{ background: "#4c9064", color: "white" }}
                    className='btn btn-xs btn-rounded'
                  >
                    <i className='far fa-check white' /> &nbsp; Program Updated
                  </button>
                ) : (
                  <button
                    onClick={() => this.markUpdated()}
                    className='btn btn-xs btn-rounded btn-white-menu'
                  >
                    <i className='fas fa-bell' /> &nbsp; Notify Of Update
                  </button>
                )}
              </div>
            </div>
            <div className='row' style={{ marginTop: "1rem" }}>
              <div className='col-auto'>
                <button
                  style={
                    this.state.showProgram === true
                      ? { textDecoration: "underline" }
                      : {}
                  }
                  className='btn btn-link white text-monospace p-smaller'
                  onClick={() =>
                    this.setState({
                      showMessages: false,
                      showProgram: true,
                      showForm: false,
                    })
                  }
                >
                  PROGRAM
                </button>
              </div>
              <div className='col-auto'>
                <button
                  style={
                    this.state.showMessages === true
                      ? { textDecoration: "underline" }
                      : {}
                  }
                  className='btn btn-link white text-monospace p-smaller'
                  onClick={() =>
                    this.setState({
                      showMessages: true,
                      showProgram: false,
                      showForm: false,
                    })
                  }
                >
                  MESSAGES
                </button>
              </div>
              <div className='col-auto'>
                <button
                  style={
                    this.state.showForm === true
                      ? { textDecoration: "underline" }
                      : {}
                  }
                  className='btn btn-link white text-monospace p-smaller'
                  onClick={() =>
                    this.setState({
                      showMessages: false,
                      showProgram: false,
                      showForm: true,
                    })
                  }
                >
                  REQUEST FORM
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className='container h-100'>
          {this.state.showMessages === true && (
            <Messages programId={this.props.match.params.id} />
          )}
          {this.state.showProgram === true && (
            <div>
              {this.state.previousRequests > 0 && (
                <div className='content-container'>
                  <table className='table'>
                    <thead>
                      <tr>
                        <th scope='col'>#</th>
                        <th scope='col'>Previous Programs</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.otherPrograms.length > 0 &&
                        this.state.otherPrograms.map((program, key) => (
                          <tr key={key}>
                            <td>{key + 1}</td>
                            <td>
                              <a
                                key={key}
                                target='_blank'
                                href={"/coaches/#/request/" + program.id}
                              >
                                {program.goal}
                              </a>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                  {this.state.program.file && (
                    <div className='col'>
                      <p>
                        <small>
                          This user still has an old file based program. Please
                          delete it when you have switched them to new training
                          system. <a href={this.state.program.file}>Download</a>{" "}
                          their old training program.{" "}
                          <u
                            style={{ cursor: "pointer" }}
                            onClick={() => this.deleteOldProgram()}
                            className='red'
                          >
                            Delete Old Program
                          </u>
                        </small>
                      </p>
                    </div>
                  )}
                </div>
              )}

              <Program programId={this.props.match.params.id} />
            </div>
          )}
          {this.state.showForm === true && (
            <div className='content-container'>
              <h5>Request Form</h5>
              <div className='row'>
                <div className='col'>
                  <label>Medical Issues</label>
                  <p className='text-capitalize'>
                    {this.state.program.medical}
                  </p>
                </div>
                <div className='col'>
                  <label>Age</label>
                  <p className='text-capitalize'>{this.state.program.age}</p>
                </div>
                <div className='col'>
                  <label>Gender</label>
                  <p className='text-capitalize'>{this.state.program.gender}</p>
                </div>
              </div>
              <div className='row'>
                <div className='col'>
                  <label>Goal</label>
                  <p className='text-capitalize'>
                    <mark>{this.state.program.goal}</mark>
                  </p>
                </div>
                <div className='col'>
                  <label>Experience</label>
                  <p className='text-capitalize'>
                    <mark>{this.state.program.training_age} years</mark>
                  </p>
                </div>
                <div className='col'>
                  <label>Frequency</label>
                  <p className='text-capitalize'>
                    <mark>{this.state.program.frequency}</mark>
                  </p>
                </div>
                <div className='col'>
                  <label>Facility</label>
                  <p className='text-capitalize'>
                    <mark>{this.state.program.facility}</mark>
                  </p>
                </div>
              </div>
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

const filterButton = {
  background: "transparent",
  borderRadius: 2,
  outline: "0 none",
  boxShadow: "none",
};

const filterButtonActive = {
  borderBottom: "2px solid #4a4090",
  background: "transparent",
  borderRadius: 2,
  color: "#4a4090",
  outline: "0 none",
  boxShadow: "none",
};

export default Request;
