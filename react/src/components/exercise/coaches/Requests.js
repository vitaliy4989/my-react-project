import React, { PureComponent } from "react";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import { NavLink } from "react-router-dom";

class Requests extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      requests: undefined,
      waitingRequests: [],
      requestsByCoach: [],
      search: "",
      showTable: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _loadRequests() {
    axiosAjax.get("/training/coach-training/").then(response => {
      this.setState({
        requests: response.data,
      });
    });
  }

  _updateWaitingRequests() {
    axiosAjax.get("/training/training-requests-search/").then(response => {
      this.setState({
        showTable: true,
        waitingRequests: response.data.requests,
        requestsByCoach: response.data.completed_by_coach,
      });
    });
  }

  componentDidMount() {
    this._loadRequests();
  }

  handleSubmit(event) {
    event.preventDefault();
    axiosAjax
      .get("/training/training-requests-search/" + this.state.search)
      .then(response => {
        this.setState({
          requests: response.data,
        });
      })
      .catch(error => {
        this.setState({
          requests: [],
        });
      });
  }

  deleteRequest(id) {
    var r = confirm("Do you want to delete this program?!");
    if (r == true) {
      axiosAjax.delete("/training/coach-training/" + id).then(response => {
        this._loadRequests();
      });
    } else {
      console.log("not deleted");
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col'>
                <h4>Exercise Program Requests</h4>
              </div>
            </div>
            <div className='row' style={{ marginTop: "1rem" }}>
              <div className='col-auto'>
                <button className='btn btn-link white text-monospace p-smaller'>
                  <u>REQUESTS</u>
                </button>
              </div>
              <div className='col-auto'>
                <button
                  className='btn btn-link white text-monospace p-smaller'
                  onClick={() => this._updateWaitingRequests()}
                >
                  REQUESTS TABLE
                </button>
              </div>
              <div className='col-auto'>
                <NavLink
                  className='btn btn-link white text-monospace p-smaller'
                  to='/manage/days'
                >
                  MANAGE DAYS
                </NavLink>
              </div>
              <div className='col-auto'>
                <NavLink
                  className='btn btn-link white text-monospace p-smaller'
                  to='/manage/exercises'
                >
                  MANAGE EXERCISES
                </NavLink>
              </div>
            </div>
          </div>
        </div>
        <div className='container'>
          {this.state.showTable === true ? (
            <div className='content-container'>
              <div className='row' style={{ marginTop: "1rem" }}>
                <div className='col'>
                  <table className='table '>
                    <thead className='thead-light'>
                      <tr>
                        <th scope='col'>Date</th>
                        <th scope='col'>Remaining requests</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.waitingRequests.length > 0
                        ? this.state.waitingRequests.map((request, key) => (
                            <tr key={key}>
                              <td>{request.date}</td>
                              <td>{request.date_count}</td>
                            </tr>
                          ))
                        : null}
                    </tbody>
                  </table>
                </div>
              </div>

              <div className='row' style={{ marginTop: "1rem" }}>
                <div className='col'>
                  <table className='table'>
                    <thead className='thead-light'>
                      <tr>
                        <th scope='col'>Coach</th>
                        <th scope='col'>Requests completed by date</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.requestsByCoach.map((coach, key) =>
                        coach.completed_today.length > 0 ? (
                          <tr key={key}>
                            <td>
                              <img
                                className='mr-3 small-profile-wrapper rounded-circle'
                                src={coach.image + "-/autorotate/yes/"}
                              />
                            </td>
                            <td>
                              <div
                                style={{
                                  maxHeight: 75,
                                  overflowX: "hidden",
                                  overflowY: "scroll",
                                }}
                              >
                                <table className='table table-sm'>
                                  <thead className='thead-light'>
                                    <tr>
                                      <th scope='col'>Date</th>
                                      <th scope='col'>Requests</th>
                                      <th scope='col'>Billable</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {coach.completed_today.map((day, key) => (
                                      <tr key={key}>
                                        <td>{day.date}</td>
                                        <td>{day.date_count}</td>
                                        <td>
                                          ${(day.date_count * 2.3).toFixed(2)}
                                        </td>
                                      </tr>
                                    ))}
                                  </tbody>
                                </table>
                              </div>
                            </td>
                          </tr>
                        ) : null,
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          ) : null}
          <div className='content-container'>
            <form onSubmit={this.handleSubmit}>
              <div className='form-row'>
                <div className='col-10'>
                  <input
                    autoComplete='off'
                    onChange={this.handleInputChange}
                    value={this.state.search}
                    name='search'
                    type='text'
                    className='form-control form-control-rounded'
                    placeholder='Search usernames...'
                    aria-describedby='basic-addon1'
                  />
                </div>
                <div className='col-2'>
                  <button className='btn btn-block btn-primary btn-rounded'>
                    <i className='far fa-search' />
                  </button>
                </div>
              </div>
            </form>
            <br />
            <button
              className='btn btn-sm btn-primary'
              onClick={() => location.reload()}
            >
              Reload Requests <i className='far fa-sync-alt' />
            </button>
          </div>
          <div className='list-group list-group-flush list-wrapper'>
            {this.state.requests !== undefined ? (
              this.state.requests.length > 0 &&
              this.state.requests.map((request, key) => (
                <div
                  key={key}
                  className='list-group-item list-group-item-action'
                >
                  <div className='row d-flex justify-content-between align-items-center'>
                    <NavLink className='col' to={"/request/" + request.id}>
                      {request.user.username}
                      <br />
                      <span className='text-muted' style={{ fontSize: 10 }}>
                        {request.date.substring(0, request.date.indexOf("T"))}
                      </span>
                    </NavLink>

                    <div className='col-auto'>
                      <button
                        type='button'
                        className='btn btn-sm btn-block btn-outline-danger'
                        onClick={() => this.deleteRequest(request.id)}
                      >
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <div
                style={{ paddingTop: "10%", paddingBottom: "10%" }}
                className='text-center'
              >
                <MDSpinner
                  color1='#4a4090'
                  color2='#4a4090'
                  color3='#4a4090'
                  color4='#4a4090'
                />
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const crossIcon = {
  cursor: "pointer",
};

const searchIcon = {
  border: "none",
};

const searchInput = {
  borderLeft: "none",
};

export default Requests;
