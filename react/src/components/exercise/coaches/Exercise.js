import React, { Component } from "react";
import Sets from "./Sets";
import axiosAjax from "../../../config/axiosAjax";

class Exercise extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSets: false,
      updateOrder: false,
      order: this.props.exercise.order,
      superset: this.props.exercise.superset,
      supersetGroup: this.props.exercise.superset_group,
    };
    this.updateExercise = this.updateExercise.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.exercise.id !== this.props.exercise.id) {
      this.setState({
        superset: this.props.exercise.superset,
        order: this.props.exercise.order,
        supersetGroup: this.props.exercise.superset_group,
      });
    }
    if (
      this.props.exercise.superset_group !== prevProps.exercise.superset_group
    ) {
      this.setState({
        supersetGroup: this.props.exercise.superset_group,
      });
    }
  }

  deleteExercise(id) {
    var r = confirm("Do you want to delete this exercise?!");
    if (r == true) {
      axiosAjax.delete("/training/training-exercise/" + id).then(response => {
        this.props.updateExercises();
      });
    }
  }

  updateExercise(event) {
    event.preventDefault();
    this.setState({ sending: true });
    const formData = new FormData();
    formData.append("order", this.state.order);
    axiosAjax({
      method: "patch",
      url: "/training/training-exercise/" + this.props.exercise.id + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.updateExercises();
      })
      .catch(function(error) {
        console.log(error);
      });
    this.setState({ sending: false });
  }

  render() {
    return (
      <div className='mt-3 mb-3 row no-gutters'>
        {this.state.supersetGroup && (
          <div
            className='col-auto'
            style={{
              width: 3,
              borderRadius: 5,
              marginRight: 10,
              marginTop: 7,
              marginBottom: 7,
              display: "flex",
              flexDirection: "column",
              backgroundColor: this.state.supersetGroup.color,
            }}
          />
        )}
        <div className='col'>
          <div>
            <div className='row d-flex align-items-center'>
              <div className='col'>
                <button
                  onClick={() =>
                    this.setState({ showSets: !this.state.showSets })
                  }
                  style={{ padding: 0 }}
                  className='btn btn-link'
                >
                  <h6 className='m-0 dark-purp d-flex align-items-center'>
                    {this.props.exercise.name}
                  </h6>
                </button>
              </div>
              {this.state.updateOrder === true && (
                <div className='col'>
                  <form onSubmit={this.updateExercise}>
                    <div className='form-row'>
                      <div className='col'>
                        <input
                          onChange={this.handleInputChange}
                          value={this.state.order}
                          name='order'
                          type='number'
                          className='form-control'
                          id='name'
                          placeholder='Exercise Order...'
                          required
                        />
                      </div>
                      <div className='col'>
                        <button className='btn btn-light' id='formButton'>
                          Change
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              )}
              <div className='col-auto'>
                <div className='dropdown'>
                  <button
                    className='btn btn-sm btn-light'
                    type='button'
                    id={"exerciseId" + this.props.exercise.id}
                    data-toggle='dropdown'
                    aria-haspopup='true'
                    aria-expanded='false'
                  >
                    <i className='far fa-ellipsis-v' />
                  </button>
                  <div
                    className='dropdown-menu dropdown-menu-right'
                    aria-labelledby={"exerciseId" + this.props.exercise.id}
                  >
                    <button
                      id='orderButton'
                      onClick={() =>
                        this.setState({ updateOrder: !this.state.updateOrder })
                      }
                      className='dropdown-item'
                    >
                      <i className='far fa-sort' /> &nbsp;{" "}
                      {this.state.updateOrder === true
                        ? "Cancel"
                        : "Change Order"}
                    </button>
                    <div className='dropdown-divider' />
                    <button
                      onClick={() =>
                        this.deleteExercise(this.props.exercise.id)
                      }
                      className='dropdown-item'
                      type='button'
                    >
                      <i className='far red fa-times' /> &nbsp; Delete Exercise
                    </button>
                  </div>
                </div>
              </div>
            </div>
            {this.state.showSets === true && (
              <Sets exerciseId={this.props.exercise.id} />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Exercise;
