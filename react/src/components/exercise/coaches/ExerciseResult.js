import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";

class ExerciseResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      added: false,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.exercise.id !== prevProps.exercise.id) {
      this.setState({
        added: false,
      });
    }
  }

  createExercise() {
    this.setState({ added: true });
    const formData = new FormData();
    formData.append("training_day", this.props.dayId);
    formData.append("name", this.props.exercise.name);
    formData.append("link", this.props.exercise.link);
    formData.append("order", this.props.exerciseOrder);
    axiosAjax({
      method: "post",
      url: "/training/training-exercise/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.updateExercises();
        this.setState({ added: true });
      })
      .catch(error => {
        this.setState({ added: false });
      });
  }

  deleteExercise() {
    var r = confirm("Do you want to delete this saved exercise?!");
    if (r == true) {
      axiosAjax
        .delete("/training/saved-exercise/" + this.props.exercise.id)
        .then(response => {
          this.props.refreshSearch();
        });
    }
  }

  render() {
    return (
      <div className='list-group-item d-flex justify-content-between align-items-center'>
        <div>
          <p className='m-0'>{this.props.exercise.name}</p>
          <p className='m-0 p-smaller text-muted'>
            {this.props.exercise.body_part}
          </p>
          <a
            target='_blank'
            className='p-smaller'
            href={this.props.exercise.link}
          >
            {this.props.exercise.link}
          </a>
        </div>
        <div>
          {this.state.added === true ? (
            <button
              id='formButton'
              style={{ background: "#4e9166" }}
              className='btn btn-round float-right'
            >
              <i className='far fa-check white' />
            </button>
          ) : (
            <button
              id='formButton'
              onClick={() => this.createExercise()}
              className='btn btn-round btn-light float-right'
            >
              <i className='far fa-plus' />
            </button>
          )}
        </div>
      </div>
    );
  }
}

export default ExerciseResult;
