import React, { Component } from "react";
import ExerciseResult from "./ExerciseResult";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";

class SearchExercises extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      exercises: undefined,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.refreshSearch = this.refreshSearch.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    axiosAjax
      .get("/training/saved-exercise", {
        params: {
          name: this.state.name,
        },
      })
      .then(response => {
        this.setState({
          exercises: response.data,
        });
      })
      .catch(error => {
        this.setState({
          exercises: [],
        });
      });
  }

  refreshSearch(event) {
    axiosAjax
      .get("/training/saved-exercise", {
        params: {
          name: this.state.name,
        },
      })
      .then(response => {
        this.setState({
          exercises: response.data,
        });
      })
      .catch(error => {
        this.setState({
          exercises: [],
        });
      });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.name !== this.state.name) {
      this.handleSubmit(event);
    }
  }

  render() {
    return (
      <React.Fragment>
        <form>
          <div className='form-row'>
            <div className='col-12'>
              <input
                autoComplete='off'
                autoFocus='true'
                onChange={this.handleInputChange}
                style={{ fontSize: 25 }}
                value={this.state.name}
                name='name'
                type='text'
                className='form-control form-control-single-border text-center'
                placeholder='Search by name or body part...'
                aria-describedby='basic-addon1'
                required
              />
            </div>
          </div>
        </form>
        {this.state.name !== "" ? (
          this.state.exercises !== undefined ? (
            this.state.exercises.length > 0 ? (
              <div className='list-group-flush list-group'>
                <div className='list-group-item text-center'>
                  <button
                    onClick={() => this.setState({ exercises: [], name: "" })}
                    className='btn btn-rounded btn-sm btn-outline-danger'
                  >
                    Cancel Search
                  </button>
                </div>
                <div
                  style={{
                    maxHeight: 300,
                    overflowX: "hidden",
                    overflowY: "scroll",
                  }}
                >
                  {this.state.exercises.map((exercise, key) => (
                    <ExerciseResult
                      refreshSearch={this.refreshSearch}
                      key={key}
                      exerciseOrder={this.props.exerciseOrder}
                      exercise={exercise}
                      dayId={this.props.dayId}
                      updateExercises={this.props.updateExercises}
                    />
                  ))}
                </div>
              </div>
            ) : (
              <p>There are no results...</p>
            )
          ) : (
            <div className='p-5 text-center'>
              <MDSpinner singleColor='#4a4090' />
            </div>
          )
        ) : null}
      </React.Fragment>
    );
  }
}

const crossIcon = {
  cursor: "pointer",
};

const searchIcon = {
  color: "#4a4090",
  border: "none",
};

const searchInput = {
  borderLeft: "none",
};

export default SearchExercises;
