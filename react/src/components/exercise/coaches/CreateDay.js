import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";

class CreateDay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      sending: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createDay = this.createDay.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  createDay(event) {
    event.preventDefault();
    this.setState({ sending: true });
    document.getElementById("formButton").disabled = true;
    const formData = new FormData();
    formData.append("training_programme", this.props.programId);
    formData.append("name", this.state.name);
    axiosAjax({
      method: "post",
      url: "/training/coach-training-day/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.updateDays();
        this.setState({
          name: "",
        });
      })
      .catch(function(error) {
        console.log(error);
      });
    document.getElementById("formButton").disabled = false;
    this.setState({ sending: false });
  }

  render() {
    return (
      <form onSubmit={this.createDay} style={{ marginTop: "2rem" }}>
        <div className='form-row'>
          <div className='col-10'>
            <input
              autoComplete='off'
              style={{ background: "white" }}
              onChange={this.handleInputChange}
              value={this.state.name}
              name='name'
              type='text'
              className='form-control form-control-rounded'
              id='name'
              placeholder='Training day name...'
              required
            />
          </div>
          <div className='col-2'>
            <button
              type='submit'
              id='formButton'
              className='btn btn-block btn-rounded btn-light'
            >
              {this.state.sending === true ? (
                <i className='fas fa-circle-notch fa-spin' />
              ) : (
                "Enter"
              )}
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default CreateDay;
