import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import Day from "./Day";
import { NavLink } from "react-router-dom";

class ManageDays extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: [],
      search: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    axiosAjax
      .get("/training/saved-training-day/", {
        params: {
          name: this.state.search,
        },
      })
      .then(response => {
        this.setState({
          days: response.data,
        });
      })
      .catch(error => {
        this.setState({
          days: [],
        });
        alert("No days matching that name.");
      });
  }

  updateSearch(event) {
    axiosAjax
      .get("/training/saved-training-day/" + this.state.search)
      .then(response => {
        this.setState({
          days: response.data,
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col'>
                <h4>Exercise Program Requests</h4>
              </div>
            </div>
            <div className='row' style={{ marginTop: "1rem" }}>
              <div className='col-auto'>
                <NavLink
                  className='btn btn-link white text-monospace p-smaller'
                  to='/requests'
                >
                  REQUESTS
                </NavLink>
              </div>
              <div className='col-auto'>
                <NavLink
                  className='btn btn-link white text-monospace p-smaller'
                  to='/manage/days'
                >
                  <u>MANAGE DAYS</u>
                </NavLink>
              </div>
              <div className='col-auto'>
                <NavLink
                  className='btn btn-link white text-monospace p-smaller'
                  to='/manage/exercises'
                >
                  MANAGE EXERCISES
                </NavLink>
              </div>
            </div>
          </div>
        </div>
        <div className='container'>
          <div className='content-container'>
            <form onSubmit={this.handleSubmit}>
              <div className='form-row'>
                <div className='col-10'>
                  <input
                    autoComplete='off'
                    onChange={this.handleInputChange}
                    value={this.state.search}
                    name='search'
                    type='text'
                    className='form-control'
                    placeholder='Search saved days...'
                    aria-describedby='basic-addon1'
                  />
                </div>
                <div className='col-2'>
                  <button className='btn btn-block btn-primary'>
                    <i className='far fa-search' />
                  </button>
                </div>
              </div>
            </form>
          </div>
          {this.state.search && this.state.days.length >= 1 && (
            <div>
              {this.state.days.map((day, key) => (
                <div key={key}>
                  <Day
                    updateDays={this.updateSearch}
                    key={key}
                    day={day}
                    dayManager={true}
                  />
                </div>
              ))}
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

const crossIcon = {
  cursor: "pointer",
};

const searchIcon = {
  background: "white",
  color: "#4a4090",
  border: "none",
};

const searchInput = {
  borderLeft: "none",
};

export default ManageDays;
