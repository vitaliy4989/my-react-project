import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import SavedDay from "./SavedDay";

class SearchDays extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: [],
      search: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    axiosAjax
      .get("/training/saved-training-day/", {
        params: {
          name: this.state.search,
        },
      })
      .then(response => {
        this.setState({
          days: response.data,
        });
      })
      .catch(error => {
        this.setState({
          days: [],
        });
      });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.search !== this.state.search) {
      this.handleSubmit(event);
    }
  }

  render() {
    return (
      <React.Fragment>
        <form style={{ marginTop: "2rem" }}>
          <div className='form-row'>
            <div className='col-12'>
              <input
                autoFocus='true'
                autoComplete='off'
                style={{ fontSize: 25 }}
                onChange={this.handleInputChange}
                value={this.state.search}
                name='search'
                type='text'
                className='form-control form-control-single-border text-center'
                placeholder='Search saved training days...'
                aria-describedby='basic-addon1'
                required
              />
            </div>
          </div>
        </form>
        {this.state.search && this.state.days.length >= 1 && (
          <div className='list-group-flush list-group'>
            <div className='list-group-item text-center'>
              <button
                onClick={() => this.setState({ days: [], search: "" })}
                className='btn btn-rounded btn-sm btn-outline-danger'
              >
                Cancel Search
              </button>
            </div>
            <div
              style={{
                maxHeight: 300,
                overflowX: "hidden",
                overflowY: "scroll",
              }}
            >
              {this.state.days.map((day, key) => (
                <div key={key}>
                  <SavedDay
                    key={key}
                    updateDays={this.props.updateDays}
                    programId={this.props.programId}
                    day={day}
                  />
                </div>
              ))}
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

const crossIcon = {
  cursor: "pointer",
};

const searchIcon = {
  color: "#4a4090",
  border: "none",
};

const searchInput = {
  borderLeft: "none",
};

export default SearchDays;
