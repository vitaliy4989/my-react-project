import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";

class CreateExercise extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      sending: false,
      link: "",
      order: "",
      bodyPart: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createExercise = this.createExercise.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  createExercise(event) {
    event.preventDefault();
    this.setState({ sending: true });
    document.getElementById("formButton").disabled = true;
    const formData = new FormData();
    formData.append("training_day", this.props.dayId);
    formData.append("name", this.state.name);
    formData.append("link", this.state.link);
    formData.append("body_part", this.state.bodyPart);
    formData.append("order", this.props.exerciseOrder);
    axiosAjax({
      method: "post",
      url: "/training/saved-exercise/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.updateExercises();
        this.setState({
          name: "",
          link: "",
        });
      })
      .catch(function(error) {
        console.log(error);
      });
    document.getElementById("formButton").disabled = false;
    this.setState({ sending: false });
  }

  render() {
    return (
      <form onSubmit={this.createExercise} style={{ marginTop: "2rem" }}>
        <div className='form-row'>
          <div className='col-lg-4'>
            <div className='form-group'>
              <label htmlFor='name'>
                Exercise Name <br />
                (capitalise first letter of each word)
              </label>
              <input
                autoComplete='off'
                autoFocus='true'
                onChange={this.handleInputChange}
                value={this.state.name}
                name='name'
                type='text'
                className='form-control form-control-single-border'
                id='name'
                placeholder='Bench Press'
                required
              />
            </div>
          </div>
          <div className='col-lg-4'>
            <div className='form-group'>
              <label htmlFor='name'>
                Exercise Link <br />
                (please search our videos for link)
              </label>
              <input
                autoComplete='off'
                onChange={this.handleInputChange}
                value={this.state.link}
                name='link'
                type='text'
                className='form-control form-control-single-border'
                id='link'
                placeholder='https://www.jamessmithacademy.com/users/#/coaching/modules/module/117'
                required
              />
            </div>
          </div>
          <div className='col-lg-4'>
            <div className='form-group'>
              <label htmlFor='name'>
                Exercise Tags <br />
                (exercise name body parts worked)
              </label>
              <input
                autoComplete='off'
                onChange={this.handleInputChange}
                value={this.state.bodyPart}
                name='bodyPart'
                type='text'
                className='form-control form-control-single-border'
                id='link'
                placeholder='bench press chest triceps push'
                required
              />
            </div>
          </div>
        </div>
        <div className='form-row' style={{ marginTop: "1rem" }}>
          <div className='col text-center'>
            <button
              type='submit'
              id='formButton'
              className='btn btn-light btn-rounded'
            >
              {this.state.sending === true
                ? "One moment please..."
                : "Create Exercise"}
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default CreateExercise;
