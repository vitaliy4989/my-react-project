import React, { Component } from "react";
import SearchExercises from "./SearchExercises";
import axiosAjax from "../../../config/axiosAjax";
import Exercise from "./Exercise";
import CreateExercise from "./CreateExercise";
import DayNotes from "./DayNotes";
import Supersets from "./Supersets";
import MDSpinner from "react-md-spinner";

class Day extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDay: false,
      exercises: undefined,
      searchExercises: false,
      updateName: false,
      updateOrder: false,
      name: this.props.day.name,
      order: this.props.day.order,
      saved: false,
      createExercise: false,
      searchExercise: true,
      type: this.props.day.type ? this.props.day.type : "",
      timeCompletedIn: this.props.day.time_completed_in,
      rounds: this.props.day.rounds,
      saveUpdates: false,
    };
    this.updateExercises = this.updateExercises.bind(this);
    this.updateDay = this.updateDay.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this._loadExercises = this._loadExercises.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _loadExercises(id) {
    axiosAjax
      .get("/training/training-exercise/" + id)
      .then(response => {
        this.setState({
          exercises: response.data,
        });
      })
      .catch(error => {
        this.setState({
          exercises: [],
        });
      });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.state.type !== prevState.type ||
      this.state.rounds !== prevState.rounds ||
      this.state.timeCompletedIn !== prevState.timeCompletedIn
    ) {
      this.setState({
        saveUpdates: true,
      });
    }
    if (this.props.day.id !== prevProps.day.id) {
      this.setState({
        showDay: false,
        exercises: undefined,
        searchExercises: false,
        updateName: false,
        updateOrder: false,
        name: this.props.day.name,
        order: this.props.day.order,
        saved: false,
        createExercise: false,
        searchExercise: true,
        type: this.props.day.type ? this.props.day.type : "",
        timeCompletedIn: this.props.day.time_completed_in,
        rounds: this.props.day.rounds,
        saveUpdates: false,
      });
    }
  }

  updateExercises(event) {
    this._loadExercises(this.props.day.id);
  }

  saveDay() {
    const formData = new FormData();
    formData.append("id", this.props.day.id);
    axiosAjax({
      method: "post",
      url: "/training/saved-training-day/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        alert("Day saved, you may now search for this day.");
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  deleteDay() {
    var r = confirm("Do you want to delete this day?!");
    if (r == true) {
      axiosAjax
        .delete("/training/training-day/" + this.props.day.id)
        .then(response => {
          this.props.updateDays();
          alert(
            "Day deleted, if you are in the day manager page, you may need to reload.",
          );
        });
    }
  }

  updateDay(event) {
    event.preventDefault();
    this.setState({ sending: true });
    const formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("order", this.state.order);
    if (this.state.type) {
      formData.append("type", this.state.type);
    }
    if (this.state.rounds) {
      formData.append("rounds", this.state.rounds);
    }
    if (this.state.timeCompletedIn) {
      formData.append("time_completed_in", this.state.timeCompletedIn);
    }
    if (this.props.dayManager === true) {
      formData.append("saved", true);
    }
    axiosAjax({
      method: "patch",
      url: "/training/training-day/" + this.props.day.id + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          updateName: false,
          updateOrder: false,
          sending: false,
          saveUpdates: false,
        });
        this.props.updateDays();
      })
      .catch(error => {
        console.log(error);
        this.setState({ sending: false });
      });
  }

  _showDay() {
    this.setState(
      {
        showDay: !this.state.showDay,
      },
      () => {
        if (this.state.showDay === true) {
          this._loadExercises(this.props.day.id);
        }
      },
    );
  }

  render() {
    return (
      <div className='content-container'>
        <div className='row d-flex align-items-center'>
          <div className='col'>
            {this.state.updateName === true ? (
              <form onSubmit={this.updateDay}>
                <div className='form-row'>
                  <div className='col'>
                    <input
                      onChange={this.handleInputChange}
                      value={this.state.name}
                      name='name'
                      type='text'
                      className='form-control form-control-sm'
                      id='name'
                      placeholder='Day Name...'
                      required
                    />
                  </div>
                  <div className='col'>
                    <button className='btn btn-light' id='formButton'>
                      Change
                    </button>
                  </div>
                </div>
              </form>
            ) : (
              <button
                style={{ padding: 0 }}
                onClick={() => this._showDay()}
                className='btn btn-link'
              >
                <h5 style={{ marginBottom: 0, color: "#4a4090" }}>
                  {this.state.name}
                </h5>
              </button>
            )}
          </div>
          {this.state.updateOrder === true && (
            <div className='col'>
              <form onSubmit={this.updateDay}>
                <div className='form-row'>
                  <div className='col'>
                    <input
                      onChange={this.handleInputChange}
                      value={this.state.order}
                      name='order'
                      type='number'
                      className='form-control form-control-sm'
                      id='name'
                      placeholder='Day Order...'
                      required
                    />
                  </div>
                  <div className='col'>
                    <button className='btn btn-light' id='formButton'>
                      Enter
                    </button>
                  </div>
                </div>
              </form>
            </div>
          )}
          <div className='col-auto'>
            <div className='dropdown'>
              <button
                type='button'
                className='btn btn-sm btn-light'
                data-toggle='dropdown'
                aria-haspopup='true'
                aria-expanded='false'
              >
                <i className='fas fa-ellipsis-v' />
              </button>
              <div className='dropdown-menu dropdown-menu-right'>
                <button
                  className='dropdown-item'
                  onClick={() =>
                    this.setState({ updateName: !this.state.updateName })
                  }
                >
                  <i className='far fa-edit' /> &nbsp;{" "}
                  {this.state.updateName === true
                    ? "Cancel Name Change"
                    : "Change Name"}
                </button>
                <button
                  id='orderButton'
                  onClick={() =>
                    this.setState({ updateOrder: !this.state.updateOrder })
                  }
                  className='dropdown-item'
                >
                  <i className='far fa-sort' /> &nbsp;{" "}
                  {this.state.updateOrder === true
                    ? "Cancel Order Change"
                    : "Change Order"}
                </button>
                {this.props.dayManager !== true && (
                  <button
                    id='formButton'
                    onClick={() => this.saveDay()}
                    className='dropdown-item'
                  >
                    <i className='far fa-save' /> &nbsp;{" "}
                    {this.state.saved === true ? "Saved" : "Save Day For Reuse"}
                  </button>
                )}
                <div className='dropdown-divider' />
                <button
                  onClick={() => this.deleteDay()}
                  className='dropdown-item'
                  type='button'
                >
                  <i className='far red fa-times' /> &nbsp; Delete From Program
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className='row mt-2'>
          <div className='col'>
            <form onSubmit={this.updateDay}>
              <div className='form-group'>
                <select
                  onChange={this.handleInputChange}
                  value={this.state.type}
                  className='form-control custom-select'
                  name='type'
                >
                  <option value='' disabled selected>
                    Select Day Type
                  </option>
                  <option value='Circuit'>Circuit</option>
                  <option value='Non-circuit'>Non-circuit</option>
                </select>
              </div>
              {this.state.type === "Circuit" && (
                <React.Fragment>
                  <div className='form-group'>
                    <input
                      type='number'
                      className='form-control'
                      name='rounds'
                      onChange={this.handleInputChange}
                      value={this.state.rounds}
                      placeholder='Number of circuit rounds'
                    />
                  </div>
                  <div className='form-group'>
                    <input
                      type='number'
                      className='form-control'
                      name='timeCompletedIn'
                      onChange={this.handleInputChange}
                      value={this.state.timeCompletedIn}
                      placeholder='Time to complete circuit in (minutes)'
                    />
                  </div>
                </React.Fragment>
              )}
              {this.state.saveUpdates === true && (
                <div className='form-group'>
                  <button className='btn btn-sm btn-primary'>
                    Save updates
                  </button>
                </div>
              )}
            </form>
          </div>
        </div>
        {this.state.showDay === true && (
          <div style={{ marginTop: 15 }}>
            <div>
              <div className='row'>
                <div className='col'>
                  <button
                    className={
                      this.state.searchExercise === true
                        ? "btn btn-block btn-primary btn-rounded"
                        : "btn btn-block btn-light btn-rounded"
                    }
                    onClick={() =>
                      this.setState({
                        searchExercise: true,
                        createExercise: false,
                      })
                    }
                  >
                    <i className='far fa-search' /> &nbsp; Search Exercises
                  </button>
                </div>
                <div className='col'>
                  <button
                    className={
                      this.state.createExercise === true
                        ? "btn btn-block btn-primary btn-rounded"
                        : "btn btn-block btn-light btn-rounded"
                    }
                    onClick={() =>
                      this.setState({
                        searchExercise: false,
                        createExercise: true,
                      })
                    }
                  >
                    <i className='far fa-plus' /> &nbsp; Add New Exercise
                  </button>
                </div>
                <div className='col'>
                  <button
                    type='button'
                    data-toggle='modal'
                    data-target={"#supersets" + this.props.day.id}
                    className='btn btn-light btn-block btn-rounded'
                  >
                    Supersets
                  </button>
                </div>
              </div>
              {this.state.searchExercise === true ? (
                <div className='row' style={{ marginTop: "2rem" }}>
                  <div className='col-12'>
                    <SearchExercises
                      dayId={this.props.day.id}
                      updateExercises={this.updateExercises}
                      exerciseOrder={
                        this.state.exercises !== undefined &&
                        this.state.exercises.length + 1
                      }
                    />
                  </div>
                </div>
              ) : null}
              {this.state.createExercise === true && (
                <CreateExercise
                  updateExercises={this.updateExercises}
                  dayId={this.props.day.id}
                  exerciseOrder={
                    this.state.exercises !== undefined &&
                    this.state.exercises.length + 1
                  }
                />
              )}
            </div>
            {this.state.exercises !== undefined ? (
              this.state.exercises.length > 0 && (
                <div className='pt-3 pb-3 animated fadeIn'>
                  {this.state.exercises.map((exercise, key) => (
                    <Exercise
                      exercise={exercise}
                      key={key}
                      updateExercises={this.updateExercises}
                    />
                  ))}
                </div>
              )
            ) : (
              <div className='p-5 text-center'>
                <MDSpinner singleColor='#4a4090' />
              </div>
            )}
            <DayNotes
              day={this.props.day}
              updateExercises={this.updateExercises}
            />
            <Supersets
              dayId={this.props.day.id}
              loadExercises={this._loadExercises}
              trainingDayExercises={this.state.exercises}
            />
          </div>
        )}
      </div>
    );
  }
}

export default Day;
