import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";

class SavedDay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      added: false,
      sending: false,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.day.id !== prevProps.day.id) {
      this.setState({
        added: false,
      });
    }
  }

  copyDayToProgram() {
    this.setState({ added: true, sending: true });
    const formData = new FormData();
    formData.append("program_id", this.props.programId);
    formData.append("day_id", this.props.day.id);
    axiosAjax({
      method: "post",
      url: "/training/copy-training-day/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.updateDays();
        this.setState({
          added: true,
          sending: false,
        });
      })
      .catch(error => {
        this.setState({
          added: false,
          sending: false,
        });
      });
  }

  render() {
    return (
      <div className='list-group-item d-flex justify-content-between align-items-center'>
        {this.props.day.name}
        {this.state.added === true ? (
          <button
            id='formButton'
            className='btn btn-round'
            style={{ background: "#4e9166" }}
          >
            <i className='white far fa-check' />
          </button>
        ) : this.state.sending === true ? (
          <div>Copying, please wait....</div>
        ) : (
          <button
            id='formButton'
            onClick={() => this.copyDayToProgram()}
            className='btn btn-light btn-round'
          >
            <i className='far fa-plus' />
          </button>
        )}
      </div>
    );
  }
}

export default SavedDay;
