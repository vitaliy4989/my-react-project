import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";

class CreateSet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reps: "12",
      rest: "60-90sec",
      tempo: "",
      weight: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createSet = this.createSet.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  createSet(event) {
    event.preventDefault();
    document.getElementById("formButton").disabled = true;
    const formData = new FormData();
    formData.append("t_p_exercise", this.props.exerciseId);
    formData.append("reps", this.state.reps);
    formData.append("rest", this.state.rest);
    formData.append("tempo", "");
    formData.append("weight", this.state.weight);
    axiosAjax({
      method: "post",
      url: "/training/training-exercise-set/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.updateSets();
        this.setState({
          reps: "12",
          rest: "60-90sec",
          tempo: "",
          weight: "",
        });
      })
      .catch(function(error) {
        console.log(error);
      });
    document.getElementById("formButton").disabled = false;
  }

  render() {
    return (
      <tr>
        <td colSpan='5'>
          <form onSubmit={this.createSet}>
            <div className='form-row'>
              <div className='col'>
                <input
                  autoFocus='autofocus'
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  value={this.state.reps}
                  name='reps'
                  type='text'
                  className='form-control'
                  id='reps'
                  placeholder='Reps...'
                  required
                />
              </div>
              <div className='col'>
                <input
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  value={this.state.rest}
                  name='rest'
                  type='text'
                  className='form-control'
                  id='name'
                  placeholder='Rest...'
                  required
                />
              </div>
              <div className='col'>
                <input
                  autoComplete='off'
                  onChange={this.handleInputChange}
                  value={this.state.weight}
                  name='weight'
                  type='text'
                  className='form-control'
                  id='name'
                  placeholder='Weight...'
                />
              </div>
              <div className='col'>
                <button id='formButton' className='btn btn-block btn-secondary'>
                  Enter
                </button>
              </div>
            </div>
          </form>
        </td>
      </tr>
    );
  }
}

export default CreateSet;
