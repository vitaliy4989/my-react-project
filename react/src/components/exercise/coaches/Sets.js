import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import Set from "./Set";

class Sets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sets: undefined,
      createSet: false,
    };
    this._removeSet = this._removeSet.bind(this);
  }

  _loadSets(id) {
    axiosAjax
      .get("/training/training-exercise-set/" + id)
      .then(response => {
        this.setState({
          sets: response.data,
        });
      })
      .catch(error => {
        this.setState({
          sets: [],
        });
      });
  }

  componentDidMount() {
    this._loadSets(this.props.exerciseId);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.exerciseId !== this.props.exerciseId) {
      this._loadSets(this.props.exerciseId);
    }
  }

  _createSet() {
    var key = this.state.sets.length - 1;
    this.state.sets.push(
      <Set
        removeSet={this._removeSet}
        setKey={key}
        key={key}
        exerciseId={this.props.exerciseId}
      />,
    );
    this.setState({
      sets: this.state.sets,
    });
  }

  _removeSet(id) {
    delete this.state.sets[id];
    this.setState({
      sets: this.state.sets,
    });
  }

  render() {
    return (
      <div>
        <div className='row'>
          <div className='col p-smaller'>kg/lbs</div>
          <div className='col p-smaller'>Reps</div>
          <div className='col p-smaller'>Rest</div>
          <div className='col p-smaller'>Save</div>
          <div className='col' />
        </div>
        {this.state.sets !== undefined ? (
          this.state.sets.length > 0 ? (
            this.state.sets.map((set, key) => (
              <Set
                deleteSet={this.props.removeSets}
                removeSet={this._removeSet}
                setKey={key}
                key={key}
                set={set}
                exerciseId={this.props.exerciseId}
              />
            ))
          ) : (
            <div className='alert alert-light mt-3 text-center'>
              There are no sets listed for the exercise.
            </div>
          )
        ) : (
          <div className='p-5 text-center'>
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
        <button
          onClick={() => this._createSet()}
          className='btn btn-sm mt-3 btn-block btn-light font-weight-bold btn-rounded'
        >
          <i className='far fa-plus' /> &nbsp; New Set
        </button>
      </div>
    );
  }
}

export default Sets;
