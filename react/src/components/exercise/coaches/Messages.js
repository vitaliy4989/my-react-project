import React, { Component } from "react";
import Comment from "../../shared/Comment";
import MDSpinner from "react-md-spinner";
import axiosAjax from "../../../config/axiosAjax";

const scrollToBottom = () => {
  const messagesDiv = document.getElementById("commentArea");
  if (messagesDiv) messagesDiv.scrollTop = messagesDiv.scrollHeight;
};

class Messages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: null,
      file: "",
      comment: "",
      sending: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._createMessage = this._createMessage.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _loadMesages() {
    axiosAjax
      .get("/training/coach-training-comment/" + this.props.programId)
      .then(response => {
        this.setState({
          comments: response.data,
        });
      });
    scrollToBottom();
  }

  componentDidMount() {
    this._loadMesages();
  }

  componentDidUpdate() {
    scrollToBottom();
  }

  componentWillReceiveProps(nextProps) {
    axiosAjax
      .get("/training/coach-training-comment/" + nextProps.programId)
      .then(response => {
        this.setState({
          comments: response.data,
        });
      });
  }

  _createMessage(event) {
    event.preventDefault();
    this.setState({ sending: true });
    document.getElementById("formButton").disabled = true;
    const formData = new FormData();
    formData.append("training", this.props.programId);
    formData.append("comment", this.state.comment);
    axiosAjax({
      method: "post",
      url: "/training/coach-training-comment/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          comment: "",
          sending: false,
        });
        this._loadMesages();
      })
      .catch(function(error) {
        console.log(error);
        this.setState({ sending: false });
      });
    document.getElementById("formButton").disabled = false;
  }

  render() {
    return (
      <div className='content-container'>
        <h5>Messages</h5>
        {this.state.comments !== null ? (
          this.state.comments.length > 0 && (
            <div
              style={{ height: 300, overflowX: "hidden", overflowY: "scroll" }}
              id='commentArea'
            >
              {this.state.comments.map((comment, key) =>
                comment.user ? (
                  <div key={key} className='row'>
                    <div className='col'>
                      <Comment
                        comment={comment.comment}
                        image={comment.user.image}
                        class='comment-left animated fadeIn'
                        textClass='comment-left-text'
                      />
                    </div>
                  </div>
                ) : (
                  <div key={key} className='row'>
                    <div className='col'>
                      <Comment
                        comment={comment.comment}
                        image={comment.coach.image}
                        noImage={true}
                        class='comment-right animated fadeIn'
                      />
                    </div>
                  </div>
                ),
              )}
            </div>
          )
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "5%", paddingBottom: "5%" }}
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
        <div
          style={{ borderTop: "solid 1px rgb(238, 241, 242)", paddingTop: 10 }}
        >
          <form onSubmit={this._createMessage}>
            <div className='form-row'>
              <div className='col-10'>
                <textarea
                  style={{ height: 41 }}
                  className='form-control'
                  onChange={this.handleInputChange}
                  value={this.state.comment}
                  name='comment'
                  placeholder='Type a message...'
                  required
                />
              </div>
              <div className='col-2'>
                <button id='formButton' className='btn btn-block btn-light'>
                  {this.state.sending === true ? (
                    <i className='fas fa-circle-notch fa-spin' />
                  ) : (
                    <i className='fab fa-lg fa-telegram-plane' />
                  )}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Messages;
