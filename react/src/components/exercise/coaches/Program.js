import React, { Component } from "react";
import CreateDay from "./CreateDay";
import axiosAjax from "../../../config/axiosAjax";
import Day from "./Day";
import SavedDays from "./SavedDays";

class Program extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newDay: false,
      savedDay: false,
      days: [],
      searchDays: true,
    };
    this.updateDays = this.updateDays.bind(this);
  }

  _loadTrainingDays() {
    axiosAjax
      .get("/training/coach-training-day/" + this.props.programId)
      .then(response => {
        this.setState({
          days: response.data,
        });
      })
      .catch(error => {
        this.setState({
          days: [],
        });
      });
  }

  componentDidMount() {
    this._loadTrainingDays();
  }

  updateDays(event) {
    this._loadTrainingDays();
  }

  render() {
    return (
      <React.Fragment>
        <div className='content-container'>
          <div className='row'>
            <div className='col'>
              <button
                className={
                  this.state.searchDays === true
                    ? "btn btn-block btn-primary btn-rounded"
                    : "btn btn-block btn-light btn-rounded"
                }
                onClick={() =>
                  this.setState({ searchDays: true, newDay: false })
                }
              >
                <i className='far fa-search' /> &nbsp; Search Days
              </button>
            </div>
            <div className='col'>
              <button
                className={
                  this.state.newDay === true
                    ? "btn btn-block btn-primary btn-rounded"
                    : "btn btn-block btn-light btn-rounded"
                }
                onClick={() =>
                  this.setState({ searchDays: false, newDay: true })
                }
              >
                <i className='far fa-plus' /> &nbsp; Add New Day
              </button>
            </div>
          </div>

          {this.state.searchDays === true && (
            <SavedDays
              updateDays={this.updateDays}
              programId={this.props.programId}
            />
          )}
          {this.state.newDay === true && (
            <CreateDay
              updateDays={this.updateDays}
              programId={this.props.programId}
            />
          )}
        </div>
        {this.state.days.length > 0 ? (
          this.state.days.map((day, key) => (
            <Day
              updateDays={this.updateDays}
              key={key}
              day={day}
              dayManager={false}
            />
          ))
        ) : (
          <div
            className='alert alert-light text-center'
            style={{ marginTop: 30 }}
          >
            Add first training day above.
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default Program;
