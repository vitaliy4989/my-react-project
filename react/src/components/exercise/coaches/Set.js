import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";

class Set extends Component {
  constructor(props) {
    super(props);
    this.state = {
      updated: false,
      id: this.props.set.id ? this.props.set.id : undefined,
      weight: this.props.set.weight
        ? this.props.set.weight.replace(/[^\d.-]/g, "")
        : 0,
      reps: this.props.set.reps ? this.props.set.reps : 12,
      rest: this.props.set.rest ? this.props.set.rest : "45",
      key: this.props.setKey,
      toSave: false,
      invalidWeight: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.updateSet = this.updateSet.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.id === this.props.set.id || !this.props.set.id) {
      if (prevState.weight !== this.state.weight) {
        this.setState({
          updated: false,
          toSave: true,
        });
      }
      if (prevState.reps !== this.state.reps) {
        this.setState({
          updated: false,
          toSave: true,
        });
      }
      if (prevState.rest !== this.state.rest) {
        this.setState({
          updated: false,
          toSave: true,
        });
      }
    }
    if (prevProps.set.id !== this.props.set.id) {
      this.setState({
        updated: false,
        id: this.props.set.id ? this.props.set.id : undefined,
        weight: this.props.set.weight
          ? this.props.set.weight.replace(/[^\d.-]/g, "")
          : 0,
        reps: this.props.set.reps ? this.props.set.reps : 0,
        rest: this.props.set.rest ? this.props.set.rest : "",
        key: this.props.setKey,
        toSave: false,
        invalidWeight: false,
      });
    }
  }

  componentDidMount() {
    if (this.state.id === undefined) {
      const formData = new FormData();
      formData.append("t_p_exercise", this.props.exerciseId);
      formData.append("reps", "");
      formData.append("rest", "");
      formData.append("tempo", "");
      formData.append("weight", "");
      axiosAjax({
        method: "post",
        url: "/training/training-exercise-set/",
        headers: { "Content-Type": "multipart/form-data" },
        data: formData,
      })
        .then(response => {
          this.setState({
            id: response.data.id,
          });
        })
        .catch(error => {
          alert("There was an error, please add the set again.");
        });
    }
  }

  updateSet(event) {
    if (this.state.id !== undefined) {
      event.preventDefault();
      this.setState({ updated: true });
      const formData = new FormData();
      formData.append("weight", this.state.weight);
      formData.append("reps", this.state.reps);
      formData.append("rest", this.state.rest);
      axiosAjax({
        method: "patch",
        url: "/training/training-exercise-set/" + this.state.id + "/",
        headers: { "Content-Type": "multipart/form-data" },
        data: formData,
      })
        .then(response => {
          this.setState({
            updated: true,
            toSave: true,
          });
        })
        .catch(error => {
          this.setState({
            updated: false,
            toSave: true,
          });
        });
    } else {
      alert("Could not update set, please remove set and try adding again.");
    }
  }

  deleteSet() {
    if (confirm("Click OK to delete this set.")) {
      this.props.removeSet(this.state.key);
      if (this.state.id) {
        axiosAjax
          .delete("/training/training-exercise-set/" + this.state.id + "/")
          .then(response => {
            console.log("set deleted");
          });
      }
    }
  }

  render() {
    return (
      <form onSubmit={this.updateSet} className='mt-3 animated fadeIn'>
        <div className='form-row'>
          <div className='col'>
            <input
              style={
                this.state.invalidWeight === true
                  ? {
                      padding: 7,
                      boxShadow: "none",
                      borderColor: "red",
                    }
                  : { padding: 7, boxShadow: "none" }
              }
              type='number'
              step='any'
              className='form-control'
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.weight}
              name='weight'
              id='weight'
              required
            />
          </div>
          <div className='col'>
            <input
              style={{ padding: 7, boxShadow: "none" }}
              className='form-control'
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.reps}
              name='reps'
              id='reps'
            />
          </div>
          <div className='col'>
            <input
              style={{ padding: 7, boxShadow: "none" }}
              className='form-control'
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.rest}
              name='rest'
              id='rest'
            />
          </div>
          <div className='col'>
            {this.state.updated === true ? (
              <button
                type='button'
                style={{
                  background: "#4c9064",
                  borderColor: "#4c9064",
                }}
                className='btn btn-block animated-2 pulse'
              >
                <i className='fas white fa-check' />
              </button>
            ) : this.state.toSave === true ? (
              <button
                type='submit'
                className='btn btn-outline-success animated-2 pulse btn-block'
              >
                <i className='fas fa-check' />
              </button>
            ) : (
              <button
                type='button'
                style={{
                  borderColor: "transparent",
                  boxShadow: "none",
                  background: "transparent",
                  cursor: "auto",
                }}
                className='btn btn-success btn-block'
              >
                <i className='fas text-muted fa-check' />
              </button>
            )}
          </div>
          <div className='col'>
            <button
              type='button'
              onClick={() => this.deleteSet()}
              style={{ padding: 7 }}
              className='btn btn-outline-danger btn-block'
            >
              <i className='far fa-times' />
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default Set;
