import React from "react";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import { NotificationManager } from "react-notifications";
import { connect } from "react-redux";
import { getPrograms } from "../../../redux/actions/training";
import "./styles.css";

class Index extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      programs: undefined,
      displayLimit: 2,
    };
  }

  componentDidMount() {
    axiosAjax.get("/training/copy-training-log-day/").then(response => {
      this.setState({
        programs: response.data,
      });
    });
  }

  _chooseImage(focus) {
    if (focus === "Full Body") {
      var image = [2, 13, 16, 17];
    } else if (focus === "Upper Body") {
      var image = [7, 11, 14, 19, 20, 23];
    } else if (focus === "Lower Body") {
      var image = [8, 22];
    } else if (focus === "Arms") {
      var image = [4, 15];
    } else {
      var image = [];
    }
    return image[Math.floor(Math.random() * image.length)];
  }

  _cloneToLogs(id) {
    NotificationManager.success(
      "James' Workout copied to My Workouts.",
      "",
      7000,
    );
    const formData = new FormData();
    formData.append("day_log_id", id);
    axiosAjax({
      method: "post",
      url: "/training/copy-training-log-day/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetPrograms();
      })
      .catch(error => {
        NotificationManager.error(
          "There was an error, please try again.",
          "",
          5000,
        );
      });
  }

  _upgrade() {
    window.location.href =
      window.location.protocol +
      "//" +
      window.location.host +
      "/users/membership-options/#standard-pricing-card";
  }

  render() {
    return (
      <React.Fragment>
        <div className='mb-1 mt-4 d-flex justify-content-between align-items-center'>
          <h6 className='m-0 section-heading'>James' Workouts</h6>
        </div>
        <div className='program-templates-list'>
          {this.state.programs !== undefined ? (
            this.state.programs.length > 0 && (
              <div className='scrollmenu' style={{ paddingLeft: 0 }}>
                {this.state.programs
                  .slice(
                    0,
                    this.props.membershipStatus === "trialing" ? 2 : 100,
                  )
                  .map((program, key) => (
                    <div key={key} className='template shadow-sm bg-white'>
                      <div className='item-body'>
                        <img
                          src={
                            "https://d3qqgqjfawam42.cloudfront.net/images/programs/james-" +
                            this._chooseImage(program.focus) +
                            ".png"
                          }
                          className='image'
                        />
                        <span className='m-2'>
                          <h6 className='text-capitalize p-smaller m-0'>
                            {program.name}
                          </h6>
                        </span>
                        <button
                          onClick={() => this._cloneToLogs(program.id)}
                          className='mb-3 btn btn-sm btn-primary font-weight-bolder btn-rounded'
                        >
                          Add to My Workouts
                        </button>
                      </div>
                    </div>
                  ))}
                {this.props.membershipStatus === "trialing" && (
                  <div
                    className='template shadow-sm bg-white'
                    onClick={this._upgrade}
                    style={{
                      background:
                        'url("https://d3qqgqjfawam42.cloudfront.net/images/app/rocket.svg"), linear-gradient(#5EB9D0, #5ED4A9)',
                      backgroundSize: "contain",
                      backgroundPosition: "bottom right",
                      backgroundRepeat: "no-repeat",
                      cursor: "pointer",
                      height: 247,
                    }}
                  >
                    <div className='item-body'>
                      <span className='m-2'>
                        <p className='font-weight-bold white'>
                          Upgrade your membership
                        </p>
                        <p className='white p-xs m-0'>
                          Access more of James' Workouts
                        </p>
                      </span>
                    </div>
                  </div>
                )}
              </div>
            )
          ) : (
            <div
              className='text-center'
              style={{ paddingTop: "5%", paddingBottom: "5%" }}
            >
              <MDSpinner singleColor='#4a4090' />
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    membershipStatus: state.user.membershipStatus,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetPrograms: () => {
      dispatch(getPrograms());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Index);
