import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import Set from "./Set";
import LightAlert from "../../shared/LightAlert";
import update from "immutability-helper";

class Sets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sets: undefined,
    };
    this._removeSet = this._removeSet.bind(this);
    this._updateSetObjectValues = this._updateSetObjectValues.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.exerciseId !== this.props.exerciseId) {
      this._loadSets();
    }
  }

  componentDidMount() {
    this._loadSets();
  }

  _createSet() {
    const currentSets = this.state.sets;
    const newSets = update(currentSets, {
      $push: [
        {
          exercise: this.state.exerciseId,
          id: undefined,
          reps: undefined,
          rest: undefined,
          weight: undefined,
        },
      ],
    });
    this.setState({
      sets: newSets,
    });
  }

  _updateSetObjectValues(index, updatedObj) {
    const currentSets = this.state.sets;
    const obj = currentSets[index];
    const newObj = update(obj, {
      exercise: { $set: updatedObj.exercise },
      id: { $set: updatedObj.id },
      weight: { $set: updatedObj.weight },
      reps: { $set: updatedObj.reps },
      rest: { $set: updatedObj.rest },
    });
    currentSets[index] = newObj;
  }

  _removeSet(index) {
    const currentSets = this.state.sets;
    const newSets = update(currentSets, {
      $splice: [[index, 1]],
    });
    this.setState({
      sets: newSets,
    });
  }

  _loadSets() {
    axiosAjax
      .get("/training/set/" + this.props.exerciseId + "/")
      .then(response => {
        this.setState({
          sets: response.data,
        });
      })
      .catch(error => {
        this.setState({
          sets: [],
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <div className='form-row text-muted'>
          <div className='col p-smaller'>Set</div>
          <div className='col p-smaller'>kg/lbs</div>
          <div className='col p-smaller'>Reps</div>
          <div className='col p-smaller'>Rest</div>
          <div className='col' />
          <div className='col' />
        </div>
        {this.state.sets !== undefined ? (
          this.state.sets.length > 0 ? (
            this.state.sets.map((set, key) => (
              <Set
                deleteSet={this.props.removeSets}
                removeSet={this._removeSet}
                updateSetValue={this._updateSetObjectValues}
                setKey={key}
                key={key}
                set={set}
                exerciseId={this.props.exerciseId}
                sets={this.state.sets}
              />
            ))
          ) : (
            <div
              className='row'
              style={{ marginTop: "1rem", marginBottom: "1rem" }}
            >
              <div className='col'>
                <LightAlert
                  class='text-center'
                  message='There are no sets for this exercise.'
                />
              </div>
            </div>
          )
        ) : (
          <div
            className='row'
            style={{ marginTop: "1rem", marginBottom: "1rem" }}
          >
            <div
              className='text-center col'
              style={{ paddingTop: "5%", paddingBottom: "5%" }}
            >
              <MDSpinner singleColor='#4a4090' />
            </div>
          </div>
        )}
        <div
          className='row'
          style={{ marginTop: "1rem", marginBottom: "1rem" }}
        >
          <div className='text-center col'>
            <button
              onClick={() => this._createSet()}
              className='btn btn-rounded font-weight-bold btn-block btn-sm btn-light'
            >
              <i className='far fa-plus' /> &nbsp; New Set
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Sets;
