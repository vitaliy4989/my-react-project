import React from "react";
import axiosAjax from "../../../config/axiosAjax";

class Set extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      updated: false,
      id: this.props.set.id ? this.props.set.id : undefined,
      weight: this.props.set.weight
        ? this.props.set.weight.replace(/[^\d.-]/g, "")
        : 0,
      reps: this.props.set.reps ? this.props.set.reps : 0,
      rest: this.props.set.rest ? this.props.set.rest : "",
      key: this.props.setKey,
      toSave: false,
      invalidWeight: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleWeightChange = this.handleWeightChange.bind(this);
    this.updateSet = this.updateSet.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleWeightChange(evt) {
    if (evt.target.validity.valid !== true) {
      this.setState({
        weight: "",
        invalidWeight: true,
      });
    } else {
      const weight = evt.target.validity.valid
        ? evt.target.value
        : parseFloat(evt.target.value);
      this.setState({
        weight: weight,
        invalidWeight: false,
      });
    }
  }

  componentDidMount() {
    if (this.state.id === undefined) {
      this.setState(
        {
          key: this.props.setKey,
        },
        () => {
          const formData = new FormData();
          formData.append("exercise", this.props.exerciseId);
          formData.append("reps", "");
          formData.append("weight", "");
          formData.append("rest", "");
          axiosAjax({
            method: "post",
            url: "/training/set/",
            headers: { "Content-Type": "multipart/form-data" },
            data: formData,
          }).then(response => {
            this.setState(
              {
                id: response.data.id,
              },
              () => {
                this.props.updateSetValue(this.state.key, {
                  exercise: this.props.exerciseId,
                  id: this.state.id,
                  weight: this.state.weight,
                  reps: this.state.reps,
                  rest: this.state.rest,
                });
              },
            );
          });
        },
      );
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.id === this.props.set.id || !this.props.set.id) {
      if (prevState.weight !== this.state.weight) {
        this.setState({
          updated: false,
          toSave: true,
        });
      }
      if (prevState.reps !== this.state.reps) {
        this.setState({
          updated: false,
          toSave: true,
        });
      }
      if (prevState.rest !== this.state.rest) {
        this.setState({
          updated: false,
          toSave: true,
        });
      }
    }
    if (prevProps.set.id !== this.props.set.id) {
      this.setState({
        updated: false,
        id: this.props.set.id ? this.props.set.id : undefined,
        weight: this.props.set.weight
          ? this.props.set.weight.replace(/[^\d.-]/g, "")
          : 0,
        reps: this.props.set.reps ? this.props.set.reps : 0,
        rest: this.props.set.rest ? this.props.set.rest : "",
        key: this.props.setKey,
        toSave: false,
        invalidWeight: false,
      });
    }
  }

  updateSet(event) {
    event.preventDefault();
    this.setState({ updated: true });
    const formData = new FormData();
    formData.append("weight", this.state.weight);
    formData.append("reps", this.state.reps);
    formData.append("rest", this.state.rest);
    axiosAjax({
      method: "patch",
      url: "/training/set/" + this.state.id + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({ updated: true, toSave: false });
        this.props.updateSetValue(this.state.key, {
          exercise: this.props.exerciseId,
          id: this.state.id,
          weight: this.state.weight,
          reps: this.state.reps,
          rest: this.state.rest,
        });
      })
      .catch(error => {
        this.setState({ updated: false, toSave: false });
      });
  }

  deleteSet() {
    if (confirm("Click OK to delete this set.")) {
      this.props.removeSet(this.state.key);
      if (this.state.id) {
        axiosAjax.delete("/training/set/" + this.state.id).then(response => {
          console.log("set deleted");
        });
      }
    }
  }

  render() {
    return (
      <form onSubmit={this.updateSet} style={{ marginTop: "1rem" }}>
        <div className='form-row'>
          <div className='col'>
            <p className='m-0' style={{ padding: 7 }}>
              {this.state.key + 1}
            </p>
          </div>
          <div className='col'>
            <input
              style={
                this.state.invalidWeight === true
                  ? {
                      padding: 7,
                      boxShadow: "none",
                      borderColor: "red",
                    }
                  : { padding: 7, boxShadow: "none" }
              }
              type='number'
              step='any'
              className='form-control '
              autoComplete='off'
              onChange={this.handleWeightChange}
              value={this.state.weight}
              name='weight'
              id='weight'
              required
            />
          </div>
          <div className='col'>
            <input
              style={{ padding: 7, boxShadow: "none" }}
              className='form-control '
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.reps}
              name='reps'
              id='reps'
            />
          </div>
          <div className='col'>
            <input
              style={{ padding: 7, boxShadow: "none" }}
              className='form-control '
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.rest}
              name='rest'
              id='rest'
            />
          </div>
          <div className='col'>
            {this.state.updated === true ? (
              <button
                type='button'
                className='btn btn-block'
                style={{
                  borderColor: "transparent",
                  boxShadow: "none",
                  background: "transparent",
                }}
              >
                <i className='fas green fa-check' />
              </button>
            ) : this.state.toSave === true ? (
              <button
                type='submit'
                className='btn btn-outline-success  animated-2 pulse btn-block'
              >
                <i className='fas fa-check' />
              </button>
            ) : (
              <button
                type='button'
                style={{
                  borderColor: "transparent",
                  boxShadow: "none",
                  background: "transparent",
                  cursor: "auto",
                }}
                className='btn  btn-block'
              >
                <i className='fas text-muted fa-check' />
              </button>
            )}
          </div>
          <div className='col'>
            <button
              type='button'
              onClick={() => this.deleteSet()}
              style={{
                borderColor: "transparent",
                boxShadow: "none",
                background: "transparent",
              }}
              className='btn btn-outline-danger btn-block'
            >
              <i className='far red fa-times' />
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default Set;
