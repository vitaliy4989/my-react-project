import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { NotificationManager } from "react-notifications";
import { getProgram } from "../../../redux/actions/training";
import connect from "react-redux/es/connect/connect";

class Options extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: undefined,
      type: this.props.type,
      rounds: this.props.rounds,
      experience: this.props.experience,
      timeCompletedIn: this.props.timeCompletedIn,
      focus: this.props.focus,
      goal: this.props.goal,
      facility: this.props.facility,
      forProgramTemplates: this.props.forProgramTemplates,
    };
    this._updateDay = this._updateDay.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        id: this.props.id,
        type: this.props.type,
        rounds: this.props.rounds,
        experience: this.props.experience,
        focus: this.props.focus,
        timeCompletedIn: this.props.timeCompletedIn,
        goal: this.props.goal,
        facility: this.props.facility,
        forProgramTemplates: this.props.forProgramTemplates,
      });
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _updateDay(event) {
    if (event) {
      event.preventDefault();
    }
    const formData = new FormData();
    formData.append("type", this.state.type);
    formData.append("rounds", this.state.rounds);
    formData.append("experience", this.state.experience);
    formData.append("time_completed_in", this.state.timeCompletedIn);
    formData.append("goal", this.state.goal);
    formData.append("focus", this.state.focus);
    formData.append("facility", this.state.facility);
    formData.append("for_program_templates", this.state.forProgramTemplates);
    axiosAjax({
      method: "patch",
      url: "/training/training-log-day/" + this.state.id + "/",
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Changes saved.");
        this.props.loadDay(this.props.dayId);
      })
      .catch(error => {
        NotificationManager.error("There was an error saving your changes.");
      });
  }

  _makeTemplate() {
    this.setState(
      {
        forProgramTemplates: !this.state.forProgramTemplates,
      },
      () => {
        this._updateDay();
      },
    );
  }

  render() {
    return (
      <div
        className='modal fade'
        id='dayOptions'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='dayOptionsLabel'
        aria-hidden='true'
      >
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title' id='dayOptionsLabel'>
                More Options
              </h5>
              <button
                type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Close'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <div className='form-group'>
                <a
                  className='btn btn-block btn-light'
                  href={"/training/training-log-day-pdf/" + this.props.dayId}
                >
                  Print &nbsp; <i className='far fa-print' />
                </a>
              </div>
              {this.props.username === "jamesshaw" && (
                <div className='form-group'>
                  {this.state.forProgramTemplates === true ? (
                    <button
                      type='button'
                      onClick={() => this._makeTemplate()}
                      className='btn btn-block btn-danger'
                    >
                      Remove as public template
                    </button>
                  ) : (
                    <button
                      type='button'
                      onClick={() => this._makeTemplate()}
                      className='btn btn-light btn-block'
                    >
                      Mark as public template
                    </button>
                  )}
                </div>
              )}
              <form onSubmit={this._updateDay}>
                <div className='form-group'>
                  <label htmlFor='type'>Program Type</label>
                  <select
                    className='form-control custom-select'
                    name='type'
                    onChange={this.handleInputChange}
                    value={this.state.type}
                  >
                    <option value='' disabled selected>
                      Select Program Type
                    </option>
                    <option value='Circuit'>Circuit</option>
                    <option value='Non-circuit'>Non-circuit</option>
                  </select>
                </div>
                {this.state.type === "Circuit" ? (
                  <React.Fragment>
                    <div className='form-group'>
                      <label htmlFor='round'>
                        How many rounds in the circuit?
                      </label>
                      <input
                        type='number'
                        step='any'
                        className='form-control'
                        value={this.state.rounds}
                        name='rounds'
                        placeholder='Rounds for circuit'
                        onChange={this.handleInputChange}
                        required
                      />
                    </div>
                    <div className='form-group'>
                      <label htmlFor='timeCompletedIn'>
                        How many minutes to complete circuit?
                      </label>
                      <input
                        type='number'
                        steps='any'
                        className='form-control'
                        value={this.state.timeCompletedIn}
                        name='timeCompletedIn'
                        placeholder='Time to complete in'
                        onChange={this.handleInputChange}
                        required
                      />
                    </div>
                  </React.Fragment>
                ) : null}
                {this.props.username === "jamesshaw" && (
                  <React.Fragment>
                    <div className='form-group'>
                      <select
                        onChange={this.handleInputChange}
                        value={this.state.focus}
                        name='focus'
                        className='form-control custom-select'
                      >
                        <option value='' disabled selected>
                          Select Area of Focus
                        </option>
                        <option value='Full Body'>Full Body</option>
                        <option value='Lower Body'>Lower Body</option>
                        <option value='Upper Body'>Upper Body</option>
                        <option value='Arms'>Arms</option>
                      </select>
                    </div>
                    <div className='form-group'>
                      <select
                        onChange={this.handleInputChange}
                        value={this.state.experience}
                        name='experience'
                        className='form-control custom-select'
                      >
                        <option value='' disabled selected>
                          Select Experience Level
                        </option>
                        <option value='Less than 1'>Less than 1 year</option>
                        <option value='1-2'>1-2 years</option>
                        <option value='2-3'>2-3 years</option>
                        <option value='3-4'>3-4 years</option>
                        <option value='4+'>4+ years</option>
                      </select>
                    </div>
                    <div className='form-group'>
                      <select
                        onChange={this.handleInputChange}
                        value={this.state.goal}
                        name='goal'
                        className='form-control custom-select'
                      >
                        <option value='' disabled selected>
                          Select General Goal
                        </option>
                        <option value='Build Muscle'>Build Muscle</option>
                        <option value='Lose Fat'>Lose Fat</option>
                        <option value='Other'>Other</option>
                      </select>
                    </div>
                    <div className='form-group'>
                      <select
                        onChange={this.handleInputChange}
                        value={this.state.facility}
                        name='facility'
                        className='form-control custom-select'
                      >
                        <option value='' disabled selected>
                          Select Exercise Facility
                        </option>
                        <option value='home'>Home</option>
                        <option value='gym'>Gym</option>
                        <option value='outside'>Outside</option>
                      </select>
                    </div>
                  </React.Fragment>
                )}
                <button className='btn btn-primary'>Update</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    username: state.user.username,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetProgram: programId => {
      dispatch(getProgram(programId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Options);
