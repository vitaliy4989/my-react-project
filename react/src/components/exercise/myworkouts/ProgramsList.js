import React, { Component } from "react";
import { connect } from "react-redux";
import { getPrograms, updatePrograms } from "../../../redux/actions/training";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import { NotificationManager } from "react-notifications";
import ProgramsListItem from "./ProgramsListItem";

class ProgramsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      programs: undefined,
      displayLimit: 2,
    };
    this._cloneToLogs = this._cloneToLogs.bind(this);
    this._removeWorkout = this._removeWorkout.bind(this);
  }

  componentDidMount() {
    this.props.onGetPrograms();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.programs !== prevProps.programs) {
      this.setState({
        programs: this.props.programs,
      });
    }
  }

  _addProgram() {
    if (this.state.programs !== undefined) {
      this.state.programs.unshift(
        <ProgramsListItem
          removeWorkout={this._removeWorkout}
          itemKey={0}
          key={0}
        />,
      );
      this.setState({
        programs: this.state.programs,
        displayLimit: 2,
      });
    }
  }

  _removeWorkout(key) {
    this.state.programs.splice(key, 1);
    this.setState({
      displayLimit: 2,
    });
    this.forceUpdate();
  }

  _cloneToLogs(id) {
    NotificationManager.success("Cloned program.", "", 5000);
    const formData = new FormData();
    formData.append("day_log_id", id);
    axiosAjax({
      method: "post",
      url: "/training/copy-training-log-day/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    }).then(response => {
      this.props.onGetPrograms();
    });
  }

  render() {
    return (
      <React.Fragment>
        <div className='mb-1 mt-4 d-flex justify-content-between align-items-center'>
          <h6 className='m-0 section-heading'>My Workouts</h6>
          <button
            onClick={() => this._addProgram()}
            className='btn btn-rounded btn-primary btn-sm'
          >
            New Workout
          </button>
        </div>
        {this.state.programs !== undefined ? (
          Array.isArray(this.state.programs) === true &&
          this.state.programs.length > 0 ? (
            <React.Fragment>
              {this.state.programs
                .slice(0, this.state.displayLimit)
                .map((program, key) => (
                  <ProgramsListItem
                    key={key}
                    id={program.id}
                    itemKey={key}
                    name={program.name}
                    date={program.date}
                    removeWorkout={this._removeWorkout}
                    cloneToLogs={this._cloneToLogs}
                  />
                ))}
              {this.state.programs.length > 2 &&
                this.state.programs.length !== this.state.displayLimit && (
                  <div className='mt-1 d-flex justify-content-end align-items-center'>
                    <button
                      className='btn btn-sm btn-rounded btn-link'
                      onClick={() =>
                        this.setState({
                          displayLimit: this.state.programs.length,
                        })
                      }
                    >
                      Show more
                    </button>
                  </div>
                )}
              {this.state.programs.length > 2 && this.state.displayLimit > 2 && (
                <div className='mt-1 d-flex justify-content-end align-items-center'>
                  <button
                    className='btn btn-sm btn-rounded btn-link'
                    onClick={() => this.setState({ displayLimit: 2 })}
                  >
                    Show less
                  </button>
                </div>
              )}
            </React.Fragment>
          ) : (
            <div className='rounded-corners shadow-sm p-3 mt-3 bg-white'>
              Log or plan your own workout.
            </div>
          )
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "5%", paddingBottom: "5%" }}
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    programs: state.training.programs,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetPrograms: () => {
      dispatch(getPrograms());
    },
    onUpdatePrograms: programs => {
      dispatch(updatePrograms(programs));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProgramsList);
