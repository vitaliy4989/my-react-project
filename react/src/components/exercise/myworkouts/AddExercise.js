import React, { Component } from "react";
import { connect } from "react-redux";
import axiosAjax from "../../../config/axiosAjax";
import { getExercises } from "../../../redux/actions/training";
import ExerciseVideoSearchResult from "./ExerciseVideoSearchResult";
import MDSpinner from "react-md-spinner";

class AddExercise extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      sending: false,
      customExercise: false,
      searchTerm: "",
      noVideoExercises: undefined,
      videoExercises: undefined,
      noResults: false,
      noVideoResults: false,
      equipmentSearchTerm: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createExercise = this.createExercise.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  createExercise(event) {
    event.preventDefault();
    this.setState({ sending: true });
    const formData = new FormData();
    formData.append("training_log_day", this.props.programId);
    formData.append("alternative_name", this.state.name);
    formData.append("order", this.props.exerciseOrder);
    axiosAjax({
      method: "post",
      url: "/training/exercise/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetExercises(this.props.programId);
        this.setState({
          name: "",
          sending: false,
        });
      })
      .catch(error => {
        this.setState({ sending: false });
      });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.searchTerm !== this.state.searchTerm) {
      this._searchExercises();
    }
  }

  _searchExercisesByEquipment(equipment) {
    this.setState(
      {
        equipmentSearchTerm: equipment,
        noVideoExercises: [],
        noResults: true,
        searchTerm: "",
      },
      () => {
        axiosAjax
          .get("/training/exercise-videos-by-equipment/", {
            params: {
              equipment: this.state.equipmentSearchTerm,
            },
          })
          .then(response => {
            this.setState({
              videoExercises: response.data,
              noVideoResults: false,
            });
          })
          .catch(error => {
            this.setState({
              noVideoResults: true,
              videoExercises: [],
            });
          });
      },
    );
  }

  _searchExercises() {
    if (this.state.searchTerm !== "") {
      this.setState({ equipmentSearchTerm: "" });
      axiosAjax
        .get("/training/exercise-videos/", {
          params: {
            name: this.state.searchTerm,
          },
        })
        .then(response => {
          this.setState({
            videoExercises: response.data,
            noVideoResults: false,
          });
        })
        .catch(error => {
          this.setState({
            noVideoResults: true,
            videoExercises: [],
          });
        });
    }
  }

  render() {
    return (
      <div
        className='modal fade'
        id='addExerciseModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='addExerciseModal'
        aria-hidden='true'
      >
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h6 className='modal-title'>Add Exercises</h6>
              <button
                type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Close'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <div className='row' style={{ marginTop: "1rem" }}>
                <div className='col'>
                  <button
                    className='btn btn-sm btn-block btn-light'
                    onClick={() =>
                      this.setState({
                        customExercise: !this.state.customExercise,
                        equipmentSearchTerm: "",
                        searchTerm: "",
                      })
                    }
                  >
                    {this.state.customExercise === true
                      ? "Search Exercises"
                      : "Add Custom"}
                  </button>
                </div>
                <div className='col'>
                  <div className='dropdown'>
                    <button
                      className='btn btn-sm btn-block btn-light dropdown-toggle'
                      role='button'
                      id='dropdownMenuLink'
                      data-toggle='dropdown'
                      aria-haspopup='true'
                      aria-expanded='false'
                    >
                      By Equipment
                    </button>

                    <div
                      className='dropdown-menu'
                      aria-labelledby='dropdownMenuLink'
                    >
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Barbell")
                        }
                      >
                        Barbell
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Rings")
                        }
                      >
                        Rings
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() => this._searchExercisesByEquipment("TRX")}
                      >
                        TRX
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Dumbbell")
                        }
                      >
                        Dumbbell
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Machine")
                        }
                      >
                        Machine
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Body Weight")
                        }
                      >
                        Body Weight
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Cardio")
                        }
                      >
                        Cardio
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Cables")
                        }
                      >
                        Cables
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Kettlebell")
                        }
                      >
                        Kettlebell
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Bands")
                        }
                      >
                        Bands
                      </button>
                      <button
                        className='dropdown-item'
                        onClick={() =>
                          this._searchExercisesByEquipment("Other")
                        }
                      >
                        Other
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              {this.state.customExercise === true ? (
                <form
                  onSubmit={this.createExercise}
                  style={{ marginTop: "1rem" }}
                >
                  <div className='form-row'>
                    <div className='col-8'>
                      <input
                        autoFocus='autofocus'
                        autoComplete='off'
                        className='form-control '
                        onChange={this.handleInputChange}
                        value={this.state.name}
                        name='name'
                        type='text'
                        placeholder='Exercise name...'
                        required
                        autoComplete='off'
                      />
                    </div>
                    <div className='col-4'>
                      <button
                        id='formButton'
                        className='btn btn-block btn-primary'
                        type='submit'
                      >
                        {this.state.sending === true ? (
                          <i className='fas fa-circle-notch fa-spin' />
                        ) : (
                          "Enter"
                        )}
                      </button>
                    </div>
                  </div>
                </form>
              ) : (
                <form style={{ marginTop: "1rem" }}>
                  <div className='form-row'>
                    <div className='col-12'>
                      <input
                        autoFocus='autofocus'
                        autoComplete='off'
                        onChange={this.handleInputChange}
                        value={this.state.searchTerm}
                        name='searchTerm'
                        type='text'
                        className='form-control'
                        placeholder='Enter exercise or body part...'
                        aria-describedby='basic-addon1'
                        required
                      />
                    </div>
                  </div>
                </form>
              )}
              {(this.state.searchTerm !== "" ||
                this.state.equipmentSearchTerm !== "") && (
                <div
                  style={{
                    maxHeight: "100vh",
                    overflowY: "scroll",
                    overflowX: "hidden",
                  }}
                >
                  <div
                    className='list-group-flush list-group list-wrapper-no-box'
                    style={{ marginTop: "2rem" }}
                  >
                    {this.state.videoExercises !== undefined ? (
                      <React.Fragment>
                        {this.state.videoExercises.length > 0 ? (
                          this.state.videoExercises.map((video, key) => (
                            <ExerciseVideoSearchResult
                              exerciseOrder={this.props.exerciseOrder}
                              key={key}
                              video={video}
                              programId={this.props.programId}
                            />
                          ))
                        ) : this.state.noVideoResults === true ? null : (
                          <div className='list-group-item '>Searching...</div>
                        )}
                      </React.Fragment>
                    ) : (
                      <div
                        className='text-center list-group-item'
                        style={{ paddingTop: "10%", paddingBottom: "10%" }}
                      >
                        <MDSpinner singleColor='#4a4090' />
                      </div>
                    )}
                    {this.state.noResults === true &&
                    this.state.noVideoResults === true ? (
                      <div className='list-group-item '>No results...</div>
                    ) : null}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onGetExercises: programId => {
      dispatch(getExercises(programId));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(AddExercise);
