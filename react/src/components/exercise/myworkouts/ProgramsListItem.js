import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import axiosAjax from "../../../config/axiosAjax";

class ProgramsListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      name: this.props.name,
      date: this.props.date,
      key: this.props.itemKey,
    };
  }

  componentDidMount() {
    this._createProgram();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.id !== prevProps.id) {
      this.setState(
        {
          id: this.props.id,
          name: this.props.name,
          date: this.props.date,
          key: this.props.itemKey,
        },
        () => {
          this._createProgram();
        },
      );
    }
  }

  _deleteLog() {
    if (confirm("Click OK to delete log.")) {
      this.props.removeWorkout(this.props.itemKey);
      axiosAjax
        .delete("/training/training-log-day/" + this.state.id + "/")
        .then(response => {
          console.log("program deleted.");
        })
        .catch(error => {
          console.log("program did not delete.");
        });
    }
  }

  _createProgram() {
    if (this.state.id === undefined) {
      const formData = new FormData();
      formData.append("name", this._getCurrentDayName());
      axiosAjax({
        method: "post",
        url: "/training/training-log-day/",
        headers: { "Content-Type": "multipart/form-data" },
        data: formData,
      })
        .then(response => {
          this.setState({
            id: response.data.id,
            name: response.data.name,
            date: response.data.date,
          });
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  _getCurrentDayName() {
    var a = new Date();
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";
    var currentDay = weekdays[a.getDay()];
    return currentDay;
  }

  render() {
    return (
      <div className='rounded-corners shadow-sm p-3 mt-3 bg-white'>
        <div className='row d-flex align-items-center'>
          <NavLink to={"/training/created/" + this.state.id} className='col'>
            {this.state.name}
            <p className='p-smaller m-0 text-muted'>
              {this.state.date
                ? this.state.date.substring(0, this.state.date.indexOf("T"))
                : ""}
            </p>
          </NavLink>
          <div className='col-auto'>
            <div className='dropdown float-right'>
              <button
                className='btn btn-light btn-sm'
                type='button'
                id={"logOptions" + this.state.id}
                data-toggle='dropdown'
                aria-haspopup='true'
                aria-expanded='false'
              >
                <i className='far fa-ellipsis-v' />
              </button>
              <div
                className='dropdown-menu dropdown-menu-right'
                aria-labelledby={"logOptions" + this.state.id}
              >
                <button
                  onClick={() => this.props.cloneToLogs(this.state.id)}
                  className='dropdown-item'
                >
                  <i className='far fa-clone' /> &nbsp; Clone Workout
                </button>
                <div className='dropdown-divider' />
                <button
                  onClick={() => this._deleteLog()}
                  className='dropdown-item'
                >
                  <i className='far red fa-times' /> &nbsp; Delete Workout
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProgramsListItem;
