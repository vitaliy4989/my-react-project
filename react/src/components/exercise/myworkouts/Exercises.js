import React, { Component } from "react";
import Exercise from "./Exercise";
import VideoExercise from "./VideoExercise";
import { connect } from "react-redux";
import { getExercises } from "../../../redux/actions/training";
import MDSpinner from "react-md-spinner";
import LightAlert from "../../shared/LightAlert";

class Exercises extends Component {
  constructor(props) {
    super(props);
    this._reRenderExercises = this._reRenderExercises.bind(this);
  }

  componentDidMount() {
    this.props.onGetExercises(this.props.logId);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.logId !== this.props.logId) {
      this.props.onGetExercises(this.props.logId);
    }
  }

  _reRenderExercises(event) {
    this.forceUpdate();
  }

  _toggleModal() {
    $("#addExerciseModal").modal("toggle");
  }

  render() {
    return (
      <React.Fragment>
        {this.props.exercises !== undefined ? (
          this.props.exercises.length > 0 ? (
            this.props.exercises.map((exercise, key) =>
              exercise.video ? (
                <VideoExercise
                  key={key}
                  exercise={exercise}
                  logId={this.props.logId}
                  reRenderExercises={this._reRenderExercises}
                  exerciseArrayIndex={key}
                />
              ) : (
                <Exercise
                  key={key}
                  exercise={exercise}
                  logId={this.props.logId}
                  reRenderExercises={this._reRenderExercises}
                  exerciseArrayIndex={key}
                />
              ),
            )
          ) : (
            <div className='p-3'>
              <LightAlert
                class='text-center'
                message={
                  <React.Fragment>
                    <p>Here you can log or plan your own exercise programs.</p>
                    <p>If you need some inspiration, try "James' Workouts".</p>
                    <button
                      className='btn btn-rounded btn-link'
                      onClick={() => this._toggleModal()}
                    >
                      Add First Exercise
                    </button>
                  </React.Fragment>
                }
                container={true}
              />
            </div>
          )
        ) : (
          <div className='text-center p-5'>
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    exercises: state.training.exercises,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetExercises: programId => {
      dispatch(getExercises(programId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Exercises);
