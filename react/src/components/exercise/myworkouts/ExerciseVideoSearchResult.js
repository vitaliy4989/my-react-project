import React, { Component } from "react";
import { connect } from "react-redux";
import axiosAjax from "../../../config/axiosAjax";
import { getExercises } from "../../../redux/actions/training";

class ExerciseVideoSearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      added: false,
      sending: false,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.video.name !== prevProps.video.name) {
      this.setState({ added: false });
    }
  }

  addToProgram() {
    const formData = new FormData();
    this.setState({ sending: true, added: true });
    formData.append("training_log_day", this.props.programId);
    formData.append("video", this.props.video.id);
    formData.append("order", this.props.exerciseOrder);
    axiosAjax({
      method: "post",
      url: "/training/exercise/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetExercises(this.props.programId);
        this.setState({
          added: true,
          sending: false,
        });
      })
      .catch(error => {
        this.setState({ sending: false, added: false });
        console.log(error);
      });
  }

  render() {
    return (
      <div className='list-group-item animated fadeIn'>
        <div className='row d-flex justify-content-between align-items-center'>
          <div className='col-8'>
            <p className='text-capitalize m-0'>{this.props.video.name}</p>
          </div>
          <div className='col-4 float-right'>
            {this.state.added === true ? (
              <button
                id='formButton'
                className='btn btn-round btn-success float-right animated pulse'
              >
                <i className='far fa-check white' />
              </button>
            ) : (
              <button
                id='formButton'
                onClick={() => this.addToProgram()}
                className='btn btn-round btn-light float-right'
              >
                {this.state.sending === true ? (
                  <i className='far fa-circle-notch fa-spin' />
                ) : (
                  <i className='far fa-plus' />
                )}
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onGetExercises: programId => {
      dispatch(getExercises(programId));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(ExerciseVideoSearchResult);
