import React, { Component } from "react";
import axiosAjax from "../../../../config/axiosAjax";

class Exercise extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      setColor: this.props.setColor,
      setId: this.props.setId,
      name: this.props.name,
      added: false,
    };
    this._addExercise = this._addExercise.bind(this);
    this._removeExercise = this._removeExercise.bind(this);
  }

  _addExercise() {
    this.setState({
      added: true,
    });
    const formData = new FormData();
    formData.append("superset_group", this.state.setId);
    axiosAjax({
      method: "patch",
      url: "/training/exercise/" + this.props.id + "/",
      data: formData,
    })
      .then(response => {
        this.setState({
          added: true,
        });
      })
      .catch(error => {
        this.setState({
          added: false,
        });
      });
  }

  _removeExercise() {
    this.setState({
      added: false,
    });
    const formData = new FormData();
    formData.append("superset_group", "");
    axiosAjax({
      method: "patch",
      url: "/training/exercise/" + this.props.id + "/",
      data: formData,
    })
      .then(response => {
        this.setState({
          added: false,
        });
      })
      .catch(error => {
        this.setState({
          added: true,
        });
      });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.setColor !== prevProps.setColor) {
      this.setState({
        setColor: this.props.setColor,
      });
    }
    if (this.props.setId !== prevProps.setId) {
      this.setState({
        setId: this.props.setId,
      });
    }
    if (this.props.id !== prevProps.id) {
      this.setState({
        id: this.props.id,
        setColor: this.props.setColor,
        setId: this.props.setId,
        name: this.props.name,
        added: false,
      });
    }
  }

  render() {
    return (
      <div
        style={{ cursor: "pointer" }}
        className='list-group-item-action list-group-item d-flex justify-content-between align-items-center'
        onClick={
          this.state.added === true ? this._removeExercise : this._addExercise
        }
      >
        {this.state.name}
        {this.state.added === true ? (
          <i style={{ color: this.state.setColor }} className='far fa-check' />
        ) : null}
      </div>
    );
  }
}

export default Exercise;
