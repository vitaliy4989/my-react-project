import React, { Component, PropTypes } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { connect } from "react-redux";
import { updateExercises, getExercises } from "../../../redux/actions/training";
import Sets from "./Sets";

class Exercise extends Component {
  constructor(props) {
    super(props);
    this.state = {
      removeSets: false,
      order: this.props.exercise.order,
      supersetGroup: this.props.exercise.superset_group,
      exerciseArrayIndex: this.props.exerciseArrayIndex,
      toMove: 0,
      changeOrder: false,
      showExerciseDetail: false,
    };
  }

  componentDidMount() {
    if (this.state.order !== this.state.exerciseArrayIndex) {
      this._saveExerciseOrder();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.exercise.id !== this.props.exercise.id) {
      this.setState(
        {
          removeSets: false,
          order: this.props.exercise.order,
          supersetGroup: this.props.exercise.superset_group,
          exerciseArrayIndex: this.props.exerciseArrayIndex,
          toMove: 0,
          changeOrder: false,
          showExerciseDetail: false,
        },
        () => {
          this._saveExerciseOrder();
        },
      );
    }
    if (prevState.toMove !== this.state.toMove) {
      this._reOrderExercises();
    }
  }

  _saveExerciseOrder() {
    const formData = new FormData();
    formData.append("order", this.state.exerciseArrayIndex);
    axiosAjax({
      method: "patch",
      url: "/training/exercise/" + this.props.exercise.id + "/",
      data: formData,
    }).then(response => {
      this.setState({
        order: response.data.order,
      });
    });
  }

  deleteExercise(id) {
    if (confirm("Press OK to delete exercise.")) {
      axiosAjax.delete("/training/exercise/" + id + "/").then(response => {
        this.props.onGetExercises(this.props.logId);
      });
    }
  }

  _reOrderExercises() {
    var exercises = this.props.exercises;
    var exerciseRemoved = exercises.splice(this.state.exerciseArrayIndex, 1)[0];
    var newIndex = this.state.exerciseArrayIndex + this.state.toMove;
    exercises.splice(newIndex, 0, exerciseRemoved);
    this.props.onUpdateExercises(exercises);
    this.props.reRenderExercises();
  }

  render() {
    return (
      <React.Fragment>
        <div className='row'>
          <div className='col'>
            <div className='embed-responsive embed-responsive-16by9'>
              <iframe
                className='embed-responsive-item'
                src={this.props.exercise.video.vimeo_url}
              />
            </div>
          </div>
        </div>
        {this.state.changeOrder === true
          ? this.state.exerciseArrayIndex !== 0 && (
              <div className='text-center m-2'>
                <button
                  onClick={() => this.setState({ toMove: -1 })}
                  className='btn btn-secondary btn-xs btn-rounded'
                >
                  <i className='fas fa-arrow-alt-up' />
                </button>
              </div>
            )
          : null}
        <div className='row no-gutters'>
          {this.state.supersetGroup ? (
            <div
              className='col-auto'
              style={{
                width: 3,
                borderRadius: 5,
                marginRight: 10,
                marginTop: 7,
                marginBottom: 7,
                display: "flex",
                flexDirection: "column",
                backgroundColor: this.state.supersetGroup.color,
              }}
            />
          ) : (
            <div
              className='col-auto'
              style={{
                width: 3,
                borderRadius: 5,
                marginRight: 10,
                marginTop: 7,
                marginBottom: 7,
                display: "flex",
                flexDirection: "column",
                backgroundColor: "transparent",
              }}
            />
          )}
          <div className='col'>
            <div
              style={{
                paddingTop: "1rem",
                paddingBottom: "1rem",
              }}
            >
              <div
                className='row d-flex justify-content-between align-items-center'
                onClick={() =>
                  this.setState({
                    showExerciseDetail: !this.state.showExerciseDetail,
                  })
                }
                style={{ cursor: "pointer" }}
              >
                <div className='col-10'>
                  <h6 className='text-capitalize p-smaller m-0'>
                    {this.props.exercise.video.name}
                    &nbsp;
                    {this.state.supersetGroup && (
                      <span className='text-muted' style={{ fontSize: 10 }}>
                        (SUPERSET)
                      </span>
                    )}
                  </h6>
                </div>
                <div className='col-2 text-center'>
                  {this.state.showExerciseDetail === true ? (
                    <i className='far text-muted fa-chevron-up' />
                  ) : (
                    <i className='far text-muted fa-chevron-right' />
                  )}
                </div>
              </div>
            </div>
            {this.state.showExerciseDetail === true ? (
              <React.Fragment>
                <Sets
                  exerciseId={this.props.exercise.id}
                  removeSets={this.state.removeSets}
                />
                <div className='row'>
                  <div className='col p-3'>
                    <button
                      className='btn btn-sm btn-link btn-block'
                      onClick={() =>
                        this.setState({
                          changeOrder: !this.state.changeOrder,
                        })
                      }
                    >
                      <i className='far fa-sort' /> &nbsp;{" "}
                      {this.state.changeOrder === true
                        ? "Finish Ordering"
                        : "Change Order"}
                    </button>
                  </div>
                  <div className='col p-3'>
                    <button
                      className='btn btn-sm btn-link btn-block'
                      onClick={() =>
                        this.deleteExercise(this.props.exercise.id)
                      }
                      type='button'
                    >
                      <i className='far fa-times' /> &nbsp; Delete Exercise
                    </button>
                  </div>
                </div>
              </React.Fragment>
            ) : null}
          </div>
          <div
            className='col-auto'
            style={{
              width: 3,
              borderRadius: 5,
              marginLeft: 10,
              marginTop: 7,
              marginBottom: 7,
              display: "flex",
              flexDirection: "column",
              backgroundColor: "transparent",
            }}
          />
        </div>
        {this.state.changeOrder === true
          ? this.state.exerciseArrayIndex !==
              this.props.exercises.length - 1 && (
              <div className='text-center m-2'>
                <button
                  onClick={() => this.setState({ toMove: 1 })}
                  className='btn btn-secondary btn-xs btn-rounded'
                >
                  <i className='fas fa-arrow-alt-down' />
                </button>
              </div>
            )
          : null}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    exercises: state.training.exercises,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetExercises: programId => {
      dispatch(getExercises(programId));
    },
    onUpdateExercises: exercises => {
      dispatch(updateExercises(exercises));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Exercise);
