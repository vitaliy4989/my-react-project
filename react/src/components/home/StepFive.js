import React, { Component } from "react";
import { connect } from "react-redux";
import { getStepFour, getStepFive } from "../../redux/actions/home";
import MDSpinner from "react-md-spinner";
import axiosAjax from "../../config/axiosAjax";
import VideoBlockStub from "../shared/VideoBlockStub";
import MenuMobile from "../../components/shared/MenuMobile";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";
import HomeStepCompleteMessage from "../HomeStepCompleteMessage";

class MealPlanning extends Component {
  constructor(props) {
    super(props);
    this.state = {
      iframeIsLoaded: false,
    };
    this.setIframeIsLoaded = this.setIframeIsLoaded.bind(this);
  }

  _createCombinations() {
    axiosAjax({
      method: "post",
      url: "/home-steps/five/",
    }).then(response => {
      this.props.onGetStepFive();
    });
    window.location.href =
      window.location.protocol +
      "//" +
      window.location.host +
      "/users/#/nutrition/recipes-combinations";
  }

  _skipStep() {
    axiosAjax({
      method: "post",
      url: "/home-steps/five/",
    }).then(response => {
      this.props.onGetStepFive();
    });
  }

  setIframeIsLoaded() {
    return this.setState({ iframeIsLoaded: true });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.stepFiveCompleted !== undefined) {
      if (this.props.stepFiveCompleted !== prevProps.stepFiveCompleted) {
        if (this.props.stepFiveCompleted === true) {
          this.setIframeIsLoaded();
        }
      }
    }
  }

  componentWillUnmount() {
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "block";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "block";
    }
  }

  componentDidMount() {
    this.props.onGetStepFour();
    this.props.onGetStepFive();

    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "none";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "none";
    }
  }

  render() {
    if (this.props.stepFiveCompleted)
      return (
        <HomeStepCompleteMessage
          pageTitle='Create Meal Plans'
          membershipStatus={this.props.membershipStatus}
          title='Step 5 Completed'
          nextStep='/home/community'
          nextStepTitle='Start Step 6'
          backLink='/home'
        >
          You have completed this step. All of the meal plans you create can be
          found in nutrition tab&nbsp;
          <a href='/users/#/nutrition/'>
            <u>here</u>
          </a>
          . Please now move on to Step 6.
        </HomeStepCompleteMessage>
      );

    return (
      <React.Fragment>
        <MenuMobile backLink='/home' />
        <div className='scroll-view'>
          <Heading
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Create Meal Plans</h4>
              </div>
            }
          />
          <HeadingMobile
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Create Meal Plans</h4>
              </div>
            }
          />
          <div
            className={
              $(window).width() > 768
                ? "container"
                : "container my-0 mx-auto p-0"
            }
          >
            <div className='home-step-wrapper'>
              {!this.state.iframeIsLoaded ? (
                <div className='row'>
                  <div className='col-lg-6'>
                    <VideoBlockStub withTitle={false} />
                  </div>
                </div>
              ) : null}

              {this.props.stepFour !== undefined &&
              this.props.stepFive !== undefined ? (
                <div className='row'>
                  <React.Fragment>
                    <div className='col-lg-6'>
                      <div className='embed-responsive embed-responsive-16by9 home-flex-step-video background-loader'>
                        <iframe
                          className='embed-responsive-item'
                          src='https://player.vimeo.com/video/268713057'
                          width='640'
                          height='350'
                          frameBorder='0'
                          webkitallowfullscreen='true'
                          mozallowfullscreen='true'
                          allowFullScreen={true}
                          onLoad={this.setIframeIsLoaded}
                        />
                      </div>
                    </div>
                    <div className='col-lg-6'>
                      <div className='home-step-container'>
                        <strong className='home-step-container__description-title'>
                          Explained
                        </strong>
                        <p className='home-step-container__description-content'>
                          If you have watched the{" "}
                          <a href='/users/#/coaching/modules/filter/nutrition/2'>
                            <u>nutrition introductory videos</u>
                          </a>
                          , you will understand why we will not write you a meal
                          plan. Check out our meal planner by clicking below.
                        </p>
                      </div>
                      <div className='shadow-sm p-2 bg-white fixed-bottom'>
                        <div className='container'>
                          <button
                            onClick={() => this._createCombinations()}
                            className='btn btn-block btn-success btn-rounded'
                          >
                            Meal Planner
                          </button>
                          <button
                            onClick={() => this._skipStep()}
                            style={{
                              color: "#00C2A2",
                              textDecoration: "underline",
                            }}
                            className='btn btn-sm btn-block btn-link'
                          >
                            Skip Step
                          </button>
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                </div>
              ) : (
                <div
                  className='text-center'
                  style={{ paddingTop: "10%", paddingBottom: "10%" }}
                >
                  <MDSpinner singleColor='#4a4090' />
                </div>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stepFour: state.home.stepFour,
    stepFive: state.home.stepFive,
    stepFiveCompleted: state.home.stepFiveCompleted,
    membershipStatus: state.user.membershipStatus,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetStepFour: () => {
      dispatch(getStepFour());
    },
    onGetStepFive: () => {
      dispatch(getStepFive());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MealPlanning);
