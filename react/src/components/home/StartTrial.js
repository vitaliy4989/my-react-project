import React, { Component } from "react";
import axiosAjax from "../../config/axiosAjax";
import connect from "react-redux/es/connect/connect";

class StartTrial extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clickedRestart: false,
    };
    this._restartTrial = this._restartTrial.bind(this);
  }

  _restartTrial(event) {
    event.preventDefault();
    if (
      confirm(
        "Click OK if you would like to restart the trial. This means that the home steps will reset and any data associated with them will be deleted.",
      )
    ) {
      this.setState({ clickedRestart: true });
      axiosAjax({
        method: "post",
        url: "/transactions/restart-trial/",
      })
        .then(response => {
          location.reload();
        })
        .catch(error => {
          this.setState({ clickedRestart: false });
        });
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col-auto d-flex flex-column justify-content-center'>
                <h4>Home</h4>
              </div>
            </div>
          </div>
        </div>
        <div className='scroll-view'>
          <div style={{ maxWidth: 300, margin: "auto", marginTop: "5%" }}>
            <img
              src='https://d3qqgqjfawam42.cloudfront.net/images/app/IMG_0829.jpg'
              className='img-fluid shadow rounded mb-4'
            />
            {this.props.membershipStatus !== "" && (
              <React.Fragment>
                <h4 className='lead text-center'>Let's get going...</h4>
                <div className='text-center animated-2 pulse'>
                  <a href='/users/membership-options'>
                    <button className='btn btn-block btn-primary enter-button-centered btn-rounded'>
                      Purchase a plan &nbsp;{" "}
                      <i className='far fa-arrow-right' />
                    </button>
                  </a>
                </div>
                <br />
                {this.state.clickedRestart === false && (
                  <div className='text-center animated-2 pulse'>
                    <button
                      onClick={this._restartTrial}
                      className='btn btn-block btn-primary enter-button-centered btn-rounded '
                    >
                      Take trial again
                    </button>
                  </div>
                )}
              </React.Fragment>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    membershipStatus: state.user.membershipStatus,
  };
};

export default connect(
  mapStateToProps,
  null,
)(StartTrial);
