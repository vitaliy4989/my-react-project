import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { getStepTwo, getStepThree } from "../../redux/actions/home";
import Request from "../exercise/personalised/Request";
import MDSpinner from "react-md-spinner";
import MenuMobile from "../../components/shared/MenuMobile";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";
import HomeStepCompleteMessage from "../HomeStepCompleteMessage";
import VideoBlockStub from "../shared/VideoBlockStub";
import axiosAjax from "../../config/axiosAjax";

class TrainingProgram extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      iframeIsLoaded: false,
    };
    this.setIframeIsLoaded = this.setIframeIsLoaded.bind(this);
    this._completeStepThree = this._completeStepThree.bind(this);
  }

  componentDidMount() {
    this.props.onGetStepTwo();
    this.props.onGetStepThree();

    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "none";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "none";
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.stepThreeCompleted !== undefined) {
      if (this.props.stepThreeCompleted !== prevProps.stepThreeCompleted) {
        if (this.props.stepThreeCompleted === true) {
          this.setIframeIsLoaded();
        }
      }
    }
  }

  componentWillUnmount() {
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "block";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "block";
    }
  }

  setIframeIsLoaded() {
    return this.setState({ iframeIsLoaded: true });
  }

  _completeStepThree(event) {
    event.preventDefault();
    axiosAjax({
      method: "post",
      url: "/home-steps/three/",
    }).then(response => {
      this.props.onGetStepThree();
    });
    window.location.href =
      window.location.protocol +
      "//" +
      window.location.host +
      "/users/#/home/your-macros";
  }

  render() {
    if (this.props.stepThree !== undefined && this.props.stepThree.completed)
      return (
        <HomeStepCompleteMessage
          pageTitle='Start Exercising'
          title='Step 3 Completed'
          nextStep='/home/your-macros'
          nextStepTitle='Start Step 4'
          backLink='/home'
        >
          This step is completed. Your exercise programs can be found in your
          exercise tab&nbsp;
          <a href='/users/#/training'>
            <u>here</u>
          </a>
          . You will receive email notifications when we update your program. If
          you do not have an email set on your account, you can set one in your{" "}
          <a href='/users/account/'>
            <u>account area</u>
          </a>
          . Please now move on to Step 4.
        </HomeStepCompleteMessage>
      );

    return (
      <React.Fragment>
        <MenuMobile back={true} backLink='/home' />
        <div className='scroll-view'>
          <Heading
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Start Exercising</h4>
              </div>
            }
          />
          <HeadingMobile
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Start Exercising</h4>
              </div>
            }
          />
          <div
            className={
              $(window).width() > 768
                ? "container"
                : "container my-0 mx-auto p-0"
            }
          >
            <div className='home-step-wrapper'>
              {!this.state.iframeIsLoaded ? (
                <div className='row'>
                  <div className='col-lg-6'>
                    <VideoBlockStub withTitle={false} />
                  </div>
                </div>
              ) : null}

              {this.props.stepTwo !== undefined &&
              this.props.stepThree !== undefined ? (
                <div className='row'>
                  <div className='col-lg-6'>
                    <div className='embed-responsive embed-responsive-16by9 bg-dark background-loader'>
                      <iframe
                        className='embed-responsive-item'
                        src='https://player.vimeo.com/video/273814379'
                        width='640'
                        height='360'
                        frameBorder='0'
                        webkitallowfullscreen='true'
                        mozallowfullscreen='true'
                        allowFullScreen={true}
                        onLoad={this.setIframeIsLoaded}
                      />
                    </div>
                  </div>
                  <div className='col-lg-6'>
                    <div className='home-step-container'>
                      <Request noBorder={true} />
                      {this.props.membershipStatus !== "trialing" ? (
                        <div style={{ marginTop: 40 }}>
                          <strong className='home-step-container__description-title'>
                            Explained
                          </strong>
                          <p className='home-step-container__description-content'>
                            Here we have a questionnaire for you to answer so
                            that we can write your personalised exercise
                            program. Please click below to watch an example.{" "}
                          </p>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
              ) : (
                <div
                  className='text-center'
                  style={{ paddingTop: "10%", paddingBottom: "10%" }}
                >
                  <MDSpinner singleColor='#4a4090' />
                </div>
              )}
              {this.props.membershipStatus === "trialing" && (
                <div className='shadow-sm p-2 bg-white fixed-bottom'>
                  <div className='container'>
                    <button
                      onClick={this._completeStepThree}
                      className='btn btn-rounded btn-block btn-success'
                    >
                      Next Step
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
        <div
          className='modal fade'
          id='trainingProgramExample'
          tabIndex='-1'
          role='dialog'
          aria-labelledby='trainingProgramExample'
          aria-hidden='true'
        >
          <div className='modal-dialog' role='document'>
            <div className='modal-content'>
              <div className='modal-header'>
                <button
                  type='button'
                  className='close'
                  data-dismiss='modal'
                  aria-label='Close'
                >
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>
              <div className='modal-body'>
                <div className='embed-responsive embed-responsive-16by9'>
                  <iframe
                    className='embed-responsive-item'
                    src='https://player.vimeo.com/video/291244650'
                    width='640'
                    height='360'
                    frameborder='0'
                    webkitallowfullscreen='true'
                    mozallowfullscreen='true'
                    allowFullScreen={true}
                    onLoad={this.setIframeIsLoaded}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stepTwo: state.home.stepTwo,
    stepThree: state.home.stepThree,
    stepThreeCompleted: state.home.stepThreeCompleted,
    membershipStatus: state.user.membershipStatus,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetStepTwo: () => {
      dispatch(getStepTwo());
    },
    onGetStepThree: () => {
      dispatch(getStepThree());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TrainingProgram);
