import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { getStepOne } from "../../redux/actions/home";
import MDSpinner from "react-md-spinner";
import HomeStepOneSubfilters from "../videos/HomeStepOneSubfilters/index";
import MenuMobile from "../../components/shared/MenuMobile";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";
import HomeStepCompleteMessage from "../HomeStepCompleteMessage";

class BreadButter extends PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onGetStepOne();

    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "none";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "none";
    }
  }

  componentWillUnmount() {
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "block";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "block";
    }
  }

  render() {
    if (this.props.stepOneCompleted)
      return (
        <HomeStepCompleteMessage
          pageTitle='Learn The Fundamentals'
          membershipStatus={this.props.membershipStatus}
          title='Step 1 Completed'
          nextStep='/home/goal-setting'
          nextStepTitle='Start Step 2'
          backLink='/home'
        >
          Ok, so you have now completed the introductory videos and should have
          a better understanding of the fundamental principles we follow. We
          have many more videos in the JSA which build on these fundamental
          principles. To watch all of our videos, you will need to&nbsp;
          <a href='/users/membership-options/'>
            <u>upgrade from your trial.</u>
          </a>
          &nbsp;Please continue to step 2.
        </HomeStepCompleteMessage>
      );

    return (
      <React.Fragment>
        <MenuMobile backLink='/home' />
        <div className='scroll-view'>
          <Heading
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Learn The Fundamentals</h4>
              </div>
            }
          />
          <HeadingMobile
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Learn The Fundamentals</h4>
              </div>
            }
          />
          <div
            className={
              $(window).width() > 768
                ? "container"
                : "container my-0 mx-auto p-0"
            }
          >
            <div className='home-step-wrapper'>
              {this.props.stepOne !== undefined ? (
                <div className='row'>
                  <React.Fragment>
                    <div className='col-lg-6'>
                      <HomeStepOneSubfilters stepOne={this.props.stepOne} />
                    </div>
                    <div className='col-lg-6'>
                      <div className='home-step-container'>
                        <strong className='home-step-container__description-title'>
                          Explained
                        </strong>
                        <p className='home-step-container__description-content'>
                          We would like to start by teaching you some
                          fundamentals. To complete this step, please watch the
                          videos on this step and mark each video as completed
                          by clicking the check button.
                        </p>
                        <p className='home-step-container__description-content'>
                          Once you have completed each video, we will move onto
                          the next step, goal setting.
                        </p>
                      </div>
                    </div>
                  </React.Fragment>
                </div>
              ) : (
                <div
                  className='text-center'
                  style={{ paddingTop: "10%", paddingBottom: "10%" }}
                >
                  <MDSpinner singleColor='#4a4090' />
                </div>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stepOne: state.home.stepOne,
    stepOneCompleted: state.home.stepOneCompleted,
    membershipStatus: state.user.membershipStatus,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetStepOne: () => {
      dispatch(getStepOne());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BreadButter);
