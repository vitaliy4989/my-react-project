import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { getStepOne, getStepTwo } from "../../redux/actions/home";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import MenuMobile from "../../components/shared/MenuMobile";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";
import HomeStepCompleteMessage from "../HomeStepCompleteMessage";

class GoalSetting extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      goal: undefined,
      goalCategory: undefined,
      update: false,
      sending: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createGoal = this.createGoal.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  componentDidMount() {
    this.props.onGetStepOne();
    this.props.onGetStepTwo();

    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "none";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "none";
    }
  }

  componentWillUnmount() {
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "block";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "block";
    }
  }

  createGoal(event) {
    this.setState({ sending: true });
    event.preventDefault();
    const formData = new FormData();
    formData.append("goal", this.state.goal);
    formData.append("check_ins", true);
    formData.append("goal_category", this.state.goalCategory);
    axiosAjax({
      method: "post",
      url: "/progress/goal/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetStepTwo();
        this.setState({
          sending: false,
        });
      })
      .catch(error => {
        this.setState({ sending: false });
      });
  }

  render() {
    if (this.props.stepTwo !== undefined && this.props.stepTwo.completed)
      return (
        <HomeStepCompleteMessage
          pageTitle='Set An Achievable Goal'
          title='Step 2 Completed'
          nextStep='/home/training-program'
          nextStepTitle='Start Step 3'
          backLink='/home'
        >
          You have completed this step! We will check in with you weekly to see
          how everything is going. When you have achieved your goal, you can
          update your goal in your progress tab&nbsp;
          <a href='/users/#/progress'>
            <u>here</u>
          </a>
          . Please now move on to Step 3.
        </HomeStepCompleteMessage>
      );

    return (
      <>
        <MenuMobile backLink='/home' />
        <div className='scroll-view'>
          <Heading
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Set An Achievable Goal</h4>
              </div>
            }
          />
          <HeadingMobile
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Set An Achievable Goal</h4>
              </div>
            }
          />
          <div
            className={
              $(window).width() > 768
                ? "container"
                : "container my-0 mx-auto p-0"
            }
          >
            <div className='home-step-wrapper'>
              {this.props.stepTwo !== undefined &&
              this.props.stepOne !== undefined ? (
                <div className='row'>
                  <div className='col-lg-6'>
                    <div className='home-step-container'>
                      {this.props.stepOne.completed === true &&
                        this.props.stepTwo.completed !== true && (
                          <div>
                            <form
                              className={
                                this.props.stepTwo.completed === true
                                  ? "animated fadeOut"
                                  : ""
                              }
                              onSubmit={this.createGoal}
                            >
                              <div className='form-group'>
                                <label htmlFor='goal'>In General</label>
                                <br />
                                <select
                                  name='goalCategory'
                                  id='goalCategory'
                                  value={this.state.goalCategory}
                                  onChange={this.handleInputChange}
                                  className='custom-select form-control-single-border text-center'
                                  style={{ maxWidth: 300 }}
                                  required
                                >
                                  <option value='' disabled selected>
                                    Select General Goal
                                  </option>
                                  <option value='Build Muscle'>
                                    Build Muscle
                                  </option>
                                  <option value='Lose Fat'>Lose Fat</option>
                                  <option value='Improve Strength'>
                                    Improve Strength
                                  </option>
                                  <option value='Improve CV'>Improve CV</option>
                                  <option value='General Health'>
                                    Improve Overall Health
                                  </option>
                                  <option value='Other'>Other</option>
                                </select>
                              </div>
                              <div className='form-group'>
                                <label htmlFor='goal'>
                                  In Detail <br />
                                  <small>(250 characters limit)</small>
                                </label>
                                <textarea
                                  maxLength='250'
                                  autoComplete='off'
                                  onChange={this.handleInputChange}
                                  value={this.state.goal}
                                  name='goal'
                                  className='form-control form-control-single-border'
                                  placeholder='Write your general goal here in more detail.'
                                  required
                                />
                              </div>
                              <div className='shadow-sm p-2 bg-white fixed-bottom'>
                                <div className='container'>
                                  <button
                                    id='formButton'
                                    className='btn btn-block btn-rounded btn-primary'
                                    disabled={this.state.sending}
                                  >
                                    Enter
                                  </button>
                                </div>
                              </div>
                            </form>
                          </div>
                        )}
                    </div>
                  </div>
                  <div className='col-lg-6'>
                    <div className='home-step-container'>
                      <strong className='home-step-container__description-title'>
                        Explained
                      </strong>
                      <p className='home-step-container__description-content'>
                        Goal setting can seem boring, but it is arguably the
                        most important part of this entire process. You should
                        not start exercising or following a diet until you have
                        decided on your goal.
                      </p>
                      <p className='home-step-container__description-content'>
                        Creating a solid goal helps you create a plan, which
                        will keep you on track and motivated. If you have not
                        watched the goal setting video, you can watch it again{" "}
                        <a href='/users/#/coaching/modules/module/56'>
                          <u>here</u>
                        </a>
                        . If you are ready to set your goal, please do so below,
                        or if you need a little help, ask us{" "}
                        <a href='/users/#/trainer-support'>
                          <u>here</u>
                        </a>
                        .
                      </p>
                    </div>
                  </div>
                </div>
              ) : (
                <div
                  className='text-center'
                  style={{ paddingTop: "10%", paddingBottom: "10%" }}
                >
                  <MDSpinner singleColor='#4a4090' />
                </div>
              )}
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stepOne: state.home.stepOne,
    stepTwo: state.home.stepTwo,
    stepTwoCompleted: state.home.stepTwoCompleted,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetStepOne: () => {
      dispatch(getStepOne());
    },
    onGetStepTwo: () => {
      dispatch(getStepTwo());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GoalSetting);
