import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { getStepThree, getStepFour } from "../../redux/actions/home";
import Calculator from "../nutrition/Calculator";
import MDSpinner from "react-md-spinner";
import MenuMobile from "../../components/shared/MenuMobile";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";
import HomeStepCompleteMessage from "../HomeStepCompleteMessage";

class YourMacros extends PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onGetStepThree();
    this.props.onGetStepFour();

    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "none";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "none";
    }
  }

  componentWillUnmount() {
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "block";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "block";
    }
  }

  render() {
    if (
      this.props.stepFour !== undefined &&
      this.props.stepFour.completed === true
    )
      return (
        <HomeStepCompleteMessage
          pageTitle='Calculate Your Macros'
          membershipStatus={this.props.membershipStatus}
          title='Step 4 Completed'
          nextStep='/home/meal-planning'
          nextStepTitle='Start Step 5'
          backLink='/home'
        >
          Your macros have been calculated. You can recalculate and visit your
          macros in the nutrition tab&nbsp;
          <a href='/users/#/nutrition/'>
            <u>here</u>
          </a>
          . Next, we are going to show you how to use the recipes to plan meals.
          Please move on to Step 5.
        </HomeStepCompleteMessage>
      );
    return (
      <React.Fragment>
        <MenuMobile backLink='/home' />
        <div className='scroll-view' id='scroll-top'>
          <Heading
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Calculate Your Macros</h4>
              </div>
            }
          />
          <HeadingMobile
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Calculate Your Macros</h4>
              </div>
            }
          />
          <div
            className={
              $(window).width() > 768
                ? "container"
                : "container my-0 mx-auto p-0"
            }
          >
            <div className='home-step-wrapper'>
              {this.props.stepThree !== undefined &&
              this.props.stepFour !== undefined ? (
                <div className='row'>
                  <React.Fragment>
                    <div className='col-lg-6'>
                      <div className='home-step-container'>
                        <Calculator noBorder={true} />
                      </div>
                    </div>
                    <div className='col-lg-6'>
                      <div className='home-step-container'>
                        <strong className='home-step-container__description-title'>
                          Explained
                        </strong>
                        <p className='home-step-container__description-content'>
                          If you wanted to save or spend money, you'd want to
                          watch your finances, right? Well here we will use the
                          numbers to make your life of 'dieting' easier. Using
                          our calculator below we can determine a start position
                          for you to get going today.
                        </p>
                      </div>
                    </div>
                  </React.Fragment>
                </div>
              ) : (
                <div
                  className='text-center'
                  style={{ paddingTop: "10%", paddingBottom: "10%" }}
                >
                  <MDSpinner singleColor='#4a4090' />
                </div>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stepThree: state.home.stepThree,
    stepFour: state.home.stepFour,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetStepThree: () => {
      dispatch(getStepThree());
    },
    onGetStepFour: () => {
      dispatch(getStepFour());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(YourMacros);
