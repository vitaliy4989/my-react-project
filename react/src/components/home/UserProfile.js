import React, { Component } from "react";
import { connect } from "react-redux";
import Heading from "../shared/Heading";
import HeadingMobile from "../shared/HeadingMobile";

class UserProfile extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <Heading
          class='app-header'
          columns={
            <div className='col'>
              <div className='row'>
                <div
                  className='col d-flex flex-column justify-content-center'
                  style={{ overflow: "hidden", whiteSpace: "nowrap" }}
                >
                  <h4>
                    Hi,{" "}
                    {this.props.firstName
                      ? this.props.firstName
                      : this.props.username}
                  </h4>
                </div>
              </div>
            </div>
          }
        />
        <HeadingMobile
          class='app-header'
          columns={
            <div className='col'>
              <div className='row'>
                <div
                  className='col d-flex flex-column justify-content-center'
                  style={{ overflow: "hidden", whiteSpace: "nowrap" }}
                >
                  {this.props.back === true ? (
                    <React.Fragment>
                      {history.length > 1 && (
                        <div
                          style={{ marginLeft: 0, paddingLeft: 0 }}
                          className='nav-link'
                          onClick={() => window.history.back()}
                        >
                          <i className='fas white p-0 m-0 fa-lg fa-chevron-left' />
                        </div>
                      )}
                    </React.Fragment>
                  ) : (
                    <h4>
                      Hi,{" "}
                      {this.props.firstName
                        ? this.props.firstName
                        : this.props.username}
                    </h4>
                  )}
                </div>
                <div className='col-auto d-flex flex-column justify-content-center'>
                  {this.props.image === undefined || !this.props.image ? (
                    <a href='/users/account/'>
                      <img
                        src='https://d3qqgqjfawam42.cloudfront.net/profile-pic-white.svg'
                        className='for-mobile rounded-circle'
                        width='30'
                        height='30'
                      />
                    </a>
                  ) : (
                    <a href='/users/account/'>
                      <img
                        src={this.props.image}
                        className='for-mobile rounded-circle'
                        width='30'
                        height='30'
                      />
                    </a>
                  )}
                </div>
              </div>
            </div>
          }
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    image: state.user.image,
    firstName: state.user.firstName,
    username: state.user.username,
  };
};

export default connect(
  mapStateToProps,
  null,
)(UserProfile);
