import React from "react";
import Programs from "../exercise/personalised/Programs";
import NewHomeTab from "../videos/NewHomeTab/index";

export default class StepsCompleted extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='container'>
        <Programs home={true} />
        <NewHomeTab />
      </div>
    );
  }
}
