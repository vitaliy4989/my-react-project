import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { getStepFive, getStepSix } from "../../redux/actions/home";
import MDSpinner from "react-md-spinner";
import axiosAjax from "../../config/axiosAjax";
import VideoBlockStub from "../shared/VideoBlockStub";
import MenuMobile from "../../components/shared/MenuMobile";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

class Community extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      iframeIsLoaded: false,
    };

    this._completeStepSix = this._completeStepSix.bind(this);
    this.setIframeIsLoaded = this.setIframeIsLoaded.bind(this);
  }

  componentDidMount() {
    this.props.onGetStepFive();
    this.props.onGetStepSix();

    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "none";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "none";
    }
  }

  componentWillUnmount() {
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "block";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "block";
    }
  }

  _completeStepSix(event) {
    event.preventDefault();
    window.open("https://www.facebook.com/groups/193449874723532/?fref=nf");
    axiosAjax({
      method: "post",
      url: "/home-steps/six/",
    }).then(response => {
      this.props.onGetStepSix();
      window.location.href =
        window.location.protocol +
        "//" +
        window.location.host +
        "/users/#/home";
    });
  }

  setIframeIsLoaded() {
    return this.setState({ iframeIsLoaded: true });
  }

  render() {
    return (
      <React.Fragment>
        <MenuMobile backLink='/home' />
        <div className='scroll-view'>
          <Heading
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Join the JSA Community</h4>
              </div>
            }
          />
          <HeadingMobile
            columns={
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Join the JSA Community</h4>
              </div>
            }
          />
          <div
            className={
              $(window).width() > 768
                ? "container"
                : "container my-0 mx-auto p-0"
            }
          >
            <div className='home-step-wrapper'>
              {!this.state.iframeIsLoaded ? (
                <div className='row'>
                  <div className='col-lg-6'>
                    <VideoBlockStub withTitle={false} />
                  </div>
                </div>
              ) : null}

              {this.props.stepFive !== undefined &&
              this.props.stepSix !== undefined ? (
                <div className='row'>
                  <div className='col-lg-6'>
                    <div className='embed-responsive embed-responsive-16by9 home-flex-step-video background-loader'>
                      <iframe
                        className='embed-responsive-item'
                        src='https://player.vimeo.com/video/290868848'
                        width='640'
                        height='360'
                        frameBorder='0'
                        webkitallowfullscreen='true'
                        mozallowfullscreen='true'
                        allowFullScreen='true'
                        onLoad={this.setIframeIsLoaded}
                      />
                    </div>
                  </div>
                  <div className='col-lg-6'>
                    <div className='home-step-container'>
                      {this.props.stepFive.completed === true ? (
                        <React.Fragment>
                          <strong className='home-step-container__description-title'>
                            Explained
                          </strong>
                          <p className='home-step-container__description-content'>
                            Never underestimate the power of a community, join
                            thousands of like-minded JSA members to celebrate
                            journey wins, new personal bests or just to have
                            your daily laugh.
                          </p>
                          {this.props.membershipStatus !== "trialing" ? (
                            <p className='home-step-container__description-content'>
                              Access the JSA unofficial Facebook group (JSAM)
                              and complete this step by clicking the button
                              below.
                            </p>
                          ) : (
                            <p className='home-step-container__description-content'>
                              Although the JSAM Facebook group is not part of a
                              paid membership, access to it is the privilege of
                              past and current JSA members.
                            </p>
                          )}
                        </React.Fragment>
                      ) : (
                        <p className='home-step-container__description-content text-muted'>
                          Never underestimate the power of a community, join
                          thousands of like-minded JSA members to celebrate
                          journey wins, new personal bests or just to have your
                          daily laugh. Join the Facebook group by finishing Step
                          5.
                        </p>
                      )}
                    </div>
                  </div>
                  <div className='shadow-sm p-2 bg-white fixed-bottom'>
                    <div className='container'>
                      {this.props.membershipStatus === "trialing" ? (
                        <a
                          href='/users/membership-options/'
                          className='btn btn-block btn-rounded btn-success'
                        >
                          Upgrade Account
                        </a>
                      ) : (
                        <a
                          onClick={this._completeStepSix}
                          target='_blank'
                          href='https://www.facebook.com/groups/193449874723532/?fref=nf'
                        >
                          <button className='btn btn-rounded btn-block btn-fb'>
                            <span className='fab fa-facebook-f' /> &nbsp;
                            Facebook Group
                          </button>
                        </a>
                      )}
                    </div>
                  </div>
                </div>
              ) : (
                <div
                  className='text-center'
                  style={{ paddingTop: "10%", paddingBottom: "10%" }}
                >
                  <MDSpinner singleColor='#4a4090' />
                </div>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stepFive: state.home.stepFive,
    stepSix: state.home.stepSix,
    membershipStatus: state.user.membershipStatus,
    homeStepSix: state.user.homeStepSix,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetStepFive: () => {
      dispatch(getStepFive());
    },
    onGetStepSix: () => {
      dispatch(getStepSix());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Community);
