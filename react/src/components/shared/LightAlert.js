import React from "react";

const LightAlert = props => {
  return (
    <div
      className={props.container === true ? "container max-width-700" : ""}
      style={props.style ? props.style : {}}
    >
      <div className={"alert alert-light " + props.class}>{props.message}</div>
    </div>
  );
};

export default LightAlert;
