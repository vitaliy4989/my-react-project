import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { getImage } from "../../redux/actions/user";

class MainNav extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.ongetImage();
  }

  render() {
    if (
      this.props.membershipStatus === "active" ||
      this.props.membershipStatus === "trialing"
    ) {
      if (this.props.membershipType !== "JSA Core") {
        return (
          <nav className='navbar navbar-expand-md navbar-light'>
            <div className='container'>
              <a className='navbar-brand' href='/'>
                <img src='https://d3qqgqjfawam42.cloudfront.net/JSA64x64-purple.svg' />
              </a>
              <button
                style={{ outline: "none" }}
                className='navbar-toggler navbar-toggler-right'
                type='button'
                data-toggle='collapse'
                data-target='#navbar'
                aria-controls='navbar'
                aria-expanded='false'
                aria-label='Toggle navigation'
              >
                <i className='fal fa-lg fa-bars dark-purp' />
              </button>
              <div className='collapse navbar-collapse' id='navbar'>
                {this.props.membershipType === "Standard Plan" ? (
                  <ul className='navbar-nav mr-auto'>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='training'
                        className='nav-link'
                        to='/training'
                      >
                        Exercise
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='coaching'
                        className='nav-link'
                        to='/coaching/modules'
                      >
                        Learn
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='nutrition'
                        className='nav-link'
                        to='/nutrition/'
                      >
                        Nutrition
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='recipes'
                        className='nav-link'
                        to='/recipes/'
                      >
                        Recipes
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='progress'
                        className='nav-link'
                        to='/progress'
                      >
                        Progress
                      </NavLink>
                    </li>
                  </ul>
                ) : (
                  <ul className='navbar-nav mr-auto'>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='home'
                        name='main-links'
                        className='nav-link'
                        to='/home'
                      >
                        Home
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='coaching'
                        className='nav-link'
                        to='/coaching/modules'
                      >
                        Learn
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='training'
                        className='nav-link'
                        to='/training'
                      >
                        Exercise
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='nutrition'
                        className='nav-link'
                        to='/nutrition/'
                      >
                        Nutrition
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='recipes'
                        className='nav-link'
                        to='/recipes/'
                      >
                        Recipes
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='progress'
                        className='nav-link'
                        to='/progress'
                      >
                        Progress
                      </NavLink>
                    </li>
                    <li className='nav-item'>
                      <NavLink
                        onClick={() => $("#navbar").collapse("hide")}
                        id='challenge'
                        className='nav-link'
                        to='/challenge'
                      >
                        Challenge
                      </NavLink>
                    </li>
                  </ul>
                )}
                <ul className='navbar-nav ml-auto'>
                  <li className='nav-item dropleft'>
                    <a
                      className='nav-link dropdown-toggle'
                      href='#'
                      id='navbarDropdownMenuLink'
                      data-toggle='dropdown'
                      aria-haspopup='true'
                      aria-expanded='false'
                    >
                      {this.props.image === undefined || !this.props.image ? (
                        <img
                          src='https://d3qqgqjfawam42.cloudfront.net/profile-pic-black.svg'
                          style={{ border: "solid 1px white" }}
                          className='rounded-circle'
                          width={window.innerWidth <= 768 ? "30" : "30"}
                          height={window.innerWidth <= 768 ? "30" : "30"}
                        />
                      ) : (
                        <img
                          src={this.props.image}
                          style={{ border: "solid 2px #4a4090" }}
                          className='rounded-circle'
                          width={window.innerWidth <= 768 ? "30" : "30"}
                          height={window.innerWidth <= 768 ? "30" : "30"}
                        />
                      )}
                    </a>
                    <div
                      className='dropdown-menu dropdown-menu-right custom-dropdown'
                      aria-labelledby='navbarDropdownMenuLink'
                    >
                      <a className='dropdown-item' href='/users/account'>
                        Account
                      </a>
                      <a
                        className='dropdown-item'
                        href='/users/membership-options/'
                      >
                        Pricing
                      </a>
                      <a
                        className='dropdown-item'
                        href='/users/#/frequently-asked-questions'
                      >
                        F.A.Qs
                      </a>
                      <a className='dropdown-item' href='/accounts/logout'>
                        Log out &nbsp; <i className='far fa-sign-out' />
                      </a>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        );
      } else {
        return (
          <nav className='navbar navbar-expand-md navbar-light'>
            <div className='container'>
              <a className='navbar-brand' href='/'>
                <img src='https://d3qqgqjfawam42.cloudfront.net/JSA64x64-purple.svg' />
              </a>
              <button
                style={{ outline: "none" }}
                className='navbar-toggler navbar-toggler-right'
                type='button'
                data-toggle='collapse'
                data-target='#navbar'
                aria-controls='navbar'
                aria-expanded='false'
                aria-label='Toggle navigation'
              >
                <i className='fal fa-lg fa-bars dark-purp' />
              </button>
              <div className='collapse navbar-collapse' id='navbar'>
                <ul className='navbar-nav mr-auto'>
                  <li className='nav-item'>
                    <NavLink
                      onClick={() => $("#navbar").collapse("hide")}
                      id='coaching'
                      className='nav-link'
                      to='/coaching/modules'
                    >
                      Learn
                    </NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink
                      onClick={() => $("#navbar").collapse("hide")}
                      id='training'
                      className='nav-link'
                      to='/training'
                    >
                      Exercise
                    </NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink
                      onClick={() => $("#navbar").collapse("hide")}
                      id='progress'
                      className='nav-link'
                      to='/progress'
                    >
                      Progress
                    </NavLink>
                  </li>
                </ul>
                <ul className='navbar-nav ml-auto'>
                  <li className='nav-item dropdown'>
                    <a
                      className='nav-link dropdown-toggle'
                      href='#'
                      id='navbarDropdownMenuLink'
                      data-toggle='dropdown'
                      aria-haspopup='true'
                      aria-expanded='false'
                    >
                      {this.props.image === undefined || !this.props.image ? (
                        <img
                          src='https://d3qqgqjfawam42.cloudfront.net/profile-pic-black.svg'
                          style={{ border: "solid 1px white" }}
                          className='rounded-circle'
                          width={window.innerWidth <= 768 ? "30" : "30"}
                          height={window.innerWidth <= 768 ? "30" : "30"}
                        />
                      ) : (
                        <img
                          src={this.props.image}
                          style={{ border: "solid 2px #4a4090" }}
                          className='rounded-circle'
                          width={window.innerWidth <= 768 ? "30" : "30"}
                          height={window.innerWidth <= 768 ? "30" : "30"}
                        />
                      )}
                    </a>
                    <div
                      className='dropdown-menu dropdown-menu-right custom-dropdown'
                      aria-labelledby='navbarDropdownMenuLink'
                    >
                      <a className='dropdown-item' href='/users/account'>
                        Account
                      </a>
                      <a
                        className='dropdown-item'
                        href='/users/membership-options/'
                      >
                        Pricing
                      </a>
                      <a
                        className='dropdown-item'
                        href='/users/#/frequently-asked-questions'
                      >
                        F.A.Qs
                      </a>
                      <a className='dropdown-item' href='/accounts/logout'>
                        Log out &nbsp; <i className='far fa-sign-out' />
                      </a>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        );
      }
    } else {
      return (
        <nav className='navbar navbar-expand-md navbar-light'>
          <div className='container'>
            <a className='navbar-brand' href='/'>
              <img src='https://d3qqgqjfawam42.cloudfront.net/JSA64x64-purple.svg' />
            </a>
            <button
              style={{ outline: "none" }}
              className='navbar-toggler navbar-toggler-right'
              type='button'
              data-toggle='collapse'
              data-target='#navbar'
              aria-controls='navbar'
              aria-expanded='false'
              aria-label='Toggle navigation'
            >
              <i className='fal fa-lg fa-bars dark-purp' />
            </button>
            <div className='collapse navbar-collapse' id='navbar'>
              <ul className='navbar-nav mr-auto'>
                <li className='nav-item'>
                  <NavLink
                    onClick={() => $("#navbar").collapse("hide")}
                    id='home'
                    name='main-links'
                    className='nav-link'
                    to='/home'
                  >
                    Home
                  </NavLink>
                </li>
              </ul>
              <ul className='navbar-nav ml-auto'>
                <li className='nav-item dropdown'>
                  <a
                    className='nav-link dropdown-toggle'
                    href='#'
                    id='navbarDropdownMenuLink'
                    data-toggle='dropdown'
                    aria-haspopup='true'
                    aria-expanded='false'
                  >
                    {this.props.image === undefined || !this.props.image ? (
                      <img
                        src='https://d3qqgqjfawam42.cloudfront.net/profile-pic-black.svg'
                        style={{ border: "solid 1px white" }}
                        className='rounded-circle'
                        width={window.innerWidth <= 768 ? "30" : "30"}
                        height={window.innerWidth <= 768 ? "30" : "30"}
                      />
                    ) : (
                      <img
                        src={this.props.image}
                        style={{ border: "solid 2px #4a4090" }}
                        className='rounded-circle'
                        width={window.innerWidth <= 768 ? "30" : "30"}
                        height={window.innerWidth <= 768 ? "30" : "30"}
                      />
                    )}
                  </a>
                  <div
                    className='dropdown-menu dropdown-menu-right custom-dropdown'
                    aria-labelledby='navbarDropdownMenuLink'
                  >
                    <a className='dropdown-item' href='/users/account'>
                      Account
                    </a>
                    <a
                      className='dropdown-item'
                      href='/users/membership-options/'
                    >
                      Pricing
                    </a>
                    <a className='dropdown-item' href='/accounts/logout'>
                      Log out &nbsp; <i className='far fa-sign-out' />
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    image: state.user.image,
    membershipStatus: state.user.membershipStatus,
    membershipType: state.user.membershipType,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ongetImage: () => {
      dispatch(getImage());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { pure: false },
)(MainNav);
