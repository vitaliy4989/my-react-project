import React, { Component } from "react";

class Uploader extends Component {
  componentDidMount() {
    try {
      const widget = uploadcare.Widget(this.uploader);
      const { value, onChange, onUploadComplete } = this.props;

      if (typeof value !== "undefined") {
        widget.value(value);
      }
      if (typeof onChange === "function") {
        widget.onChange(onChange);
      }
      if (typeof onUploadComplete === "function") {
        widget.onUploadComplete(onUploadComplete);
      }
    } catch (e) {
      alert(
        "There was an issue loading our image provider, please try to refresh the page or clear your cache.",
      );
    }
  }

  getInputAttributes() {
    const attributes = Object.assign({}, this.props);

    delete attributes.value;
    delete attributes.onChange;
    delete attributes.onUploadComplete;

    return attributes;
  }

  render() {
    const attributes = this.getInputAttributes();

    return (
      <div style={this.props.styling}>
        <input
          type='hidden'
          ref={input => (this.uploader = input)}
          {...attributes}
          data-images-only={this.props.imageOnly}
          data-crop={this.props.dataCrop ? this.props.dataCrop : "true"}
          required
        />
      </div>
    );
  }
}

export default Uploader;
