import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { getImage } from "../../redux/actions/user";
import ChatNotifications from "../chat/ChatNotifications";

class MainNavMobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: false,
      IPhoneX: false,
      howToAddPwa: false,
      standalone: false,
    };
  }

  componentDidMount() {
    $(".button-collapse").sideNav({
      edge: "right",
      closeOnClick: true,
    });

    var sideNavScrollbar = document.querySelector(".custom-scrollbar");
    if (sideNavScrollbar) {
      Ps.initialize(sideNavScrollbar);
    }

    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    var ratio = window.devicePixelRatio || 1;
    var screen = {
      width: window.screen.width * ratio,
      height: window.screen.height * ratio,
    };

    if (iOS && screen.width == 1125 && screen.height === 2436) {
      this.setState({ IPhoneX: true });
    }

    if (
      window.matchMedia("(display-mode: standalone)").matches ||
      window.navigator.standalone === true
    ) {
      this.setState({ standalone: true });
    }

    this.props.ongetImage();
  }

  render() {
    if (
      this.props.membershipStatus === "active" ||
      this.props.membershipStatus === "trialing"
    ) {
      if (this.props.membershipType !== "JSA Core") {
        return (
          <React.Fragment>
            <div id='slide-out' className='side-nav side-nav-purple'>
              <ul className='custom-scrollbar'>
                <li>
                  <div className='logo-wrapper'>
                    <a href='/'>
                      <img
                        src='https://d3qqgqjfawam42.cloudfront.net/JSA64x64-white.svg'
                        className='img-fluid flex-center'
                      />
                    </a>
                  </div>
                </li>
                <li>
                  {this.props.membershipType === "Standard Plan" ? (
                    <ul className='collapsible'>
                      <li>
                        <NavLink activeStyle={linkActive} to='/recipes/'>
                          <i className='far fa-book-open' /> &nbsp; Recipes
                        </NavLink>
                      </li>
                      <li>
                        <NavLink activeStyle={linkActive} to='/progress'>
                          <i className='far fa-chart-line' /> &nbsp; Progress
                        </NavLink>
                      </li>
                    </ul>
                  ) : (
                    <ul className='collapsible'>
                      <li>
                        <NavLink activeStyle={linkActive} to='/nutrition/'>
                          <i className='far fa-utensils' /> &nbsp; Nutrition
                        </NavLink>
                      </li>
                      <li>
                        <NavLink activeStyle={linkActive} to='/recipes/'>
                          <i className='far fa-book-open' /> &nbsp; Recipes
                        </NavLink>
                      </li>
                      <li>
                        <NavLink activeStyle={linkActive} to='/progress'>
                          <i className='far fa-chart-line' /> &nbsp; Progress
                        </NavLink>
                      </li>
                      {this.props.membershipType !== "Standard Plan" && (
                        <li>
                          <NavLink activeStyle={linkActive} to='/challenge'>
                            <i className='far fa-trophy' /> &nbsp; Challenge
                          </NavLink>
                        </li>
                      )}
                      <li>
                        <NavLink
                          activeStyle={linkActive}
                          to='/frequently-asked-questions'
                        >
                          <i className='far fa-question-circle' /> &nbsp; F.A.Qs
                        </NavLink>
                      </li>
                    </ul>
                  )}
                  <ul className='collapsible'>
                    <li>
                      <a href='/users/account'>
                        <i className='far fa-user-circle' /> &nbsp; Account
                      </a>
                    </li>
                    <li>
                      <a href='/users/membership-options/'>
                        <i className='far fa-credit-card-blank' /> &nbsp;
                        Pricing
                      </a>
                    </li>
                    <li>
                      <a href='/emails/contact-us'>
                        <i className='far fa-envelope' /> &nbsp; Contact Us
                      </a>
                    </li>
                    {sessionStorage.membershipStatus === "active" &&
                    this.props.membershipType !== "Standard Plan" ? (
                      <li>
                        <a
                          target='_blank'
                          href='https://www.facebook.com/groups/193449874723532/'
                        >
                          <i className='fab fa-facebook-f' /> &nbsp; Community
                        </a>
                      </li>
                    ) : null}
                    {this.state.standalone === false && (
                      <li>
                        <a
                          onClick={() =>
                            this.setState({
                              howToAddPwa: !this.state.howToAddPwa,
                            })
                          }
                          href='#'
                        >
                          <i className='fal fa-mobile' /> &nbsp; Mobile App
                        </a>
                      </li>
                    )}
                    <li>
                      <a href='/accounts/logout'>
                        <i className='far fa-sign-out' /> &nbsp; Logout
                      </a>
                    </li>
                  </ul>
                  {sessionStorage.membershipStatus === "trialing" ? (
                    <ul className='collapsible'>
                      <li>
                        <a href='/users/membership-options/'>
                          <div className='btn btn-sm btn-light'>
                            UPGRADE ACCOUNT
                          </div>
                        </a>
                      </li>
                    </ul>
                  ) : null}
                </li>
              </ul>
            </div>

            <nav className='mobile-nav-bar'>
              <div className='container-fluid'>
                <div
                  className='mobile-bottom-navbar-container row d-flex justify-content-between align-items-center'
                  style={
                    this.state.IPhoneX === true &&
                    this.state.standalone === true
                      ? { paddingBottom: 25 }
                      : {}
                  }
                >
                  {this.props.membershipType === "Standard Plan" ? (
                    <React.Fragment>
                      <NavLink
                        activeStyle={activeLink}
                        className='tab col text-center'
                        to='/training'
                      >
                        <i className='far fa-lg fa-dumbbell' />
                        <div className='link'>Exercise</div>
                      </NavLink>
                      <NavLink
                        activeStyle={activeLink}
                        className='tab col text-center'
                        to='/coaching/modules'
                      >
                        <i className='far fa-lg fa-video' />
                        <div className='link'>Learn</div>
                      </NavLink>
                      <NavLink
                        activeStyle={activeLink}
                        className='tab col text-center'
                        to='/nutrition/'
                      >
                        <i className='far fa-utensils' />
                        <div className='link'>Nutrition</div>
                      </NavLink>
                      <a
                        href='#'
                        className='tab col text-center button-collapse'
                        data-activates='slide-out'
                      >
                        <i className='far fa-lg fa-bars' />
                        <div className='link'>More</div>
                      </a>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <NavLink
                        activeStyle={activeLink}
                        className='tab col text-center'
                        to='/home'
                      >
                        <i className='far fa-lg fa-home' />
                        <div className='link'>Home</div>
                      </NavLink>
                      <NavLink
                        activeStyle={activeLink}
                        className='tab col text-center'
                        to='/coaching/modules'
                      >
                        <i className='far fa-lg fa-video' />
                        <div className='link'>Learn</div>
                      </NavLink>
                      <NavLink
                        activeStyle={activeLink}
                        className='tab col text-center'
                        to='/training'
                      >
                        <i className='far fa-lg fa-dumbbell' />
                        <div className='link'>Exercise</div>
                      </NavLink>
                      <NavLink
                        activeStyle={activeLink}
                        className='tab col text-center'
                        to='/trainer-support'
                      >
                        <i className='far fa-lg fa-comments-alt' />
                        <ChatNotifications />
                        <div className='link'>Support</div>
                      </NavLink>
                      <a
                        href='#'
                        className='tab col text-center button-collapse'
                        data-activates='slide-out'
                      >
                        <i className='far fa-lg fa-bars' />
                        <div className='link'>More</div>
                      </a>
                    </React.Fragment>
                  )}
                </div>
              </div>
            </nav>
            {this.state.howToAddPwa === true && (
              <div style={{ maxWidth: 500, margin: "auto", fontSize: 14 }}>
                <div className='speech-bubble animated-2 bounceIn'>
                  <div style={{ marginBottom: "1rem" }}>
                    <i
                      onClick={() =>
                        this.setState({ howToAddPwa: !this.state.howToAddPwa })
                      }
                      style={{
                        padding: "3px 6px",
                        background: "#F1F2F4",
                        borderRadius: 50,
                      }}
                      className='fal red fa-times float-right'
                    />
                  </div>
                  <p className='p-smaller'>
                    On iOS, open the site in Safari, click{" "}
                    <img
                      className='rounded'
                      src='/share-option.png'
                      height='20'
                    />{" "}
                    below and then{" "}
                    <img
                      className='rounded'
                      src='/home-button.png'
                      height='20'
                    />{" "}
                    'Add To Home Screen'.
                  </p>
                  <p className='p-smaller'>
                    On Android, open the site in Chrome, click the menu button
                    and then 'Add to homescreen'.
                  </p>
                </div>
              </div>
            )}
          </React.Fragment>
        );
      } else {
        return (
          <React.Fragment>
            <div id='slide-out' className='side-nav side-nav-purple'>
              <ul className='custom-scrollbar'>
                <li>
                  <div className='logo-wrapper'>
                    <a href='/'>
                      <img
                        src='https://d3qqgqjfawam42.cloudfront.net/JSA64x64-white.svg'
                        className='img-fluid flex-center'
                      />
                    </a>
                  </div>
                </li>

                <li>
                  <ul className='collapsible'>
                    <li>
                      <NavLink activeStyle={linkActive} to='/progress'>
                        <i className='far fa-chart-line' /> &nbsp; Progress
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        activeStyle={linkActive}
                        to='/frequently-asked-questions'
                      >
                        <i className='far fa-question-circle' /> &nbsp; F.A.Qs
                      </NavLink>
                    </li>
                  </ul>
                  <ul className='collapsible'>
                    <li>
                      <a href='/users/account'>
                        <i className='far fa-user-circle' /> &nbsp; Account
                      </a>
                    </li>
                    <li>
                      <a href='/users/membership-options/'>
                        <i className='far fa-credit-card-blank' /> &nbsp;
                        Pricing
                      </a>
                    </li>
                    <li>
                      <a href='/emails/contact-us'>
                        <i className='far fa-envelope' /> &nbsp; Contact Us
                      </a>
                    </li>
                    {this.state.standalone === false && (
                      <li>
                        <a
                          onClick={() =>
                            this.setState({
                              howToAddPwa: !this.state.howToAddPwa,
                            })
                          }
                          href='#'
                        >
                          <i className='fal fa-mobile' /> &nbsp; Mobile App
                        </a>
                      </li>
                    )}
                    <li>
                      <a href='/accounts/logout'>
                        <i className='far fa-sign-out' /> &nbsp; Logout
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>

            <nav className='mobile-nav-bar'>
              <div className='container-fluid'>
                <div
                  className='mobile-bottom-navbar-container row d-flex justify-content-between align-items-center'
                  style={
                    this.state.IPhoneX === true &&
                    this.state.standalone === true
                      ? { paddingBottom: 25 }
                      : {}
                  }
                >
                  <NavLink
                    activeStyle={activeLink}
                    className='tab col text-center'
                    to='/coaching/modules'
                  >
                    <i className='far fa-lg fa-video' />
                    <div className='link'>Learn</div>
                  </NavLink>
                  <NavLink
                    activeStyle={activeLink}
                    className='tab col text-center'
                    to='/training'
                  >
                    <i className='far fa-lg fa-dumbbell' />
                    <div className='link'>Exercise</div>
                  </NavLink>
                  <NavLink
                    activeStyle={activeLink}
                    className='tab col text-center'
                    to='/trainer-support'
                  >
                    <i className='far fa-lg fa-comments-alt' />
                    <ChatNotifications />
                    <div className='link'>Support</div>
                  </NavLink>
                  <a
                    href='#'
                    className='tab col text-center button-collapse'
                    data-activates='slide-out'
                  >
                    <i className='far fa-lg fa-bars' />
                    <div className='link'>More</div>
                  </a>
                </div>
              </div>
            </nav>
            {this.state.howToAddPwa === true && (
              <div style={{ maxWidth: 500, margin: "auto", fontSize: 14 }}>
                <div className='speech-bubble animated-2 bounceIn'>
                  <div style={{ marginBottom: "1rem" }}>
                    <i
                      onClick={() =>
                        this.setState({ howToAddPwa: !this.state.howToAddPwa })
                      }
                      style={{
                        padding: "3px 6px",
                        background: "#F1F2F4",
                        borderRadius: 50,
                      }}
                      className='fal red fa-times float-right'
                    />
                  </div>
                  <p className='p-smaller'>
                    On iOS, open the site in Safari, click{" "}
                    <img
                      className='rounded'
                      src='/share-option.png'
                      height='20'
                    />{" "}
                    below and then{" "}
                    <img
                      className='rounded'
                      src='/home-button.png'
                      height='20'
                    />{" "}
                    'Add To Home Screen'.
                  </p>
                  <p className='p-smaller'>
                    On Android, open the site in Chrome, click the menu button
                    and then 'Add to homescreen'.
                  </p>
                </div>
              </div>
            )}
          </React.Fragment>
        );
      }
    } else {
      return (
        <React.Fragment>
          <div id='slide-out' className='side-nav side-nav-purple'>
            <ul className='custom-scrollbar'>
              <li>
                <div className='logo-wrapper'>
                  <a href='/'>
                    <img
                      src='https://d3qqgqjfawam42.cloudfront.net/JSA64x64-white.svg'
                      className='img-fluid flex-center'
                    />
                  </a>
                </div>
              </li>

              <li>
                <ul className='collapsible'>
                  <li>
                    <a href='/users/account'>
                      <i className='far fa-user-circle' /> &nbsp; Account
                    </a>
                  </li>
                  <li>
                    <a href='/users/membership-options/'>
                      <i className='far fa-credit-card-blank' /> &nbsp; Pricing
                    </a>
                  </li>
                  <li>
                    <a href='/emails/contact-us'>
                      <i className='far fa-envelope' /> &nbsp; Contact Us
                    </a>
                  </li>
                  {this.state.standalone === false && (
                    <li>
                      <a
                        onClick={() =>
                          this.setState({
                            howToAddPwa: !this.state.howToAddPwa,
                          })
                        }
                        href='#'
                      >
                        <i className='fal fa-mobile' /> &nbsp; Mobile App
                      </a>
                    </li>
                  )}
                  <li>
                    <a href='/accounts/logout'>
                      <i className='far fa-sign-out' /> &nbsp; Logout
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>

          <nav className='mobile-nav-bar'>
            <div className='container-fluid'>
              <div
                className='mobile-bottom-navbar-container row d-flex justify-content-between align-items-center'
                style={
                  this.state.IPhoneX === true && this.state.standalone === true
                    ? { paddingBottom: 25 }
                    : {}
                }
              >
                <NavLink
                  activeStyle={activeLink}
                  className='tab col text-center'
                  to='/home'
                >
                  <i className='far fa-lg fa-home' />
                  <div className='link'>Home</div>
                </NavLink>
                <a
                  href='#'
                  className='tab col text-center button-collapse'
                  data-activates='slide-out'
                >
                  <i className='far fa-lg fa-bars' />
                  <div className='link'>More</div>
                </a>
              </div>
            </div>
          </nav>
          {this.state.howToAddPwa === true && (
            <div style={{ maxWidth: 500, margin: "auto", fontSize: 14 }}>
              <div className='speech-bubble animated-2 bounceIn'>
                <div style={{ marginBottom: "1rem" }}>
                  <i
                    onClick={() =>
                      this.setState({ howToAddPwa: !this.state.howToAddPwa })
                    }
                    style={{
                      padding: "3px 6px",
                      background: "#F1F2F4",
                      borderRadius: 50,
                    }}
                    className='fal red fa-times float-right'
                  />
                </div>
                <p className='p-smaller'>
                  On iOS, open the site in Safari, click{" "}
                  <img
                    className='rounded'
                    src='/share-option.png'
                    height='20'
                  />{" "}
                  below and then{" "}
                  <img className='rounded' src='/home-button.png' height='20' />{" "}
                  'Add To Home Screen'.
                </p>
                <p className='p-smaller'>
                  On Android, open the site in Chrome, click the menu button and
                  then 'Add to homescreen'.
                </p>
              </div>
            </div>
          )}
        </React.Fragment>
      );
    }
  }
}

const activeLink = {
  color: "#4a4090",
  fontWeight: 900,
};

const linkActive = {
  color: "#4a4090",
  backgroundColor: "#e9ecef",
  border: "none",
};

const mapStateToProps = (state, ownProps) => {
  return {
    image: state.user.image,
    membershipStatus: state.user.membershipStatus,
    membershipType: state.user.membershipType,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ongetImage: () => {
      dispatch(getImage());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { pure: false },
)(MainNavMobile);
