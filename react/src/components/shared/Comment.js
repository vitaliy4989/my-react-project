import React from "react";

const Comment = props => {
  return (
    <React.Fragment>
      {props.date && (
        <div style={{ fontSize: 10 }} className='text-muted text-center m-1'>
          {props.date}
        </div>
      )}
      <div className={props.class ? props.class : ""} id={props.key}>
        <div className='media' style={props.styles}>
          {props.noImage === true ? null : props.image ? (
            <img
              className='mr-3 small-profile-wrapper rounded-circle'
              src={props.image + "-/autorotate/yes/"}
            />
          ) : (
            <img
              className='mr-3 small-profile-wrapper rounded-circle'
              src='https://d3qqgqjfawam42.cloudfront.net/profile-pic-black.svg'
            />
          )}
          <div className={"media-body " + props.textClass}>
            {props.heading && <p className='mt-0'>{props.heading}</p>}
            <div style={comment} className='p-smaller comment-wrapper'>
              {props.comment}
              {props.attachment && (
                <a href={props.attachment}>
                  <i className='fas fa-paperclip' />
                </a>
              )}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

const comment = {
  whiteSpace: "pre-wrap",
};

export default Comment;
