import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class MobileMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollDown: false,
    };
  }

  render() {
    return (
      <div className='for-mobile' style={{ marginBottom: "-1px" }}>
        <nav className='navbar navbar-expand second-nav'>
          <div className='container'>
            <ul className='navbar-nav'>
              {this.props.back === true && history.length > 1 ? (
                <li className='nav-item'>
                  <div
                    style={{ marginLeft: 0, paddingLeft: 0 }}
                    className='nav-link'
                    onClick={() => window.history.back()}
                  >
                    <i className='fas white m-0 p-0 fa-lg fa-chevron-left' />
                  </div>
                </li>
              ) : this.props.backLink ? (
                <li className='nav-item'>
                  <NavLink
                    style={{ marginLeft: 0, paddingLeft: 0 }}
                    className='nav-link'
                    to={this.props.backLink}
                  >
                    <i className='fas white m-0 p-0 fa-lg fa-chevron-left' />
                  </NavLink>
                </li>
              ) : null}
              {this.props.dropdown === true && (
                <li className='nav-item dropdown'>
                  <a
                    className='nav-link dropdown-toggle'
                    style={{ paddingLeft: 0 }}
                    href='#'
                    id='navbarDropdown'
                    role='button'
                    data-toggle='dropdown'
                    aria-haspopup='true'
                    aria-expanded='false'
                  >
                    {this.props.dropDownTitle}
                  </a>
                  <div
                    className='dropdown-menu'
                    aria-labelledby='navbarDropdown'
                  >
                    {this.props.dropDownOne}
                    {this.props.dropDownTwo}
                    {this.props.dropDownThree}
                    {this.props.dropDownFour}
                    {this.props.dropDownFive}
                    {this.props.dropDownSix}
                    {this.props.dropDownSeven}
                    {this.props.dropDownEight}
                    {this.props.dropDownNine}
                    {this.props.dropDownTen}
                    {this.props.dropDownEleven}
                    {this.props.dropDownTwelve}
                    {this.props.dropDownThirteen}
                  </div>
                </li>
              )}
              {this.props.linkOne && (
                <li className='nav-item'>{this.props.linkOne}</li>
              )}
              {this.props.linkTwo && (
                <li className='nav-item'>{this.props.linkTwo}</li>
              )}
              {this.props.linkThree && (
                <li className='nav-item'>{this.props.linkThree}</li>
              )}
              {this.props.linkFour && (
                <li className='nav-item'>{this.props.linkFour}</li>
              )}
            </ul>
            <ul className='navbar-nav ml-auto'>
              {this.props.linkRightOne && (
                <li className='nav-item'>{this.props.linkRightOne}</li>
              )}
              {this.props.linkRightTwo && (
                <li className='nav-item'>{this.props.linkRightTwo}</li>
              )}
              {this.props.linkRightThree && (
                <li className='nav-item'>{this.props.linkRightThree}</li>
              )}
              {this.props.linkRightFour && (
                <li className='nav-item'>{this.props.linkRightFour}</li>
              )}
              {this.props.linkRightFive && (
                <li className='nav-item'>{this.props.linkRightFive}</li>
              )}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default MobileMenu;
