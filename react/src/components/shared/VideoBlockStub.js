import React, { Fragment } from "react";

const titleStub = (
  <svg
    viewBox='0 0 341 25'
    version='1.1'
    xmlns='http://www.w3.org/2000/svg'
    className='video-block-stub-animation'
  >
    <title>Title stub</title>
    <desc>Created with sketchtool.</desc>
    <g
      id='Page-1'
      stroke='none'
      strokeWidth='1'
      fill='none'
      fillRule='evenodd'
      opacity='0.2'
    >
      <g
        id='4.1-video--waiting-for-download'
        transform='translate(-17.000000, -291.000000)'
        fill='#7E8494'
        fillRule='nonzero'
      >
        <g id='Group-3' transform='translate(17.000000, 291.000000)'>
          <rect id='Rectangle' x='0' y='0' width='93' height='7' rx='3.5' />
          <rect id='Rectangle' x='0' y='18' width='108' height='7' rx='3.5' />
          <path
            d='M286.5,18 L337.5,18 C339.432997,18 341,19.5670034 341,21.5 L341,21.5 C341,23.4329966 339.432997,25 337.5,25 L286.5,25 C284.567003,25 283,23.4329966 283,21.5 L283,21.5 C283,19.5670034 284.567003,18 286.5,18 Z'
            id='Rectangle'
          />
        </g>
      </g>
    </g>
  </svg>
);

const descriptionStub = (
  <svg
    viewBox='0 0 341 124'
    version='1.1'
    xmlns='http://www.w3.org/2000/svg'
    className='video-block-stub-animation'
  >
    <title>Description stub</title>
    <desc>Created with sketchtool.</desc>
    <g
      id='Page-1'
      stroke='none'
      strokeWidth='1'
      fill='none'
      fillRule='evenodd'
      opacity='0.2'
    >
      <g
        id='4.1-video--waiting-for-download'
        transform='translate(-17.000000, -353.000000)'
        fill='#7E8494'
        fillRule='nonzero'
      >
        <g id='Group-2' transform='translate(17.000000, 353.000000)'>
          <rect id='Rectangle' x='0' y='0' width='116' height='7' rx='3.5' />
          <g id='Rectangle-2' transform='translate(0.000000, 19.000000)'>
            <path
              d='M3.5,0 L211.689873,0 C213.62287,1.50965273e-14 215.189873,1.56700338 215.189873,3.5 L215.189873,3.5 C215.189873,5.43299662 213.62287,7 211.689873,7 L3.5,7 C1.56700338,7 2.36723813e-16,5.43299662 0,3.5 L0,3.5 C-2.36723813e-16,1.56700338 1.56700338,3.55085719e-16 3.5,0 Z'
              id='Rectangle'
            />
            <path
              d='M235.5,0 L336.094937,0 C338.027933,-3.55085719e-16 339.594937,1.56700338 339.594937,3.5 L339.594937,3.5 C339.594937,5.43299662 338.027933,7 336.094937,7 L235.5,7 C233.567003,7 232,5.43299662 232,3.5 L232,3.5 C232,1.56700338 233.567003,3.55085719e-16 235.5,0 Z'
              id='Rectangle'
            />
            <path
              d='M128.5,20 L336.689873,20 C338.62287,20 340.189873,21.5670034 340.189873,23.5 L340.189873,23.5 C340.189873,25.4329966 338.62287,27 336.689873,27 L128.5,27 C126.567003,27 125,25.4329966 125,23.5 L125,23.5 C125,21.5670034 126.567003,20 128.5,20 Z'
              id='Rectangle'
            />
            <path
              d='M3.5,39 L336.5,39 C338.432997,39 340,40.5670034 340,42.5 L340,42.5 C340,44.4329966 338.432997,46 336.5,46 L3.5,46 C1.56700338,46 2.36723813e-16,44.4329966 0,42.5 L0,42.5 C-2.36723813e-16,40.5670034 1.56700338,39 3.5,39 Z'
              id='Rectangle'
            />
            <path
              d='M3.5,20 L104.094937,20 C106.027933,20 107.594937,21.5670034 107.594937,23.5 L107.594937,23.5 C107.594937,25.4329966 106.027933,27 104.094937,27 L3.5,27 C1.56700338,27 6.80813023e-16,25.4329966 4.4408921e-16,23.5 L0,23.5 C-2.36723813e-16,21.5670034 1.56700338,20 3.5,20 Z'
              id='Rectangle'
            />
          </g>
          <g id='Rectangle-2' transform='translate(0.000000, 77.000000)'>
            <path
              d='M3.5,1 L211.689873,1 C213.62287,1 215.189873,2.56700338 215.189873,4.5 L215.189873,4.5 C215.189873,6.43299662 213.62287,8 211.689873,8 L3.5,8 C1.56700338,8 2.36723813e-16,6.43299662 0,4.5 L0,4.5 C-2.36723813e-16,2.56700338 1.56700338,1 3.5,1 Z'
              id='Rectangle'
            />
            <path
              d='M235.905063,0 L336.5,0 C338.432997,-3.55085719e-16 340,1.56700338 340,3.5 L340,3.5 C340,5.43299662 338.432997,7 336.5,7 L235.905063,7 C233.972067,7 232.405063,5.43299662 232.405063,3.5 L232.405063,3.5 C232.405063,1.56700338 233.972067,3.55085719e-16 235.905063,0 Z'
              id='Rectangle'
            />
            <path
              d='M128.310127,20 L336.5,20 C338.432997,20 340,21.5670034 340,23.5 L340,23.5 C340,25.4329966 338.432997,27 336.5,27 L128.310127,27 C126.37713,27 124.810127,25.4329966 124.810127,23.5 L124.810127,23.5 C124.810127,21.5670034 126.37713,20 128.310127,20 Z'
              id='Rectangle'
            />
            <path
              d='M3.5,40 L336.5,40 C338.432997,40 340,41.5670034 340,43.5 L340,43.5 C340,45.4329966 338.432997,47 336.5,47 L3.5,47 C1.56700338,47 2.36723813e-16,45.4329966 0,43.5 L0,43.5 C-2.36723813e-16,41.5670034 1.56700338,40 3.5,40 Z'
              id='Rectangle'
            />
            <path
              d='M3.5,20 L104.094937,20 C106.027933,20 107.594937,21.5670034 107.594937,23.5 L107.594937,23.5 C107.594937,25.4329966 106.027933,27 104.094937,27 L3.5,27 C1.56700338,27 6.80813023e-16,25.4329966 4.4408921e-16,23.5 L0,23.5 C-2.36723813e-16,21.5670034 1.56700338,20 3.5,20 Z'
              id='Rectangle'
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default ({ withTitle = true }) => (
  <div className='video-block-stub__wrapper'>
    <div className='video-block-stub__image-wrapper'>
      <img
        src='../main/media/Video-preload-image.png'
        className='video-block-stub__image'
        alt=''
      />
    </div>

    {withTitle ? (
      <div className='video-block-stub__title'>{titleStub}</div>
    ) : null}

    <div
      className={`video-block-stub__description
        ${!withTitle ? " video-block-stub__description_single" : ""}`}
    >
      {descriptionStub}
    </div>

    <div className={`video-block-stub__action`}>
      <button
        className='btn btn-rounded btn-block btn-light video-block-stub-animation'
        disabled
      >
        <i className='far fa-lg fa-check' />
      </button>
    </div>
  </div>
);
