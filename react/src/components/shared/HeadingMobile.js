import React from "react";

const HeadingMobile = props => {
  return window.innerWidth <= 768 ? (
    <header className={"app-header " + props.class}>
      <div className='container'>
        <div className='row'>{props.columns}</div>
      </div>
    </header>
  ) : null;
};

export default HeadingMobile;
