import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import HomeStepOneModules from "../HomeStepOneModules/index";
import VideoBlockStub from "../../shared/VideoBlockStub";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subfilters: undefined,
      index: 0,
    };
    this._changeSubfilter = this._changeSubfilter.bind(this);
  }

  _displayUncompletedSubFilter() {
    if (this.props.stepOne !== undefined) {
      if (
        this.props.stepOne.training_completed >=
        this.props.stepOne.training_total
      ) {
        this.setState({
          index: 1,
        });
      } else {
        this.setState({
          index: 0,
        });
      }
    }
  }

  componentDidMount() {
    axiosAjax.get("/weeks/introduction-sub-filters").then(response => {
      this.setState(
        {
          subfilters: response.data,
        },
        () => {
          //this._displayUncompletedSubFilter();
        },
      );
    });
  }

  _changeSubfilter() {
    this._displayUncompletedSubFilter();
  }

  render() {
    if (!this.state.subfilters || this.state.subfilters.length <= 0)
      return <VideoBlockStub />;

    return (
      <React.Fragment>
        {this.state.subfilters !== undefined ? (
          this.state.subfilters.length > 0 ? (
            <HomeStepOneModules
              stepOne={this.props.stepOne}
              id={this.state.subfilters[this.state.index].id}
              subfilters={this.state.subfilters}
              subfilterIndex={this.state.index}
              changeSubfilter={this._changeSubfilter}
              completed={this.props.stepOne.completed}
            />
          ) : (
            <VideoBlockStub />
          )
        ) : (
          <VideoBlockStub />
        )}
      </React.Fragment>
    );
  }
}

export default Index;
