import React from "react";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import VideoThumbnail from "./VideoThumbnail";

export default class Modules extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modules: undefined,
      mainFilter: this.props.mainFilter,
    };
  }

  _getSubFilterModules(id) {
    axiosAjax.get("/modules/category/" + id + "/").then(response => {
      this.setState({
        modules: response.data,
      });
    });
  }

  componentDidMount() {
    this._getSubFilterModules(this.props.id);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.id !== this.props.id) {
      this._getSubFilterModules(this.props.id);
    }
    if (prevProps.mainFilter !== this.props.mainFilter) {
      this.setState({
        mainFilter: this.props.mainFilter,
      });
    }
  }

  render() {
    return this.state.modules !== undefined ? (
      this.state.modules.length > 0 && (
        <div className='container'>
          <div className='row'>
            {this.state.modules.map((module, key) => (
              <VideoThumbnail
                key={key}
                id={module.module.id}
                video={module.module.video}
                name={module.module.name}
                completed={module.completed}
                availableOnTrial={module.module.available_on_trial}
                vimeoUrl={module.module.vimeo_url}
              />
            ))}
          </div>
        </div>
      )
    ) : (
      <div className='h-100 d-flex flex-column justify-content-center'>
        <div className='text-center'>
          <MDSpinner singleColor='#4a4090' />
        </div>
      </div>
    );
  }
}
