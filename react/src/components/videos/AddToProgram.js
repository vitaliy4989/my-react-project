import React, { Component } from "react";
import axiosAjax from "../../config/axiosAjax";
import { connect } from "react-redux";
import { getPrograms } from "../../redux/actions/training";

class AddToProgram extends Component {
  constructor(props) {
    super(props);
    this.state = {
      added: false,
      sending: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.videoId !== nextProps.videoId) {
      this.setState({
        added: false,
      });
    }
  }

  addToProgram() {
    const formData = new FormData();
    this.setState({ sending: true, added: true });
    formData.append("training_log_day", this.props.programId);
    formData.append("video", this.props.videoId);
    axiosAjax({
      method: "post",
      url: "/training/exercise/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          added: true,
          sending: false,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({ sending: false, added: false });
      });
  }

  render() {
    return this.state.added === true ? (
      <button className='btn btn-round btn-active animated pulse'>
        <i className='fas fa-check white' />
      </button>
    ) : (
      <button
        onClick={() => this.addToProgram()}
        className='btn btn-light btn-round'
      >
        {this.state.sending === true ? (
          <i className='fas fa-circle-notch fa-spin' />
        ) : (
          <i className='fas  fa-plus' />
        )}
      </button>
    );
  }
}

export default AddToProgram;
