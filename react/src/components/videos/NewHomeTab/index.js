import React from "react";
import VideoThumbnail from "../VideoThumbnail";
import MDSpinner from "react-md-spinner";
import axiosAjax from "../../../config/axiosAjax";

class New extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      videos: undefined,
    };
  }

  componentDidMount() {
    axiosAjax.get("/modules/recent").then(response => {
      this.setState({
        videos: response.data,
      });
    });
  }

  render() {
    return (
      <React.Fragment>
        <h6 className='section-heading mt-4'>New Videos</h6>
        {this.state.videos !== undefined ? (
          this.state.videos.length > 0 && (
            <div className='new-video-scroll'>
              {this.state.videos.map((module, key) => (
                <VideoThumbnail
                  key={key}
                  home={true}
                  id={module.module.id}
                  video={module.module.video}
                  name={module.module.name}
                  completed={module.completed}
                  vimeoUrl={module.module.vimeo_url}
                />
              ))}
            </div>
          )
        ) : (
          <div className='text-center'>
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default New;
