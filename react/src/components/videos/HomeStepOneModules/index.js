import React from "react";
import axiosAjax from "../../../config/axiosAjax";
import HomeStepOneVideo from "../HomeStepOneVideo/index";
import VideoBlockStub from "../../shared/VideoBlockStub";

export default class Modules extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modules: undefined,
      index: 0,
      subfiltersLength: 0,
    };
    this._nextModule = this._nextModule.bind(this);
  }

  _getSubFilterModules(id) {
    axiosAjax.get("/modules/category/" + id + "/").then(response => {
      this.setState({
        modules: response.data,
        subfiltersLength: response.data.length,
      });
    });
  }

  componentDidMount() {
    this._getSubFilterModules(this.props.id);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.id !== this.props.id) {
      this.setState(
        {
          index: 0,
          modules: undefined,
          subfiltersLength: 0,
        },
        () => {
          this._getSubFilterModules(this.props.id);
        },
      );
    }
  }

  _nextModule() {
    this.setState({
      index: this.state.index + 1,
    });
  }

  render() {
    return this.state.modules !== undefined ? (
      this.state.modules.length > 0 && (
        <>
          {/*<VideoBlockStub />*/}
          <HomeStepOneVideo
            module={this.state.modules[this.state.index]}
            nextModule={this._nextModule}
            subfiltersLength={this.state.subfiltersLength}
            currentIndex={this.state.index}
            stepOne={this.props.stepOne}
            subfilters={this.props.subfilters}
            subfilterIndex={this.props.subfilterIndex}
            changeSubfilter={this.props.changeSubfilter}
            stepOneCompleted={this.props.completed}
          />
        </>
      )
    ) : (
      <VideoBlockStub />
    );
  }
}
