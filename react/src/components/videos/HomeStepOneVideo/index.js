import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { connect } from "react-redux";
import { getStepOne } from "../../../redux/actions/home";
import VideoBlockStub from "../../shared/VideoBlockStub";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      video: undefined,
      id: undefined,
      enrolled: undefined,
    };
  }

  _loadVideo(id) {
    axiosAjax.get("/modules/module/" + id).then(response => {
      this.setState({
        id: response.data.module.id,
        video: response.data.module,
        enrolled: response.data.module_enrolled,
      });
    });
  }

  componentDidMount() {
    this._loadVideo(this.props.module.module.id);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.module) {
      if (prevProps.module.module.id !== this.props.module.module.id) {
        this.setState(
          {
            enrolled: false,
            video: undefined,
          },
          () => {
            this._loadVideo(this.props.module.module.id);
          },
        );
      }
    }
  }

  _markCompleted() {
    this.setState({ enrolled: true });
    const formData = new FormData();
    formData.append("module", this.props.module.module.id);
    axiosAjax({
      method: "post",
      url: "/modules/enrolled/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          enrolled: true,
        });
        this.props.onGetStepOne();
      })
      .catch(error => {
        this.setState({ enrolled: false });
      });
  }

  render() {
    const { video, enrolled } = this.state;
    const {
      currentIndex,
      subfilters,
      subfiltersLength,
      subfilterIndex,
      changeSubfilter,
      nextModule,
    } = this.props;

    const renderEnrolledButton = enrolled => {
      if (enrolled === undefined) {
        return (
          <div className='col-12'>
            <button className='btn btn-rounded btn-block btn-light' disabled>
              <i className='far fa-lg fa-check' />
            </button>
          </div>
        );
      } else if (enrolled === false) {
        return (
          <div className='col-12'>
            <button
              onClick={() => this._markCompleted()}
              className='btn btn-rounded  btn-block btn-light'
            >
              <i className='far fa-lg fa-check' />
            </button>
          </div>
        );
      }

      return (
        <React.Fragment>
          <div className='col-6'>
            <button className='btn btn-rounded btn-success btn-block'>
              <i className='far fa-lg fa-check white' />
            </button>
          </div>

          {currentIndex < subfiltersLength - 1 ? (
            <div className='col-6'>
              <button
                onClick={() => nextModule()}
                className='btn btn-rounded btn-block btn-light'
              >
                Next &nbsp; <i className='far fa-chevron-right' />
              </button>
            </div>
          ) : (
            <div className='col-6'>
              <button
                onClick={() => changeSubfilter()}
                className='btn btn-rounded btn-block btn-light'
              >
                Next &nbsp; <i className='far fa-chevron-right' />
              </button>
            </div>
          )}
        </React.Fragment>
      );
    };

    if (video === undefined || !video.vimeo_url) return <VideoBlockStub />;

    return (
      <React.Fragment>
        <div className='embed-responsive embed-responsive-16by9'>
          <div className='embed-responsive-item home-flex-step-video d-flex justify-content-center align-items-center background-loader'>
            {video !== undefined ? (
              <iframe src={video.vimeo_url} className='home-flex-step-video' />
            ) : null}
          </div>
        </div>
        <div className='home-step-container shadow-sm bg-white rounded-bottom'>
          <p className='m-0 p-smaller'>
            <strong>
              {video ? video.name : <span className='invisible'>Loading…</span>}
            </strong>
          </p>
          {video !== undefined ? (
            <div className='row'>
              <div className='col'>
                <p className='text-muted p-xs m-0'>
                  Introducing {subfilters[subfilterIndex].name}
                </p>
              </div>
              <div className='col-auto'>
                <p className='text-muted p-xs m-0'>
                  Video{" "}
                  <strong>
                    {currentIndex + 1} / {subfiltersLength}
                  </strong>
                </p>
              </div>
            </div>
          ) : (
            <span>Loading…</span>
          )}
        </div>
        <div className='shadow-sm p-2 bg-white fixed-bottom'>
          <div className='container'>
            <div className='row'>{renderEnrolledButton(enrolled)}</div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stepOne: state.home.stepOne,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetStepOne: () => {
      dispatch(getStepOne());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Index);
