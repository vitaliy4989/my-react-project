import React, { Component } from "react";
import axiosAjax from "../../config/axiosAjax";

class MarkFavourite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favourite: this.props.favourite ? this.props.favourite : false,
      sending: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.videoId !== nextProps.videoId) {
      this.setState({
        favourite: nextProps.favourite ? nextProps.favourite : false,
      });
    }
  }

  markFavourite() {
    const formData = new FormData();
    this.setState({ favourite: true });
    formData.append("module", this.props.videoId);
    axiosAjax({
      method: "post",
      url: "/modules/favourites/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          sending: false,
          favourite: true,
        });
      })
      .catch(error => {
        this.setState({ sending: false, favourite: false });
      });
  }

  unmarkFavourite() {
    this.setState({ favourite: false });
    axiosAjax
      .delete("/modules/favourites/" + this.props.videoId + "/")
      .then(response => {
        this.setState({
          sending: false,
          favourite: false,
        });
      })
      .catch(error => {
        this.setState({ sending: false, favourite: true });
      });
  }

  render() {
    return this.state.favourite === false ? (
      <button
        onClick={() => this.markFavourite()}
        className=' btn-rounded btn btn-light btn-block'
      >
        {this.state.sending === true ? (
          <i className='fas fa-circle-notch fa-spin' />
        ) : (
          <i className='far fa-lg fa-heart' />
        )}
      </button>
    ) : (
      <button
        onClick={() => this.unmarkFavourite()}
        className=' btn-rounded btn btn-light btn-block'
      >
        <i className='fas fa-lg fa-heart' />
      </button>
    );
  }
}

export default MarkFavourite;
