import React from "react";
import VideoThumbnail from "./VideoThumbnail";
import MDSpinner from "react-md-spinner";
import { connect } from "react-redux";
import { getFavouriteVideos } from "../../redux/actions/videos";
import LightAlert from "../shared/LightAlert";

class Favourite extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onGetFavouriteVideos();
  }

  render() {
    return this.props.favouriteVideos !== null ? (
      this.props.favouriteVideos.length > 0 ? (
        <div className='container'>
          <div className='row'>
            {this.props.favouriteVideos.map((favourite, key) => (
              <VideoThumbnail
                key={key}
                id={favourite.module.id}
                video={favourite.module.video}
                name={favourite.module.name}
                vimeoUrl={favourite.module.vimeo_url}
              />
            ))}
          </div>
        </div>
      ) : (
        <div className='h-100 d-flex flex-column justify-content-center text-center'>
          <LightAlert
            message='Save favourite videos by clicking the heart icon. '
            container={true}
          />
        </div>
      )
    ) : (
      <div className='h-100 d-flex flex-column justify-content-center'>
        <div className='text-center'>
          <MDSpinner singleColor='#4a4090' />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    favouriteVideos: state.videos.favouriteVideos,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetFavouriteVideos: () => {
      dispatch(getFavouriteVideos());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Favourite);
