import React, { Component } from "react";
import axiosAjax from "../../config/axiosAjax";
import { connect } from "react-redux";
import { updateMainFilter } from "../../redux/actions/videos";

class MarkCompleted extends Component {
  constructor(props) {
    super(props);
    this.state = {
      completed: this.props.completed ? this.props.completed : false,
      sending: false,
    };
    this.markCompleted = this.markCompleted.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.videoId !== nextProps.videoId ||
      nextProps.completed !== this.props.completed
    ) {
      this.setState({
        completed: nextProps.completed ? nextProps.completed : false,
      });
    }
  }

  markCompleted() {
    this.setState({ sending: true, completed: true });
    const formData = new FormData();
    formData.append("module", this.props.videoId);
    axiosAjax({
      method: "post",
      url: "/modules/enrolled/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          sending: false,
          completed: true,
        });
        this.props.onUpdateMainFilter(true);
      })
      .catch(error => {
        this.setState({ sending: false, completed: false });
      });
  }

  render() {
    return this.state.completed === false ? (
      <button
        onClick={() => this.markCompleted()}
        className='btn btn-rounded btn-block btn-light'
      >
        {this.state.sending === true ? (
          <i className='fas fa-circle-notch fa-spin' />
        ) : (
          <i className='far fa-lg fa-check' />
        )}
      </button>
    ) : (
      <button
        style={{ background: "#00C2A2", boxShadow: "none", cursor: "default" }}
        className='btn btn-rounded btn-block'
      >
        <i className='far fa-lg fa-check white' />
      </button>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUpdateMainFilter: update => {
      dispatch(updateMainFilter(update));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(MarkCompleted);
