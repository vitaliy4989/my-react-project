import React from "react";

const MenuButton = props => {
  return $(window).width() <= 768 ? (
    <a
      id='videoMenuButton'
      data-activates='slide-out-videos'
      className='nav-link  button-collapse-videos'
    >
      <i className='far fa-lg white fa-bars' />
    </a>
  ) : (
    <div className='d-flex justify-content-between align-items-center'>
      <div className='sub-menu-title'>Video Menu</div>
      <button
        id='videoMenuButton'
        data-activates='slide-out-videos'
        className='btn btn-rounded btn-sub-menu float-right button-collapse-videos'
      >
        <i className='far dark-purp fa-bars' />
      </button>
    </div>
  );
};

export default MenuButton;
