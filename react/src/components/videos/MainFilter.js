import React, { Component } from "react";
import SubFilters from "./SubFilters";

export default class MainFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSubFilters: false,
      id: this.props.id,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.id !== prevState.id) {
      return {
        id: nextProps.id,
      };
    }
    return null;
  }

  render() {
    return (
      <li>
        <a
          data-toggle='collapse'
          href={"#filter" + this.props.id}
          onClick={() =>
            this.setState({ showSubFilters: !this.state.showSubFilters })
          }
          aria-expanded='false'
          aria-controls={"filter" + this.props.id}
          className='collapsible-header'
        >
          {this.props.name}
          <div className='float-right' style={{ marginRight: 15 }}>
            {this.state.showSubFilters === true ? (
              <i className='fas fa-angle-down' />
            ) : (
              <i className='fas fa-angle-left' />
            )}
          </div>
        </a>
        <div className='collapse' id={"filter" + this.props.id}>
          <SubFilters
            iterationKey={this.props.iterationKey}
            id={this.props.id}
          />
        </div>
      </li>
    );
  }
}
