import React from "react";
import axiosAjax from "../../config/axiosAjax";
import VideoThumbnail from "./VideoThumbnail";
import MDSpinner from "react-md-spinner";
import LightAlert from "../shared/LightAlert";
import connect from "react-redux/es/connect/connect";

class Uncompleted extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      videos: null,
    };
  }

  componentDidMount() {
    axiosAjax.get("/modules/uncompleted").then(response => {
      this.setState({
        videos: response.data,
      });
    });
  }

  render() {
    return this.props.membershipStatus === "active" ? (
      this.state.videos !== null ? (
        this.state.videos.length > 0 ? (
          <div className='container'>
            <div className='row'>
              {this.state.videos.map((video, key) => (
                <VideoThumbnail
                  key={key}
                  id={video.id}
                  video={video.video}
                  name={video.name}
                  availableOnTrial={video.available_on_trial}
                  vimeoUrl={video.vimeo_url}
                />
              ))}
            </div>
          </div>
        ) : (
          <div className='h-100 d-flex flex-column justify-content-center text-center'>
            <LightAlert
              message="You have completed all the videos, but don't worry, we will be adding more!"
              container={true}
            />
          </div>
        )
      ) : (
        <div className='h-100 d-flex flex-column justify-content-center'>
          <div className='text-center'>
            <MDSpinner singleColor='#4a4090' />
          </div>
        </div>
      )
    ) : (
      <div className='h-100 d-flex flex-column justify-content-center text-center'>
        <LightAlert
          message='You must have an active account to watch these videos.'
          container={true}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    membershipStatus: state.user.membershipStatus,
  };
};

export default connect(
  mapStateToProps,
  null,
)(Uncompleted);
