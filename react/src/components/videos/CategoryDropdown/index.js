import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { NavLink } from "react-router-dom";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subFilters: undefined,
      currentFilter: undefined,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _getSubFilters() {
    axiosAjax
      .get("/weeks/sub-filters/" + this.props.mainFilterId + "/")
      .then(response => {
        this.setState({
          subFilters: response.data,
        });
      })
      .catch(error => {
        this.setState({
          subFilters: undefined,
        });
      });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.mainFilterId !== this.props.mainFilterId) {
      this._getSubFilters();
    }
  }

  render() {
    return (
      <div>
        <div className='dropdown'>
          <button
            className='btn btn-link dropdown-toggle'
            type='button'
            id='categoryMenuButton'
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            {this.props.subFilterName}
          </button>
          <div
            className='dropdown-menu dropdown-menu-right'
            aria-labelledby='categoryMenuButton'
          >
            {this.state.subFilters !== undefined ? (
              this.state.subFilters.length > 0 ? (
                this.state.subFilters.map((subFilter, key) => (
                  <NavLink
                    key={key}
                    className='dropdown-item'
                    to={
                      "/coaching/modules/filter/" +
                      subFilter.subfilter.name
                        .split(" ")
                        .join("-")
                        .toLowerCase() +
                      "/" +
                      subFilter.subfilter.id
                    }
                  >
                    {subFilter.subfilter.name}
                  </NavLink>
                ))
              ) : (
                <option>Loading filters</option>
              )
            ) : (
              <option>Loading filters</option>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Index;
