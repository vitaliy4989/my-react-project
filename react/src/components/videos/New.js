import React from "react";
import VideoThumbnail from "./VideoThumbnail";
import MDSpinner from "react-md-spinner";
import axiosAjax from "../../config/axiosAjax";

class New extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      videos: undefined,
    };
  }

  componentDidMount() {
    axiosAjax.get("/modules/recent").then(response => {
      this.setState({
        videos: response.data,
      });
    });
  }

  render() {
    return this.state.videos !== undefined ? (
      this.state.videos.length > 0 && (
        <div className='container'>
          <div className='row'>
            {this.state.videos.map((module, key) => (
              <VideoThumbnail
                key={key}
                id={module.module.id}
                video={module.module.video}
                name={module.module.name}
                availableOnTrial={module.module.available_on_trial}
                completed={module.completed}
                vimeoUrl={module.module.vimeo_url}
              />
            ))}
          </div>
        </div>
      )
    ) : (
      <div className='h-100 d-flex flex-column justify-content-center'>
        <div className='text-center'>
          <MDSpinner singleColor='#4a4090' />
        </div>
      </div>
    );
  }
}

export default New;
