import React from "react";
import axiosAjax from "../../config/axiosAjax";
import { NavLink } from "react-router-dom";

export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      search: "",
      searchIcon: <i className='far dark-purp fa-search' />,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    axiosAjax
      .get("/modules/module/", {
        params: {
          search: this.state.search,
        },
      })
      .then(response => {
        this.setState({
          data: response.data,
        });
      })
      .catch(error => {
        this.setState({
          data: [],
        });
      });
    this.setState({
      searchIcon: (
        <i
          className='far fa-times-circle'
          style={crossIcon}
          onClick={() =>
            this.setState({
              search: "",
              searchIcon: <i className='far dark-purp fa-search' />,
            })
          }
        />
      ),
    });
  }

  render() {
    return window.innerWidth <= 768 ? (
      <div>
        <form
          onChange={this.handleSubmit}
          style={{ marginLeft: "auto", marginRight: "auto" }}
        >
          <div className='input-group list-group-flush video-search'>
            <input
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.search}
              name='search'
              className='form-control'
              placeholder='Search videos...'
              style={{
                borderRadius: 20,
                borderTopRightRadius: 0,
                borderBottomRightRadius: 0,
                borderRight: 0,
                borderColor: "#CED4DA",
              }}
              aria-describedby='video-search-icon'
            />
            <div className='input-group-append'>
              <span
                className='input-group-text video-search-icon'
                id='video-search-icon'
              >
                {this.state.searchIcon}
              </span>
            </div>
          </div>
        </form>
        {this.state.search !== "" ? (
          <div
            className='list-group list-wrapper'
            style={{
              position: "absolute",
              zIndex: 1000,
              width: "92%",
              marginTop: "1rem",
              padding: 10,
            }}
          >
            {this.state.data.length > 0 ? (
              <div
                style={{
                  maxHeight: 200,
                  overflowX: "hidden",
                  overflowY: "scroll",
                }}
              >
                {this.state.data.map((data, key) => (
                  <NavLink
                    key={key}
                    className='p-smaller list-group-item d-flex justify-content-between align-items-center'
                    style={{
                      paddingTop: 7,
                      paddingBottom: 7,
                      boxShaow: "none",
                      borderBottomWidth: 1,
                    }}
                    onClick={() => this.setState({ data: "", search: "" })}
                    to={"/coaching/modules/module/" + data.id}
                  >
                    <div>{data.name}</div>
                  </NavLink>
                ))}
              </div>
            ) : (
              <div className='list-group-item'>
                <p className='m-0  black'> No results...</p>
              </div>
            )}
          </div>
        ) : null}
      </div>
    ) : (
      <div>
        <form onChange={this.handleSubmit}>
          <div className='input-group video-search'>
            <input
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.search}
              name='search'
              className='form-control'
              placeholder='Search videos...'
              style={{
                borderRadius: 20,
                borderTopRightRadius: 0,
                borderBottomRightRadius: 0,
                borderRight: 0,
                borderColor: "#CED4DA",
              }}
              aria-describedby='video-search-icon'
            />
            <div className='input-group-append'>
              <span
                className='input-group-text video-search-icon'
                id='video-search-icon'
              >
                {this.state.searchIcon}
              </span>
            </div>
          </div>
        </form>
        {this.state.search !== "" ? (
          <div
            className='list-group-flush list-group list-wrapper'
            style={{
              marginBottom: 15,
              position: "absolute",
              width: "92%",
              zIndex: 1000,
              marginTop: "1rem",
              padding: 10,
            }}
          >
            {this.state.data.length > 0 ? (
              <div
                style={{
                  maxHeight: 300,
                  overflowX: "hidden",
                  overflowY: "scroll",
                }}
              >
                {this.state.data.map((data, key) => (
                  <NavLink
                    style={{ boxShaow: "none", borderBottomWidth: 1 }}
                    key={key}
                    className='list-group-item d-flex justify-content-between align-items-center'
                    onClick={() => this.setState({ data: "", search: "" })}
                    to={"/coaching/modules/module/" + data.id}
                  >
                    <div className='p-smaller'>{data.name}</div>
                  </NavLink>
                ))}
              </div>
            ) : (
              <div className='list-group-item'>
                <p className='m-0 black'> No results...</p>
              </div>
            )}
          </div>
        ) : null}
      </div>
    );
  }
}

const crossIcon = {
  cursor: "pointer",
};

const searchIcon = {
  border: "none",
};
