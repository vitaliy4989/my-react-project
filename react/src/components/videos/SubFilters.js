import React, { Component } from "react";
import axiosAjax from "../../config/axiosAjax";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { updateMainFilter } from "../../redux/actions/videos";

class SubFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subFilters: [],
    };
  }

  _getSubFilters(id) {
    axiosAjax.get("/weeks/sub-filters/" + id + "/").then(response => {
      this.setState({
        subFilters: response.data,
      });
    });
    this.props.onUpdateMainFilter(false);
  }

  componentDidMount() {
    this._getSubFilters(this.props.id);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.currentVideoMainFilterId === this.props.id &&
      this.props.updateMainFilter === true
    ) {
      this._getSubFilters(this.props.id);
    }
  }

  render() {
    return (
      <ul>
        {this.state.subFilters.length > 0 &&
          this.state.subFilters.map((subFilter, key) => (
            <li key={"subFilter" + key}>
              <NavLink
                style={sidePanelLink}
                activeStyle={linkActive}
                to={
                  "/coaching/modules/filter/" +
                  subFilter.subfilter.name
                    .split(" ")
                    .join("-")
                    .toLowerCase() +
                  "/" +
                  subFilter.subfilter.id
                }
              >
                {subFilter.subfilter.name}
              </NavLink>
              {subFilter.completed ? null : (
                <div style={lockedMessage}>
                  <i className='fal fa-lock dark-purp' aria-hidden='true' />{" "}
                  &nbsp; Complete <strong>{subFilter.subfilter.name}</strong> to
                  move on.
                </div>
              )}
            </li>
          ))}
      </ul>
    );
  }
}

const lockedMessage = {
  fontSize: 10,
  color: "grey",
  paddingLeft: 25,
};

const linkActive = {
  color: "#4a4090",
  backgroundColor: "#e9ecef",
  border: "none",
  borderRadius: 0,
};

const sidePanelLink = {
  border: "none",
  paddingLeft: 25,
};

const mapStateToProps = (state, ownProps) => {
  return {
    currentVideoMainFilterId: state.videos.currentVideoMainFilterId,
    updateMainFilter: state.videos.updateMainFilter,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onUpdateMainFilter: update => {
      dispatch(updateMainFilter(update));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { pure: false },
)(SubFilters);
