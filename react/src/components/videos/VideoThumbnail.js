import React from "react";
import { NavLink } from "react-router-dom";
import "./NewHomeTab/styles.css";
import axiosAjax from "../../config/axiosAjax";
import connect from "react-redux/es/connect/connect";

class VideoThumbnail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      videoThumb: "https://d3qqgqjfawam42.cloudfront.net/JSA-icon-new.png",
    };
  }

  _getVideoThumb() {
    if (this.props.vimeoUrl) {
      var videoId = this.props.vimeoUrl.replace(/\D/g, "");
      axiosAjax({
        method: "get",
        url: "https://vimeo.com/api/v2/video/" + videoId + ".json",
      })
        .then(response => {
          this.setState({
            videoThumb: response.data[0].thumbnail_large,
          });
        })
        .catch(error => {
          this.setState({
            videoThumb: "",
          });
        });
    }
  }

  componentDidMount() {
    this._getVideoThumb();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.id !== this.props.id) {
      this._getVideoThumb();
    }
  }

  render() {
    return this.props.home === true ? (
      <div id={"video" + this.props.id} className='item shadow-sm'>
        <NavLink
          to={"/coaching/modules/module/" + this.props.id}
          className='card'
        >
          <img
            className='card-img-top text-center'
            src={this.state.videoThumb}
            alt='Loading thumbnail image...'
          />
          <div className='card-body text-center'>
            <p>{this.props.name}</p>
            {this.props.completed ? (
              <p className='green m-0'>Completed</p>
            ) : (
              <p className='text-muted m-0'>Uncompleted</p>
            )}
          </div>
        </NavLink>
      </div>
    ) : (
      <div
        id={"video" + this.props.id}
        className='col-lg-4'
        style={{ marginTop: 30 }}
      >
        {this.props.membershipStatus === "trialing" ? (
          this.props.availableOnTrial === true ? (
            <NavLink
              className='card shadow-sm'
              to={"/coaching/modules/module/" + this.props.id}
            >
              <img
                className='card-img-top text-center'
                src={this.state.videoThumb}
                alt='Loading thumbnail image...'
              />
              <div className='card-body text-center'>
                <p>{this.props.name}</p>
                {this.props.completed ? (
                  <p className='green m-0'>Completed</p>
                ) : (
                  <p className='text-muted m-0'>Uncompleted</p>
                )}
              </div>
            </NavLink>
          ) : (
            <div className='card shadow-sm'>
              <img
                className='card-img-top text-center'
                src={this.state.videoThumb}
                alt='Loading thumbnail image...'
              />
              <div className='card-body text-center'>
                <p>{this.props.name}</p>
                <p className='p-smaller m-0'>
                  To watch, please{" "}
                  <a href='/users/membership-options/'>
                    <u>upgrade from trial</u>
                  </a>
                  .
                </p>
              </div>
            </div>
          )
        ) : (
          <NavLink
            className='card shadow-sm'
            to={"/coaching/modules/module/" + this.props.id}
          >
            <img
              className='card-img-top text-center'
              src={this.state.videoThumb}
              alt='Loading thumbnail image...'
            />
            <div className='card-body text-center'>
              <p>{this.props.name}</p>
              {this.props.completed ? (
                <p className='green m-0'>Completed</p>
              ) : (
                <p className='text-muted m-0'>Uncompleted</p>
              )}
            </div>
          </NavLink>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    membershipStatus: state.user.membershipStatus,
    membershipType: state.user.membershipType,
  };
};

export default connect(
  mapStateToProps,
  null,
)(VideoThumbnail);
