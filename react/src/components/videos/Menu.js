import React from "react";
import { NavLink } from "react-router-dom";
import axiosAjax from "../../config/axiosAjax";
import MainFilter from "./MainFilter";
import MDSpinner from "react-md-spinner";

export default class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mainFilters: undefined,
    };
  }

  _getMainFilters() {
    axiosAjax.get("/weeks/main-filters").then(response => {
      this.setState({
        mainFilters: response.data,
      });
    });
  }

  componentDidMount() {
    this._getMainFilters();
    $(".button-collapse-videos").sideNav({
      edge: "right",
      closeOnClick: true,
    });
    var sideNavScrollbar = document.querySelector(".custom-scrollbar-videos");
    Ps.initialize(sideNavScrollbar);
  }

  render() {
    return (
      <React.Fragment>
        <div id='slide-out-videos' className='side-nav'>
          <ul className='custom-scrollbar-videos'>
            <li>
              {this.state.mainFilters !== undefined ? (
                <ul className='collapsible collapsible-accordion'>
                  {this.state.mainFilters.length > 0 &&
                    this.state.mainFilters.map((filter, key) => (
                      <MainFilter
                        key={key}
                        id={filter.id}
                        iterationKey={key}
                        name={filter.name}
                      />
                    ))}
                  <li>
                    <NavLink
                      to='/coaching/modules/uncompleted'
                      activeStyle={linkActive}
                    >
                      Uncompleted
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to='/coaching/modules/favourite'
                      activeStyle={linkActive}
                    >
                      Favourite
                    </NavLink>
                  </li>
                </ul>
              ) : (
                <div
                  style={{ paddingTop: "10%", paddingBottom: "10%" }}
                  className='text-center'
                >
                  <MDSpinner
                    color1='#4a4090'
                    color2='#4a4090'
                    color3='#4a4090'
                    color4='#4a4090'
                  />
                </div>
              )}
            </li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

const linkActive = {
  color: "#4a4090",
  backgroundColor: "#e9ecef",
  border: "none",
  borderRadius: 0,
};
