import React, { Component } from "react";
import MDSpinner from "react-md-spinner";
import axiosAjax from "../../config/axiosAjax";
import { NavLink } from "react-router-dom";

export default class OtherRelatedVideos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videos: undefined,
    };
  }

  componentDidMount() {
    axiosAjax
      .get("/modules/other-related-videos", {
        params: {
          subfilter_id: this.props.subFilter,
          module_id: this.props.videoId,
        },
      })
      .then(response => {
        this.setState({
          videos: response.data,
        });
      })
      .catch(error => {
        this.setState({
          videos: [],
        });
      });
  }

  render() {
    return (
      <div className='list-group list-group-flush' style={{ marginTop: 20 }}>
        <div className='list-group-item'>
          <p className='m-0 text-muted p-smaller'>All videos in:</p>
          <h6 className='m-0'>{this.props.subFilterName}</h6>
        </div>
        {this.state.videos !== undefined ? (
          this.state.videos.length > 0 &&
          this.state.videos.map((video, key) => (
            <NavLink
              key={key}
              to={"/coaching/modules/module/" + video.module.id}
              activeClassName='font-weight-bold'
              className='list-group-item d-flex justify-content-between align-items-center'
            >
              <div>
                <p className='m-0 p-smaller'>{video.module.name}</p>
                {video.completed === true ? (
                  <div style={{ fontSize: 10 }} className='green'>
                    Completed
                  </div>
                ) : (
                  <div style={{ fontSize: 10 }} className='text-muted'>
                    Uncompleted
                  </div>
                )}
              </div>
              {video.module.id !== this.props.videoId ? (
                <i className='far fa-chevron-right' />
              ) : null}
            </NavLink>
          ))
        ) : (
          <div
            className='text-center list-group-item'
            style={{ paddingTop: "10%", paddingBottom: "10%" }}
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </div>
    );
  }
}
