import React from "react";
import axiosAjax from "../../config/axiosAjax";
import { NotificationManager } from "react-notifications";

export default class UpdateEntry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      entry: this.props.entry.id,
      challenge: this.props.entry.challenge,
      story: this.props.entry.story,
      gender: this.props.entry.gender,
      goal: this.props.entry.goal,
      target: this.props.entry.target,
      target_units: this.props.entry.target_units,
      age: this.props.entry.age,
      country: this.props.entry.country,
      updated: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.updateEntry = this.updateEntry.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  updateEntry(event) {
    event.preventDefault();
    document.getElementById("formButton").disabled = true;
    const formData = new FormData();
    formData.append("story", this.state.story);
    formData.append("gender", this.state.gender);
    formData.append("goal", this.state.goal);
    formData.append("target", this.state.target);
    formData.append("target_units", this.state.target_units);
    formData.append("age", this.state.age);
    formData.append("country", this.state.country);
    formData.append("agreed_to_terms", true);
    axiosAjax({
      method: "patch",
      url: "/challenge/entry/" + this.state.entry + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Entry form updated.", "", 5000);
        this.setState({
          updated: true,
        });
        document.getElementById("formButton").disabled = false;
      })
      .catch(error => {
        document.getElementById("formButton").disabled = false;
        this.setState({
          updated: false,
        });
      });
  }

  render() {
    return (
      <div className='animated fadeIn'>
        <div className='content-container max-width-700'>
          <h3>You are enrolled</h3>
          <div className='embed-responsive embed-responsive-4by3'>
            <iframe
              className='embed-responsive-item'
              src='https://player.vimeo.com/video/309639653?title=0&byline=0&portrait=0'
              width='640'
              height='480'
              frameBorder='0'
              webkitallowfullscreen
              mozallowfullscreen
              allowFullScreen
            />
          </div>
          <p>
            <i className='far fa-exclamation-circle' /> &nbsp; You may update
            your entry form until the competition starts and will be able to
            enter challenge measurements and images when the challenge begins.
            For more information, please refer to the email you will have
            received after enrolling.
          </p>
          <form onSubmit={this.updateEntry}>
            <div className='form-group'>
              <label>Your story</label>
              <textarea
                className='form-control form-control-single-border'
                onChange={this.handleInputChange}
                value={this.state.story}
                id='story'
                name='story'
                required
              />
            </div>

            <div className='form-group'>
              <label>Goal (Be Specific)</label>
              <input
                type='text'
                className='form-control form-control-single-border'
                onChange={this.handleInputChange}
                value={this.state.goal}
                id='goal'
                name='goal'
                placeholder='Example: Lose 2kg'
                maxLength='50'
                required
              />
            </div>

            <div className='form-group row'>
              <div className='col-md-6'>
                <label>Numerical Target</label>
                <input
                  type='number'
                  step='0.00000001'
                  className='form-control form-control-single-border'
                  onChange={this.handleInputChange}
                  value={this.state.target}
                  id='target'
                  name='target'
                  placeholder='Example: 2'
                  required
                />
              </div>
              <div className='col-md-6'>
                <label>Units</label>
                <input
                  type='text'
                  className='form-control form-control-single-border'
                  onChange={this.handleInputChange}
                  value={this.state.target_units}
                  id='target_units'
                  name='target_units'
                  placeholder='Example: kg'
                  required
                />
              </div>
            </div>
            <div className='form-group row'>
              <div className='col-md-6'>
                <label>Gender</label>
                <select
                  className='custom-select form-control form-control-single-border'
                  onChange={this.handleInputChange}
                  value={this.state.gender}
                  name='gender'
                  id='gender'
                  required
                >
                  <option value='Female'>Female</option>
                  <option value='Male'>Male</option>
                </select>
              </div>
              <div className='col-md-6'>
                <label>Age</label>
                <input
                  className='form-control form-control-single-border'
                  onChange={this.handleInputChange}
                  value={this.state.age}
                  name='age'
                  id='age'
                  placeholder='35'
                  required
                />
              </div>
            </div>
            <div className='form-group'>
              <label>Country</label>
              <input
                type='text'
                className='form-control form-control-single-border'
                onChange={this.handleInputChange}
                value={this.state.country}
                id='country'
                name='country'
                placeholder='United Kingdom'
                required
              />
            </div>
            <div className='text-center'>
              <input
                type='submit'
                className='btn btn-primary enter-button-centered btn-rounded'
                id='formButton'
                name='submit'
                value='Update Entry'
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const included = {
  fontSize: 14,
  margin: 10,
};

const challengeImage = {
  width: "100%",
  marginTop: 25,
};

const line = {
  width: "90%",
  color: "rgba(74, 64, 144, 1)",
};

const terms = {
  border: "solid",
  borderWidth: 1,
  borderColor: "#f6f6f6",
  borderRadius: 5,
  maxHeight: 200,
  overflow: "scroll",
  fontSize: 13,
};
