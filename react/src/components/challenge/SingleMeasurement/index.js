import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { NotificationManager } from "react-notifications";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      date: this.props.date,
      measurement: this.props.measurement,
      showUpdateButton: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._updateMeasurement = this._updateMeasurement.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.id !== prevProps.id) {
      this.setState({
        id: this.props.id,
        date: this.props.date,
        measurement: this.props.measurement,
        showUpdateButton: false,
      });
    }
    if (this.state.measurement !== prevState.measurement) {
      this.setState({
        showUpdateButton: true,
      });
    }
    if (this.state.id !== prevState.id) {
      this.setState({
        showUpdateButton: false,
      });
    }
  }

  _updateMeasurement() {
    this.setState({
      showUpdateButton: false,
    });
    const formData = new FormData();
    formData.append("chart_entry", this.state.measurement);
    axiosAjax({
      method: "patch",
      url: "/challenge/chart-entry/" + this.state.id + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Measurement updated.", "", 5000);
        this.setState({
          showUpdateButton: false,
        });
        this.props.reloadChart(event);
      })
      .catch(error => {
        this.setState({
          showUpdateButton: true,
        });
        NotificationManager.error(
          "There was an error updating your measurement.",
          "",
          5000,
        );
      });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  render() {
    return (
      <div
        className='row shadow-sm mb-2 p-2 bg-white'
        style={{ borderRadius: 10, margin: 15 }}
      >
        <div className='col d-flex flex-column justify-content-center'>
          <input
            step='0.00000001'
            type='number'
            onChange={this.handleInputChange}
            name='measurement'
            className='form-control text-center'
            value={this.state.measurement}
          />
        </div>
        {this.state.showUpdateButton === true && (
          <div className='col d-flex flex-column justify-content-center'>
            <button
              onClick={() => this._updateMeasurement()}
              className='btn btn-outline-success'
            >
              <i className='far fa-check' />
            </button>
          </div>
        )}
        <div className='col d-flex flex-column justify-content-center'>
          <button
            onClick={() => this.props.deleteMeasurement(this.state.id)}
            className='btn btn-light'
          >
            <i className='far fa-times' />
          </button>
        </div>
        <div className='col d-flex flex-column justify-content-center p-xs text-center'>
          {this.state.date}
        </div>
      </div>
    );
  }
}

export default Index;
