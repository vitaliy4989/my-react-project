import React from "react";
import axiosAjax from "../../config/axiosAjax";

export default class CreateEntry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      entry: "",
      challenge: this.props.challenge,
      first_name: "",
      last_name: "",
      email: sessionStorage.email,
      story: "",
      gender: "",
      goal: "",
      target: "",
      target_units: "",
      age: "",
      country: "",
      created: false,
      sending: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createEntry = this.createEntry.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  createEntry(event) {
    event.preventDefault();
    this.setState({ sending: true });
    $("#formButton").hide();
    const formData = new FormData();
    formData.append("challenge", this.state.challenge);
    formData.append("first_name", this.state.first_name);
    formData.append("last_name", this.state.last_name);
    formData.append("email", this.state.email);
    formData.append("story", this.state.story);
    formData.append("gender", this.state.gender);
    formData.append("goal", this.state.goal);
    formData.append("target", this.state.target);
    formData.append("target_units", this.state.target_units);
    formData.append("age", this.state.age);
    formData.append("country", this.state.country);
    formData.append("agreed_to_terms", true);
    axiosAjax({
      method: "post",
      url: "/challenge/entry/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          created: true,
          sending: false,
        });
      })
      .catch(error => {
        $("#formButton").show();
        this.setState({ sending: false, created: false });
      });
  }

  render() {
    return (
      <div id='entry-form-top'>
        <div className='content-container max-width-700'>
          {this.state.created === true && (
            <div className='alert alert-success' role='alert'>
              You have successfully entered into the {this.state.challenge.name}{" "}
              challenge, you will receive an email with more information and
              instruction.
            </div>
          )}
          <div className='embed-responsive embed-responsive-4by3'>
            <iframe
              className='embed-responsive-item'
              src='https://player.vimeo.com/video/309639653?title=0&byline=0&portrait=0'
              width='640'
              height='480'
              frameBorder='0'
              webkitallowfullscreen
              mozallowfullscreen
              allowFullScreen
            />
          </div>
          <h6>Instructions</h6>
          <p>
            Your goal should be specific and measurable and will be used to
            create your chart. If your goal is not measurable, your chart will
            not make any sense. For example, a valid goal could be;{" "}
            <i>'reduce my waist size to a 32'</i>. The units for this goal is{" "}
            <i>'waist size'</i> and the numerical target is <i>'32'</i>.
          </p>
          <p>
            Your first entry on your chart would be your current waist size and
            each week, you would enter your waist size. If by week 12, your
            final measurement was 32, you will have reached your goal.
          </p>
          <p>
            You will be able to enter chart data when the challenge begins,
            which is after enrolment closes.
          </p>
          <hr />
          <h6>Entry Form</h6>
          <form onSubmit={this.createEntry}>
            {!sessionStorage.email ? (
              <div className='form-group'>
                <label>Email</label>
                <input
                  type='email'
                  className={
                    this.state.email !== ""
                      ? "is-valid form-control form-control-single-border"
                      : "form-control form-control-single-border"
                  }
                  onChange={this.handleInputChange}
                  value={this.state.email}
                  id='email'
                  name='email'
                  placeholder='Email'
                  required
                />
              </div>
            ) : (
              <input
                type='hidden'
                className='form-control form-control-single-border'
                onChange={this.handleInputChange}
                value={this.state.email}
                id='email'
                name='email'
                placeholder='Email'
                required
              />
            )}
            <div className='form-group row'>
              <div className='col-md-6'>
                <label>First Name</label>
                <input
                  type='text'
                  className={
                    this.state.first_name !== ""
                      ? "is-valid form-control form-control-single-border"
                      : "form-control form-control-single-border"
                  }
                  onChange={this.handleInputChange}
                  value={this.state.first_name}
                  id='first_name'
                  name='first_name'
                  placeholder='First name'
                  required
                />
              </div>
              <div className='col-md-6'>
                <label>Last Name</label>
                <input
                  type='text'
                  className={
                    this.state.last_name !== ""
                      ? "is-valid form-control form-control-single-border"
                      : "form-control form-control-single-border"
                  }
                  onChange={this.handleInputChange}
                  value={this.state.last_name}
                  id='last_name'
                  name='last_name'
                  placeholder='Last name'
                  required
                />
              </div>
            </div>
            <div className='form-group'>
              <label>Your Story</label>
              <textarea
                className={
                  this.state.story !== ""
                    ? "is-valid form-control form-control-single-border"
                    : "form-control form-control-single-border"
                }
                onChange={this.handleInputChange}
                value={this.state.story}
                id='story'
                name='story'
                placeholder='Just a little bit about why you are doing this!'
                required
              />
            </div>
            <div className='form-group'>
              <label>Goal (50 character limit)</label>
              <input
                type='text'
                className={
                  this.state.goal !== ""
                    ? "is-valid form-control form-control-single-border"
                    : "form-control form-control-single-border"
                }
                onChange={this.handleInputChange}
                value={this.state.goal}
                id='goal'
                name='goal'
                placeholder='Reduce my waist size to a 32'
                maxLength='50'
                required
              />
            </div>
            <div className='form-group row'>
              <div className='col-md-4'>
                <label>Numerical Target</label>
                <input
                  type='number'
                  step='0.00000001'
                  className={
                    this.state.target !== ""
                      ? "is-valid form-control form-control-single-border"
                      : "form-control form-control-single-border"
                  }
                  onChange={this.handleInputChange}
                  value={this.state.target}
                  id='target'
                  name='target'
                  placeholder='32'
                  required
                />
              </div>
              <div className='col-md-8'>
                <label>Units</label>
                <input
                  type='text'
                  className={
                    this.state.target_units !== ""
                      ? "is-valid form-control form-control-single-border"
                      : "form-control form-control-single-border"
                  }
                  onChange={this.handleInputChange}
                  value={this.state.target_units}
                  id='target_units'
                  name='target_units'
                  placeholder='Waist size'
                  required
                />
              </div>
            </div>
            <div className='form-group row'>
              <div className='col-md-6'>
                <label>Gender</label>
                <select
                  className={
                    this.state.gender !== ""
                      ? "is-valid form-control custom-select form-control-single-border"
                      : "form-control custom-select form-control-single-border"
                  }
                  onChange={this.handleInputChange}
                  value={this.state.gender}
                  name='gender'
                  id='gender'
                  required
                >
                  <option value='' disabled selected>
                    Select Gender
                  </option>
                  <option value='Female'>Female</option>
                  <option value='Male'>Male</option>
                </select>
              </div>
              <div className='col-md-6'>
                <label>Age</label>
                <input
                  type='number'
                  className={
                    this.state.age !== ""
                      ? "is-valid form-control form-control-single-border"
                      : "form-control form-control-single-border"
                  }
                  onChange={this.handleInputChange}
                  value={this.state.age}
                  name='age'
                  id='age'
                  placeholder='35'
                  required
                />
              </div>
            </div>
            <div className='form-group'>
              <label>Country</label>
              <input
                type='text'
                className={
                  this.state.country !== ""
                    ? "is-valid form-control form-control-single-border"
                    : "form-control form-control-single-border"
                }
                onChange={this.handleInputChange}
                value={this.state.country}
                id='country'
                name='country'
                placeholder='United Kingdom'
                required
              />
            </div>
            <div className='form-group'>
              <label>Please read, understand and agree to the terms</label>
              <div style={terms}>
                <ol>
                  <li>
                    I understand that every week I must upload a photo and the
                    numerical value for my goal.
                  </li>
                  <li>
                    I understand that other factors, such as personal story will
                    be factored into the final decision.
                  </li>
                  <li>
                    If there is an issue where the participant cannot provide
                    their chart entry or photo for a week, they can upload it in
                    the following weeks.
                  </li>
                  <li>The competition will start on the 17th January 2019.</li>
                  <li>
                    The challenge winner can not substitute their position for
                    friend or family member.
                  </li>
                  <li>
                    Once flights, accommodation and other entertainment are
                    booked, the dates can only be changed if it comes at no
                    extra cost to the James Smith Academy. The winner may change
                    the dates of their flights, accommodation and entertainment
                    if they incur the cost of doing so and the James Smith
                    Academy agree to the changes.
                  </li>
                  <li>
                    The James Smith Academy will not provide travel insurance or
                    insurance of any kind for the first prize winner.
                  </li>
                  <li>
                    The James Smith Academy is not liable for any harm, damages
                    or losses which may result from participation in the
                    challenge.
                  </li>
                  <li>
                    The James Smith Academy will pick a winner based on the the
                    participants personal story, achievement of goal, progress,
                    and consistency of numerical and photo entry.
                  </li>
                </ol>
              </div>
            </div>
            <div className='form-group'>
              <label>I agree to the terms stated above.</label>
              <input
                type='checkbox'
                className='form-control '
                id='agree_terms'
                name='agree_terms'
                required
              />
            </div>
            {this.state.created === true && (
              <div className='alert alert-success animated fadeIn' role='alert'>
                You have successfully entered into the{" "}
                {this.state.challenge.name} challenge, you will receive an email
                with more information and instruction.
              </div>
            )}
            <div className='text-center'>
              <button
                type='submit'
                id='formButton'
                className='btn btn-primary enter-button-centered btn-rounded'
              >
                {this.state.sending === true ? (
                  <i className='fas fa-circle-notch fa-spin' />
                ) : (
                  "Submit Entry"
                )}
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const terms = {
  maxHeight: 200,
  overflow: "scroll",
  fontSize: 13,
};
