import React from "react";
import axiosAjax from "../../config/axiosAjax";
import { NavLink } from "react-router-dom";
import MDSpinner from "react-md-spinner";

export default class Judging extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      challenges: [],
      challengeId: "",
      entries: null,
      searchEntries: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._entryNotFailed = this._entryNotFailed.bind(this);
  }

  componentDidMount() {
    axiosAjax.get("/challenge/challenge/").then(response => {
      var challenges = response.data;
      var last_challenge = challenges[0];
      this.setState(
        {
          challenges: response.data,
          challengeId: last_challenge.id,
        },
        () => {
          this._entryNotFailed(event);
        },
      );
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _entryNotFailed(event) {
    this.setState({ searchEntries: true });
    if (event) {
      event.preventDefault();
    }
    axiosAjax
      .get("/challenge/entry-not-failed/", {
        params: {
          challenge_id: this.state.challengeId,
        },
      })
      .then(response => {
        this.setState({
          entries: response.data,
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col d-flex flex-column justify-content-center'>
                <h4>Challenge Judging</h4>
              </div>
            </div>
          </div>
        </div>
        <div className='container'>
          <div className='content-container max-width-500'>
            <h6>Choose Challenge</h6>
            <form onSubmit={this._entryNotFailed}>
              <div className='form-row'>
                <div className='col-12'>
                  <select
                    className='custom-select form-control-single-border'
                    style={{ height: 40 }}
                    onChange={this.handleInputChange}
                    value={this.state.challengeId}
                    name='challengeId'
                    required
                  >
                    <option value='filter-by'>Choose challenge</option>
                    {this.state.challenges.map((challenge, key) => (
                      <option key={key} value={challenge.id}>
                        {challenge.name}
                      </option>
                    ))}
                  </select>
                </div>
                <div className='col-12 text-center'>
                  <button className='waves-effect btn btn-primary btn-rounded enter-button-centered'>
                    Enter
                  </button>
                </div>
              </div>
            </form>
          </div>
          {this.state.searchEntries === true && (
            <div className='content-container'>
              <h6>Entries</h6>
              <div className='list-group list-group-flush animated fadeIn'>
                {this.state.entries !== null ? (
                  this.state.entries.map((entry, key) => (
                    <NavLink
                      to={"/judge/" + entry.id}
                      key={key}
                      className='list-group-item list-group-item-action d-flex justify-content-between align-items-center animated fadeIn'
                    >
                      <span>
                        {entry.fail_judging === false &&
                        entry.pass_judging === false ? (
                          <div className='unjudged'>
                            {key + 1} &nbsp; Not Judged
                          </div>
                        ) : (
                          entry.pass_judging === true && (
                            <div className='passed-judging'>
                              {key + 1} &nbsp; Passed
                            </div>
                          )
                        )}
                      </span>
                      <span>
                        <i className='far fa-chevron-right' />
                      </span>
                    </NavLink>
                  ))
                ) : (
                  <div
                    style={{ paddingTop: "10%", paddingBottom: "10%" }}
                    className='text-center'
                  >
                    <MDSpinner
                      color1='#4a4090'
                      color2='#4a4090'
                      color3='#4a4090'
                      color4='#4a4090'
                    />
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}
