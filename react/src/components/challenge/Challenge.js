import React from "react";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import { NavLink } from "react-router-dom";

export default class Challenge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      challenges: undefined,
    };
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    axiosAjax.get("/challenge/challenge").then(response => {
      this.setState({
        challenges: response.data,
      });
    });
  }

  render() {
    return (
      <div className='container'>
        <div className='animated fadeIn'>
          <div className='mb-1 mt-4 d-flex justify-content-between align-items-center'>
            <h6 className='m-0 section-heading'>Challenges</h6>
          </div>
          {this.state.challenges !== undefined ? (
            this.state.challenges.length > 0 &&
            this.state.challenges.map((challenge, key) =>
              challenge.finished === true ? (
                <div
                  key={key}
                  className='rounded-corners bg-white shadow-sm p-3 mt-3 d-flex justify-content-between align-items-center'
                >
                  <div>
                    {challenge.name}
                    <div className='p-xs text-muted'>Ends {challenge.end}</div>
                  </div>
                  <span style={greyPill}>Ended</span>
                </div>
              ) : challenge.close_enrollment === true ? (
                <NavLink
                  key={key}
                  to={"/challenge/data/" + challenge.id}
                  className='rounded-corners bg-white shadow-sm p-3 mt-3 d-flex justify-content-between align-items-center'
                >
                  <span>
                    {challenge.name}
                    <div className='p-xs text-muted'>Ends {challenge.end}</div>
                  </span>
                  <span style={greenPill}>Enter data</span>
                </NavLink>
              ) : (
                <NavLink
                  key={key}
                  to={"/challenge/entry/" + challenge.id}
                  className='rounded-corners bg-white shadow-sm p-3 mt-3 d-flex justify-content-between align-items-center'
                >
                  <div>
                    {challenge.name}
                    <div className='p-xs text-muted'>Ends {challenge.end}</div>
                  </div>
                  <span style={greenPill}>Enrol here</span>
                </NavLink>
              ),
            )
          ) : (
            <div style={{ paddingTop: "5%", paddingBottom: "5%" }}>
              <div className='text-center'>
                <MDSpinner singleColor='#4a4090' />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const greenPill = {
  fontSize: ".871rem",
  padding: "3px 15px",
  textAlign: "center",
  borderRadius: "20px",
  background: "#00C2A2",
  color: "white",
};

const greyPill = {
  fontSize: ".871rem",
  padding: "3px 15px",
  textAlign: "center",
  borderRadius: "20px",
  background: "#f8f8f8",
};
