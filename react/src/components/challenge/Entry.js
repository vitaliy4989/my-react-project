import React from "react";
import axiosAjax from "../../config/axiosAjax";
import UpdateEntry from "./UpdateEntry";
import CreateEntry from "./CreateEntry";

export default class Entry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      entry: null,
    };
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    axiosAjax
      .get("/challenge/entry/" + this.props.id + "/")
      .then(response => {
        this.setState({
          entry: response.data,
        });
      })
      .catch(error => {
        this.setState({
          entry: null,
        });
      });
  }

  render() {
    return this.state.entry !== null ? (
      <UpdateEntry entry={this.state.entry} challenge={this.props.id} />
    ) : (
      <CreateEntry entry={this.state.entry} challenge={this.props.id} />
    );
  }
}

const included = {
  fontSize: 14,
  margin: 10,
};

const challengeImage = {
  width: "100%",
  marginTop: 25,
};
