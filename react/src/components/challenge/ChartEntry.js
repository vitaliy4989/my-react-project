import React from "react";
import axiosAjax from "../../config/axiosAjax";
import {
  LineChart,
  Line,
  Tooltip,
  YAxis,
  XAxis,
  ResponsiveContainer,
} from "recharts";
import MDSpinner from "react-md-spinner";
import SingleMeasurement from "./SingleMeasurement/index";

export default class ChartEntry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      entry: this.props.entry,
      entryId: this.props.entryId,
      entryName: "",
      chartEntry: "",
      updateChartEntry: "",
      forJudging: this.props.forJudging,
      invalidMeasurement: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleMeasurementChange = this.handleMeasurementChange.bind(this);
    this.enterData = this.enterData.bind(this);
    this._deleteMeasurement = this._deleteMeasurement.bind(this);
    this.getEntry = this.getEntry.bind(this);
  }

  _loadChartData() {
    axiosAjax
      .get("/challenge/chart-entry/" + this.props.entryId + "/")
      .then(response => {
        this.setState({
          data: response.data,
        });
      })
      .catch(error => {
        this.setState({
          data: [],
        });
      });
  }

  componentDidMount() {
    this._loadChartData();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleMeasurementChange(evt) {
    if (evt.target.validity.valid !== true) {
      this.setState({
        chartEntry: "",
        invalidMeasurement: true,
      });
    } else {
      const chartEntry = evt.target.validity.valid
        ? evt.target.value
        : this.state.chartEntry;
      this.setState({
        chartEntry: chartEntry,
        invalidMeasurement: false,
      });
    }
  }

  getEntry(event) {
    event.preventDefault();
    this._loadChartData();
  }

  enterData(event) {
    event.preventDefault();
    if (this.state.invalidMeasurement === true) {
      alert("Please enter only numbers.");
    } else {
      const formData = new FormData();
      formData.append("entry", this.props.entryId);
      formData.append("chart_entry", this.state.chartEntry);
      axiosAjax({
        method: "post",
        url: "/challenge/chart-entry/",
        headers: { "Content-Type": "multipart/form-data" },
        data: formData,
      })
        .then(response => {
          this._loadChartData();
          this.setState({
            chartEntry: "",
          });
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  _deleteMeasurement(id) {
    if (
      confirm(
        "Warning: Click OK to confirm you want to delete this measurement.",
      )
    ) {
      axiosAjax.delete("/challenge/chart-entry/" + id + "/").then(response => {
        this._loadChartData();
      });
    }
  }

  render() {
    if (this.state.data !== null) {
      var chart_data = [];
      this.state.data.map(data =>
        chart_data.push({
          chart_entry: data.chart_entry,
          date: data.date.substring(0, data.date.indexOf("T")),
        }),
      );
    }

    return this.state.data !== null && this.props.entry !== null ? (
      <div className={$(window).width() > 768 ? "container" : ""}>
        <div className='content-container-no-shadow'>
          <ResponsiveContainer width='100%' height={250}>
            <LineChart data={chart_data}>
              <YAxis domain={["dataMin", "dataMax"]} />
              <XAxis dataKey='date' />
              <Tooltip />
              <Line
                type='monotone'
                dataKey='chart_entry'
                name={this.props.entry.target_units}
                stroke='#5F47FF'
                strokeWidth={2}
                dot={{ strokeWidth: 1 }}
              />
            </LineChart>
          </ResponsiveContainer>
          <div className='row'>
            <div className='col-auto'>
              <p
                className='text-muted'
                style={{
                  marginTop: "1rem",
                  marginBottom: 0,
                }}
              >
                Units: {this.props.entry.target_units}
              </p>
            </div>
            <div className='col'>
              <p
                className='text-muted'
                style={{ marginTop: "1rem", marginBottom: 0 }}
              >
                Goal: {this.props.entry.goal}
              </p>
            </div>
          </div>
        </div>

        {this.props.forJudging !== true && (
          <div className='row'>
            <div className='col-lg-4'>
              <div className='content-container-no-shadow text-center'>
                <h6 className='m-0'>New Measurement</h6>
                <p className='p-xs text-muted'>Please enter number, no units</p>
                <form onSubmit={this.enterData} className='form-row'>
                  <div className='col-12'>
                    <input
                      style={
                        this.state.invalidMeasurement === true
                          ? { borderColor: "red" }
                          : {}
                      }
                      type='number'
                      step='any'
                      onChange={this.handleMeasurementChange}
                      value={this.state.chartEntry}
                      name='chartEntry'
                      className='form-control form-control-single-border text-center'
                      autoComplete='off'
                      placeholder={
                        "Enter " +
                        this.props.entry.target_units +
                        " measurement"
                      }
                      required
                    />
                  </div>
                  <div className='col-12 text-center'>
                    <button className='btn btn-primary enter-button-centered btn-rounded'>
                      Enter
                    </button>
                  </div>
                </form>
              </div>
            </div>

            <div className='col-lg-8'>
              <div style={{ marginTop: 30 }}>
                {this.state.data.length >= 1 &&
                  this.state.data.map((data, key) => (
                    <SingleMeasurement
                      key={key}
                      date={data.date.substring(0, data.date.indexOf("T"))}
                      id={data.id}
                      measurement={data.chart_entry}
                      deleteMeasurement={this._deleteMeasurement}
                      reloadChart={this.getEntry}
                    />
                  ))}
              </div>
            </div>
          </div>
        )}
      </div>
    ) : (
      <div className='h-100 d-flex flex-column justify-content-center'>
        <div className='text-center'>
          <MDSpinner singleColor='#4a4090' />
        </div>
      </div>
    );
  }
}
