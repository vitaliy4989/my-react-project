import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { NotificationManager } from "react-notifications";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      image: this.props.image,
      date: this.props.date,
      bodyPosition: this.props.bodyPosition,
      weekNumber: this.props.weekNumber,
      updateImage: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._updateImage = this._updateImage.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.bodyPosition !== prevState.bodyPosition) {
      this.setState({
        updateImage: true,
      });
    }
    if (this.state.weekNumber !== prevState.weekNumber) {
      this.setState({
        updateImage: true,
      });
    }
    if (this.props.id !== prevProps.id) {
      this.setState({
        id: this.props.id,
        image: this.props.image,
        date: this.props.date,
        bodyPosition: this.props.bodyPosition,
        weekNumber: this.props.weekNumber,
        updateImage: false,
      });
    }
  }

  _updateImage(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("body_position", this.state.bodyPosition);
    formData.append("image_entry", this.state.image);
    formData.append("week_number", Number(this.state.weekNumber));
    axiosAjax({
      method: "patch",
      url: "/challenge/image-entry/" + this.state.id + "/",
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Image updated.", "", 5000);
      })
      .catch(error => {
        NotificationManager.error("There has been an error.", "", 5000);
      });
  }

  render() {
    return (
      <div className='card'>
        <img
          className='card-img-top'
          src={this.state.image + "-/autorotate/yes/"}
        />
        <div className='card-body'>
          <div className='row'>
            <div className='col'>
              <form onSubmit={this._updateImage}>
                <div className='form-group'>
                  <select
                    className='form-control custom-select'
                    onChange={this.handleInputChange}
                    name='bodyPosition'
                    value={this.state.bodyPosition}
                  >
                    <option value='' disabled selected>
                      Select Body Position
                    </option>
                    <option value='Back'>Back</option>
                    <option value='Front'>Front</option>
                    <option value='Side Left'>Side Left</option>
                    <option value='Side Right'>Side Right</option>
                    <option value='Other'>Other</option>
                  </select>
                </div>
                <div className='form-group'>
                  <select
                    className='form-control custom-select'
                    onChange={this.handleInputChange}
                    name='weekNumber'
                    value={this.state.weekNumber}
                  >
                    <option value='' disabled selected>
                      Select Week Number
                    </option>
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                    <option value={4}>4</option>
                    <option value={5}>5</option>
                    <option value={6}>6</option>
                    <option value={7}>7</option>
                    <option value={8}>8</option>
                    <option value={9}>9</option>
                    <option value={10}>10</option>
                    <option value={11}>11</option>
                    <option value={12}>12</option>
                  </select>
                </div>
                {this.state.updateImage === true && (
                  <div className='form-group'>
                    <button className='btn btn-sm btn-outline-success btn-block'>
                      Update
                    </button>
                  </div>
                )}
              </form>
              <div className='form-group'>
                <button
                  className='btn btn-block btn-sm btn-light'
                  onClick={() => this.props.deletePhoto(this.state.id)}
                >
                  <i className='far fa-times' />
                </button>
              </div>
              <small className='text-muted'>{this.state.date}</small>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Index;
