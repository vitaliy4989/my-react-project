import React from "react";
import axiosAjax from "../../config/axiosAjax";
import ChartEntry from "./ChartEntry";
import ImageEntry from "./ImageEntry";

export default class JudgeEntry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      entryId: this.props.match.params.id,
      username: "",
      email: "",
      firstName: "",
      failed: false,
      passed: false,
      entry: null,
    };
  }

  componentDidMount() {
    axiosAjax
      .get("/challenge/judging-entry/" + this.props.match.params.id + "/")
      .then(response => {
        this.setState({
          entry: response.data,
          username: response.data.user.username,
          email: response.data.user.email,
          firstName: response.data.user.first_name,
          failed: response.data.fail_judging,
          passed: response.data.pass_judging,
        });
      });
  }

  _failEntry() {
    this.setState({ failed: true, passed: false });
    const formData = new FormData();
    formData.append("fail_judging", true);
    axiosAjax({
      method: "patch",
      url: "/challenge/entry/" + this.props.match.params.id + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          failed: true,
          passed: false,
        });
      })
      .catch(error => {
        this.setState({
          failed: false,
        });
      });
  }

  _passEntry() {
    this.setState({ passed: true, failed: false });
    const formData = new FormData();
    formData.append("pass_judging", true);
    axiosAjax({
      method: "patch",
      url: "/challenge/entry/" + this.props.match.params.id + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          passed: true,
          failed: false,
        });
      })
      .catch(error => {
        this.setState({
          passed: false,
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <div className='site-headings-2'>
          <div className='container'>
            <div className='row'>
              <div className='col d-flex flex-column justify-content-center'>
                <h4 className='m-0'>{this.state.firstName}</h4>
                <p className='white m-0'>{this.state.username}</p>
                <p className='p-smaller white m-0'>{this.state.email}</p>
              </div>
              <div className='col-auto d-flex flex-column justify-content-center'>
                {this.state.passed === true ? (
                  <button
                    style={{ background: "#4c9064", color: "white" }}
                    className='btn btn-rounded float-right'
                  >
                    Passed
                  </button>
                ) : (
                  <button
                    onClick={() => this._passEntry()}
                    className='btn btn-rounded btn-white-menu float-right'
                  >
                    Pass
                  </button>
                )}
              </div>
              <div className='col-auto d-flex flex-column justify-content-center'>
                {this.state.failed === true ? (
                  <button
                    style={{ background: "#f44336", color: "white" }}
                    className='btn btn-rounded float-right'
                  >
                    Failed
                  </button>
                ) : (
                  <button
                    onClick={() => this._failEntry()}
                    className='btn btn-rounded btn-white-menu float-right'
                  >
                    Fail
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className='container'>
          <h4 className='m-5' style={{ fontWeight: 900 }}>
            Chart
          </h4>
          <ChartEntry
            entryId={this.state.entryId}
            entry={this.state.entry}
            forJudging={true}
          />
          <h4 className='m-5' style={{ fontWeight: 900 }}>
            Images
          </h4>
          <ImageEntry
            entryId={this.state.entryId}
            entry={this.state.entry}
            forJudging={true}
          />
        </div>
      </React.Fragment>
    );
  }
}
