import React from "react";
import axiosAjax from "../../config/axiosAjax";
import Uploader from "../shared/Uploader";
import MDSpinner from "react-md-spinner";
import Image from "./SingleImage/index";

export default class ImageEntry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      entry: this.props.entry,
      file: "",
      entryId: this.props.entryId,
      message: "",
      uploaded: false,
      uploadAgain: false,
      forJudging: this.props.forJudging,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getEntry = this.getEntry.bind(this);
    this._deletePhoto = this._deletePhoto.bind(this);
  }

  _loadImages() {
    axiosAjax
      .get("/challenge/image-entry/" + this.props.entryId + "/")
      .then(response => {
        this.setState({
          data: response.data,
        });
      })
      .catch(error => {
        this.setState({
          data: [],
        });
      });
  }

  componentDidMount() {
    this._loadImages();
    this.setState({
      entry: this.props.entry,
    });
  }

  getEntry(event) {
    event.preventDefault();
    this._loadImages();
  }

  handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("entry", this.props.entryId);
    formData.append("image_entry", this.state.file.uuid);
    axiosAjax({
      method: "post",
      url: "/challenge/image-entry/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this._loadImages();
        this.setState({
          uploaded: false,
          uploadAgain: true,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  _deletePhoto(id) {
    var r = confirm("Warning: Click OK to delete this image.");
    if (r == true) {
      axiosAjax.delete("/challenge/image-entry/" + id + "/").then(response => {
        this._loadImages();
      });
    }
  }

  render() {
    return this.state.data !== null && this.props.entry !== null ? (
      <div className='container'>
        {this.props.forJudging !== true && (
          <div className='content-container text-center max-width-500 m-b-25'>
            <h6>Challenge images</h6>
            <form onSubmit={this.handleSubmit}>
              <div className='form-row'>
                <div className='col-12 text-center'>
                  <Uploader
                    id='file'
                    name='file'
                    imageOnly={true}
                    onChange={file => {
                      if (file) {
                        file.progress(info => console.log("uploaded"));
                        file.done(info => this.setState({ uploaded: true }));
                      }
                    }}
                    onUploadComplete={info =>
                      this.setState({ file: info, uploadAgain: false })
                    }
                  />
                </div>
                <div className='col-12 text-center'>
                  {this.state.uploaded === true && (
                    <button className='waves-effect enter-button-centered btn-rounded btn btn-primary'>
                      Click to upload <i className='fas fa-cloud-upload-alt' />
                    </button>
                  )}
                  {this.state.uploadAgain === true && (
                    <p>Click link to upload another image.</p>
                  )}
                </div>
              </div>
            </form>

            {this.state.message != "" && (
              <div
                className='alert alert-success alert-dismissible fade show'
                role='alert'
                style={{ marginTop: "1rem" }}
              >
                <strong>{this.state.message}</strong>
                <button
                  type='button'
                  className='close'
                  data-dismiss='alert'
                  aria-label='Close'
                  onClick={() => this.setState({ message: "" })}
                >
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>
            )}
          </div>
        )}

        {this.state.data.length >= 1 && (
          <div className='card-columns'>
            {this.state.data.map((data, key) => (
              <Image
                key={key}
                id={data.id}
                image={data.image_entry}
                date={data.date.substring(0, data.date.indexOf("T"))}
                deletePhoto={this._deletePhoto}
                weekNumber={data.week_number}
                bodyPosition={data.body_position}
              />
            ))}
          </div>
        )}
      </div>
    ) : (
      <div className='h-100 d-flex flex-column justify-content-center'>
        <div className='text-center'>
          <MDSpinner singleColor='#4a4090' />
        </div>
      </div>
    );
  }
}
