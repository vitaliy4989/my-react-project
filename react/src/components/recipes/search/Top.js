import React from "react";
import axiosAjax from "../../../config/axiosAjax";
import Recipe from "../Recipe";
import MDSpinner from "react-md-spinner";

export default class Top extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recipes: null,
    };
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    axiosAjax.get("/recipes/top/").then(response => {
      this.setState({
        recipes: response.data,
      });
    });
  }

  render() {
    return this.state.recipes !== null ? (
      this.state.recipes.length > 0 && (
        <div className='container' style={{ marginTop: 25 }}>
          <div className='card-columns'>
            {this.state.recipes.map((recipe, key) => (
              <React.Fragment key={key}>
                {Recipe({
                  recipe_id: recipe.id,
                  photo: recipe.photo,
                  name: recipe.name,
                })}
              </React.Fragment>
            ))}
          </div>
        </div>
      )
    ) : (
      <div className='h-100 d-flex flex-column justify-content-center'>
        <div className='text-center'>
          <MDSpinner singleColor='#4a4090' />
        </div>
      </div>
    );
  }
}
