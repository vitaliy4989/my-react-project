import React from "react";
import axiosAjax from "../../../config/axiosAjax";
import Recipe from "../Recipe";
import MDSpinner from "react-md-spinner";
import LightAlert from "../../shared/LightAlert";

export default class Favourites extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recipes: undefined,
    };
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    axiosAjax.get("/recipes/favourite/").then(response => {
      this.setState({
        recipes: response.data,
      });
    });
  }

  render() {
    return this.state.recipes !== undefined ? (
      this.state.recipes.length > 0 ? (
        <div className='container'>
          <div className='card-columns'>
            {this.state.recipes.map((recipe, key) => (
              <React.Fragment key={key}>
                {Recipe({
                  recipe_id: recipe.recipe.id,
                  photo: recipe.recipe.photo,
                  name: recipe.recipe.name,
                })}
              </React.Fragment>
            ))}
          </div>
        </div>
      ) : (
        <div className='h-100 d-flex flex-column justify-content-center text-center'>
          <LightAlert
            message='You have not saved any favourite recipes.'
            container={true}
          />
        </div>
      )
    ) : (
      <div className='h-100 d-flex flex-column justify-content-center'>
        <div className='text-center'>
          <MDSpinner singleColor='#4a4090' />
        </div>
      </div>
    );
  }
}
