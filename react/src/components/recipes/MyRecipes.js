import React from "react";
import axiosAjax from "../../config/axiosAjax";
import UpdateRecipe from "./UpdateRecipe";

export default class MyRecipes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recipes: [],
    };
  }

  componentDidMount() {
    axiosAjax.get("/recipes/my-recipes/").then(response => {
      this.setState({
        recipes: response.data,
      });
    });
  }

  render() {
    console.log(this.state.recipes);

    return (
      <div style={{ marginTop: 25 }}>
        {this.state.recipes.length > 0 ? (
          <div className='row'>
            {this.state.recipes.map(recipe => (
              <UpdateRecipe
                key={recipe.id}
                id={recipe.id}
                name={recipe.name}
                protein={recipe.protein}
                carbs={recipe.carbs}
                fat={recipe.fat}
                description={recipe.description}
                ingredients={recipe.ingredients}
                method={recipe.method}
                photo={recipe.photo}
                type={recipe.type_meal}
                servings={recipe.servings}
                approved={recipe.approved}
              />
            ))}
          </div>
        ) : (
          <div className='text-center alert alert-light'>
            You have not created any recipes.
          </div>
        )}
      </div>
    );
  }
}
