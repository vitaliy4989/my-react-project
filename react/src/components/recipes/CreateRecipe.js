import React from "react";
import axiosAjax from "../../config/axiosAjax";
import Uploader from "../shared/Uploader";
import { NotificationManager } from "react-notifications";

export default class CreateRecipe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      file: "",
      protein: "",
      carbs: "",
      fat: "",
      type: "snack",
      description: "",
      method: "",
      ingredients: "",
      servings: 1,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("protein", this.state.protein);
    formData.append("carbs", this.state.carbs);
    formData.append("fat", this.state.fat);
    formData.append("photo", this.state.file.uuid);
    formData.append("type_meal", this.state.type);
    formData.append("description", this.state.description);
    formData.append("ingredients", this.state.ingredients);
    formData.append("servings", this.state.servings);
    formData.append("method", this.state.method);
    axiosAjax({
      method: "post",
      url: "/recipes/recipe/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(function(response) {
        NotificationManager.success("Recipe created, thanks!", "", 5000);
        this.setState({
          name: "",
          protein: "",
          carbs: "",
          fat: "",
          photo: "",
          description: "",
          ingredients: "",
          method: "",
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div className='content-container'>
        <h5 className='m-0'>Create Recipe</h5>
        <p>Please do not screenshot other peoples recipes.</p>
        <form onSubmit={this.handleSubmit}>
          <div className='form-group'>
            <label>Recipe Image (required)</label>
            <br />
            <Uploader
              id='file'
              name='file'
              imageOnly={true}
              dataCrop='400x400'
              onChange={file => {
                if (file) {
                  file.progress(info => console.log("uploaded"));
                  file.done(info => console.log("done"));
                }
              }}
              onUploadComplete={info => this.setState({ file: info })}
            />
          </div>

          <div className='form-group'>
            <label htmlFor='name'>Name</label>
            <input
              autoComplete='off'
              onChange={this.handleInputChange}
              value={this.state.name}
              type='text'
              className='form-control form-control-single-border'
              name='name'
              placeholder='Name'
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='type'>Type</label>
            <select
              className='form-control  custom-select form-control-single-border'
              onChange={this.handleInputChange}
              value={this.state.type}
              name='type'
            >
              <option value='snack'>Snack</option>
              <option value='drink'>Drink</option>
              <option value='treat'>Treat</option>
              <option value='breakfast'>Breakfast</option>
              <option value='lunch'>Lunch</option>
              <option value='dinner'>Dinner</option>
              <option value='box'>Lunch Box</option>
              <option value='meal-prep'>Meal Prep</option>
              <option value='vegetarian'>Vegetarian</option>
            </select>
          </div>

          <div className='form-group'>
            <label htmlFor='protein'>Total servings in recipe</label>
            <input
              autoComplete='off'
              type='number'
              step='0.00000001'
              onChange={this.handleInputChange}
              value={this.state.servings}
              className='form-control form-control-single-border'
              name='servings'
              placeholder={this.state.servings}
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='protein'>
              Protein per {this.state.servings} servings
            </label>
            <input
              onChange={this.handleInputChange}
              value={this.state.protein}
              type='number'
              step='0.00000001'
              className='form-control form-control-single-border'
              name='protein'
              placeholder='Protein'
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='carbs'>
              Carbs per {this.state.servings} servings
            </label>
            <input
              onChange={this.handleInputChange}
              value={this.state.carbs}
              type='number'
              step='0.00000001'
              className='form-control form-control-single-border'
              name='carbs'
              placeholder='Carbs'
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='fat'>Fat per {this.state.servings} servings</label>
            <input
              onChange={this.handleInputChange}
              value={this.state.fat}
              type='number'
              step='0.00000001'
              className='form-control form-control-single-border'
              name='fat'
              placeholder='Fat'
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='description'>Describe this recipe</label>
            <textarea
              onChange={this.handleInputChange}
              value={this.state.description}
              name='description'
              className='form-control form-control-single-border'
              placeholder='Describe this recipe'
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='ingredients'>
              List recipe ingredients (list each ingredient on new line)
            </label>
            <textarea
              onChange={this.handleInputChange}
              value={this.state.ingredients}
              name='ingredients'
              className='form-control form-control-single-border'
              placeholder='List recipe ingredients'
              required
            />
          </div>

          <div className='form-group'>
            <label htmlFor='method'>
              Cooking method (list each step on new line)
            </label>
            <textarea
              onChange={this.handleInputChange}
              value={this.state.method}
              name='method'
              className='form-control form-control-single-border'
              placeholder='Cooking method'
              required
            />
          </div>

          <div className='text-center'>
            <button
              type='submit'
              className='btn btn-primary btn-rounded enter-button-centered'
            >
              Upload
            </button>
          </div>
        </form>
      </div>
    );
  }
}
