import React from "react";
import axiosAjax from "../../config/axiosAjax";

export default class AddRecipeToMealPlan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      added: false,
    };
  }

  _saveRecipeToMealPlan() {
    const formData = new FormData();
    this.setState({ added: true });
    formData.append("combination_id", this.props.combinationId);
    formData.append("recipe_id", this.props.recipeId);
    axiosAjax({
      method: "post",
      url: "/recipes/combination-recipe/",
      data: formData,
    })
      .then(response => {
        this.setState({
          added: true,
        });
      })
      .catch(error => {
        this.setState({
          added: false,
        });
      });
  }

  render() {
    return this.state.added === true ? (
      <button className='btn float-right btn-round btn-active animated pulse'>
        <i className='far fa-check white' />
      </button>
    ) : (
      <button
        onClick={() => this._saveRecipeToMealPlan()}
        className='btn btn-light float-right btn-round'
      >
        <i className='far fa-plus' />
      </button>
    );
  }
}
