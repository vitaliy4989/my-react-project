import React from "react";
import { NavLink } from "react-router-dom";

export default class Menu extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      nav: this.props.nav,
      recipes: false,
      combinations: false,
    };
  }

  componentDidMount() {
    if (window.innerWidth > 768) {
      $(".button-collapse").sideNav({
        edge: "right",
        closeOnClick: true,
      });
      var sideNavScrollbar = document.querySelector(".custom-scrollbar");
      Ps.initialize(sideNavScrollbar);
    }
  }

  render() {
    return $(window).width() > 768 ? (
      <div id='slide-out' className='side-nav'>
        <ul className='custom-scrollbar'>
          <li>
            <h6 className='side-nav-heading'>Other Filters</h6>
          </li>

          <li>
            <ul className='collapsible collapsible-accordion'>
              <li>
                <NavLink activeStyle={linkActive} to='/recipes/filter/featured'>
                  Top
                </NavLink>
              </li>
              <li>
                <NavLink activeStyle={linkActive} to='/recipes/filter/new'>
                  New
                </NavLink>
              </li>
              <li>
                <NavLink
                  activeStyle={linkActive}
                  to='/recipes/filter/favourite'
                >
                  Favourite
                </NavLink>
              </li>
              <li>
                <NavLink activeStyle={linkActive} to='/recipes/recipes-my'>
                  My Recipes
                </NavLink>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    ) : null;
  }
}

const linkActive = {
  color: "#4a4090",
  backgroundColor: "#e9ecef",
  border: "none",
};
