import React from "react";
import axiosAjax from "../../config/axiosAjax";
import Recipe from "./Recipe";
import { NotificationManager } from "react-notifications";

export default class MyRecipes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      name: this.props.name,
      type: this.props.type,
      protein: this.props.protein,
      carbs: this.props.carbs,
      fat: this.props.fat,
      description: this.props.description,
      method: this.props.method,
      ingredients: this.props.ingredients,
      servings: this.props.servings,
      approved: this.props.approved,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.updateRecipe = this.updateRecipe.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  updateRecipe(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("type_meal", this.state.type);
    formData.append("protein", this.state.protein);
    formData.append("carbs", this.state.carbs);
    formData.append("fat", this.state.fat);
    formData.append("description", this.state.description);
    formData.append("ingredients", this.state.ingredients);
    formData.append("method", this.state.method);
    formData.append("servings", this.state.servings);
    formData.append("approved", this.state.approved);
    axiosAjax({
      method: "patch",
      url: "/recipes/recipe/" + this.state.id + "/",
      data: formData,
    })
      .then(function(response) {
        NotificationManager.success("Recipe updated!", "", 5000);
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  deleteRecipe(id) {
    event.preventDefault();
    axiosAjax.delete("/recipes/recipe/" + id + "/").then(response => {
      location.reload();
    });
  }

  render() {
    return (
      <div className='col-lg-6'>
        <div className='content-container'>
          <img
            className='img-fluid'
            src={this.props.photo + "-/autorotate/yes/"}
          />
          <div className='card-body'>
            <form onSubmit={this.updateRecipe}>
              <input type='hidden' value={this.state.id} name='id' />
              <div className='form-group'>
                <label>Name</label>
                <input
                  onChange={this.handleInputChange}
                  className='form-control form-control-single-border'
                  type='text'
                  value={this.state.name}
                  name='name'
                />
              </div>
              <div className='form-group'>
                <label>Meal type</label>
                <select
                  className='form-control  custom-select form-control-single-border'
                  onChange={this.handleInputChange}
                  value={this.state.type}
                  name='type'
                >
                  <option value='snack'>Snack</option>
                  <option value='drink'>Drink</option>
                  <option value='treat'>Treat</option>
                  <option value='breakfast'>Breakfast</option>
                  <option value='lunch'>Lunch</option>
                  <option value='dinner'>Dinner</option>
                  <option value='meal-prep'>Meal Prep</option>
                  <option value='vegetarian'>Vegetarian</option>
                </select>
              </div>
              <div className='form-group'>
                <label htmlFor='protein'>Total servings in recipe</label>
                <input
                  autoComplete='off'
                  type='number'
                  step='0.00000001'
                  onChange={this.handleInputChange}
                  value={this.state.servings}
                  className='form-control form-control-single-border'
                  name='servings'
                  placeholder={this.state.servings}
                  required
                />
              </div>
              <div className='form-group'>
                <label>Protein per {this.state.servings} servings</label>
                <input
                  onChange={this.handleInputChange}
                  className='form-control form-control-single-border'
                  step='0.00000001'
                  type='number'
                  value={this.state.protein}
                  name='protein'
                />
              </div>
              <div className='form-group'>
                <label>Carbs per {this.state.servings} servings</label>
                <input
                  onChange={this.handleInputChange}
                  className='form-control form-control-single-border'
                  step='0.00000001'
                  type='number'
                  value={this.state.carbs}
                  name='carbs'
                />
              </div>
              <div className='form-group'>
                <label>Fat per {this.state.servings} servings</label>
                <input
                  onChange={this.handleInputChange}
                  className='form-control form-control-single-border'
                  step='0.00000001'
                  type='number'
                  value={this.state.fat}
                  name='fat'
                />
              </div>
              <div className='form-group'>
                <label>Description</label>
                <textarea
                  style={textareaStyle}
                  onChange={this.handleInputChange}
                  value={this.state.description}
                  className='form-control form-control-single-border'
                  name='description'
                  required
                >
                  {this.state.description}
                </textarea>
              </div>
              {this.props.ingredients !== "" && (
                <div className='form-group'>
                  <label>Ingredients</label>
                  <textarea
                    style={textareaStyle}
                    onChange={this.handleInputChange}
                    value={this.state.ingredients}
                    className='form-control form-control-single-border'
                    name='ingredients'
                    required
                  >
                    {this.state.ingredients}
                  </textarea>
                </div>
              )}
              {this.props.method !== "" && (
                <div className='form-group'>
                  <label>Method</label>
                  <textarea
                    style={textareaStyle}
                    onChange={this.handleInputChange}
                    value={this.state.method}
                    className='form-control form-control-single-border'
                    name='method'
                    required
                  >
                    {this.state.method}
                  </textarea>
                </div>
              )}
              <div className='text-center'>
                <button className='btn btn-primary btn-rounded enter-button-centered'>
                  Update
                </button>
              </div>
            </form>
          </div>
          <div style={{ marginTop: "1rem" }}>
            <button
              onClick={() => this.deleteRecipe(this.state.id)}
              className='btn  btn-outline-danger'
            >
              Delete
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const textareaStyle = {
  whiteSpace: "pre-wrap",
};
