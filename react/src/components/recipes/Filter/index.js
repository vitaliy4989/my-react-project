import React from "react";
import localforage from "localforage";
import "./styles.css";

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterName: true,
      filterType: false,
      filterProtein: false,
      filterCalories: false,
      name: "",
      type: "",
      protein: "",
      calories: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  componentDidMount() {
    localforage.getItem("filterName").then(value => {
      if (value !== null && this.state.name === "") {
        this.setState({
          name: value,
        });
      } else {
        this.setState({
          name: "",
        });
      }
    });
    localforage.getItem("filterType").then(value => {
      if (value !== null && this.state.type === "") {
        this.setState({
          type: value,
        });
      } else {
        this.setState({
          type: "",
        });
      }
    });
    localforage.getItem("filterProtein").then(value => {
      if (value !== null && this.state.protein === "") {
        this.setState({
          protein: value,
        });
      } else {
        this.setState({
          protein: "",
        });
      }
    });
    localforage.getItem("filterCalories").then(value => {
      if (value !== null && this.state.calories === "") {
        this.setState({
          calories: value,
        });
      } else {
        this.setState({
          calories: "",
        });
      }
    });
  }

  _clearName() {
    this.setState({
      name: "",
    });
    localforage.removeItem("filterName");
  }

  _clearType() {
    this.setState({
      type: "",
    });
    localforage.removeItem("filterType");
  }

  _clearProtein() {
    this.setState({
      protein: "",
    });
    localforage.removeItem("filterProtein");
  }

  _clearCalories() {
    this.setState({
      calories: "",
    });
    localforage.removeItem("filterCalories");
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.name !== this.state.name) {
      localforage.setItem("filterName", this.state.name);
      this._generateUrl();
    }
    if (prevState.type !== this.state.type) {
      localforage.setItem("filterType", this.state.type);
      this._generateUrl();
    }
    if (prevState.protein !== this.state.protein) {
      localforage.setItem("filterProtein", this.state.protein);
      this._generateUrl();
    }
    if (prevState.calories !== this.state.calories) {
      localforage.setItem("filterCalories", this.state.calories);
      this._generateUrl();
    }
  }

  _generateUrl() {
    if (this.state.name !== "") {
      var name = this.state.name;
    } else {
      var name = "undefined";
    }
    if (this.state.type !== "") {
      var type = this.state.type;
    } else {
      var type = "undefined";
    }
    if (this.state.protein !== "") {
      var protein = this.state.protein;
    } else {
      var protein = "undefined";
    }
    if (this.state.calories !== "") {
      var calories = this.state.calories;
    } else {
      var calories = "undefined";
    }
    window.location.href =
      window.location.protocol +
      "//" +
      window.location.host +
      "/users/#/recipes/query/" +
      name +
      "/" +
      type +
      "/" +
      protein +
      "/" +
      calories +
      "/";
  }

  render() {
    return (
      <div className='container' style={{ marginTop: 30 }}>
        <div className='input-group recipe-search'>
          {this.state.filterName === true ? (
            <input
              autoComplete='off'
              type='text'
              className='form-control form-control-rounded'
              onChange={this.handleInputChange}
              value={this.state.name}
              name='name'
              placeholder='Enter a recipe name...'
              required
            />
          ) : null}
          {this.state.filterType === true ? (
            <select
              className='custom-select form-control-rounded'
              autoFocus={true}
              style={{ height: 40 }}
              onChange={this.handleInputChange}
              value={this.state.type}
              name='type'
              required
            >
              <option value='filter-by'>Choose from recipe types...</option>
              <option value='snack'>Snack</option>
              <option value='drink'>Drink</option>
              <option value='treat'>Treat</option>
              <option value='breakfast'>Breakfast</option>
              <option value='lunch'>Lunch</option>
              <option value='dinner'>Dinner</option>
              <option value='box'>Lunch Box</option>
              <option value='meal-prep'>Meal Prep</option>
            </select>
          ) : null}
          {this.state.filterProtein === true ? (
            <input
              autoComplete='off'
              type='number'
              onChange={this.handleInputChange}
              value={this.state.protein}
              name='protein'
              className='form-control form-control-rounded'
              placeholder='Protein amount greater than...'
              required
            />
          ) : null}
          {this.state.filterCalories === true ? (
            <input
              autoComplete='off'
              type='number'
              onChange={this.handleInputChange}
              value={this.state.calories}
              name='calories'
              className='form-control form-control-rounded'
              placeholder='Calorie amount greater than...'
              required
            />
          ) : null}
          <div className='input-group-append'>
            <button
              type='button'
              className='btn  btn-primary btn-rounded dropdown-toggle'
              data-toggle='dropdown'
              aria-haspopup='true'
              aria-expanded='false'
            >
              <span className='sr-only'>Toggle Dropdown</span>
            </button>
            <div className='dropdown-menu dropdown-menu-right'>
              <button
                style={{ cursor: "pointer" }}
                type='button'
                onClick={() =>
                  this.setState({
                    filterName: true,
                    filterType: false,
                    filterProtein: false,
                    filterCalories: false,
                  })
                }
                className={
                  this.state.filterName === true
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Filter by name
              </button>
              <button
                style={{ cursor: "pointer" }}
                type='button'
                onClick={() =>
                  this.setState({
                    filterName: false,
                    filterType: true,
                    filterProtein: false,
                    filterCalories: false,
                  })
                }
                className={
                  this.state.filterType === true
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Filter by type
              </button>
              <button
                style={{ cursor: "pointer" }}
                type='button'
                onClick={() =>
                  this.setState({
                    filterName: false,
                    filterType: false,
                    filterProtein: true,
                    filterCalories: false,
                  })
                }
                className={
                  this.state.filterProtein === true
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Filter by protein
              </button>
              <button
                style={{ cursor: "pointer" }}
                type='button'
                onClick={() =>
                  this.setState({
                    filterName: false,
                    filterType: false,
                    filterProtein: false,
                    filterCalories: true,
                  })
                }
                className={
                  this.state.filterCalories === true
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Filter by calories
              </button>
            </div>
          </div>
        </div>
        <div className='filter-tags'>
          {this.state.name !== "" && this.state.name ? (
            <div className='tag'>
              {this.state.name} &nbsp;{" "}
              <i
                onClick={() => this._clearName()}
                className='far clear-filter white fa-times-circle'
              />
            </div>
          ) : null}
          {this.state.type !== "" && this.state.type ? (
            <div className='tag'>
              {this.state.type} &nbsp;{" "}
              <i
                onClick={() => this._clearType()}
                className='far clear-filter white fa-times-circle'
              />
            </div>
          ) : null}
          {this.state.protein !== "" && this.state.protein ? (
            <div className='tag'>
              {this.state.protein}g &nbsp;{" "}
              <i
                onClick={() => this._clearProtein()}
                className='far clear-filter white fa-times-circle'
              />
            </div>
          ) : null}
          {this.state.calories !== "" && this.state.calories ? (
            <div className='tag'>
              {this.state.calories}cals &nbsp;{" "}
              <i
                onClick={() => this._clearCalories()}
                className='far clear-filter white fa-times-circle'
              />
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}
