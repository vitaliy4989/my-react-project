import React from "react";

const openLink = link => {
  window.open(link, "_self");
};

const Recipe = props => {
  return (
    <div
      onClick={() => openLink("/users/#/recipes/recipe/" + props.recipe_id)}
      className='card shadow-sm'
      style={{ cursor: "pointer" }}
    >
      <img className='card-img-top' src={props.photo + "-/autorotate/yes/"} />
      <div className='card-body text-center'>
        <p className='card-title'>{props.name}</p>
      </div>
    </div>
  );
};

export default Recipe;
