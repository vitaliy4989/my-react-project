import React from "react";

const MenuButton = props => {
  return (
    <div className='d-flex justify-content-between align-items-center'>
      <div className='sub-menu-title'>Other Filters</div>
      <button
        data-activates='slide-out'
        className='btn btn-white-menu btn-rounded float-right button-collapse waves-effect'
      >
        <i className='far dark-purp fa-bars' />
      </button>
    </div>
  );
};

export default MenuButton;
