import React from "react";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import Recipe from "../Recipe";
import LightAlert from "../../shared/LightAlert";

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: undefined,
      type: undefined,
      protein: undefined,
      calories: undefined,
      recipes: undefined,
    };
  }

  componentDidMount() {
    this.setState(
      {
        name: this.props.name,
        type: this.props.type,
        protein: this.props.protein,
        calories: this.props.calories,
      },
      () => {
        this._queryRecipes();
      },
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.name !== this.props.name) {
      this.setState(
        {
          name: this.props.name,
        },
        () => {
          this._queryRecipes();
        },
      );
    }
    if (prevProps.type !== this.props.type) {
      this.setState(
        {
          type: this.props.type,
        },
        () => {
          this._queryRecipes();
        },
      );
    }
    if (prevProps.protein !== this.props.protein) {
      this.setState(
        {
          protein: this.props.protein,
        },
        () => {
          this._queryRecipes();
        },
      );
    }
    if (prevProps.calories !== this.props.calories) {
      this.setState(
        {
          calories: this.props.calories,
        },
        () => {
          this._queryRecipes();
        },
      );
    }
  }

  _queryRecipes() {
    const queryParams = {};

    if (this.state.name && this.state.name !== "undefined") {
      queryParams.recipe_name = this.state.name;
    }
    if (this.state.type && this.state.type !== "undefined") {
      queryParams.recipe_type = this.state.type;
    }
    if (this.state.protein && this.state.protein !== "undefined") {
      queryParams.recipe_protein = this.state.protein;
    }
    if (this.state.calories && this.state.calories !== "undefined") {
      queryParams.recipe_calories = this.state.calories;
    }
    axiosAjax
      .get("/recipes/search/", {
        params: queryParams,
      })
      .then(response => {
        this.setState({
          recipes: response.data,
        });
      })
      .catch(error => {
        this.setState({
          recipes: [],
        });
      });
  }

  render() {
    return this.state.recipes !== undefined ? (
      this.state.recipes.length > 0 ? (
        <div className='container'>
          <div className='card-columns' style={{ marginTop: 30 }}>
            {this.state.recipes.map((recipe, key) => (
              <React.Fragment key={key}>
                {Recipe({
                  recipe_id: recipe.id,
                  photo: recipe.photo,
                  name: recipe.name,
                })}
              </React.Fragment>
            ))}
          </div>
        </div>
      ) : (
        <div className='h-100 d-flex flex-column justify-content-center'>
          <LightAlert
            class='text-center'
            message='Sorry, no results match your search.'
            container={true}
          />
        </div>
      )
    ) : (
      <div className='h-100 d-flex flex-column justify-content-center'>
        <div className='text-center'>
          <MDSpinner singleColor='#4a4090' />
        </div>
      </div>
    );
  }
}
