import React from "react";
import axiosAjax from "../../config/axiosAjax";
import { connect } from "react-redux";
import cn from "classnames";
import { updateCaloriesProtein } from "../../redux/actions/nutrition";
import { getStepFour } from "../../redux/actions/home";
import { NotificationManager } from "react-notifications";
import "./styles.css";

class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      goal: "",
      gender: "",
      age: "",
      activity: "",
      weight: "",
      height: "",
      calories: "",
      protein: "",
      carbs: "",
      fats: "",
      units: "",
      bmr: "",
      tdee: "",
      calTarget: "",
      proTarget: "",
      calDeducted: "",
      message: "",
      sending: false,
      calculated: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getFormControlClassName = this.getFormControlClassName.bind(this);
    this.renderInput = this.renderInput.bind(this);
    this.renderLabel = this.renderLabel.bind(this);
    window.scrollTo(0, 0);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  componentWillUnmount() {
    if (this.props.noBorder !== true) {
      if (document.querySelector(".mobile-nav-bar")) {
        document.querySelector(".mobile-nav-bar").style.display = "block";
      }
      if (document.querySelector(".trainer-support-button")) {
        document.querySelector(".trainer-support-button").style.display =
          "block";
      }
    }
  }

  componentDidMount() {
    if (document.querySelector(".mobile-nav-bar")) {
      document.querySelector(".mobile-nav-bar").style.display = "none";
    }
    if (document.querySelector(".trainer-support-button")) {
      document.querySelector(".trainer-support-button").style.display = "none";
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.state.gender === "female") {
      if (this.state.units === "metric") {
        var BMR =
          655 +
          9.6 * this.state.weight +
          1.8 * this.state.height -
          4.7 * this.state.age;
        var proTarget = 1.5 * this.state.weight;
      } else if (this.state.units === "imperial") {
        var BMR =
          655 +
          4.35 * this.state.weight +
          4.7 * this.state.height -
          4.7 * this.state.age;
        var proTarget = 1.5 * (this.state.weight / 2.2);
      }

      if (this.state.goal === "lose") {
        var tdee = BMR * this.state.activity;
        var difference = tdee * 0.15;
        var calTarget = tdee - difference;
        var calDeducted = calTarget - proTarget * 4;

        this.setState({
          bmr: BMR.toFixed(0),
          tdee: tdee.toFixed(0),
          calTarget: calTarget.toFixed(0),
          proTarget: proTarget.toFixed(0),
          calDeducted: calDeducted.toFixed(0),
        });
      } else if (this.state.goal === "gain") {
        var tdee = BMR * this.state.activity;
        var difference = tdee * 0.15;
        var calTarget = tdee + difference;
        var calDeducted = calTarget - proTarget * 4;

        this.setState({
          bmr: BMR.toFixed(0),
          tdee: tdee.toFixed(0),
          calTarget: calTarget.toFixed(0),
          proTarget: proTarget.toFixed(0),
          calDeducted: calDeducted.toFixed(0),
        });
      } else if (this.state.goal === "maintain") {
        var tdee = BMR * this.state.activity;
        //var difference = tdee * 0.15;
        var calTarget = tdee;
        console.log(tdee);
        var calDeducted = calTarget - proTarget * 4;

        this.setState({
          bmr: BMR.toFixed(0),
          tdee: tdee.toFixed(0),
          calTarget: calTarget.toFixed(0),
          proTarget: proTarget.toFixed(0),
          calDeducted: calDeducted.toFixed(0),
        });
      }
    } else if (this.state.gender === "male") {
      if (this.state.units === "metric") {
        var BMR =
          66 +
          13.7 * this.state.weight +
          5 * this.state.height -
          6.8 * this.state.age;
        var proTarget = 1.5 * this.state.weight;
      } else if (this.state.units === "imperial") {
        var BMR =
          66 +
          6.23 * this.state.weight +
          12.7 * this.state.height -
          6.8 * this.state.age;
        var proTarget = 1.5 * (this.state.weight / 2.2);
      }

      if (this.state.goal === "lose") {
        var tdee = BMR * this.state.activity;
        var difference = tdee * 0.15;
        var calTarget = tdee - difference;
        var calDeducted = calTarget - proTarget * 4;

        this.setState({
          bmr: BMR.toFixed(0),
          tdee: tdee.toFixed(0),
          calTarget: calTarget.toFixed(0),
          proTarget: proTarget.toFixed(0),
          calDeducted: calDeducted.toFixed(0),
        });
      } else if (this.state.goal === "gain") {
        var tdee = BMR * this.state.activity;
        var difference = tdee * 0.15;
        var calTarget = tdee + difference;
        var calDeducted = calTarget - proTarget * 4;

        this.setState({
          bmr: BMR.toFixed(0),
          tdee: tdee.toFixed(0),
          calTarget: calTarget.toFixed(0),
          proTarget: proTarget.toFixed(0),
          calDeducted: calDeducted.toFixed(0),
        });
      } else if (this.state.goal === "maintain") {
        var tdee = BMR * this.state.activity;
        //var difference = tdee * 0.15;
        var calTarget = tdee;
        console.log(tdee);
        var calDeducted = calTarget - proTarget * 4;

        this.setState({
          bmr: BMR.toFixed(0),
          tdee: tdee.toFixed(0),
          calTarget: calTarget.toFixed(0),
          proTarget: proTarget.toFixed(0),
          calDeducted: calDeducted.toFixed(0),
        });
      }
    }
    if (
      Math.round(proTarget) > 0 &&
      Math.round(calTarget) > 0 &&
      (proTarget && calTarget)
    ) {
      this.setState({ sending: true });
      const formData = new FormData();
      formData.append("protein", Math.round(proTarget));
      formData.append("calories", Math.round(calTarget));
      axiosAjax({
        method: "post",
        url: "/nutrition-tracker/macros/",
        headers: { "Content-Type": "multipart/form-data" },
        data: formData,
      })
        .then(response => {
          this.setState({
            sending: false,
            calculated: true,
          });
          this.props.onUpdateCaloriesProtein(
            response.data.protein,
            response.data.calories,
          );
          this.props.onGetStepFour();
          $("#scroll-top").animate({ scrollTop: 0 }, "fast");
        })
        .catch(error => {
          NotificationManager.error(
            "There was an error saving your macros.",
            "",
            5000,
          );
          this.setState({
            sending: false,
            calculated: false,
          });
        });
    } else {
      alert(
        "Please make sure that all of the fields are filled out. There is an error with the calculation.",
      );
    }
  }

  getFormControlClassName(isValid, isCustom: false) {
    return cn(
      { "is-valid": isValid },
      { "custom-select": isCustom },
      "form-control form-control-single-border calculator-form__section-description",
    );
  }

  renderInput(value, name, type, placeholder, step) {
    return (
      <input
        onChange={this.handleInputChange}
        value={value}
        step={step}
        name={name}
        type={type}
        id={name}
        className={this.getFormControlClassName(value > 0)}
        placeholder={placeholder}
        required
      />
    );
  }

  renderLabel(target, title) {
    return (
      <label className='calculator-form__section-title' htmlFor={target}>
        {title}
      </label>
    );
  }

  render() {
    return (
      <React.Fragment>
        {this.state.calculated === false ? (
          <div
            className={
              this.props.stepFour === true
                ? "home-step-container calculator-wrapper"
                : "max-width-500"
            }
          >
            <form
              className={
                this.props.noBorder === true
                  ? "calculator-form"
                  : "calculator-form mt-4 mb-5"
              }
              onSubmit={this.handleSubmit}
            >
              <div className='form-row'>
                <div className='col'>
                  <div className='form-group'>
                    {this.renderLabel("goal", "Your goal")}
                    <select
                      onChange={this.handleInputChange}
                      value={this.state.goal}
                      name='goal'
                      className={this.getFormControlClassName(
                        this.state.goal.length,
                        true,
                      )}
                      required
                    >
                      <option value='' disabled selected>
                        Select Goal
                      </option>
                      <option value='gain'>Build Muscle</option>
                      <option value='lose'>Lose Fat</option>
                      <option value='maintain'>Maintain</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className='form-row'>
                <div className='col'>
                  <div className='form-group'>
                    {this.renderLabel("units", "Units")}
                    <select
                      onChange={this.handleInputChange}
                      value={this.state.units}
                      name='units'
                      className={this.getFormControlClassName(
                        this.state.units.length,
                        true,
                      )}
                      required
                    >
                      <option value='' disabled selected>
                        Select Units
                      </option>
                      <option value='metric'>Metric (cm/kg)</option>
                      <option value='imperial'>Imperial (in/lbs)</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className='form-row'>
                <div className='col-6'>
                  <div className='form-group'>
                    {this.renderLabel("gender", "Gender")}
                    <select
                      onChange={this.handleInputChange}
                      value={this.state.gender}
                      name='gender'
                      className={this.getFormControlClassName(
                        this.state.gender.length,
                        true,
                      )}
                      required
                    >
                      <option value='' disabled selected>
                        Select Gender
                      </option>
                      <option value='female'>Female</option>
                      <option value='male'>Male</option>
                    </select>
                  </div>
                </div>
                <div className='col-6'>
                  <div className='form-group'>
                    {this.renderLabel("age", "Age")}
                    {this.renderInput(this.state.age, "age", "number", "Age")}
                  </div>
                </div>
              </div>
              <div className='form-row'>
                <div className='col-6'>
                  <div className='form-group'>
                    {this.renderLabel(
                      "weight",
                      `Weight
                       ${
                         this.state.units === "metric"
                           ? "in kilograms (kg)"
                           : ""
                       }
                       ${
                         this.state.units === "imperial"
                           ? "in pounds (lbs)"
                           : ""
                       }`,
                    )}
                    {this.renderInput(
                      this.state.weight,
                      "weight",
                      "number",
                      "Weight",
                      "any",
                    )}
                  </div>
                </div>
                <div className='col-6'>
                  <div className='form-group'>
                    {this.renderLabel(
                      "height",
                      `Height
                       ${
                         this.state.units === "metric"
                           ? "in centimeters (cm)"
                           : ""
                       }
                       ${
                         this.state.units === "imperial" ? "in inches (in)" : ""
                       }`,
                    )}
                    {this.renderInput(
                      this.state.height,
                      "height",
                      "number",
                      "Height",
                      "any",
                    )}
                  </div>
                </div>
              </div>
              <div className='form-row'>
                <div className='col'>
                  {this.renderLabel("activity", "Activity level")}
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.activity}
                    name='activity'
                    id='activity'
                    className={this.getFormControlClassName(
                      this.state.activity.length,
                      true,
                    )}
                    required
                  >
                    <option value='' disabled selected>
                      Select Activity Level
                    </option>
                    <option value='1.1'>
                      Sedentary (little or no exercise)
                    </option>
                    <option value='1.275'>
                      Lightly active (light exercise/sports 1-3 days/week)
                    </option>
                    <option value='1.35'>
                      Moderately active (moderate exercise/sports 3-5 days/week)
                    </option>
                    <option value='1.525'>
                      Very active (hard exercise/sports 6-7 days a week)
                    </option>
                  </select>
                </div>
              </div>
              {this.state.sending === false ? (
                this.props.stepFour === true ? (
                  <div
                    className='shadow-sm p-2 bg-white fixed-bottom'
                    style={{ background: "white" }}
                  >
                    <div className='container'>
                      <button className='btn btn-block btn-rounded btn-primary'>
                        Enter
                      </button>
                    </div>
                  </div>
                ) : (
                  <div className='shadow-sm p-2 bg-white pb-3 fixed-bottom'>
                    <div className='container'>
                      <button
                        id='formButton'
                        className='btn btn-block btn-primary btn-rounded'
                      >
                        Calculate
                      </button>
                    </div>
                  </div>
                )
              ) : null}
            </form>
          </div>
        ) : (
          <div className='max-width-500'>
            <div className='mt-4'>
              <h6>Calculated Requirements</h6>
              <div className='row'>
                <div className='col'>
                  <h6>
                    {this.state.bmr} <br />
                    <div className='text-muted' style={{ fontSize: 10 }}>
                      BMR
                    </div>
                  </h6>
                </div>
                <div className='col'>
                  <h6>
                    {this.state.tdee} <br />
                    <div className='text-muted' style={{ fontSize: 10 }}>
                      TDEE
                    </div>
                  </h6>
                </div>
              </div>
              <div className='row'>
                <div className='col'>
                  <h5 className='green m-0'>
                    {this.state.calTarget} <br />
                    <div className='text-muted' style={{ fontSize: 12 }}>
                      Calorie target
                    </div>
                  </h5>
                </div>
                <div className='col'>
                  <h5 className='green m-0'>
                    {this.state.proTarget} <br />
                    <div className='text-muted' style={{ fontSize: 12 }}>
                      Protein target (g)
                    </div>
                  </h5>
                </div>
              </div>

              <button
                className='btn btn-xs pl-0 btn-link'
                onClick={() =>
                  this.setState({ calculated: false, sending: false })
                }
              >
                <u>
                  <i className='fas fa-eraser' /> &nbsp; Recalculate Macros
                </u>
              </button>
            </div>
            <div className='mt-4'>
              <h6>Requirements Explained</h6>
              <p>
                Your calorie target is the amount you should aim to eat each
                day, however I strongly suggest you multiply this number by 7 to
                produce your weekly total. This means that you can fluctuate
                your calories to match your weekly lifestyle, like going out for
                meals at the weekend! Go over one day, come under the next.
              </p>
              <p>
                Your calorie target includes the calories from your protein
                target, so please do not consume your calorie target and then
                your protein target ontop. The amount of protein has been set at
                1.5g per kg. It's not the end of the world if you don't eat the
                full total but over time please do get as close to the target as
                you can and do not worry about going over.
              </p>
              <p>
                As we're adopting a flexible dieting approach here within The
                Academy so we don't get too hung up on the carbs/fats split, the
                two numbers that are most important for composition change are
                Calories & Protein.
              </p>
              <p>
                If you deduct protein from your calories you're left with{" "}
                {this.state.calDeducted} calories to get from carbs and fats,
                please be mindful to go a bit carb heavy on training days and
                dial them back a bit on rest days.
              </p>
              <p>
                Again, not hugely important, there you have your initial
                calories to trial for size.
              </p>
              <p>
                This is the Harris-Benedict formula for determining an{" "}
                <strong>estimate</strong> for your caloric requirements.
              </p>
              <p>
                You should always consult with a health professional before
                following any diet.
              </p>
              {this.props.stepFour === true && (
                <div
                  className='shadow-sm p-2 bg-white fixed-bottom'
                  style={{ background: "white" }}
                >
                  <div className='container'>
                    <a
                      href='/users/#/home/meal-planning'
                      className='btn btn-block btn-rounded btn-secondary'
                    >
                      Start Next Step
                    </a>
                  </div>
                </div>
              )}
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUpdateCaloriesProtein: (protein, calories) => {
      dispatch(updateCaloriesProtein(protein, calories));
    },
    onGetStepFour: () => {
      dispatch(getStepFour());
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(Calculator);
