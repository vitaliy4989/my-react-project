import React from "react";
import RecipeInfo from "../planner/RecipeInfo";

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recipeInfo: false,
    };
  }

  render() {
    return (
      <div className='item'>
        <div className='img-container'>
          {this.state.recipeInfo === true ? (
            <RecipeInfo recipeId={this.props.recipe.id} />
          ) : (
            <img src={this.props.recipe.photo + "-/autorotate/yes/"} />
          )}
        </div>
        <div class='item-body'>
          <h6 className='text-capitalize m-0'>{this.props.recipe.type_meal}</h6>
          <p className='p-smaller m-0'>{this.props.recipe.name}</p>
          <button
            className='btn p-0 btn-sm btn-link'
            onClick={() =>
              this.setState({ recipeInfo: !this.state.recipeInfo })
            }
          >
            {this.state.recipeInfo === true ? "Less info" : "More info"}
          </button>
        </div>
      </div>
    );
  }
}
