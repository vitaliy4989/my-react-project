import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import LightAlert from "../../shared/LightAlert";

class SavedMeals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      combinations: undefined,
      displayLimit: 2,
    };
  }

  componentDidMount() {
    axiosAjax.get("/recipes/combination/").then(response => {
      this.setState({
        combinations: response.data,
      });
    });
  }

  render() {
    return (
      <React.Fragment>
        <div className='mb-1 mt-4 d-flex justify-content-between align-items-center'>
          <h6 className='p-0 m-0 section-heading'>Meal Plans</h6>
          {this.props.protein ? (
            <NavLink to='/nutrition/recipes-combinations'>
              <button className='btn btn-rounded btn-sm  btn-primary'>
                New Plan
              </button>
            </NavLink>
          ) : null}
        </div>
        {this.state.combinations !== undefined ? (
          this.state.combinations.length > 0 &&
          Array.isArray(this.state.combinations) === true ? (
            <React.Fragment>
              {this.state.combinations
                .slice(0, this.state.displayLimit)
                .map((combination, key) => (
                  <NavLink
                    to={"/nutrition/planned-meals-saved/" + combination.id}
                    className='rounded-corners bg-white shadow-sm p-3 mt-3 d-flex justify-content-between align-items-center'
                    key={key}
                  >
                    {combination.name}
                    <span>
                      <i className='far fa-chevron-right' />
                    </span>
                  </NavLink>
                ))}
              {this.state.combinations.length > 2 &&
                this.state.combinations.length !== this.state.displayLimit && (
                  <div className='mt-1 d-flex justify-content-end align-items-center'>
                    <button
                      className='btn btn-sm btn-rounded btn-link'
                      onClick={() =>
                        this.setState({
                          displayLimit: this.state.combinations.length,
                        })
                      }
                    >
                      Show more
                    </button>
                  </div>
                )}
              {this.state.combinations.length > 2 &&
                this.state.displayLimit > 2 && (
                  <div className='mt-1 d-flex justify-content-end align-items-center'>
                    <button
                      className='btn btn-sm btn-rounded btn-link'
                      onClick={() => this.setState({ displayLimit: 2 })}
                    >
                      Show less
                    </button>
                  </div>
                )}
            </React.Fragment>
          ) : this.props.protein ? (
            <div className='rounded-corners bg-white shadow-sm p-3 mt-3 d-flex justify-content-between align-items-center'>
              <p className='m-0'>
                Create a meal plan by clicking New Plan above.
              </p>
            </div>
          ) : (
            <div className='rounded-corners bg-white shadow-sm p-3 mt-3 d-flex justify-content-between align-items-center'>
              <p className='m-0'>
                Please calculate your macros above to begin planning meals.
              </p>
            </div>
          )
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "5%", paddingBottom: "5%" }}
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    protein: state.nutrition.protein,
  };
};

export default connect(
  mapStateToProps,
  null,
)(SavedMeals);
