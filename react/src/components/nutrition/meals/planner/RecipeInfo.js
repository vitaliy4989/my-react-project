import React from "react";
import axiosAjax from "../../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";

export default class RecipeInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recipe: undefined,
    };
  }

  componentDidMount() {
    this._loadRecipe();
  }

  _loadRecipe() {
    axiosAjax
      .get("/recipes/recipe/" + this.props.recipeId)
      .then(response => {
        this.setState({
          recipe: response.data.recipe,
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div
        className='animated fadeIn'
        style={{
          background: "white",
          borderLeft: "#edf2f9 solid 2px",
          borderRight: "#edf2f9 solid 2px",
          padding: 15,
          height: "100%",
          overflow: "scroll",
        }}
      >
        {this.state.recipe !== undefined ? (
          <React.Fragment>
            <div>
              <label>Nutrition</label>
              <div className='row' style={{ marginBottom: ".5rem" }}>
                <div className='col-3'>
                  <p className='m-0 p-smaller'>{this.state.recipe.calories}</p>
                  <div className='text-muted' style={{ fontSize: 10 }}>
                    Calories
                  </div>
                </div>
                <div className='col-3'>
                  <p className='m-0 p-smaller'>{this.state.recipe.protein}g</p>
                  <div className='text-muted' style={{ fontSize: 10 }}>
                    Protein
                  </div>
                </div>
                <div className='col-3'>
                  <p className='m-0 p-smaller'>{this.state.recipe.carbs}g</p>
                  <div className='text-muted' style={{ fontSize: 10 }}>
                    Carbs
                  </div>
                </div>
                <div className='col-3'>
                  <p className='m-0 p-smaller'>{this.state.recipe.fat}g</p>
                  <div className='text-muted' style={{ fontSize: 10 }}>
                    Fat
                  </div>
                </div>
              </div>
              <div className='row'>
                <div className='col'>
                  <p className='p-smaller text-muted'>
                    Per {this.state.recipe.servings}{" "}
                    {this.state.recipe.servings === 1 ? "serving" : "servings"}
                  </p>
                </div>
              </div>
            </div>
            <label>Description</label>
            <p className='p-smaller' style={textareaStyle}>
              {this.state.recipe.description}
            </p>
            {this.state.recipe.ingredients !== null && (
              <React.Fragment>
                <label>Ingredients</label>
                <p className='p-smaller' style={textareaStyle}>
                  {this.state.recipe.ingredients}
                </p>
              </React.Fragment>
            )}
            {this.state.recipe.method !== null && (
              <React.Fragment>
                <label>Method</label>
                <p className='p-smaller' style={textareaStyle}>
                  {this.state.recipe.method}
                </p>
              </React.Fragment>
            )}
            <p className='m-0 p-smaller'>
              <strong>From</strong> {this.state.recipe.username}
            </p>
          </React.Fragment>
        ) : (
          <div className='h-100 d-flex flex-column justify-content-center'>
            <div className='text-center'>
              <MDSpinner singleColor='#4a4090' />
            </div>
          </div>
        )}
      </div>
    );
  }
}

const textareaStyle = {
  whiteSpace: "pre-wrap",
};
