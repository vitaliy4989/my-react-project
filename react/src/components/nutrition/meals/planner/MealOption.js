import React, { Component } from "react";
import axiosAjax from "../../../../config/axiosAjax";
import { connect } from "react-redux";
import { getDayMeals } from "../../../../redux/actions/meals";
import { NotificationManager } from "react-notifications";
import RecipeInfo from "./RecipeInfo";

class MealOption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionId: this.props.optionId ? this.props.optionId : undefined,
      type: "filter-by",
      recipes: [],
      counter: 0,
      locked: false,
      meal: this.props.meal ? this.props.meal : undefined,
      sending: false,
      servings: 1,
      updateServings: false,
      moreInfo: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.nextRecipe = this.nextRecipe.bind(this);
    this.backRecipe = this.backRecipe.bind(this);
    this._updateServings = this._updateServings.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.type !== this.state.type) {
      axiosAjax
        .get("/recipes/by-meal-approved/", {
          params: {
            type: this.state.type,
          },
        })
        .then(response => {
          this.setState({
            recipes: response.data,
          });
        });
    }
  }

  componentDidMount() {
    if (this.props.meal !== undefined) {
      axiosAjax
        .get("/recipes/by-meal-approved/", {
          params: {
            type: this.props.meal.recipe.type_meal,
          },
        })
        .then(response => {
          this.setState({
            recipes: response.data,
          });
          response.data.map(
            (recipe, key) =>
              recipe.id === this.props.meal.recipe.id &&
              this.setState({
                counter: key,
                locked: true,
                type: this.props.meal.recipe.type_meal,
                servings: this.props.meal.servings,
              }),
          );
        });
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  saveMeal(id) {
    const formData = new FormData();
    this.setState({ locked: true });
    formData.append("combination_id", this.props.combinationId);
    formData.append("recipe_id", id);
    axiosAjax({
      method: "post",
      url: "/recipes/combination-recipe/",
      data: formData,
    })
      .then(response => {
        this.setState({
          locked: true,
          optionId: response.data.id,
        });
        this.props.onGetDayMeals(this.props.combinationId);
      })
      .catch(error => {
        this.setState({ locked: false });
      });
  }

  deleteMeal(id) {
    const formData = new FormData();
    this.setState({ locked: false });
    formData.append("combination_id", this.props.combinationId);
    formData.append("recipe_id", id);
    axiosAjax({
      method: "delete",
      url: "/recipes/combination-recipe/" + this.state.optionId + "/",
      data: formData,
    })
      .then(response => {
        this.setState({
          locked: false,
          sending: false,
        });
        this.props.onGetDayMeals(this.props.combinationId);
      })
      .catch(error => {
        this.setState({ locked: true });
      });
  }

  _updateServings(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("combination_id", this.props.combinationId);
    formData.append("servings", this.state.servings);
    axiosAjax({
      method: "patch",
      url: "/recipes/combination-recipe/" + this.state.optionId + "/",
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Meal servings updated.");
        this.props.onGetDayMeals(this.props.combinationId);
      })
      .catch(error => {
        this.setState({ locked: false });
      });
  }

  nextRecipe() {
    if (this.state.counter !== this.state.recipes.length) {
      this.setState({
        counter: this.state.counter + 1,
        moreInfo: false,
      });
    }
  }

  backRecipe() {
    if (this.state.counter !== 0) {
      this.setState({
        counter: this.state.counter - 1,
        moreInfo: false,
      });
    }
  }

  _openLink(recipeLink) {
    if (recipeLink.indexOf("www.jamessmithacademy.com") !== -1) {
      var link = recipeLink.replace("https://www.jamessmithacademy.com", "");
      window.location.href =
        window.location.protocol + "//" + window.location.host + link;
    } else {
      window.open(recipeLink);
      return false;
    }
  }

  render() {
    return (
      <div
        className='col-lg-4 col-md-6'
        id={this.props.mealIndex + "mealOption"}
      >
        <div
          className='content-container'
          style={{
            marginBottom: 0,
            borderBottomLeftRadius: 0,
            borderBottomRightRadius: 0,
          }}
        >
          <div className='row'>
            {this.state.locked === false && (
              <div className='col-auto'>
                <button
                  onClick={() => this.props.removeMeal(this.props.mealIndex)}
                  className='btn btn-light'
                >
                  <i className='fas red fa-trash' />
                </button>
              </div>
            )}
            {this.state.locked === false ? (
              <div className='col'>
                <select
                  className='custom-select form-control-single-border'
                  onChange={this.handleInputChange}
                  value={this.state.type}
                  name='type'
                >
                  <option value='filter-by'>Meal type...</option>
                  <option value='snack'>Snack</option>
                  <option value='drink'>Drink</option>
                  <option value='treat'>Treat</option>
                  <option value='breakfast'>Breakfast</option>
                  <option value='lunch'>Lunch</option>
                  <option value='dinner'>Dinner</option>
                  <option value='box'>Lunch Box</option>
                  <option value='meal-prep'>Meal Prep</option>
                </select>
              </div>
            ) : (
              <div className='col'>
                <h6 className='text-center m-0 text-capitalize'>
                  {this.state.type}
                </h6>
              </div>
            )}
          </div>
          {this.state.type !== undefined && (
            <div className='row' style={{ marginTop: "1rem" }}>
              {this.state.locked === false && (
                <div className='col'>
                  {this.state.counter > 0 && (
                    <button
                      onClick={() => this.backRecipe()}
                      className='btn btn-rounded btn-light float-left '
                    >
                      <i className='fas fa-chevron-left' />
                    </button>
                  )}
                </div>
              )}

              <div className='col text-center'>
                {this.props.suggestedPlan !== true
                  ? this.state.recipes.length >= 1 && (
                      <React.Fragment>
                        {this.state.counter >= 0 &&
                        this.state.counter <= this.state.recipes.length - 1 &&
                        this.state.locked === true ? (
                          <button
                            onClick={() =>
                              this.deleteMeal(
                                this.state.recipes[this.state.counter].id,
                              )
                            }
                            className='btn btn-primary btn-rounded'
                          >
                            {this.state.sending === true ? (
                              <i className='fas fa-circle-notch fa-spin' />
                            ) : (
                              "Change meal"
                            )}
                          </button>
                        ) : (
                          <button
                            onClick={() =>
                              this.saveMeal(
                                this.state.recipes[this.state.counter].id,
                              )
                            }
                            className='btn btn-primary btn-rounded'
                          >
                            {this.state.sending === true ? (
                              <i className='fas fa-circle-notch fa-spin' />
                            ) : (
                              "Save meal"
                            )}
                          </button>
                        )}
                      </React.Fragment>
                    )
                  : null}
              </div>
              {this.state.locked === false && (
                <div className='col'>
                  {this.state.counter < this.state.recipes.length - 1 && (
                    <button
                      onClick={() => this.nextRecipe()}
                      className='btn btn-rounded btn-light float-right '
                    >
                      <i className='fas fa-chevron-right' />
                    </button>
                  )}
                </div>
              )}
            </div>
          )}
        </div>
        {this.state.recipes.length > 0 ? (
          this.state.counter >= 0 &&
          this.state.counter <= this.state.recipes.length - 1 && (
            <React.Fragment>
              <div style={{ height: 300 }}>
                {this.state.moreInfo === true ? (
                  <RecipeInfo
                    recipeId={this.state.recipes[this.state.counter].id}
                  />
                ) : (
                  <div style={{ width: "100%", overflow: "hidden" }}>
                    <img
                      src={
                        this.state.recipes[this.state.counter].photo +
                        "-/format/auto/-/quality/lightest/-/resize/500x/-/progressive/yes/"
                      }
                      className='img-fluid animated fadeIn'
                    />
                  </div>
                )}
              </div>
              <div
                className='content-container'
                style={{
                  borderTopLeftRadius: 0,
                  borderTopRightRadius: 0,
                  borderTop: "none",
                  marginTop: 0,
                }}
              >
                <div className='row'>
                  <div className='col text-center'>
                    <p className='p-0 m-0'>
                      {this.state.recipes[this.state.counter].name}
                    </p>
                    <button
                      className='btn btn-link'
                      onClick={() =>
                        this.setState({ moreInfo: !this.state.moreInfo })
                      }
                    >
                      {this.state.moreInfo === true
                        ? "Hide meal info"
                        : "Show meal info"}
                    </button>
                  </div>
                </div>
                <div className='row' style={{ marginTop: ".5rem" }}>
                  <div className='col text-center'>
                    <p className='p-0 m-0'>
                      {this.state.recipes[this.state.counter].calories}
                    </p>
                    <div className='text-muted' style={{ fontSize: 10 }}>
                      Calories
                    </div>
                  </div>
                  <div className='col text-center'>
                    <p className='p-0 m-0'>
                      {this.state.recipes[this.state.counter].protein}
                    </p>
                    <div className='text-muted' style={{ fontSize: 10 }}>
                      Protein
                    </div>
                  </div>
                </div>
                <div className='text-center'>
                  <button
                    type='button'
                    style={{ marginTop: "1rem" }}
                    onClick={() =>
                      this.setState({
                        updateServings: !this.state.updateServings,
                      })
                    }
                    className='btn btn-sm btn-rounded btn-light'
                  >
                    {this.state.updateServings === true
                      ? "Done"
                      : "Edit Servings"}
                  </button>
                </div>
                {this.state.updateServings === true ? (
                  this.state.locked === true ? (
                    <div
                      style={{ marginTop: ".5rem" }}
                      className='animated-2 fadeIn'
                    >
                      <p className='p-smaller'>
                        Macros for this recipe are shown for{" "}
                        {this.state.recipes[this.state.counter].servings}{" "}
                        servings size. If this is too much or little, update
                        meal servings below.
                      </p>
                      <form
                        className='form-row'
                        onSubmit={this._updateServings}
                      >
                        <div className='col-8'>
                          <input
                            className='form-control'
                            type='number'
                            step='0.00000001'
                            onChange={this.handleInputChange}
                            value={this.state.servings}
                            name='servings'
                          />
                        </div>
                        <div className='col-4'>
                          <button className='btn btn-block btn-light'>
                            Save
                          </button>
                        </div>
                      </form>
                    </div>
                  ) : (
                    <div
                      className='alert alert-light'
                      style={{ marginTop: "1rem" }}
                    >
                      Please first lock this meal in to change the servings.
                    </div>
                  )
                ) : null}
              </div>
            </React.Fragment>
          )
        ) : (
          <div />
        )}
      </div>
    );
  }
}

const clearButton = {
  backgroundColor: "transparent",
  border: "none",
};

const mapStateToProps = (state, ownProps) => {
  return {
    day: state.meals.day,
    meals: state.meals.meals,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetDayMeals: id => {
      dispatch(getDayMeals(id));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MealOption);
