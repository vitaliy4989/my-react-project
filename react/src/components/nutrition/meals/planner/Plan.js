import React, { Component } from "react";
import axiosAjax from "../../../../config/axiosAjax";
import MealOption from "./MealOption";
import { connect } from "react-redux";
import { getCaloriesProtein } from "../../../../redux/actions/nutrition";
import { getDayMeals } from "../../../../redux/actions/meals";
import MDSpinner from "react-md-spinner";
import MenuMobile from "../../../shared/MenuMobile";
import { NotificationManager } from "react-notifications";
import Heading from "../../../shared/Heading";
import HeadingMobile from "../../../shared/HeadingMobile";
import "../../styles.css";

class Plan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id ? this.props.id : undefined,
      name: "",
      numberOfMeals: 0,
      mealOptions: [],
      help: false,
      sending: false,
      updateName: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this._createCombination = this._createCombination.bind(this);
    this.removeMeal = this.removeMeal.bind(this);
    this._updateCombination = this._updateCombination.bind(this);
    window.scrollTo(0, 0);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _createCombination(event) {
    event.preventDefault();
    this.setState({ sending: true });
    const formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("protein", 0);
    formData.append("calories", 0);
    axiosAjax({
      method: "post",
      url: "/recipes/combination/",
      data: formData,
    })
      .then(response => {
        this.setState({
          id: response.data.id,
          sending: false,
        });
        window.location.href =
          window.location.protocol +
          "//" +
          window.location.host +
          "/users/#/nutrition/planned-meals-saved/" +
          response.data.id;
      })
      .catch(function(error) {
        console.log(error);
        this.setState({ sending: false });
      });
  }

  _deleteCombination() {
    NotificationManager.warning(
      "Click me to confirm the delete.",
      "",
      5000,
      () => {
        axiosAjax
          .delete("/recipes/combination/" + this.state.id + "/")
          .then(response => {
            window.location.href = "/users/#/nutrition";
          });
      },
    );
  }

  removeMeal(element) {
    delete this.state.mealOptions[element];
    this.setState({
      mealOptions: this.state.mealOptions,
    });
  }

  _updateCombination(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("name", this.state.name);
    axiosAjax({
      method: "patch",
      url: "/recipes/combination/" + this.state.id + "/",
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Day name updated.");
        this.setState({ updateName: false });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  addMealOption() {
    this.state.mealOptions.push(
      <MealOption
        key={this.state.numberOfMeals}
        mealIndex={this.state.numberOfMeals}
        removeMeal={this.removeMeal}
        mealNumber={this.state.numberOfMeals + 1}
        combinationId={this.state.id}
      />,
    );
    this.setState({
      numberOfMeals: this.state.numberOfMeals + 1,
    });
    NotificationManager.success("Meal added below.", "", 5000);
  }

  componentDidMount() {
    if (this.props.protein === undefined && this.props.calories === undefined) {
      this.props.onGetCaloriesProtein();
    }
    if (this.props.id) {
      this.props.onGetDayMeals(this.props.id);
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.id && this.props.dayId) {
      if (this.state.id !== this.props.dayId) {
        this.setState({
          id: this.props.dayId,
          name: this.props.day.name,
          numberOfMeals: this.props.meals.length,
        });
        this.props.meals.map((meal, key) =>
          this.state.mealOptions.push(
            <MealOption
              key={key}
              meal={meal}
              optionId={meal.id}
              mealIndex={key}
              removeMeal={this.removeMeal}
              mealNumber={key + 1}
              combinationId={this.state.id}
            />,
          ),
        );
      }
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.state.id !== undefined ? (
          <React.Fragment>
            <MenuMobile
              back={true}
              linkRightTwo={
                <a
                  href={"/recipes/combinations-pdf/" + this.state.id}
                  className='nav-link'
                >
                  <div className='white'>Print</div>
                </a>
              }
              linkRightOne={
                <a
                  onClick={() => this._deleteCombination()}
                  className='nav-link'
                >
                  <div className='white'>Delete</div>
                </a>
              }
              linkRightThree={
                <a onClick={() => this.addMealOption()} className='nav-link'>
                  <div className='white second-nav-bold'>New Meal</div>
                </a>
              }
            />
            <div className='scroll-view grey-background'>
              {this.props.meals !== undefined ? (
                <React.Fragment>
                  <div className='meal-planner'>
                    <div className='container'>
                      <div className='mb-1 mt-4 pb-2'>
                        <div className='for-desktop'>
                          <div className='row mb-4'>
                            <div className='col text-center'>
                              <a
                                href={
                                  "/recipes/combinations-pdf/" + this.state.id
                                }
                                className='waves-effect btn btn-rounded btn-white-menu'
                              >
                                <i className='fas fa-print' />
                              </a>
                            </div>
                            <div className='col text-center'>
                              <button
                                type='button'
                                className='waves-effect btn btn-rounded btn-white-menu'
                                onClick={() => this.addMealOption()}
                              >
                                <i className='fas fa-plus' /> New Meal
                              </button>
                            </div>
                            <div className='col text-center'>
                              <button
                                onClick={() => this._deleteCombination()}
                                className='waves-effect btn btn-rounded btn-white-menu'
                              >
                                <i className='fas red fa-trash' />
                              </button>
                            </div>
                          </div>
                        </div>

                        <div className='row'>
                          <div className='col'>
                            {this.props.protein !== undefined ? (
                              this.props.protein !== 0 ? (
                                <div className='row'>
                                  <div className='col text-center'>
                                    <h5 className='m-0 macro-value'>
                                      {Math.round(
                                        this.props.protein -
                                          (this.props.dayProtein !== undefined
                                            ? this.props.dayProtein
                                            : 0),
                                      )}
                                      g{" "}
                                      <span className='p-xs'>
                                        / {this.props.protein}g
                                      </span>
                                    </h5>
                                    <div className='white font-weight-bold p-xs'>
                                      <i className='fal fa-steak' /> Protein
                                    </div>
                                  </div>
                                  <div className='col text-center'>
                                    <h5 className='m-0 macro-value'>
                                      {Math.round(
                                        this.props.calories -
                                          (this.props.dayCalories !== undefined
                                            ? this.props.dayCalories
                                            : 0),
                                      )}{" "}
                                      <span className='p-xs'>
                                        / {this.props.calories}
                                      </span>
                                    </h5>
                                    <div className='white font-weight-bold p-xs'>
                                      <i className='fal fa-fire' /> Calories
                                    </div>
                                  </div>
                                </div>
                              ) : (
                                <div className='row no-gutters'>
                                  <div className='col'>
                                    <div
                                      className='content-container-dark-back text-center'
                                      style={{
                                        marginBottom: 0,
                                        borderBottomLeftRadius: 0,
                                        borderBottomRightRadius: 0,
                                        borderTopRightRadius: 0,
                                        borderBottom: "none",
                                      }}
                                    >
                                      Please first use the calculator{" "}
                                      <a href='/users/#/nutrition/calculator'>
                                        here
                                      </a>{" "}
                                      to determine your nutritional requirements
                                      before planning your meals.
                                    </div>
                                  </div>
                                </div>
                              )
                            ) : (
                              <div
                                className='text-center'
                                style={{
                                  paddingTop: "10%",
                                  paddingBottom: "10%",
                                }}
                              >
                                <MDSpinner singleColor='#4a4090' />
                              </div>
                            )}
                          </div>
                        </div>
                        <div className='row mt-3'>
                          <div className='col text-center'>
                            {this.state.updateName === true ? (
                              <form
                                onSubmit={this._updateCombination}
                                className='mb-3'
                              >
                                <div className='form-row'>
                                  <div className='col d-flex flex-column justify-content-center'>
                                    <input
                                      autoFocus='autofocus'
                                      autoComplete='off'
                                      name='name'
                                      onChange={this.handleInputChange}
                                      value={this.state.name}
                                      style={{ backgroundColor: "white" }}
                                      className='form-control form-control-rounded'
                                      placeholder='Name'
                                      required
                                    />
                                  </div>
                                  <div className='col-auto d-flex flex-column justify-content-center'>
                                    <button className='btn btn-rounded btn-white-menu'>
                                      <i className='fas dark-purp fa-check' />
                                    </button>
                                  </div>
                                </div>
                              </form>
                            ) : (
                              <h6 className='white m-0'>
                                {this.state.name} &nbsp;{" "}
                                <i
                                  style={{ cursor: "pointer" }}
                                  onClick={() =>
                                    this.setState({
                                      updateName: !this.state.updateName,
                                    })
                                  }
                                  className='fal fa-xs white fa-pen'
                                />
                              </h6>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='container' id='mealOptions'>
                    <div className='row' style={{ marginTop: "2rem" }}>
                      {this.state.mealOptions}
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                <div className='h-100 d-flex flex-column justify-content-center'>
                  <div className='text-center'>
                    <MDSpinner singleColor='#4a4090' />
                  </div>
                </div>
              )}
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <MenuMobile back={true} />
            <Heading
              columns={
                <div className='col'>
                  <h4>New Plan</h4>
                </div>
              }
            />
            <HeadingMobile
              columns={
                <div className='col'>
                  <h4>New Plan</h4>
                </div>
              }
            />
            <div className='scroll-view grey-background'>
              <div className='container'>
                <div className='content-container max-width-500'>
                  <h6 className='text-center'>Enter meal plan name</h6>
                  <form onSubmit={this._createCombination}>
                    <div className='form-row'>
                      <div className='col-12'>
                        <input
                          autoComplete='off'
                          type='text'
                          name='name'
                          onChange={this.handleInputChange}
                          value={this.state.name}
                          className='form-control form-control-single-border text-center'
                          placeholder='Enter plan name, i.e. Monday'
                          required
                        />
                      </div>
                    </div>
                    <div className='form-row'>
                      <div className='col-12 text-center'>
                        <button className='btn btn-rounded btn-primary enter-button-centered'>
                          {this.state.sending === true ? (
                            <i className='fas fa-circle-notch fa-spin' />
                          ) : (
                            "Enter"
                          )}
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const inputStyle = {
  background: "#4a4090",
  color: "white",
  border: "none",
  fontSize: "2rem",
  lineHeight: "2.5rem",
  letterSpacing: ".0125rem",
  fontWeight: 700,
  width: "80%",
  boxShadow: "none",
  padding: 5,
  paddingLeft: 0,
  outline: "none",
};

const mapStateToProps = (state, ownProps) => {
  return {
    protein: state.nutrition.protein,
    calories: state.nutrition.calories,
    day: state.meals.day,
    dayId: state.meals.dayId,
    dayProtein: state.meals.dayProtein,
    dayCalories: state.meals.dayCalories,
    meals: state.meals.meals,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetCaloriesProtein: () => {
      dispatch(getCaloriesProtein());
    },
    onGetDayMeals: id => {
      dispatch(getDayMeals(id));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Plan);
