import React from "react";
import axiosAjax from "../../../../config/axiosAjax";
import SearchCombination from "../SearchCombination/index";
import { connect } from "react-redux";
import { getCaloriesProtein } from "../../../../redux/actions/nutrition";
import LightAlert from "../../../shared/LightAlert";

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      meals: 3,
      protein: "",
      calories: "",
      combination: [],
      searching: false,
      startSearch: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.onGetCaloriesProtein();
    this.setState({
      protein: this.props.protein,
      calories: this.props.calories,
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.protein !== this.props.protein &&
      prevProps.calories !== this.props.calories
    ) {
      this.setState({
        protein: this.props.protein,
        calories: this.props.calories,
      });
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ startSearch: true, searching: true });
    axiosAjax
      .get("/recipes/combination-search/", {
        params: {
          meals: this.state.meals,
          protein: this.state.protein,
          calories: this.state.calories,
        },
      })
      .then(response => {
        this.setState({
          combination: response.data,
          searching: false,
        });
      })
      .catch(error => {
        this.setState({
          searching: false,
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <div className='container'>
          <div className='content-container'>
            <form onSubmit={this.handleSubmit}>
              <div className='form-row'>
                <div className='col-lg-4 col-md-4'>
                  <label>Meals</label>
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.meals}
                    name='meals'
                    className='custom-select form-control-single-border text-center'
                    required
                  >
                    <option value='3'>Breakfast, Lunch, Dinner</option>
                    <option value='4'>Breakfast, Lunch, Snack, Dinner</option>
                  </select>
                </div>
                <div className='col-lg-4 col-md-4'>
                  <label>Protein target</label>
                  <input
                    type='number'
                    onChange={this.handleInputChange}
                    step='0.00000001'
                    value={this.state.protein}
                    name='protein'
                    className='form-control form-control-single-border'
                    required
                  />
                </div>
                <div className='col-lg-4 col-md-4'>
                  <label>Calorie target</label>
                  <input
                    type='number'
                    onChange={this.handleInputChange}
                    step='0.00000001'
                    value={this.state.calories}
                    name='calories'
                    className='form-control form-control-single-border'
                    required
                  />
                </div>
              </div>
              <p className='p-smaller text-muted m-1'>
                * Randomly generated meal plans may be below or above your
                macros. Please adjust plans after saving them.
              </p>
              <div className='form-row' style={{ marginTop: "1rem" }}>
                <div className='col text-center'>
                  {this.state.searching === true ? (
                    <div style={{ margin: "1rem" }}>
                      <i className='fas fa-spinner fa-lg dark-purp fa-spin' />
                      <p className='m-1 p-smaller'>Searching, one moment...</p>
                    </div>
                  ) : (
                    <button
                      type='submit'
                      className='btn btn-rounded btn-primary enter-button-centered'
                    >
                      Search &nbsp; <i className='far fa-search' />
                    </button>
                  )}
                  {this.state.startSearch === true && (
                    <div style={{ marginTop: "1rem" }}>
                      <p>
                        <i className='fas fa-arrow-up' />
                      </p>
                      <p>Click search again for a different batch.</p>
                    </div>
                  )}
                </div>
              </div>
            </form>
          </div>
        </div>
        {this.state.startSearch === true ? (
          this.state.searching === false ? (
            this.state.combination.length >= 1 ? (
              this.state.combination.map((combination, key) => (
                <SearchCombination
                  key={key}
                  recipes={combination.recipes}
                  combinationId={key}
                  combinationCal={combination.calories}
                  combinationPro={combination.protein}
                />
              ))
            ) : (
              <div className='text-center' style={{ marginTop: 30 }}>
                <LightAlert
                  container={true}
                  message={
                    <div>
                      No results...please change your input. You may also{" "}
                      <a href='/users/#/nutrition/meal-planner'>
                        <u>create a plan from scratch</u>
                      </a>
                      .
                    </div>
                  }
                />
              </div>
            )
          ) : (
            <div className='text-center' style={{ marginTop: 30 }}>
              <LightAlert container={true} message='Searching...' />
            </div>
          )
        ) : null}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    protein: state.nutrition.protein,
    calories: state.nutrition.calories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetCaloriesProtein: () => {
      dispatch(getCaloriesProtein());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Search);
