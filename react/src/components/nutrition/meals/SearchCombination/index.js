import React from "react";
import Recipe from "../../../recipes/Recipe";
import axiosAjax from "../../../../config/axiosAjax";
import SearchCombinationRecipe from "../SearchCombinationRecipe/index";

export default class SearchCombination extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      saved: false,
      recipeIds: "",
      viewMeals: false,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.recipes.toString() !== this.props.recipes.toString()) {
      this.setState({
        saved: false,
        viewMeals: false,
      });
    }
  }

  componentDidMount() {
    var recipesIds = [];
    this.props.recipes.map((recipe, key) => recipesIds.push(recipe.id));
    this.setState({ recipeIds: recipesIds.toString() });
  }

  _savePlan() {
    this.setState({ saved: true });
    const formData = new FormData();
    formData.append("name", "Generated Meal Plan");
    formData.append("recipe_ids", this.state.recipeIds);
    formData.append("protein", this.props.combinationPro);
    formData.append("calories", this.props.combinationCal);
    axiosAjax({
      method: "post",
      url: "/recipes/save-generated-combination/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({ saved: true });
      })
      .catch(error => {
        this.setState({ saved: false });
        console.log(error);
      });
  }

  render() {
    return (
      <div className='container search-combination'>
        <div className='content-container'>
          <div className='row text-center'>
            <div className='col'>
              <h6 className='m-0 green'>{this.props.combinationCal}</h6>
              <div className='text-muted' style={{ fontSize: 10 }}>
                Calories
              </div>
            </div>
            <div className='col'>
              <h6 className='m-0 green'>{this.props.combinationPro}</h6>
              <div className='text-muted' style={{ fontSize: 10 }}>
                Protein
              </div>
            </div>
          </div>
          <div className='scrollmenu'>
            {this.props.recipes.map((recipe, key) => (
              <SearchCombinationRecipe
                key={key + "recipeCombination" + recipe.id}
                recipe={recipe}
              />
            ))}
          </div>
          <div className='row' style={{ marginTop: "1rem" }}>
            <div className='col text-center'>
              {this.state.saved === true ? (
                <button className='btn btn-primary btn-rounded'>Saved</button>
              ) : (
                <button
                  className='btn btn-primary btn-rounded'
                  onClick={() => this._savePlan()}
                >
                  Save to meal plans
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
