import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { connect } from "react-redux";
import { getDay, getMeals } from "../../../redux/actions/nutrition";

class CreateMeal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      added: false,
    };
  }

  addMealToDay() {
    const formData = new FormData();
    this.setState({ added: true });
    formData.append("day_id", this.props.dayId);
    if (this.props.recipeId) {
      formData.append("recipe_id", this.props.recipeId);
    } else if (this.props.foodId) {
      formData.append("food_id", this.props.foodId);
    }
    formData.append("name", this.props.name);
    formData.append("protein", this.props.protein);
    formData.append("calories", this.props.calories);
    axiosAjax({
      method: "post",
      url: "/nutrition-tracker/food-log-meal/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.setState({
          added: true,
        });
        this.props.onGetDay(this.props.dayId);
        this.props.onGetMeals(this.props.dayId);
      })
      .catch(error => {
        console.log(error);
        this.setState({
          added: false,
        });
      });
  }

  render() {
    return this.props.foodResult === true ? (
      this.state.added === true ? (
        <div style={{ marginTop: "1rem" }}>
          <p className='green'>Added! You can edit this food on the log. </p>
          <button
            onClick={() => this.addMealToDay()}
            className='btn btn-secondary'
          >
            Add again?
          </button>
        </div>
      ) : (
        <button
          onClick={() => this.addMealToDay()}
          className='btn btn-secondary'
        >
          Add to log
        </button>
      )
    ) : this.state.added === true ? (
      <button className='btn float-right btn-round btn-active animated pulse'>
        <i className='fas fa-check white' />
      </button>
    ) : (
      <button
        onClick={() => this.addMealToDay()}
        className='btn btn-light float-right btn-round'
      >
        <i className='fas fa-plus' />
      </button>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    day: state.nutrition.day,
    meals: state.nutrition.meals,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetDay: dayId => {
      dispatch(getDay(dayId));
    },
    onGetMeals: dayId => {
      dispatch(getMeals(dayId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateMeal);
