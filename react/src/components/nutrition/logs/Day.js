import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { connect } from "react-redux";
import {
  getDay,
  getMeals,
  getDays,
  getCaloriesProtein,
} from "../../../redux/actions/nutrition";
import Meal from "./Meal";
import Search from "../../foods/Search";
import MDSpinner from "react-md-spinner";
import MenuMobile from "../../../components/shared/MenuMobile";
import { NavLink } from "react-router-dom";
import { NotificationManager } from "react-notifications";
import Heading from "../../shared/Heading";
import HeadingMobile from "../../shared/HeadingMobile";

class Day extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: false,
      addFood: false,
    };
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    this.props.onGetDay(this.props.id);
    this.props.onGetMeals(this.props.id);
    if (this.props.protein === undefined && this.props.calories === undefined) {
      this.props.onGetCaloriesProtein();
    }
  }

  _toggleModal() {
    $("#selectFoodModal").modal("toggle");
  }

  render() {
    return (
      <React.Fragment>
        <MenuMobile
          back={true}
          linkRightOne={
            <a className='nav-link' onClick={() => this._toggleModal()}>
              <div className='white second-nav-bold'>Add Food</div>
            </a>
          }
        />

        {this.props.day !== undefined && this.props.meals !== undefined ? (
          <React.Fragment>
            <div className='scroll-view'>
              <Heading
                columns={
                  <div className='col-12'>
                    <div className='row'>
                      <div className='col d-flex flex-column justify-content-center'>
                        <h4 className='m-0'>{this.props.day.name}</h4>
                        <p className='m-0 p-smaller'>
                          {this.props.day.date.substring(
                            0,
                            this.props.day.date.indexOf("T"),
                          )}
                        </p>
                      </div>
                      <div className='col-auto for-desktop'>
                        <button
                          type='button'
                          className='btn btn-rounded btn-white-menu'
                          data-toggle='modal'
                          data-target='#selectFoodModal'
                        >
                          <i className='fas fa-plus' /> Add Food
                        </button>
                      </div>
                    </div>
                    <hr />
                    <div className='row'>
                      <div className='col'>
                        {isNaN(this.props.day.remaining_protein) === false && (
                          <React.Fragment>
                            <h6 className='p-0 m-0 white'>
                              {Math.round(this.props.day.remaining_protein)} g
                            </h6>
                            <div style={{ fontSize: 10 }} className='white'>
                              Remaining Protein
                            </div>
                            <div className='progress'>
                              <div
                                className='progress-bar'
                                role='progressbar'
                                style={{
                                  width:
                                    (Math.round(
                                      this.props.day.remaining_protein,
                                    ) /
                                      this.props.protein) *
                                      100 >
                                    0
                                      ? String(
                                          (Math.round(
                                            this.props.day.remaining_protein,
                                          ) /
                                            this.props.protein) *
                                            100 +
                                            "%",
                                        )
                                      : "0%",
                                }}
                                aria-valuemin='0'
                                aria-valuemax='100'
                              />
                            </div>
                          </React.Fragment>
                        )}
                      </div>
                      <div className='col'>
                        {isNaN(this.props.day.remaining_calories) === false && (
                          <React.Fragment>
                            <h6 className='p-0 m-0 white'>
                              {Math.round(this.props.day.remaining_calories)}
                            </h6>
                            <div style={{ fontSize: 10 }} className='white'>
                              Remaining Calories
                            </div>
                            <div className='progress'>
                              <div
                                className='progress-bar'
                                role='progressbar'
                                style={{
                                  width:
                                    (Math.round(
                                      this.props.day.remaining_calories,
                                    ) /
                                      this.props.calories) *
                                      100 >
                                    0
                                      ? String(
                                          (Math.round(
                                            this.props.day.remaining_calories,
                                          ) /
                                            this.props.calories) *
                                            100 +
                                            "%",
                                        )
                                      : "0%",
                                }}
                                aria-valuemin='0'
                                aria-valuemax='100'
                              />
                            </div>
                          </React.Fragment>
                        )}
                      </div>
                    </div>
                  </div>
                }
              />

              <HeadingMobile
                columns={
                  <div className='col-12'>
                    <div className='row'>
                      <div className='col d-flex flex-column justify-content-center'>
                        <h4 className='m-0'>{this.props.day.name}</h4>
                        <p className='m-0 p-smaller'>
                          {this.props.day.date.substring(
                            0,
                            this.props.day.date.indexOf("T"),
                          )}
                        </p>
                      </div>
                      <div className='col-auto for-desktop'>
                        <button
                          type='button'
                          className='btn btn-rounded btn-white-menu'
                          data-toggle='modal'
                          data-target='#selectFoodModal'
                        >
                          <i className='fas fa-plus' /> Add Food
                        </button>
                      </div>
                      <div className='col-auto for-desktop'>
                        <button
                          className='waves-effect btn btn-rounded btn-white-menu'
                          onClick={() => this._deleteDay()}
                        >
                          <i className='fas red fa-trash' />
                        </button>
                      </div>
                    </div>
                    <hr />
                    <div className='row'>
                      <div className='col'>
                        {isNaN(this.props.day.remaining_protein) === false && (
                          <React.Fragment>
                            <h6 className='p-0 m-0 white'>
                              {Math.round(this.props.day.remaining_protein)} g
                            </h6>
                            <div style={{ fontSize: 10 }} className='white'>
                              Remaining Protein
                            </div>
                            <div className='progress'>
                              <div
                                className='progress-bar'
                                role='progressbar'
                                style={{
                                  width: String(
                                    (Math.round(
                                      this.props.day.remaining_protein,
                                    ) /
                                      this.props.protein) *
                                      100 +
                                      "%",
                                  ),
                                }}
                                aria-valuemin='0'
                                aria-valuemax='100'
                              />
                            </div>
                          </React.Fragment>
                        )}
                      </div>
                      <div className='col'>
                        {isNaN(this.props.day.remaining_calories) === false && (
                          <React.Fragment>
                            <h6 className='p-0 m-0 white'>
                              {Math.round(this.props.day.remaining_calories)}
                            </h6>
                            <div style={{ fontSize: 10 }} className='white'>
                              Remaining Calories
                            </div>
                            <div className='progress'>
                              <div
                                className='progress-bar'
                                role='progressbar'
                                style={{
                                  width: String(
                                    (Math.round(
                                      this.props.day.remaining_calories,
                                    ) /
                                      this.props.calories) *
                                      100 +
                                      "%",
                                  ),
                                }}
                                aria-valuemin='0'
                                aria-valuemax='100'
                              />
                            </div>
                          </React.Fragment>
                        )}
                      </div>
                    </div>
                  </div>
                }
              />

              <div className='container'>
                {this.props.meals.length > 0 &&
                  this.props.meals.map((meal, key) => (
                    <Meal key={key} meal={meal} dayId={this.props.id} />
                  ))}
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div className='h-100 d-flex flex-column justify-content-center'>
            <div className='text-center'>
              <MDSpinner singleColor='#4a4090' />
            </div>
          </div>
        )}
        <Search id={this.props.id} />
      </React.Fragment>
    );
  }
}

const deleteMeal = {
  cursor: "pointer",
  fontSize: 8,
};

const mapStateToProps = (state, ownProps) => {
  return {
    day: state.nutrition.day,
    meals: state.nutrition.meals,
    protein: state.nutrition.protein,
    calories: state.nutrition.calories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetDay: dayId => {
      dispatch(getDay(dayId));
    },
    onGetDays: () => {
      dispatch(getDays());
    },
    onGetMeals: dayId => {
      dispatch(getMeals(dayId));
    },
    onGetCaloriesProtein: () => {
      dispatch(getCaloriesProtein());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Day);
