import React, { Component } from "react";
import { connect } from "react-redux";
import { getDays, getCaloriesProtein } from "../../../redux/actions/nutrition";
import { NavLink } from "react-router-dom";
import axiosAjax from "../../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import CreateDay from "./CreateDay";
import LightAlert from "../../shared/LightAlert";
import { NotificationManager } from "react-notifications";

class Days extends Component {
  constructor(props) {
    super(props);
    this.state = {
      create: false,
      displayLimit: 2,
    };
    this._closeCreateDay = this._closeCreateDay.bind(this);
  }

  componentDidMount() {
    this.props.onGetDays();
    this.props.onGetCaloriesProtein();
  }

  _deleteLog(id) {
    if (confirm("Click OK to delete log.")) {
      axiosAjax
        .delete("/nutrition-tracker/food-log-day/" + id + "/")
        .then(response => {
          this.props.onGetDays();
        });
    }
  }

  _cloneLog(id) {
    NotificationManager.success("Log cloned.", "", 5000);
    const formData = new FormData();
    formData.append("log_id", id);
    axiosAjax({
      method: "post",
      url: "/nutrition-tracker/clone-food-log-day/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetDays();
      })
      .catch(error => {
        NotificationManager.error("There has been an error", "", 5000);
      });
  }

  _closeCreateDay() {
    this.setState({
      create: !this.state.create,
      displayLimit: 2,
    });
  }

  render() {
    return (
      <React.Fragment>
        <div className='mb-1 mt-4 d-flex justify-content-between align-items-center'>
          <h6 className='m-0 section-heading'>Food Logs</h6>
          {this.props.protein ? (
            <span>
              <button
                onClick={() => this.setState({ create: !this.state.create })}
                className='btn btn-rounded btn-primary btn-sm'
              >
                {this.state.create === true ? "Done" : "New Log"}
              </button>
            </span>
          ) : null}
        </div>
        {this.state.create === true && (
          <CreateDay closeCreateDay={this._closeCreateDay} />
        )}
        {this.props.days !== undefined ? (
          this.props.days.length > 0 &&
          Array.isArray(this.props.days) === true ? (
            <React.Fragment>
              {this.props.days
                .slice(0, this.state.displayLimit)
                .map((day, key) => (
                  <div
                    className='rounded-corners bg-white shadow-sm p-3 mt-3'
                    key={key}
                  >
                    <div className='row d-flex align-items-center'>
                      <NavLink
                        to={"/nutrition/logging/day/" + day.id}
                        className='col'
                      >
                        {day.name}
                        <p className='p-smaller text-muted m-0'>
                          {day.date.substring(0, day.date.indexOf("T"))}
                        </p>
                      </NavLink>
                      <div className='col-auto'>
                        <div className='dropdown float-right'>
                          <button
                            className='btn btn-light btn-sm'
                            type='button'
                            id={"logOptions" + day.id}
                            data-toggle='dropdown'
                            aria-haspopup='true'
                            aria-expanded='false'
                          >
                            <i className='far fa-ellipsis-v' />
                          </button>
                          <div
                            className='dropdown-menu dropdown-menu-right'
                            aria-labelledby={"logOptions" + day.id}
                          >
                            <button
                              onClick={() => this._cloneLog(day.id)}
                              className='dropdown-item'
                            >
                              <i className='far fa-clone' /> &nbsp; Clone Log
                            </button>
                            <div className='dropdown-divider' />
                            <button
                              onClick={() => this._deleteLog(day.id)}
                              className='dropdown-item'
                            >
                              <i className='far red fa-times' /> &nbsp; Delete
                              Log
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              {this.props.days.length > 2 &&
                this.props.days.length !== this.state.displayLimit && (
                  <div className='mt-1 d-flex justify-content-end align-items-center'>
                    <button
                      className='btn btn-sm btn-rounded btn-link'
                      onClick={() =>
                        this.setState({ displayLimit: this.props.days.length })
                      }
                    >
                      Show more
                    </button>
                  </div>
                )}
              {this.props.days.length > 2 && this.state.displayLimit > 2 && (
                <div className='mt-1 d-flex justify-content-end align-items-center'>
                  <button
                    className='btn btn-sm btn-rounded btn-link'
                    onClick={() => this.setState({ displayLimit: 2 })}
                  >
                    Show less
                  </button>
                </div>
              )}
            </React.Fragment>
          ) : this.props.protein ? (
            <div className='rounded-corners bg-white shadow-sm p-3 mt-3 d-flex justify-content-between align-items-center'>
              <p className='m-0'>
                Click New Log to begin logging food for the day.
              </p>
            </div>
          ) : (
            <div className='rounded-corners bg-white shadow-sm p-3 mt-3 d-flex justify-content-between align-items-center'>
              <p className='m-0'>
                Please calculate your macros above to begin logging food.
              </p>
            </div>
          )
        ) : (
          <div
            className='text-center'
            style={{ paddingTop: "5%", paddingBottom: "5%" }}
          >
            <MDSpinner singleColor='#4a4090' />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    days: state.nutrition.days,
    protein: state.nutrition.protein,
    calories: state.nutrition.calories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetDays: () => {
      dispatch(getDays());
    },
    onGetCaloriesProtein: () => {
      dispatch(getCaloriesProtein());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Days);
