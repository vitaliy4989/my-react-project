import React, { Component } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { getDays, getCaloriesProtein } from "../../../redux/actions/nutrition";

class CreateDay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      create: false,
      sending: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createDay = this.createDay.bind(this);
  }

  componentDidMount() {
    this.props.onGetCaloriesProtein();
    this._getCurrentDayName();
  }

  _getCurrentDayName() {
    var a = new Date();
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";
    var currentDay = weekdays[a.getDay()];
    this.setState({
      name: currentDay,
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  createDay(event) {
    event.preventDefault();
    this.setState({ sending: true });
    const formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("remaining_protein", this.props.protein);
    formData.append("remaining_calories", this.props.calories);
    axiosAjax({
      method: "post",
      url: "/nutrition-tracker/food-log-day/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetDays();
        this.setState({
          name: "",
          sending: false,
        });
        this.props.closeCreateDay();
      })
      .catch(error => {
        console.log(error);
        this.setState({ sending: false });
      });
  }

  render() {
    return (
      <div className='rounded-corners bg-white shadow-sm p-3 mt-3'>
        {this.props.protein > 0 && this.props.calories ? (
          <form onSubmit={this.createDay}>
            <div className='form-row'>
              <div className='col-8'>
                <input
                  autoComplete='off'
                  autoFocus='autofocus'
                  className='form-control '
                  type='text'
                  value={this.state.name}
                  onChange={this.handleInputChange}
                  name='name'
                  placeholder='e.g. Tuesday...'
                  required
                />
              </div>
              <div className='col-4'>
                <button className='btn btn-block btn-light'>
                  {this.state.sending === true ? (
                    <i className='fas fa-circle-notch fa-spin' />
                  ) : (
                    "Enter"
                  )}
                </button>
              </div>
            </div>
          </form>
        ) : (
          <div className='text-center'>
            Calculate your macros
            <NavLink to='/nutrition/calculator'> here.</NavLink>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    protein: state.nutrition.protein,
    calories: state.nutrition.calories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetDays: () => {
      dispatch(getDays());
    },
    onGetCaloriesProtein: () => {
      dispatch(getCaloriesProtein());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateDay);
