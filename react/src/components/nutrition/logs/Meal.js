import React, { Component, PropTypes } from "react";
import axiosAjax from "../../../config/axiosAjax";
import { connect } from "react-redux";
import { getDay, getMeals } from "../../../redux/actions/nutrition";
import { NotificationManager } from "react-notifications";

class Meal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      servings: 0,
      updateMeal: false,
      protein: 0,
      calories: 0,
      fat: 0,
      carbs: 0,
      proteinPerServing: 0,
      caloriesPerServing: 0,
      carbsPerServing: 0,
      fatPerServing: 0,
      startingCalories: 0,
      startingProtein: 0,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.updateServing = this.updateServing.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _calculateMealMacros() {
    if (
      this.props.meal.protein !== null &&
      this.props.meal.calories !== null &&
      this.props.meal.fat !== null &&
      this.props.meal.carbs !== null
    ) {
      this.setState(
        {
          servings: this.props.meal.servings,
          updateMeal: false,
          proteinPerServing: this.props.meal.protein / this.props.meal.servings,
          caloriesPerServing:
            this.props.meal.calories / this.props.meal.servings,
          carbsPerServing: this.props.meal.carbs / this.props.meal.servings,
          fatPerServing: this.props.meal.fat / this.props.meal.servings,
        },
        () => {
          this.setState({
            protein: Math.round(
              this.state.proteinPerServing * this.state.servings,
            ),
            calories: Math.round(
              this.state.caloriesPerServing * this.state.servings,
            ),
            fat: Math.round(this.state.fatPerServing * this.state.servings),
            carbs: Math.round(this.state.carbsPerServing * this.state.servings),
            startingCalories: Math.round(
              this.state.caloriesPerServing * this.state.servings,
            ),
            startingProtein: Math.round(
              this.state.proteinPerServing * this.state.servings,
            ),
          });
        },
      );
    } else {
      this.setState(
        {
          servings: this.props.meal.servings,
          updateMeal: false,
          proteinPerServing: this.props.meal.recipe
            ? this.props.meal.recipe.protein / this.props.meal.servings
            : this.props.meal.food.protein / this.props.meal.servings,
          caloriesPerServing: this.props.meal.recipe
            ? this.props.meal.recipe.calories / this.props.meal.servings
            : this.props.meal.food.calories / this.props.meal.servings,
          fatPerServing: this.props.meal.recipe
            ? this.props.meal.recipe.fat / this.props.meal.servings
            : this.props.meal.food.fat / this.props.meal.servings,
          carbsPerServing: this.props.meal.recipe
            ? this.props.meal.recipe.carbs / this.props.meal.servings
            : this.props.meal.food.carbs / this.props.meal.servings,
        },
        () => {
          this.setState({
            protein: Math.round(
              this.state.proteinPerServing * this.state.servings,
            ),
            calories: Math.round(
              this.state.caloriesPerServing * this.state.servings,
            ),
            fat: Math.round(this.state.fatPerServing * this.state.servings),
            carbs: Math.round(this.state.carbsPerServing * this.state.servings),
            startingCalories: Math.round(
              this.state.caloriesPerServing * this.state.servings,
            ),
            startingProtein: Math.round(
              this.state.proteinPerServing * this.state.servings,
            ),
          });
        },
      );
    }
  }

  componentDidMount() {
    this._calculateMealMacros();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.servings !== this.state.servings) {
      this.setState({
        protein: Math.round(this.state.proteinPerServing * this.state.servings),
        calories: Math.round(
          this.state.caloriesPerServing * this.state.servings,
        ),
        fat: Math.round(this.state.fatPerServing * this.state.servings),
        carbs: Math.round(this.state.carbsPerServing * this.state.servings),
      });
    }
    if (prevProps.meal.id !== this.props.meal.id) {
      this._calculateMealMacros();
    }
  }

  deleteMeal() {
    if (confirm("Click OK to delete this food.")) {
      const formData = new FormData();
      formData.append("day_id", this.props.dayId);
      formData.append("protein", this.state.protein);
      formData.append("calories", this.state.calories);
      axiosAjax({
        method: "delete",
        url: "/nutrition-tracker/food-log-meal/" + this.props.meal.id + "/",
        data: formData,
      })
        .then(response => {
          this.props.onGetDay(this.props.dayId);
          this.props.onGetMeals(this.props.dayId);
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  updateServing(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("servings", this.state.servings);
    formData.append("day_id", this.props.dayId);
    formData.append("protein", this.state.protein);
    formData.append("calories", this.state.calories);
    formData.append("carbs", this.state.carbs);
    formData.append("fat", this.state.fat);
    formData.append("starting_calories", this.state.startingCalories);
    formData.append("starting_protein", this.state.startingProtein);
    axiosAjax({
      method: "patch",
      url: "/nutrition-tracker/food-log-meal/" + this.props.meal.id + "/",
      headers: { "Content-Type": "multipart/form-data" },
      data: formData,
    })
      .then(response => {
        this.props.onGetDay(this.props.dayId);
        this.setState({
          editServing: false,
          startingCalories: this.state.calories,
          startingProtein: this.state.protein,
        });
        NotificationManager.success("Servings updated.");
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className='content-container'>
        {this.props.meal.recipe ? (
          <React.Fragment>
            <div
              className='row d-flex align-items-center'
              style={{ cursor: "pointer" }}
              onClick={() =>
                this.setState({ updateMeal: !this.state.updateMeal })
              }
            >
              <div className='col-8' style={{ overflow: "hidden" }}>
                <p className='text-capitalize m-0' style={foodTitle}>
                  {this.props.meal.recipe.name}
                </p>
              </div>
              <div className='col-4'>
                <button
                  onClick={() =>
                    this.setState({ updateMeal: !this.state.updateMeal })
                  }
                  className='btn btn-sm btn-rounded float-right btn-light'
                >
                  {this.state.updateMeal === true ? (
                    <i className='fas fa-angle-down' />
                  ) : (
                    <i className='fas fa-angle-left' />
                  )}
                </button>
              </div>
            </div>
            {this.state.updateMeal === true && (
              <div style={{ marginTop: "1rem" }}>
                <div className='container'>
                  <div className='row' style={nutritionalInfo}>
                    <div className='col d-flex justify-content-between align-items-center'>
                      <p className='lead m-0'>Total Calories</p>
                      <p className='green lead m-0'>{this.state.calories}</p>
                    </div>
                  </div>
                  <div className='row d-flex justify-content-between align-items-center'>
                    <div className='col-4 text-center' style={macros}>
                      <p className='m-0'>
                        <span className='green'>{this.state.protein}g</span> Pro
                      </p>
                    </div>
                    <div className='col-4 text-center' style={macros}>
                      <p className='m-0'>
                        <span className='green'>{this.state.carbs}g</span> Carb
                      </p>
                    </div>
                    <div className='col-4 text-center' style={macros}>
                      <p className='m-0'>
                        <span className='green'>{this.state.fat}g</span> Fat
                      </p>
                    </div>
                  </div>
                </div>
                <div style={{ marginBottom: "1rem", marginTop: "1rem" }}>
                  <p className='p-smaller m-0'>Current servings</p>
                  <form onSubmit={this.updateServing}>
                    <div className='form-row'>
                      <div className='col'>
                        <input
                          type='number'
                          step='0.00000001'
                          autoComplete='off'
                          className='form-control form-control-single-border'
                          onChange={this.handleInputChange}
                          value={this.state.servings}
                          name='servings'
                          required
                        />
                      </div>
                      <div className='col'>
                        <button className='btn btn-light'>Update</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div className='row' style={{ marginTop: "1rem" }}>
                  <div className='col-12'>
                    <button
                      className='btn btn-sm btn-block btn-outline-danger'
                      onClick={() => this.deleteMeal()}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            )}
          </React.Fragment>
        ) : (
          <React.Fragment>
            <div
              className='row d-flex align-items-center'
              style={{ cursor: "pointer" }}
              onClick={() =>
                this.setState({ updateMeal: !this.state.updateMeal })
              }
            >
              <div className='col-8' style={{ overflow: "hidden" }}>
                <p className='text-capitalize m-0' style={foodTitle}>
                  {this.props.meal.food.name.replace(/,/g, ", ")}
                </p>
              </div>
              <div className='col-4'>
                <button
                  onClick={() =>
                    this.setState({ updateMeal: !this.state.updateMeal })
                  }
                  className='btn float-right btn-sm btn-rounded btn-light'
                >
                  {this.state.updateMeal === true ? (
                    <i className='fas fa-angle-down' />
                  ) : (
                    <i className='fas fa-angle-left' />
                  )}
                </button>
              </div>
            </div>
            {this.state.updateMeal === true && (
              <div style={{ marginTop: "1rem" }}>
                <div className='container'>
                  <div className='row' style={nutritionalInfo}>
                    <div className='col d-flex justify-content-between align-items-center'>
                      <p className='lead m-0'>Total Calories</p>
                      <p className='green lead m-0'>{this.state.calories}</p>
                    </div>
                  </div>
                  <div className='row d-flex justify-content-between align-items-center'>
                    <div className='col-4 text-center' style={macros}>
                      <p className='m-0'>
                        <span className='green'>{this.state.protein}g</span> Pro
                      </p>
                    </div>
                    <div className='col-4 text-center' style={macros}>
                      <p className='m-0'>
                        <span className='green'>{this.state.carbs}g</span> Carb
                      </p>
                    </div>
                    <div className='col-4 text-center' style={macros}>
                      <p className='m-0'>
                        <span className='green'>{this.state.fat}g</span> Fat
                      </p>
                    </div>
                  </div>
                </div>
                <div style={{ marginBottom: "1rem", marginTop: "1rem" }}>
                  <p className='p-smaller'>
                    Servings ({this.props.meal.food.serving_size})
                  </p>
                  <form onSubmit={this.updateServing}>
                    <div className='form-row'>
                      <div className='col'>
                        <input
                          type='number'
                          step='0.00000001'
                          autoComplete='off'
                          className='form-control form-control-single-border'
                          onChange={this.handleInputChange}
                          value={this.state.servings}
                          name='servings'
                          required
                        />
                      </div>
                      <div className='col'>
                        <button className='btn  btn-light'>Update</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div className='row' style={{ marginTop: "1rem" }}>
                  <div className='col-12'>
                    <button
                      onClick={() => this.deleteMeal()}
                      className='btn btn-block btn-outline-danger'
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const macros = {
  fontSize: 10,
  margin: 0,
  border: "solid 1px #EFF0F2",
  padding: 5,
};

const nutritionalInfo = {
  border: "solid 1px #EFF0F2",
  padding: 5,
};

const foodTitle = {
  wordWrap: "break-word",
};

const editServing = {
  cursor: "pointer",
};

const editServingInput = {
  background: "transparent",
  border: "none",
};

const mapDispatchToProps = dispatch => {
  return {
    onGetDay: dayId => {
      dispatch(getDay(dayId));
    },
    onGetMeals: dayId => {
      dispatch(getMeals(dayId));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(Meal);
