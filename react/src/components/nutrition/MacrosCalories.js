import React, { Component } from "react";
import { connect } from "react-redux";
import { getCaloriesProtein } from "../../redux/actions/nutrition";
import { NavLink } from "react-router-dom";
import MDSpinner from "react-md-spinner";
import LightAlert from "../shared/LightAlert";
import "./styles.css";

class MacrosCalories extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onGetCaloriesProtein();
  }

  render() {
    return (
      <div className='macros-calories'>
        <div className='container'>
          {this.props.protein !== undefined &&
          this.props.calories !== undefined ? (
            <div className='mb-1 mt-4 pb-2'>
              <div className='row'>
                <div className='col text-center'>
                  <h5 className='m-0 macro-value'>{this.props.protein}g</h5>
                  <div className='white font-weight-bold p-xs'>
                    <i className='fal fa-steak' /> Protein target
                  </div>
                </div>
                <div className='col text-center'>
                  <h5 className='m-0 macro-value'>{this.props.calories}</h5>
                  <div className='white font-weight-bold p-xs'>
                    <i className='fal fa-fire' /> Calorie target
                  </div>
                </div>
              </div>
              <div className='text-center mt-2'>
                <NavLink
                  className='btn shadow btn-rounded btn-xs btn-light'
                  to='/nutrition/calculator'
                >
                  {this.props.protein ? "Recalculate" : "Calculate your macros"}
                </NavLink>
              </div>
            </div>
          ) : (
            <div
              className='text-center'
              style={{ paddingTop: "5%", paddingBottom: "5%" }}
            >
              <MDSpinner singleColor='#4a4090' />
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    protein: state.nutrition.protein,
    calories: state.nutrition.calories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetCaloriesProtein: () => {
      dispatch(getCaloriesProtein());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MacrosCalories);
