/**
 * @flow
 * @format
 */
import React, { PureComponent } from "react";
import { NavLink } from "react-router-dom";
import cn from "classnames";

import HomeStepProgress from "../HomeStepProgress";
import "./styles.css";

type Props = {
  condition?: boolean,
  delay?: number,
  title: string,
  subTitle: string,
  stepProgress: number,
  isCompleted: boolean,
  linkTo: string,
  isUnlocked?: boolean,
};

class HomeStepCard extends PureComponent<Props> {
  static defaultProps = {
    condition: true,
    delay: 0,
    isUnlocked: true,
  };

  getSectionClassName = () => {
    const { condition, delay } = this.props;

    return cn(
      {
        "home-flex-step shadow-sm": condition,
        "home-flex-step-locked": !condition,
      },
      `fadeInUp animated-2 delay-${delay}x`,
    );
  };

  handleLinkClick = e => {
    const { isUnlocked } = this.props;
    if (!isUnlocked) e.preventDefault();
  };

  render() {
    const {
      title,
      subTitle,
      stepProgress,
      isCompleted,
      linkTo,
      isUnlocked,
      lockedLabel,
    } = this.props;

    const actionButton = () =>
      isCompleted ? (
        <i className='fas green fa-2x fa-check-circle' />
      ) : (
        <button
          className={cn("btn btn-rounded pr-3 pl-3 btn-primary", {
            ["active-home-step"]: stepProgress !== 0,
          })}
        >
          Go
        </button>
      );

    const stepIsLocked = () =>
      isUnlocked ? (
        actionButton()
      ) : (
        <div className='text-muted text-center'>
          <i className='fal fa-xs fa-lock-alt' />
          <p style={{ fontSize: 10 }} className='m-0 text-muted'>
            {lockedLabel}
          </p>
        </div>
      );

    return (
      <div className={this.getSectionClassName()}>
        <NavLink
          onClick={this.handleLinkClick}
          to={linkTo}
          className='home-flex-step-body'
        >
          <div className='homeStepContent'>
            <div className='homeStepTitles'>
              <h5 className='homeStepTitle'>{title}</h5>
              <h6 className='homeStepSubtitle'>{subTitle}</h6>
              <HomeStepProgress value={stepProgress} />
            </div>
            <div className='homeStepAction'>{stepIsLocked()}</div>
          </div>
        </NavLink>
      </div>
    );
  }
}

export default HomeStepCard;
