/**
 * @flow
 * @format
 */
import React from "react";
import type ReactComponent from "react";
import type ReactNode from "react";
import { NavLink } from "react-router-dom";

import "./styles.css";
import MenuMobile from "../../components/shared/MenuMobile";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

type Props = {
  pageTitle: string,
  membershipStatus?: boolean,
  title: string,
  nextStep: string,
  nextStepTitle: string,
  children: ReactNode | string,
  backLink: string,
};

const upgradeLink = "/users/membership-options/";

const HomeStepCompleteMessage = ({
  pageTitle,
  membershipStatus,
  title,
  nextStep,
  nextStepTitle,
  children,
  backLink,
}): ReactComponent<Props> => (
  <>
    <MenuMobile backLink={backLink} />
    <Heading
      columns={
        <div className='col d-flex flex-column justify-content-center'>
          <h4>{pageTitle}</h4>
        </div>
      }
    />
    <HeadingMobile
      columns={
        <div className='col d-flex flex-column justify-content-center'>
          <h4>{pageTitle}</h4>
        </div>
      }
    />
    <div className='homeStepCompleteMessageContainer'>
      <div className='animated-2 pulse mb-3'>
        <i className='fal fa-5x green fa-check-circle homeStepCompleteMessageIcon' />
      </div>
      <h3 className='homeStepCompleteMessageTitle'>{title}</h3>
      <p className='homeStepCompleteMessage'>{children}</p>
      {membershipStatus === "trialing" && (
        <a className='homeStepCompleteMessageUpgrade' href={upgradeLink}>
          Upgrade Account
        </a>
      )}

      <NavLink to={nextStep} className='homeStepCompleteMessageNext'>
        {nextStepTitle}
      </NavLink>
    </div>
  </>
);

HomeStepCompleteMessage.defaultProps = {
  membershipStatus: false,
};

export default HomeStepCompleteMessage;
