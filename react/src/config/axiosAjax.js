import axios from "axios";

const localHost = false;

if (localHost === true) {
  var baseURL = "http://localhost:8000/";
} else {
  var baseURL = "https://www.jamessmithacademy.com";
}

var axiosAjax = axios.create({
  baseURL: baseURL,
});

axiosAjax.interceptors.response.use(
  function(response) {
    // Do something with response data
    return response;
  },
  function(error) {
    if (error.response) {
      if (error.response.status === 401) {
        alert(
          "Error: Please logout, clear cookies/history/cache and login again.",
        );
      } else if (error.response.status === 403) {
        alert(
          "Error: Please logout, clear your cookies/history/cache and login again.",
        );
      } else if (error.response.status === 404) {
        //Error: API Route is Missing or Undefined
      } else if (error.response.status === 405) {
        //Error: API Route Method Not Allowed
      } else if (error.response.status === 422) {
        //Validation Message
      } else if (error.response.status >= 500) {
        alert("Error: There was a server error.");
      }
    }

    return Promise.reject(error);
  },
);

axiosAjax.defaults.xsrfCookieName = "csrftoken";
axiosAjax.defaults.xsrfHeaderName = "X-CSRFToken";

export default axiosAjax;
