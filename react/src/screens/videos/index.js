import React, { Component } from "react";
import Menu from "../../components/videos/Menu";
import MenuButton from "../../components/videos/MenuButton";
import Search from "../../components/videos/Search";
import New from "../../components/videos/New";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

class VideosTab extends Component {
  constructor(props) {
    super(props);
    localStorage.setItem("nextVideo", false);
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <React.Fragment>
        <Menu />
        <div className='scroll-view grey-background'>
          <Heading
            columns={
              <React.Fragment>
                <div className='col d-flex flex-column justify-content-center'>
                  <h4>Learn</h4>
                </div>
                <div className='col-auto d-flex flex-column justify-content-center'>
                  <div className='for-desktop'>
                    <Search />
                  </div>
                </div>
                <div className='col-auto d-flex flex-column justify-content-center'>
                  <MenuButton />
                </div>
              </React.Fragment>
            }
          />
          <HeadingMobile
            columns={
              <div className='col'>
                <div className='row'>
                  <div className='col d-flex flex-column justify-content-center'>
                    <h4>Learn</h4>
                  </div>
                  <div className='col-auto d-flex flex-column justify-content-center'>
                    <div className='for-desktop'>
                      <Search />
                    </div>
                  </div>
                  <div className='col-auto d-flex flex-column justify-content-center'>
                    <MenuButton />
                  </div>
                </div>
                <div className='row' style={{ marginTop: "1rem" }}>
                  <div className='col-12 d-flex flex-column justify-content-center'>
                    <Search />
                  </div>
                </div>
              </div>
            }
          />
          <div className='container' style={{ marginTop: 30 }}>
            <p className='text-muted m-0 p-0 text-capitalize'>
              Current category: <strong>New Videos</strong>
            </p>
          </div>
          <New />
        </div>
      </React.Fragment>
    );
  }
}

export default VideosTab;
