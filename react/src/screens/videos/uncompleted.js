import React, { Component } from "react";
import Menu from "../../components/videos/Menu";
import MenuButton from "../../components/videos/MenuButton";
import Uncompleted from "../../components/videos/Uncompleted";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

class UncompletedTab extends Component {
  constructor(props) {
    super(props);
    localStorage.setItem("nextVideo", false);
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <React.Fragment>
        <Menu />
        <div className='scroll-view grey-background'>
          <Heading
            columns={
              <React.Fragment>
                <div className='col d-flex flex-column justify-content-center'>
                  <h4>Learn</h4>
                </div>
                <div className='col-auto d-flex flex-column justify-content-center'>
                  <MenuButton />
                </div>
              </React.Fragment>
            }
          />
          <HeadingMobile
            columns={
              <React.Fragment>
                <div className='col d-flex flex-column justify-content-center'>
                  <h4>Learn</h4>
                </div>
                <div className='col-auto d-flex flex-column justify-content-center'>
                  <MenuButton />
                </div>
              </React.Fragment>
            }
          />
          <div className='container' style={{ marginTop: 30 }}>
            <p className='text-muted m-0 p-0 text-capitalize'>
              Current category: <strong>Uncompleted</strong>
            </p>
          </div>
          <Uncompleted />
        </div>
      </React.Fragment>
    );
  }
}

export default UncompletedTab;
