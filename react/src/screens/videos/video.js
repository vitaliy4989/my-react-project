import React, { Component } from "react";
import Menu from "../../components/videos/Menu";
import MenuButton from "../../components/videos/MenuButton";
import MarkFavourite from "../../components/videos/MarkFavourite";
import MarkCompleted from "../../components/videos/MarkCompleted";
import AddToProgram from "../../components/videos/AddToProgram";
import { connect } from "react-redux";
import { getPrograms } from "../../redux/actions/training";
import {
  updateCurrentMainFilterId,
  updateMainFilter,
} from "../../redux/actions/videos";
import MDSpinner from "react-md-spinner";
import axiosAjax from "../../config/axiosAjax";
import Player from "@vimeo/player";
import OtherRelatedVideos from "../../components/videos/OtherRelatedVideos";

class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: undefined,
      add: false,
      video: undefined,
      favourite: undefined,
      enrolled: undefined,
    };
  }

  _getVideo(id) {
    axiosAjax.get("/modules/module/" + id).then(response => {
      this.setState({
        id: response.data.module.id,
        video: response.data.module,
        favourite: response.data.favourite,
        enrolled: response.data.module_enrolled,
      });
      this.props.onUpdateCurrentMainFilterId(
        response.data.module.sub_filter.main,
      );
    });
    window.scrollTo(0, 0);
  }

  _mountVimeoPlayer() {
    var video = document.querySelector("#video" + this.state.id);
    const player = new Player(video);
    player.on("ended", () => {
      if (this.state.enrolled !== true) {
        const formData = new FormData();
        formData.append("module", this.state.id);
        axiosAjax({
          method: "post",
          url: "/modules/enrolled/",
          headers: { "Content-Type": "multipart/form-data" },
          data: formData,
        })
          .then(response => {
            this.setState({
              enrolled: true,
            });
            this.props.onUpdateMainFilter(true);
          })
          .catch(function(error) {
            console.log(error);
          });
      }
    });
  }

  _nextVideo() {
    const currentVideo = this.state.video;
    axiosAjax
      .get("/modules/next/", {
        params: {
          module_id: this.props.match.params.id,
          current_order: currentVideo.order,
          sub_filter: currentVideo.sub_filter.id,
        },
      })
      .then(response => {
        window.location.href =
          window.location.protocol +
          "//" +
          window.location.host +
          "/users/#/coaching/modules/module/" +
          response.data.module.id;
      })
      .catch(error => {
        console.log(error);
        this.setState({ video: currentVideo });
      });
  }

  _getPrograms() {
    this.setState(
      {
        add: !this.state.add,
      },
      () => {
        if (this.state.add === true) {
          this.props.onGetPrograms();
        }
      },
    );
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      parseInt(nextProps.match.params.id) !== parseInt(prevState.id) &&
      prevState.id !== undefined
    ) {
      return {
        id: nextProps.match.params.id,
        video: undefined,
        add: false,
        favourite: undefined,
        enrolled: undefined,
      };
    }
    return null;
  }

  componentDidMount() {
    this._getVideo(this.props.match.params.id);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.video !== undefined) {
      this._mountVimeoPlayer();
    }
    if (this.state.video === undefined) {
      this._getVideo(this.props.match.params.id);
    }
  }

  render() {
    return (
      <React.Fragment>
        <Menu />
        <div className='scroll-view'>
          <div className='app-header'>
            <div className='container'>
              <div className='row'>
                <div className='col d-flex flex-column justify-content-center'>
                  {history.length > 1 && (
                    <div
                      style={{
                        marginLeft: 0,
                        paddingLeft: 0,
                      }}
                      className='nav-link for-mobile'
                      onClick={() => window.history.back()}
                    >
                      <i className='fas white p-0 m-0 fa-lg fa-chevron-left' />
                    </div>
                  )}
                  <h4 className='for-desktop'>
                    {this.state.video !== undefined && this.state.video.name}
                  </h4>
                </div>
                <div className='col-auto d-flex flex-column justify-content-center'>
                  <MenuButton />
                </div>
              </div>
            </div>
          </div>
          {this.state.video !== undefined ? (
            <React.Fragment>
              <div className='for-mobile'>
                <div
                  id={"video" + this.state.id}
                  className='embed-responsive embed-responsive-16by9'
                >
                  <div
                    className='embed-responsive-item'
                    dangerouslySetInnerHTML={{
                      __html: this.state.video.video,
                    }}
                  />
                </div>
              </div>
              <div className='container'>
                <div className='row'>
                  <div className='col-lg-8'>
                    <div className='for-desktop'>
                      <div
                        id={"video" + this.state.id}
                        style={{ marginTop: 30 }}
                        className='embed-responsive embed-responsive-16by9'
                      >
                        <div
                          className='embed-responsive-item'
                          dangerouslySetInnerHTML={{
                            __html: this.state.video.video,
                          }}
                        />
                      </div>
                    </div>
                    <div className='for-mobile' style={{ marginTop: "1rem" }}>
                      <h6 className='m-0 p-0'>{this.state.video.name}</h6>
                    </div>
                    <div style={{ marginTop: "1rem" }} className='row'>
                      {this.state.video.available_for_program === true && (
                        <div className='col'>
                          <button
                            type='button'
                            className='btn-rounded btn btn-block btn-light'
                            onClick={() => this._getPrograms()}
                          >
                            {this.state.add === true ? (
                              <i className='far fa-lg fa-times' />
                            ) : (
                              <i className='far fa-lg fa-plus' />
                            )}
                          </button>
                        </div>
                      )}
                      <div className='col text-center'>
                        <MarkFavourite
                          videoId={this.state.id}
                          favourite={this.state.favourite}
                        />
                      </div>
                      <div className='col text-center'>
                        <MarkCompleted
                          videoId={this.state.id}
                          completed={this.state.enrolled}
                        />
                      </div>
                      {localStorage.nextVideo === "true" && (
                        <div className='col'>
                          <button
                            className='btn btn-rounded btn-block btn-light'
                            onClick={() => this._nextVideo()}
                          >
                            {window.innerWidth <= 768 ? (
                              <i className='far fa-lg fa-chevron-right' />
                            ) : (
                              <React.Fragment>
                                Next &nbsp;{" "}
                                <i className='far fa-chevron-right' />
                              </React.Fragment>
                            )}
                          </button>
                        </div>
                      )}
                    </div>
                    {this.state.add === true && (
                      <div
                        style={{
                          marginTop: "1rem",
                          marginBottom: "1.5rem",
                        }}
                        className='list-wrapper list-group list-group-flush'
                      >
                        <div className='list-group-item list-group-heading'>
                          <h6 className='m-0'>Add exercise to log</h6>
                        </div>
                        {this.props.programs !== undefined &&
                        this.props.programs.length > 0 ? (
                          this.props.programs.map((program, key) => (
                            <div
                              key={key}
                              className='list-group-item d-flex justify-content-between align-items-center'
                            >
                              {program.name}
                              <AddToProgram
                                programId={program.id}
                                videoId={this.state.id}
                              />
                            </div>
                          ))
                        ) : (
                          <div className='list-group-item d-flex justify-content-between align-items-center'>
                            <small>You have not yet logged any training.</small>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                  {this.state.video.sub_filter !== null && (
                    <div className='col-lg-4'>
                      <OtherRelatedVideos
                        subFilter={this.state.video.sub_filter.id}
                        subFilterName={this.state.video.sub_filter.name}
                        videoId={this.state.video.id}
                      />
                    </div>
                  )}
                </div>
              </div>
            </React.Fragment>
          ) : (
            <div className='h-100 d-flex flex-column justify-content-center'>
              <div className='text-center'>
                <MDSpinner singleColor='#4a4090' />
              </div>
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    programs: state.training.programs,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetPrograms: () => {
      dispatch(getPrograms());
    },
    onUpdateMainFilter: update => {
      dispatch(updateMainFilter(update));
    },
    onUpdateCurrentMainFilterId: id => {
      dispatch(updateCurrentMainFilterId(id));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Video);
