import React, { Component } from "react";
import Menu from "../../components/videos/Menu";
import MenuButton from "../../components/videos/MenuButton";
import Modules from "../../components/videos/Modules";
import CategoryDropdown from "../../components/videos/CategoryDropdown/index";
import Search from "../../components/videos/Search";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";
import axiosAjax from "../../config/axiosAjax";

export default class CategoryTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subFilterName: undefined,
      mainFilter: undefined,
    };
    localStorage.setItem("nextVideo", true);
    window.scrollTo(0, 0);
  }

  _getSubFilter(id) {
    axiosAjax.get("/weeks/sub-filter/" + id + "/").then(response => {
      this.setState({
        subFilterName: response.data.name,
        mainFilter: response.data.main,
      });
    });
  }

  componentDidMount() {
    this._getSubFilter(this.props.match.params.id);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      this._getSubFilter(this.props.match.params.id);
    }
  }

  render() {
    return (
      <React.Fragment>
        <Menu />
        <div className='scroll-view grey-background'>
          <Heading
            columns={
              <React.Fragment>
                <div
                  className='col d-flex flex-column justify-content-center'
                  style={{ overflow: "hidden", whiteSpace: "nowrap" }}
                >
                  <h4 className='text-capitalize m-0'>Learn</h4>
                </div>
                <div className='col-auto d-flex flex-column justify-content-center'>
                  <div className='for-desktop'>
                    <Search />
                  </div>
                </div>
                <div className='col-auto d-flex flex-column justify-content-center'>
                  <MenuButton />
                </div>
              </React.Fragment>
            }
          />
          <HeadingMobile
            columns={
              <div className='col'>
                <div className='row'>
                  <div
                    className='col d-flex flex-column justify-content-center'
                    style={{ overflow: "hidden", whiteSpace: "nowrap" }}
                  >
                    <h4 className='text-capitalize m-0'>Learn</h4>
                  </div>
                  <div className='col-auto d-flex flex-column justify-content-center'>
                    <div className='for-desktop'>
                      <Search />
                    </div>
                  </div>
                  <div className='col-auto d-flex flex-column justify-content-center'>
                    <MenuButton />
                  </div>
                </div>
                <div className='row' style={{ marginTop: "1rem" }}>
                  <div className='col-12 d-flex flex-column justify-content-center'>
                    <Search />
                  </div>
                </div>
              </div>
            }
          />
          <div className='container' style={{ marginTop: 30 }}>
            <div className='row no-gutters'>
              <div className='col-auto d-flex flex-column justify-content-center'>
                <p className='text-muted m-0'>Current category:</p>
              </div>
              <div className='col d-flex flex-column justify-content-center'>
                <CategoryDropdown
                  mainFilterId={this.state.mainFilter}
                  subFilterName={this.state.subFilterName}
                />
              </div>
            </div>
          </div>
          <Modules
            id={this.props.match.params.id}
            mainFilter={this.state.mainFilter}
          />
        </div>
      </React.Fragment>
    );
  }
}
