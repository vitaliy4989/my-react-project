import React from "react";
import Topics from "../../components/faqs/Topics";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const index = props => {
  return (
    <div className='scroll-view'>
      <Heading
        columns={
          <div className='col d-flex flex-column justify-content-center'>
            <h4>F.A.Qs</h4>
          </div>
        }
      />
      <HeadingMobile
        columns={
          <div className='col d-flex flex-column justify-content-center'>
            <h4>F.A.Qs</h4>
          </div>
        }
      />
      <div className='container'>
        <Topics />
      </div>
    </div>
  );
};

export default index;
