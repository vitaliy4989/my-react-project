import React from "react";
import MenuMobile from "../../components/shared/MenuMobile";
import Entry from "../../components/challenge/Entry";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const EntryTab = props => {
  return (
    <React.Fragment>
      <MenuMobile back={true} />
      <div className='scroll-view'>
        <Heading
          columns={
            <div className='col'>
              <h4>Entry Form</h4>
            </div>
          }
        />
        <HeadingMobile
          columns={
            <div className='col'>
              <h4>Entry Form</h4>
            </div>
          }
        />
        <div className='container'>
          <Entry id={props.match.params.id} />
        </div>
      </div>
    </React.Fragment>
  );
};

export default EntryTab;
