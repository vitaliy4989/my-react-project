import React from "react";
import MenuMobile from "../../components/shared/MenuMobile";
import ChartEntry from "../../components/challenge/ChartEntry";
import ImageEntry from "../../components/challenge/ImageEntry";
import axiosAjax from "../../config/axiosAjax";
import MDSpinner from "react-md-spinner";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

export default class challenge_data extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: false,
      chart: true,
      entry: null,
    };
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    axiosAjax
      .get("/challenge/entry/", {
        params: {
          challenge_id: this.props.match.params.id,
        },
      })
      .then(response => {
        this.setState({
          entry: response.data,
        });
      })
      .catch(error => {
        this.setState({
          entry: false,
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <MenuMobile back={true} />
        <div className='scroll-view grey-background'>
          <Heading
            class='app-header-tabs'
            columns={
              <div className='col'>
                <div className='row'>
                  <div className='col d-flex flex-column justify-content-center'>
                    <h4>Challenge Data</h4>
                  </div>
                </div>
                <div className='row'>
                  <div className='col'>
                    <div className='option-header-wrapper'>
                      <div
                        className={
                          this.state.chart === true
                            ? "option-header option-header-active"
                            : "option-header"
                        }
                        onClick={() =>
                          this.setState({ chart: true, images: false })
                        }
                      >
                        Chart
                      </div>
                      <div
                        className={
                          this.state.images === true
                            ? "option-header option-header-active"
                            : "option-header"
                        }
                        onClick={() =>
                          this.setState({ chart: false, images: true })
                        }
                      >
                        Images
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            }
          />
          <HeadingMobile
            class='app-header-tabs'
            columns={
              <div className='col'>
                <div className='row'>
                  <div className='col d-flex flex-column justify-content-center'>
                    <h4>Challenge Data</h4>
                  </div>
                </div>
                <div className='row'>
                  <div className='col'>
                    <div className='option-header-wrapper'>
                      <div
                        className={
                          this.state.chart === true
                            ? "option-header option-header-active"
                            : "option-header"
                        }
                        onClick={() =>
                          this.setState({ chart: true, images: false })
                        }
                      >
                        Chart
                      </div>
                      <div
                        className={
                          this.state.images === true
                            ? "option-header option-header-active"
                            : "option-header"
                        }
                        onClick={() =>
                          this.setState({ chart: false, images: true })
                        }
                      >
                        Images
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            }
          />
          {this.state.entry !== null ? (
            this.state.entry !== false ? (
              <React.Fragment>
                {this.state.chart === true && (
                  <ChartEntry
                    entryId={this.state.entry.id}
                    entry={this.state.entry}
                  />
                )}
                {this.state.images === true && (
                  <ImageEntry
                    entryId={this.state.entry.id}
                    entry={this.state.entry}
                  />
                )}
              </React.Fragment>
            ) : (
              <div className='h-100 d-flex flex-column justify-content-center'>
                <div className='container'>
                  <div className='alert alert-light text-center'>
                    <h6 className='lead'>JSA Challenges</h6>
                    <p>
                      The JSA Challenge is a big part of what we do. This is not
                      a 6 pack competition or anything like that. We simply
                      challenge you to set a goal to achieve in 3 months. The
                      winner is taken on holiday or receives a cash prize of
                      10,000 dollars. Find out more information on the challenge{" "}
                      <a href='/#about-challenges'>
                        <u>here</u>
                      </a>
                      .
                    </p>
                  </div>
                </div>
              </div>
            )
          ) : (
            <div className='h-100 d-flex flex-column justify-content-center'>
              <div className='text-center'>
                <MDSpinner singleColor='#4a4090' />
              </div>
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}
