import React from "react";
import Challenge from "../../components/challenge/Challenge";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const ChallengeTab = props => {
  return (
    <div className='scroll-view grey-background'>
      <Heading
        columns={
          <div className='col d-flex flex-column justify-content-center'>
            <h4>Challenge</h4>
          </div>
        }
      />
      <HeadingMobile
        columns={
          <div className='col d-flex flex-column justify-content-center'>
            <h4>Challenge</h4>
          </div>
        }
      />
      <Challenge />
    </div>
  );
};

export default ChallengeTab;
