import React, { Component } from "react";
import MenuMobile from "../../../components/shared/MenuMobile";
import AddExercise from "../../../components/exercise/myworkouts/AddExercise";
import Exercises from "../../../components/exercise/myworkouts/Exercises";
import { connect } from "react-redux";
import { getProgram } from "../../../redux/actions/training";
import axiosAjax from "../../../config/axiosAjax";
import { NotificationManager } from "react-notifications";
import Heading from "../../../components/shared/Heading";
import HeadingMobile from "../../../components/shared/HeadingMobile";
import Supersets from "../../../components/exercise/myworkouts/Supersets/index";
import Options from "../../../components/exercise/myworkouts/Options";

class ProgramTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      create: false,
      changeName: false,
      name: "",
      notes: "",
      saveNotes: false,
      type: "",
      rounds: 0,
      experience: "",
      timeCompletedIn: 0,
      goal: "",
      facility: "",
      focus: "",
      forProgramTemplates: false,
      id: undefined,
      saveCircuit: false,
    };
    window.scrollTo(0, 0);
    this._updateDay = this._updateDay.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this._loadDay = this._loadDay.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  _loadDay(id) {
    axiosAjax
      .get("/training/training-log-day/" + id + "/")
      .then(response => {
        this.setState(
          {
            name: response.data.name ? response.data.name : "",
            notes: response.data.notes ? response.data.notes : "",
            type: response.data.type ? response.data.type : "",
            rounds: response.data.rounds ? response.data.rounds : 0,
            focus: response.data.focus ? response.data.focus : "",
            experience: response.data.experience
              ? response.data.experience
              : "",
            timeCompletedIn: response.data.time_completed_in
              ? response.data.time_completed_in
              : 0,
            goal: response.data.goal ? response.data.goal : "",
            facility: response.data.facility ? response.data.facility : "",
            forProgramTemplates: response.data.for_program_templates
              ? response.data.for_program_templates
              : false,
          },
          () => {
            this.setState({
              id: response.data.id,
              saveCircuit: false,
              saveNotes: false,
            });
          },
        );
      })
      .catch(error => {
        this.setState({});
      });
  }

  componentDidMount() {
    this._loadDay(this.props.match.params.id);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.rounds !== prevState.rounds) {
      this.setState({
        saveCircuit: true,
      });
    }
    if (this.state.timeCompletedIn !== prevState.timeCompletedIn) {
      this.setState({
        saveCircuit: true,
      });
    }
    if (this.state.notes !== prevState.notes) {
      this.setState({
        saveNotes: true,
      });
    }
  }

  _updateDay(event) {
    this.setState({
      saveCircuit: false,
      changeName: false,
      saveNotes: false,
    });
    event.preventDefault();
    const formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("type", this.state.type);
    formData.append("rounds", this.state.rounds);
    formData.append("experience", this.state.experience);
    formData.append("time_completed_in", this.state.timeCompletedIn);
    formData.append("focus", this.state.focus);
    formData.append("goal", this.state.goal);
    formData.append("facility", this.state.facility);
    formData.append("notes", this.state.notes);
    formData.append("for_program_templates", this.state.forProgramTemplates);
    axiosAjax({
      method: "patch",
      url: "/training/training-log-day/" + this.props.match.params.id + "/",
      data: formData,
    })
      .then(response => {
        NotificationManager.success("Changes saved.");
        this._loadDay(this.props.match.params.id);
      })
      .catch(error => {
        NotificationManager.error("There was an error saving your changes.");
      });
  }

  _toggleModal() {
    $("#addExerciseModal").modal("toggle");
  }

  _toggleSupersets() {
    $("#supersets").modal("toggle");
  }

  _toggleOptions() {
    $("#dayOptions").modal("toggle");
  }

  render() {
    return (
      <React.Fragment>
        <MenuMobile
          back={true}
          linkRightOne={
            <a onClick={() => this._toggleSupersets()} className='nav-link'>
              <div className='white'>Supersets</div>
            </a>
          }
          linkRightTwo={
            <a onClick={() => this._toggleModal()} className='nav-link'>
              <div className='second-nav-bold white'>Add Exercise</div>
            </a>
          }
          linkRightThree={
            <a onClick={() => this._toggleOptions()} className='ml-2 nav-link'>
              <div className='white'>
                <i className='fas fa-ellipsis-v' />
              </div>
            </a>
          }
        />
        <div className='scroll-view'>
          <Heading
            columns={
              <React.Fragment>
                <div className='col'>
                  {this.state.changeName === true ? (
                    <form onSubmit={this._updateDay}>
                      <div className='form-row'>
                        <div className='col d-flex flex-column justify-content-center'>
                          <input
                            autoFocus='autofocus'
                            maxLength='50'
                            name='name'
                            autoComplete='off'
                            onChange={this.handleInputChange}
                            value={this.state.name}
                            style={{ backgroundColor: "white" }}
                            className='form-control form-control-rounded'
                            placeholder='Day name'
                            required
                          />
                        </div>
                        <div className='col-auto d-flex flex-column justify-content-center'>
                          <button className='btn btn-rounded btn-white-menu'>
                            <i className='fas dark-purp fa-check' />
                          </button>
                        </div>
                      </div>
                    </form>
                  ) : (
                    <h4 className='m-0'>
                      {this.state.name} &nbsp;{" "}
                      <i
                        style={{ cursor: "pointer" }}
                        onClick={() =>
                          this.setState({
                            changeName: !this.state.changeName,
                          })
                        }
                        className='fal fa-xs white fa-pen'
                      />
                    </h4>
                  )}
                </div>
                <div className='col-auto'>
                  <button
                    type='button'
                    className='btn btn-rounded btn-white-menu'
                    data-toggle='modal'
                    data-target='#supersets'
                  >
                    Supersets
                  </button>
                </div>
                <div className='col-auto'>
                  <button
                    type='button'
                    className='btn btn-rounded btn-white-menu'
                    data-toggle='modal'
                    data-target='#addExerciseModal'
                  >
                    Add Exercise
                  </button>
                </div>
                <div className='col-auto'>
                  <button
                    type='button'
                    className='btn btn-rounded btn-white-menu'
                    data-toggle='modal'
                    data-target='#dayOptions'
                  >
                    Options
                  </button>
                </div>
              </React.Fragment>
            }
          />
          <HeadingMobile
            columns={
              <div className='col'>
                {this.state.changeName === true ? (
                  <form onSubmit={this._updateDay}>
                    <div className='form-row'>
                      <div className='col d-flex flex-column justify-content-center'>
                        <input
                          autoFocus='autofocus'
                          maxLength='50'
                          name='name'
                          autoComplete='off'
                          onChange={this.handleInputChange}
                          value={this.state.name}
                          style={{ backgroundColor: "white" }}
                          className='form-control form-control-rounded'
                          placeholder='Day name'
                          required
                        />
                      </div>
                      <div className='col-auto d-flex flex-column justify-content-center'>
                        <button className='btn btn-rounded btn-white-menu'>
                          <i className='fas dark-purp fa-check' />
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <h4 className='m-0'>
                    {this.state.name} &nbsp;{" "}
                    <i
                      style={{ cursor: "pointer" }}
                      onClick={() =>
                        this.setState({
                          changeName: !this.state.changeName,
                        })
                      }
                      className='fal fa-xs white fa-pen'
                    />
                  </h4>
                )}
              </div>
            }
          />
          <div
            className={
              $(window).width() > 768 ? "container max-width-700 mt-4" : ""
            }
          >
            {this.state.type === "Circuit" && (
              <div className='content-container mb-5'>
                <form onSubmit={this._updateDay}>
                  <div className='form-row'>
                    <div className='col text-center'>
                      <input
                        style={{
                          fontSize: 30,
                          fontWeight: 900,
                          border: "none",
                          height: 50,
                          background: "transparent",
                          color: "#4a4090",
                        }}
                        className='form-control text-center'
                        type='number'
                        name='rounds'
                        value={this.state.rounds}
                        onChange={this.handleInputChange}
                      />
                      <p className='text-muted m-0 p-xs'>Rounds</p>
                    </div>
                    <div className='col text-center'>
                      <input
                        style={{
                          fontSize: 30,
                          fontWeight: 900,
                          height: 50,
                        }}
                        className='form-control text-center'
                        type='number'
                        step='any'
                        name='timeCompletedIn'
                        value={this.state.timeCompletedIn}
                        onChange={this.handleInputChange}
                      />
                      <p className='text-muted m-0 p-xs'>
                        Minutes Completed In
                      </p>
                    </div>
                  </div>
                  {this.state.saveCircuit === true && (
                    <div className='row mt-3'>
                      <div className='col'>
                        <button className='btn btn-block btn-sm btn-light btn-rounded'>
                          Save
                        </button>
                      </div>
                    </div>
                  )}
                </form>
              </div>
            )}
            {this.props.exercises !== undefined &&
            this.props.exercises.length > 0 ? (
              <form onSubmit={this._updateDay} className='mt-3 p-3'>
                <div className='form-group'>
                  <textarea
                    name='notes'
                    autoComplete='off'
                    onChange={this.handleInputChange}
                    value={this.state.notes}
                    className='form-control'
                    placeholder='Write notes here...'
                    style={{
                      border: "none",
                      padding: 0,
                      borderRadius: 0,
                      minHeight: 100,
                    }}
                  />
                </div>
                {this.state.saveNotes === true && (
                  <div className='form-group'>
                    <button className='btn btn-sm btn-light btn-block btn-rounded'>
                      Save notes
                    </button>
                  </div>
                )}
              </form>
            ) : null}
            <Exercises logId={this.props.match.params.id} />
          </div>
        </div>
        <AddExercise
          programId={this.props.match.params.id}
          exerciseOrder={
            this.props.exercises === undefined
              ? 0
              : this.props.exercises.length + 1
          }
        />
        <Supersets dayId={this.props.match.params.id} />
        <Options
          dayId={this.props.match.params.id}
          loadDay={this._loadDay}
          id={this.state.id}
          name={this.state.name}
          type={this.state.type}
          focus={this.state.focus}
          rounds={this.state.rounds}
          experience={this.state.experience}
          timeCompletedIn={this.state.timeCompletedIn}
          goal={this.state.goal}
          facility={this.state.facility}
          forProgramTemplates={this.state.forProgramTemplates}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    program: state.training.program,
    exercises: state.training.exercises,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetProgram: programId => {
      dispatch(getProgram(programId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProgramTab);
