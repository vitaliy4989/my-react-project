import React from "react";
import TrainingLogs from "../../components/exercise/myworkouts/ProgramsList";
import Requested from "../../components/exercise/personalised/Programs";
import JamesWorkouts from "../../components/exercise/JamesWorkouts/index";
import NeatUp247 from "../../components/exercise/NeatUp247";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";
import connect from "react-redux/es/connect/connect";

const TrainingTab = props => {
  return (
    <React.Fragment>
      <div className='scroll-view grey-background'>
        <Heading
          columns={
            <div className='col d-flex flex-column justify-content-center'>
              <h4>Exercise</h4>
            </div>
          }
        />
        <HeadingMobile
          columns={
            <div className='col d-flex flex-column justify-content-center'>
              <h4>Exercise</h4>
            </div>
          }
        />
        <div className='container'>
          {props.membershipType !== "Standard Plan" && <Requested />}
          <JamesWorkouts />
          <TrainingLogs />
          <NeatUp247 />
        </div>
      </div>
      <div
        className='modal fade'
        id='trainingProgramExample'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='trainingProgramExample'
        aria-hidden='true'
      >
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <button
                type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Close'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <div className='embed-responsive embed-responsive-16by9'>
                <iframe
                  className='embed-responsive-item'
                  src='https://player.vimeo.com/video/291244650'
                  width='640'
                  height='360'
                  frameBorder='0'
                  webkitallowfullscreen='true'
                  mozallowfullscreen='true'
                  allowFullScreen={true}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='jamesWorkoutsInfo'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='jamesWorkoutsInfo'
        aria-hidden='true'
      >
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title'>James' Workouts</h5>
              <button
                type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Close'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <p>
                Available on{" "}
                <span
                  style={{ color: "#DAA520" }}
                  className='text-uppercase font-weight-bold'
                >
                  Premium
                </span>{" "}
                and{" "}
                <span className='text-muted text-uppercase font-weight-bold'>
                  Standard
                </span>
              </p>
              <p>
                James' Workouts aim to provide you with new workouts. Each day,
                I personally write a new workout which you may use by adding to
                your My Workouts section.
              </p>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    membershipType: state.user.membershipType,
  };
};

export default connect(
  mapStateToProps,
  null,
)(TrainingTab);
