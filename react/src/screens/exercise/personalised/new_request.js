import React, { Component } from "react";
import Request from "../../../components/exercise/personalised/Request";
import MenuMobile from "../../../components/shared/MenuMobile";
import Heading from "../../../components/shared/Heading";
import HeadingMobile from "../../../components/shared/HeadingMobile";

const RequestTab = props => {
  return (
    <React.Fragment>
      <MenuMobile back={true} />
      <div className='scroll-view grey-background'>
        <Heading
          columns={
            <div className='col'>
              <h4>Request Personalised Program</h4>
            </div>
          }
        />
        <HeadingMobile
          columns={
            <div className='col'>
              <h4>Request Personalised Program</h4>
            </div>
          }
        />
        <div className='container'>
          <Request />
        </div>
      </div>
    </React.Fragment>
  );
};

export default RequestTab;
