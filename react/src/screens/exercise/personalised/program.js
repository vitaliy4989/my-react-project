import React, { Component } from "react";
import { connect } from "react-redux";
import { getRequestedProgram } from "../../../redux/actions/training";
import Comments from "../../../components/exercise/personalised/Comments";
import Days from "../../../components/exercise/personalised/Days";
import MenuMobile from "../../../components/shared/MenuMobile";
import MDSpinner from "react-md-spinner";
import Heading from "../../../components/shared/Heading";
import HeadingMobile from "../../../components/shared/HeadingMobile";
import { NavLink } from "react-router-dom";

class Training extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: false,
      training: true,
    };
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    this.props.onGetRequestedProgram(this.props.match.params.id);
  }

  render() {
    return (
      <React.Fragment>
        <MenuMobile backLink='/training' />
        <div className='scroll-view grey-background'>
          <Heading
            class='app-header-tabs'
            columns={
              <div className='col'>
                <div className='row'>
                  <div className='col d-flex flex-column justify-content-center'>
                    <h4 className='text-capitalize'>
                      {this.props.requestedProgram
                        ? this.props.requestedProgram.goal
                        : "Loading..."}
                    </h4>
                    <p className='m-0 p-smaller'>
                      {this.props.requestedProgram
                        ? this.props.requestedProgram.date.substring(
                            0,
                            this.props.requestedProgram.date.indexOf("T"),
                          )
                        : null}
                    </p>
                  </div>
                </div>
                <div className='row'>
                  <div className='col'>
                    <div className='option-header-wrapper'>
                      <div
                        className={
                          this.state.training === true
                            ? "option-header option-header-active"
                            : "option-header"
                        }
                        onClick={() =>
                          this.setState({
                            comments: false,
                            training: true,
                          })
                        }
                      >
                        Training
                      </div>
                      <NavLink
                        className='option-header'
                        to={
                          "/training/requested/messages/" +
                          this.props.match.params.id
                        }
                      >
                        Messages{" "}
                        {this.props.requestedProgram
                          ? this.props.requestedProgram.message_update ===
                              true && (
                              <span className='badge badge-danger'>1</span>
                            )
                          : null}
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>
            }
          />
          <HeadingMobile
            class='app-header-tabs'
            columns={
              <div className='col'>
                <div className='row'>
                  <div className='col d-flex flex-column justify-content-center'>
                    <h4 className='text-capitalize'>
                      {this.props.requestedProgram
                        ? this.props.requestedProgram.goal
                        : "Loading..."}
                    </h4>
                    <p className='m-0 p-smaller'>
                      {this.props.requestedProgram
                        ? this.props.requestedProgram.date.substring(
                            0,
                            this.props.requestedProgram.date.indexOf("T"),
                          )
                        : null}
                    </p>
                  </div>
                </div>
                <div className='row'>
                  <div className='col'>
                    <div className='option-header-wrapper'>
                      <div
                        className={
                          this.state.training === true
                            ? "option-header option-header-active"
                            : "option-header"
                        }
                        onClick={() =>
                          this.setState({
                            comments: false,
                            training: true,
                          })
                        }
                      >
                        Training
                      </div>
                      <NavLink
                        className='option-header'
                        to={
                          "/training/requested/messages/" +
                          this.props.match.params.id
                        }
                      >
                        Messages{" "}
                        {this.props.requestedProgram
                          ? this.props.requestedProgram.message_update ===
                              true && (
                              <span className='badge badge-danger'>1</span>
                            )
                          : null}
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>
            }
          />
          {this.props.requestedProgram ? (
            <React.Fragment>
              {this.state.comments === true && (
                <Comments
                  messageUpdate={this.props.requestedProgram.message_update}
                  programId={this.props.match.params.id}
                />
              )}
              {this.state.training === true && (
                <div className='container'>
                  <Days trainingId={this.props.match.params.id} />
                </div>
              )}
            </React.Fragment>
          ) : (
            <div className='h-100 d-flex flex-column justify-content-center'>
              <div className='text-center'>
                <MDSpinner singleColor='#4a4090' />
              </div>
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    requestedProgram: state.training.requestedProgram,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetRequestedProgram: programId => {
      dispatch(getRequestedProgram(programId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Training);
