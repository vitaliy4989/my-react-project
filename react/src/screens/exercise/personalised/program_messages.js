import React, { Component } from "react";
import { connect } from "react-redux";
import { getRequestedProgram } from "../../../redux/actions/training";
import Comments from "../../../components/exercise/personalised/Comments";
import { NavLink } from "react-router-dom";
import Heading from "../../../components/shared/Heading";

class Training extends Component {
  constructor(props) {
    super(props);
    window.scrollTo(0, 0);
  }

  componentDidMount() {
    if (!this.props.requestedProgram) {
      this.props.onGetRequestedProgram(this.props.match.params.id);
    }
  }

  render() {
    return (
      <React.Fragment>
        <Heading
          columns={
            <div className='col'>
              <div className='row'>
                <div className='col d-flex flex-column justify-content-center'>
                  <NavLink
                    className='white'
                    to={"/training/requested/" + this.props.match.params.id}
                  >
                    <i className='fas white p-0 m-0 fa-lg fa-chevron-left' />{" "}
                    &nbsp; Program
                  </NavLink>
                </div>
              </div>
            </div>
          }
        />
        <nav className='navbar navbar-expand second-nav for-mobile'>
          <div className='container'>
            <ul className='navbar-nav'>
              <li className='nav-item'>
                <NavLink
                  style={{ marginLeft: 0, paddingLeft: 0 }}
                  className='nav-link'
                  to={"/training/requested/" + this.props.match.params.id}
                >
                  <i className='fas white p-0 m-0 fa-lg fa-chevron-left' />{" "}
                  &nbsp; Program
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
        <Comments
          messageUpdate={
            this.props.requestedProgram !== undefined
              ? this.props.requestedProgram.message_update
              : null
          }
          programId={this.props.match.params.id}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    requestedProgram: state.training.requestedProgram,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetRequestedProgram: programId => {
      dispatch(getRequestedProgram(programId));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Training);
