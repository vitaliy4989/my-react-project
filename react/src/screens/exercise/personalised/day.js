import React, { PureComponent } from "react";
import MenuMobile from "../../../components/shared/MenuMobile";
import Exercises from "../../../components/exercise/personalised/Exercises";
import axiosAjax from "../../../config/axiosAjax";
import Heading from "../../../components/shared/Heading";
import HeadingMobile from "../../../components/shared/HeadingMobile";
import AddExercise from "../../../components/exercise/personalised/AddExercise";
import Supersets from "../../../components/exercise/personalised/Supersets/index";

class Day extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dayName: undefined,
      changeName: false,
      notes: undefined,
      dayChangeLogs: "",
      saveNotes: false,
      isLoaded: false,
      type: undefined,
      timeCompletedIn: undefined,
      rounds: undefined,
      saveCircuit: false,
    };
    this._updateDay = this._updateDay.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    window.scrollTo(0, 0);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  componentDidMount() {
    axiosAjax
      .get("/training/training-day/" + this.props.match.params.id + "/")
      .then(response => {
        this.setState(
          {
            dayName: this.cleanDayName(response.data.name),
            notes: response.data.notes ? response.data.notes : "",
            dayChangeLogs: response.data.user_change_log,
            type: response.data.type,
            rounds: response.data.rounds,
            timeCompletedIn: response.data.time_completed_in,
          },
          () => {
            this.setState({
              isLoaded: true,
              saveCircuit: false,
            });
          },
        );
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.isLoaded === true) {
      if (prevState.notes !== this.state.notes) {
        this.setState({
          saveNotes: true,
        });
      }
    }
    if (this.state.rounds !== prevState.rounds) {
      this.setState({
        saveCircuit: true,
      });
    }
    if (this.state.timeCompletedIn !== prevState.timeCompletedIn) {
      this.setState({
        saveCircuit: true,
      });
    }
  }

  cleanDayName(dayName) {
    if (dayName) {
      var s = dayName;
      var n = s.indexOf("-");
      return s.substring(0, n != -1 ? n : s.length);
    }
  }

  _updateDay(event) {
    this.setState({ changeName: false });
    event.preventDefault();
    const formData = new FormData();
    formData.append("name", this.state.dayName.replace("-", " "));
    formData.append("notes", this.state.notes);
    if (this.state.rounds) {
      formData.append("rounds", this.state.rounds);
    }
    if (this.state.timeCompletedIn) {
      formData.append("time_completed_in", this.state.timeCompletedIn);
    }
    axiosAjax({
      method: "patch",
      url: "/training/training-day/" + this.props.match.params.id + "/",
      data: formData,
    })
      .then(response => {
        this.setState({
          dayName: this.cleanDayName(response.data.name),
          saveNotes: false,
          saveCircuit: false,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  _toggleModal() {
    $("#addExerciseModal").modal("toggle");
  }

  _toggleSupersets() {
    $("#supersets").modal("toggle");
  }

  render() {
    return (
      <React.Fragment>
        <MenuMobile
          back={true}
          linkRightOne={
            <a
              href={"/training/user-training-pdf/" + this.props.match.params.id}
              className='nav-link'
            >
              <div className='white'>Print</div>
            </a>
          }
          linkRightTwo={
            <a onClick={() => this._toggleSupersets()} className='nav-link'>
              <div className='white'>Supersets</div>
            </a>
          }
          linkRightThree={
            <a onClick={() => this._toggleModal()} className='nav-link'>
              <div className='second-nav-bold white'>Add Exercise</div>
            </a>
          }
        />
        <div className='scroll-view'>
          <Heading
            columns={
              <React.Fragment>
                <div className='col'>
                  {this.state.changeName === true ? (
                    <React.Fragment>
                      <form onSubmit={this._updateDay}>
                        <div className='form-row'>
                          <div className='col d-flex flex-column justify-content-center'>
                            <input
                              autoFocus='autofocus'
                              maxLength='50'
                              name='dayName'
                              autoComplete='off'
                              onChange={this.handleInputChange}
                              value={this.state.dayName}
                              style={{ backgroundColor: "white" }}
                              className='form-control form-control-rounded'
                              placeholder='Day name'
                              required
                            />
                          </div>
                          <div className='col-auto d-flex flex-column justify-content-center'>
                            <button className='btn btn-rounded btn-white-menu'>
                              <i className='fas dark-purp fa-check' />
                            </button>
                          </div>
                        </div>
                      </form>
                      <p className='p-smaller mt-2'>
                        Please avoid using the character " - " in the day name.
                      </p>
                    </React.Fragment>
                  ) : (
                    <h4 className='m-0'>
                      {this.state.dayName} &nbsp;{" "}
                      <i
                        style={{ cursor: "pointer" }}
                        onClick={() =>
                          this.setState({
                            changeName: !this.state.changeName,
                          })
                        }
                        className='fal fa-xs white fa-pen'
                      />
                    </h4>
                  )}
                </div>
                <div className='col-auto'>
                  <a
                    href={
                      "/training/user-training-pdf/" +
                      this.props.match.params.id
                    }
                  >
                    <button className='waves-effect btn btn-rounded btn-white-menu'>
                      <i className='fas fa-print' />
                    </button>
                  </a>
                </div>
                <div className='col-auto'>
                  <button
                    type='button'
                    className='btn btn-rounded btn-white-menu'
                    data-toggle='modal'
                    data-target='#supersets'
                  >
                    Supersets
                  </button>
                </div>
                <div className='col-auto'>
                  <button
                    type='button'
                    className='btn btn-rounded btn-white-menu'
                    data-toggle='modal'
                    data-target='#addExerciseModal'
                  >
                    Add Exercise
                  </button>
                </div>
              </React.Fragment>
            }
          />
          <HeadingMobile
            columns={
              <React.Fragment>
                {this.state.changeName === true ? (
                  <div className='col'>
                    <form onSubmit={this._updateDay}>
                      <div className='form-row'>
                        <div className='col d-flex flex-column justify-content-center'>
                          <input
                            autoFocus='autofocus'
                            maxLength='50'
                            name='dayName'
                            autoComplete='off'
                            onChange={this.handleInputChange}
                            value={this.state.dayName}
                            className='form-control form-control-rounded'
                            style={{ backgroundColor: "white" }}
                            placeholder='Day name'
                            required
                          />
                        </div>
                        <div className='col-auto d-flex flex-column justify-content-center'>
                          <button className='btn btn-rounded btn-white-menu'>
                            <i className='fas dark-purp fa-check' />
                          </button>
                        </div>
                      </div>
                    </form>
                    <p className='p-smaller mt-2'>
                      Please avoid using the character " - " in the day name.
                    </p>
                  </div>
                ) : (
                  <React.Fragment>
                    <div
                      className='col'
                      style={{
                        overflow: "hidden",
                        whiteSpace: "nowrap",
                      }}
                    >
                      <h4 className='m-0'>{this.state.dayName}</h4>
                    </div>
                    <div className='col-auto'>
                      <h4 className='m-0'>
                        <i
                          style={{
                            cursor: "pointer",
                          }}
                          onClick={() =>
                            this.setState({
                              changeName: !this.state.changeName,
                            })
                          }
                          className='fal fa-xs white fa-pen'
                        />
                      </h4>
                    </div>
                  </React.Fragment>
                )}
              </React.Fragment>
            }
          />
          <div
            className={
              $(window).width() > 768 ? "container max-width-700 mt-4" : ""
            }
          >
            {this.state.type === "Circuit" && (
              <div className='content-container' style={{ border: "none" }}>
                <form onSubmit={this._updateDay}>
                  <div className='form-row'>
                    <div className='col text-center'>
                      <input
                        style={{
                          fontSize: 30,
                          fontWeight: 900,
                          border: "none",
                          height: 50,
                          background: "transparent",
                          color: "#4a4090",
                        }}
                        className='form-control text-center'
                        type='number'
                        name='rounds'
                        value={this.state.rounds}
                        onChange={this.handleInputChange}
                      />
                      <p className='text-muted m-0 p-xs'>Rounds</p>
                    </div>
                    <div className='col text-center'>
                      <input
                        style={{
                          fontSize: 30,
                          fontWeight: 900,
                          height: 50,
                        }}
                        className='form-control text-center'
                        type='number'
                        step='any'
                        name='timeCompletedIn'
                        value={this.state.timeCompletedIn}
                        onChange={this.handleInputChange}
                      />
                      <p className='text-muted m-0 p-xs'>
                        Minutes Completed In
                      </p>
                    </div>
                  </div>
                  {this.state.saveCircuit === true && (
                    <div className='row mt-3'>
                      <div className='col text-center'>
                        <button className='btn btn-sm btn-light btn-block btn-rounded'>
                          Save
                        </button>
                      </div>
                    </div>
                  )}
                </form>
              </div>
            )}
            <div className='p-3'>
              <form onSubmit={this._updateDay}>
                <div className='form-group'>
                  <textarea
                    name='notes'
                    autoComplete='off'
                    onChange={this.handleInputChange}
                    value={this.state.notes}
                    className='form-control'
                    placeholder='Write notes here...'
                    style={{
                      border: "none",
                      padding: 0,
                      borderRadius: 0,
                      minHeight: 100,
                    }}
                  />
                </div>
                {this.state.saveNotes === true && (
                  <div className='form-group'>
                    <button className='btn btn-sm btn-light btn-block btn-rounded'>
                      Save notes
                    </button>
                  </div>
                )}
              </form>
            </div>
            <Exercises
              dayId={this.props.match.params.id}
              dayChangeLogs={this.state.dayChangeLogs}
              dayType={this.state.type}
            />
          </div>
        </div>
        <AddExercise
          dayChangeLogs={this.state.dayChangeLogs}
          dayId={this.props.match.params.id}
        />
        <Supersets dayId={this.props.match.params.id} />
      </React.Fragment>
    );
  }
}

export default Day;
