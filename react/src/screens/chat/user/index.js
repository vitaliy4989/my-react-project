import React from "react";
import Comments from "../../../components/chat/user/Comments";
import Heading from "../../../components/shared/Heading";
import { NavLink } from "react-router-dom";

export default class index extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <Heading
          columns={
            <div className='col'>
              <div className='row'>
                <div className='col d-flex flex-column justify-content-center'>
                  {history.length > 1 ? (
                    <div
                      style={{ cursor: "pointer" }}
                      onClick={() => window.history.back()}
                    >
                      <i className='fas white m-0 p-0 fa-lg fa-chevron-left' />{" "}
                      &nbsp; Back
                    </div>
                  ) : (
                    <NavLink className='white' to={"/home"}>
                      <i className='fas white p-0 m-0 fa-lg fa-chevron-left' />{" "}
                      &nbsp; Home
                    </NavLink>
                  )}
                </div>
              </div>
            </div>
          }
        />
        <nav className='navbar navbar-expand second-nav for-mobile'>
          <div className='container'>
            <ul className='navbar-nav'>
              {history.length > 1 ? (
                <li className='nav-item'>
                  <div
                    style={{ marginLeft: 0, paddingLeft: 0 }}
                    className='nav-link white'
                    onClick={() => window.history.back()}
                  >
                    <i className='fas white m-0 p-0 fa-lg fa-chevron-left' />{" "}
                    &nbsp; Back
                  </div>
                </li>
              ) : (
                <li className='nav-item'>
                  <NavLink
                    style={{ marginLeft: 0, paddingLeft: 0 }}
                    className='nav-link'
                    to={"/home"}
                  >
                    <i className='fas white p-0 m-0 fa-lg fa-chevron-left' />{" "}
                    &nbsp; Home
                  </NavLink>
                </li>
              )}
            </ul>
          </div>
        </nav>
        <Comments />
      </React.Fragment>
    );
  }
}
