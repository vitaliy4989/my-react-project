import React from "react";
import cn from "classnames";
import UserProfile from "../../components/home/UserProfile";
import localforage from "localforage";
import { connect } from "react-redux";
import {
  getStepOne,
  getStepTwo,
  getStepThree,
  getStepFour,
  getStepFive,
  getStepSix,
  updateStepOne,
  updateStepTwo,
  updateStepThree,
  updateStepFour,
  updateStepFive,
  updateStepSix,
} from "../../redux/actions/home";
import MDSpinner from "react-md-spinner";
import StepsCompleted from "../../components/home/StepsCompleted";
import { stepOneProgress } from "../../Helpers/HomeStepsProgressbar";
import HomeStepCard from "../../components/HomeStepCard";

class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      howToAddPwa: false,
      dismissMessage: false,
    };
  }

  _stepOne() {
    localforage
      .getItem(sessionStorage.username + "stepOne")
      .then(value => {
        if (value === true) {
          this.props.onUpdateStepOne(value);
        } else {
          this.props.onGetStepOne();
        }
      })
      .catch(err => {
        this.props.onGetStepOne();
      });
  }

  _stepTwo() {
    localforage
      .getItem(sessionStorage.username + "stepTwo")
      .then(value => {
        if (value === true) {
          this.props.onUpdateStepTwo(value);
        } else {
          this.props.onGetStepTwo();
        }
      })
      .catch(err => {
        this.props.onGetStepTwo();
      });
  }

  _stepThree() {
    localforage
      .getItem(sessionStorage.username + "stepThree")
      .then(value => {
        if (value === true) {
          this.props.onUpdateStepThree(value);
        } else {
          this.props.onGetStepThree();
        }
      })
      .catch(err => {
        this.props.onGetStepThree();
      });
  }

  _stepFour() {
    localforage
      .getItem(sessionStorage.username + "stepFour")
      .then(value => {
        if (value === true) {
          this.props.onUpdateStepFour(value);
        } else {
          this.props.onGetStepFour();
        }
      })
      .catch(err => {
        this.props.onGetStepFour();
      });
  }

  _stepFive() {
    localforage
      .getItem(sessionStorage.username + "stepFive")
      .then(value => {
        if (value === true) {
          this.props.onUpdateStepFive(value);
        } else {
          this.props.onGetStepFive();
        }
      })
      .catch(err => {
        this.props.onGetStepFive();
      });
  }

  _stepSix() {
    localforage
      .getItem(sessionStorage.username + "stepSix")
      .then(value => {
        if (value === true) {
          this.props.onUpdateStepSix(value);
        } else {
          this.props.onGetStepSix();
        }
      })
      .catch(err => {
        this.props.onGetStepSix();
      });
  }

  _dismissMessage() {
    localforage.setItem("dismissMessage" + sessionStorage.username, true);
    this.setState({ dismissMessage: true });
  }

  _getSectionClassName(condition = true, delay = 0) {
    return cn(
      {
        "home-flex-step shadow-sm": condition,
        "home-flex-step-locked": !condition,
      },
      `fadeInUp animated-2 delay-${delay}x`,
    );
  }

  componentDidMount() {
    localforage.getItem(
      "dismissMessage" + sessionStorage.username,
      (err, value) => {
        this.setState({ dismissMessage: value });
      },
    );
    this._stepOne();
    this._stepTwo();
    this._stepThree();
    this._stepFour();
    this._stepFive();
    this._stepSix();
  }

  render() {
    const stepOneProgressValue = stepOneProgress(this.props.stepOne);
    return (
      <React.Fragment>
        <div className='scroll-view grey-background'>
          <UserProfile />
          {this.props.stepSixCompleted === undefined ? (
            <div className='h-100 d-flex flex-column justify-content-center'>
              <div className='text-center'>
                <MDSpinner singleColor='#4a4090' />
              </div>
            </div>
          ) : this.props.stepSixCompleted === true ? (
            <StepsCompleted />
          ) : (
            <div className='container' style={{ padding: 15.5 }}>
              {this.props.stepFiveCompleted !== true ? (
                this.state.dismissMessage === true ? null : (
                  <div
                    className='alert alert-light alert-dismissible fade show fadeIn animated-2 delay-3x'
                    role='alert'
                    style={{ margin: "1%" }}
                  >
                    <div>
                      Let us help you get started with the steps below. We also
                      suggest{" "}
                      <a
                        style={{ cursor: "pointer" }}
                        onClick={() =>
                          this.setState({
                            howToAddPwa: !this.state.howToAddPwa,
                          })
                        }
                      >
                        <u className='dark-purp'>adding our web app</u>
                      </a>{" "}
                      to your mobile home screen.
                    </div>
                    <button
                      type='button'
                      className='close'
                      data-dismiss='alert'
                      aria-label='Close'
                    >
                      <span aria-hidden='true'>&times;</span>
                    </button>
                    <button
                      style={{ paddingLeft: 0 }}
                      onClick={() => this._dismissMessage()}
                      className='btn btn-sm btn-link'
                    >
                      Do not show this again.
                    </button>
                  </div>
                )
              ) : (
                <h6
                  style={{ marginTop: "2%", marginBottom: "2%" }}
                  className='text-center lead'
                >
                  Home Steps
                </h6>
              )}
              <div className='home-flex'>
                <HomeStepCard
                  title='Step 1'
                  subTitle='Learn The Fundamentals'
                  isCompleted={this.props.stepOneCompleted}
                  stepProgress={stepOneProgressValue}
                  linkTo='/home/bread-butter'
                />

                <HomeStepCard
                  title='Step 2'
                  subTitle='Set An Achievable Goal'
                  isCompleted={this.props.stepTwoCompleted}
                  isUnlocked={!!this.props.stepOneCompleted}
                  stepProgress={this.props.stepTwoCompleted ? 100 : 0}
                  condition={this.props.stepOneCompleted}
                  delay={1}
                  lockedLabel={"Complete \nStep 1."}
                  linkTo='/home/goal-setting'
                />

                <HomeStepCard
                  title='Step 3'
                  subTitle='Start Exercising'
                  isCompleted={this.props.stepThreeCompleted}
                  isUnlocked={!!this.props.stepTwoCompleted}
                  stepProgress={this.props.stepThreeCompleted ? 100 : 0}
                  condition={this.props.stepTwoCompleted}
                  delay={2}
                  lockedLabel={"Complete \nStep 2."}
                  linkTo='/home/training-program'
                />

                <HomeStepCard
                  title='Step 4'
                  subTitle='Calculate Your Macros'
                  isCompleted={this.props.stepFourCompleted}
                  isUnlocked={!!this.props.stepThreeCompleted}
                  stepProgress={this.props.stepFourCompleted ? 100 : 0}
                  condition={this.props.stepThreeCompleted}
                  delay={3}
                  lockedLabel={"Complete \nStep 3."}
                  linkTo='/home/your-macros'
                />

                <HomeStepCard
                  title='Step 5'
                  subTitle='Create Some Meal Plans'
                  isCompleted={this.props.stepFiveCompleted}
                  isUnlocked={!!this.props.stepFourCompleted}
                  stepProgress={this.props.stepFiveCompleted ? 100 : 0}
                  condition={this.props.stepFourCompleted}
                  delay={4}
                  lockedLabel={"Complete \nStep 4."}
                  linkTo='/home/meal-planning'
                />

                {this.props.membershipType !== "Standard Plan" ? (
                  <HomeStepCard
                    title='Step 6'
                    subTitle='Join The JSA Community'
                    isCompleted={this.props.stepSixCompleted}
                    isUnlocked={!!this.props.stepFiveCompleted}
                    stepProgress={this.props.stepSixCompleted ? 100 : 0}
                    condition={this.props.stepFiveCompleted}
                    delay={5}
                    lockedLabel={"Complete \nStep 5."}
                    linkTo='/home/community'
                  />
                ) : null}
              </div>
            </div>
          )}
        </div>
        {this.state.howToAddPwa === true && (
          <div style={{ maxWidth: 500, margin: "auto", fontSize: 14 }}>
            <div
              style={{
                padding: 15,
                background: "white",
                border: "1px solid #eaecef",
              }}
              className='animated-2 bounceIn'
            >
              <div style={{ marginBottom: "1rem" }}>
                <i
                  onClick={() =>
                    this.setState({ howToAddPwa: !this.state.howToAddPwa })
                  }
                  style={{
                    padding: "3px 6px",
                    background: "#F1F2F4",
                    borderRadius: 50,
                    cursor: "pointer",
                  }}
                  className='fal red fa-times float-right'
                />
              </div>
              <p className='p-smaller'>
                On iOS, open the site in Safari, click{" "}
                <img className='rounded' src='/share-option.png' height='20' />{" "}
                below and then{" "}
                <img className='rounded' src='/home-button.png' height='20' />{" "}
                'Add To Home Screen'.
              </p>
              <p className='p-smaller'>
                On Android, open the site in Chrome, click the menu button and
                then 'Add to homescreen'.
              </p>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stepOne: state.home.stepOne,
    stepTwo: state.home.stepTwo,
    stepThree: state.home.stepThree,
    stepFour: state.home.stepFour,
    stepFive: state.home.stepFive,
    stepSix: state.home.stepSix,

    stepOneCompleted: state.home.stepOneCompleted,
    stepTwoCompleted: state.home.stepTwoCompleted,
    stepThreeCompleted: state.home.stepThreeCompleted,
    stepFourCompleted: state.home.stepFourCompleted,
    stepFiveCompleted: state.home.stepFiveCompleted,
    stepSixCompleted: state.home.stepSixCompleted,

    membershipType: state.user.membershipType,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetStepOne: () => {
      dispatch(getStepOne());
    },
    onGetStepTwo: () => {
      dispatch(getStepTwo());
    },
    onGetStepThree: () => {
      dispatch(getStepThree());
    },
    onGetStepFour: () => {
      dispatch(getStepFour());
    },
    onGetStepFive: () => {
      dispatch(getStepFive());
    },
    onGetStepSix: () => {
      dispatch(getStepSix());
    },
    onUpdateStepOne: value => {
      dispatch(updateStepOne(this.props.stepOne, value));
    },
    onUpdateStepTwo: value => {
      dispatch(updateStepTwo(this.props.stepTwo, value));
    },
    onUpdateStepThree: value => {
      dispatch(updateStepThree(this.props.stepThree, value));
    },
    onUpdateStepFour: value => {
      dispatch(updateStepFour(this.props.stepFour, value));
    },
    onUpdateStepFive: value => {
      dispatch(updateStepFive(this.props.stepFive, value));
    },
    onUpdateStepSix: value => {
      dispatch(updateStepSix(this.peops.stepSix, value));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(index);
