import React from "react";
import Menu from "../../components/recipes/Menu";
import MenuButton from "../../components/recipes/MenuButton";
import MenuMobile from "../../components/shared/MenuMobile";
import New from "../../components/recipes/search/New";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const NewRecipesTab = props => {
  return (
    <React.Fragment>
      <MenuMobile back={true} />
      <div className='scroll-view grey-background'>
        <Heading
          columns={
            <React.Fragment>
              <div className='col'>
                <h4>New</h4>
              </div>
              <div className='col-auto d-flex flex-column justify-content-center'>
                <MenuButton />
              </div>
            </React.Fragment>
          }
        />
        <HeadingMobile
          columns={
            <div className='col'>
              <h4>New</h4>
            </div>
          }
        />
        <New />
      </div>
      <Menu />
    </React.Fragment>
  );
};

export default NewRecipesTab;
