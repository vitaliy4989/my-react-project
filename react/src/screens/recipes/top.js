import React from "react";
import Menu from "../../components/recipes/Menu";
import MenuButton from "../../components/recipes/MenuButton";
import MenuMobile from "../../components/shared/MenuMobile";
import Top from "../../components/recipes/search/Top";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const FeaturedRecipesTab = props => {
  return (
    <React.Fragment>
      <MenuMobile back={true} />
      <div className='scroll-view grey-background'>
        <Heading
          columns={
            <React.Fragment>
              <div className='col'>
                <h4>Top</h4>
              </div>
              <div className='col-auto d-flex flex-column justify-content-center'>
                <MenuButton />
              </div>
            </React.Fragment>
          }
        />
        <HeadingMobile
          columns={
            <div className='col'>
              <h4>Top</h4>
            </div>
          }
        />
        <Top />
      </div>
      <Menu />
    </React.Fragment>
  );
};

export default FeaturedRecipesTab;
