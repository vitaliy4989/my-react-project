import React from "react";
import MenuMobile from "../../components/shared/MenuMobile";
import Favourites from "../../components/recipes/search/Favourites";
import Menu from "../../components/recipes/Menu";
import MenuButton from "../../components/recipes/MenuButton";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const FavouritesTab = props => {
  return (
    <React.Fragment>
      <MenuMobile back={true} />
      <div className='scroll-view grey-background'>
        <Heading
          class='m-b-25'
          columns={
            <React.Fragment>
              <div className='col'>
                <h4>Favourites</h4>
              </div>
              <div className='col-auto d-flex flex-column justify-content-center'>
                <MenuButton />
              </div>
            </React.Fragment>
          }
        />
        <HeadingMobile
          class='m-b-25'
          columns={
            <div className='col'>
              <h4>Favourites</h4>
            </div>
          }
        />
        <Favourites />
      </div>
      <Menu />
    </React.Fragment>
  );
};

export default FavouritesTab;
