import React, { Component } from "react";
import MenuMobile from "../../components/shared/MenuMobile";
import CreateMeal from "../../components/nutrition/logs/CreateMeal";
import axiosAjax from "../../config/axiosAjax";
import ReactStars from "react-stars";
import { connect } from "react-redux";
import { getDays } from "../../redux/actions/nutrition";
import MDSpinner from "react-md-spinner";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";
import AddRecipeToMealPlan from "../../components/recipes/AddRecipeToMealPlan";

class Recipe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      days: undefined,
      dayId: "",
      recipe: undefined,
      average: "",
      addMeal: false,
      sending: false,
      favourite: false,
      combinations: undefined,
    };
    this.rateRecipe = this.rateRecipe.bind(this);
    this.markFavourite = this.markFavourite.bind(this);
    window.scrollTo(0, 0);
  }

  _loadRecipe() {
    axiosAjax
      .get("/recipes/recipe/" + this.props.match.params.id)
      .then(response => {
        this.setState({
          recipe: response.data.recipe,
          average: response.data.recipe.average,
          favourite: response.data.favourite,
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  componentDidMount() {
    this._loadRecipe();
  }

  approveRecipe() {
    const formData = new FormData();
    formData.append("approved", true);
    axiosAjax({
      method: "patch",
      url: "/recipes/recipe/" + this.state.recipe.id + "/",
      data: formData,
    })
      .then(response => {
        alert("Approved!");
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  markFavourite() {
    this.setState({ favourite: true });
    const formData = new FormData();
    formData.append("recipe", this.state.recipe.id);
    axiosAjax({
      method: "post",
      url: "/recipes/favourite/",
      data: formData,
    })
      .then(response => {
        this.setState({ favourite: true });
      })
      .catch(error => {
        this.setState({ favourite: false });
      });
  }

  deleteFavourite() {
    this.setState({ favourite: false });
    axiosAjax
      .delete("/recipes/favourite/" + this.state.recipe.id + "/")
      .then(response => {
        this.setState({
          favourite: false,
        });
      })
      .catch(error => {
        this.setState({
          favourite: true,
        });
      });
  }

  rateRecipe(newRating) {
    const formData = new FormData();
    formData.append("rating", newRating);
    formData.append("recipe_id", this.state.recipe.id);
    axiosAjax({
      method: "post",
      url: "/recipes/rate/",
      data: formData,
    })
      .then(response => {
        this.setState({
          average: response.data.average,
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  _getMealPlans() {
    axiosAjax.get("/recipes/combination/").then(response => {
      this.setState({
        combinations: response.data,
      });
    });
  }

  _addToFoodLogs() {
    this.setState(
      {
        addMeal: !this.state.addMeal,
      },
      () => {
        if (this.state.addMeal === true) {
          this.props.onGetDays();
          this._getMealPlans();
        }
      },
    );
  }

  render() {
    return (
      <React.Fragment>
        <MenuMobile back={true} />
        {this.state.recipe !== undefined ? (
          <div className='scroll-view'>
            <Heading
              columns={
                <div className='col d-flex flex-column justify-content-center'>
                  <h4>
                    {this.state.recipe !== undefined && this.state.recipe.name}
                  </h4>
                </div>
              }
            />
            <HeadingMobile
              columns={
                <div className='col d-flex flex-column justify-content-center'>
                  <h4>
                    {this.state.recipe !== undefined && this.state.recipe.name}
                  </h4>
                </div>
              }
            />
            <div className={$(window).width() > 768 ? "container" : ""}>
              <div className='row'>
                <div className='col-lg-6'>
                  <img
                    className='img-fluid'
                    style={
                      $(window).width() > 768
                        ? { marginTop: 30, borderRadius: 8 }
                        : {}
                    }
                    src={this.state.recipe.photo + "-/autorotate/yes/"}
                  />
                  <div style={{ padding: 15 }}>
                    <div className='row' style={{ marginTop: 30 }}>
                      <div className='col-4'>
                        <button
                          type='button'
                          className=' btn btn-rounded btn-block btn-light'
                          onClick={() => this._addToFoodLogs()}
                        >
                          {this.state.addMeal === true ? (
                            <i className='far fa-lg fa-times' />
                          ) : (
                            <i className='far fa-lg fa-plus' />
                          )}
                        </button>
                      </div>
                      <div className='col-4'>
                        {this.state.favourite === true ? (
                          <button
                            onClick={() => this.deleteFavourite()}
                            type='button'
                            className=' btn-rounded btn btn-block btn-light'
                          >
                            <div>
                              {this.state.sending === true ? (
                                <i className='fas fa-lg fa-circle-notch fa-spin' />
                              ) : (
                                <i className='fas fa-lg fa-heart' />
                              )}
                            </div>
                          </button>
                        ) : (
                          <button
                            onClick={() => this.markFavourite()}
                            type='button'
                            className=' btn-rounded btn btn-block btn-light'
                          >
                            <div>
                              {this.state.sending === true ? (
                                <i className='fas fa-circle-notch fa-spin' />
                              ) : (
                                <i className='far fa-lg fa-heart' />
                              )}
                            </div>
                          </button>
                        )}
                      </div>
                      <div className='col-4'>
                        <a href={"/recipes/recipe-pdf/" + this.state.recipe.id}>
                          <button
                            type='button'
                            className=' btn btn-rounded btn-block btn-light'
                          >
                            <i className='far fa-lg fa-print' />
                          </button>
                        </a>
                      </div>
                    </div>
                    {this.state.addMeal === true ? (
                      <React.Fragment>
                        {this.state.combinations !== undefined ? (
                          this.state.combinations.length > 0 ? (
                            <div style={listGroupScroll} className='mt-4'>
                              <div className='list-group list-group-flush'>
                                <div className='list-group-item list-group-heading'>
                                  <h6 className='no-margin-padding'>
                                    Meal plans
                                  </h6>
                                </div>
                                {this.state.combinations.map(
                                  (combination, key) => (
                                    <div
                                      key={key}
                                      className='list-group-item d-flex justify-content-between align-items-center'
                                    >
                                      {combination.name}
                                      <AddRecipeToMealPlan
                                        combinationId={combination.id}
                                        recipeId={this.state.id}
                                      />
                                    </div>
                                  ),
                                )}
                              </div>
                            </div>
                          ) : (
                            <div className='mt-4 p-smaller'>
                              <h6 className='no-margin-padding'>Meal plans</h6>
                              You may add this recipe to your meal plans. First
                              you need to create a meal plan{" "}
                              <a href='/users/#/nutrition/meal-planner'>here</a>
                              .
                            </div>
                          )
                        ) : (
                          <div
                            className='text-center mt-4'
                            style={{ paddingTop: "5%", paddingBottom: "5%" }}
                          >
                            <MDSpinner singleColor='#4a4090' />
                          </div>
                        )}
                      </React.Fragment>
                    ) : null}
                  </div>
                </div>
                <div className='col-lg-6'>
                  <div style={{ padding: 15 }} className='mt-2'>
                    <div style={{ marginBottom: "1rem" }}>
                      <h6>Nutrition</h6>
                      <div className='row' style={{ marginBottom: ".5rem" }}>
                        <div className='col-3'>
                          <p className='m-0'>{this.state.recipe.calories}</p>
                          <div className='text-muted' style={{ fontSize: 10 }}>
                            Calories
                          </div>
                        </div>
                        <div className='col-3'>
                          <p className='m-0'>{this.state.recipe.protein}g</p>
                          <div className='text-muted' style={{ fontSize: 10 }}>
                            Protein
                          </div>
                        </div>
                        <div className='col-3'>
                          <p className='m-0'>{this.state.recipe.carbs}g</p>
                          <div className='text-muted' style={{ fontSize: 10 }}>
                            Carbs
                          </div>
                        </div>
                        <div className='col-3'>
                          <p className='m-0'>{this.state.recipe.fat}g</p>
                          <div className='text-muted' style={{ fontSize: 10 }}>
                            Fat
                          </div>
                        </div>
                      </div>
                      <div className='row'>
                        <div className='col'>
                          <p className='p-smaller text-muted'>
                            Per {this.state.recipe.servings}{" "}
                            {this.state.recipe.servings === 1
                              ? "serving"
                              : "servings"}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div style={{ marginBottom: "1rem" }}>
                      <h6>Description</h6>
                      <p style={textareaStyle}>
                        {this.state.recipe.description}
                      </p>
                    </div>
                    {this.state.recipe.ingredients !== null && (
                      <div style={{ marginBottom: "1rem" }}>
                        <h6>Ingredients</h6>
                        <p style={textareaStyle}>
                          {this.state.recipe.ingredients}
                        </p>
                      </div>
                    )}
                    {this.state.recipe.method !== null && (
                      <div style={{ marginBottom: "1rem" }}>
                        <h6>Method</h6>
                        <p style={textareaStyle}>{this.state.recipe.method}</p>
                      </div>
                    )}
                    <div style={{ marginBottom: 15 }}>
                      <h6>Rating</h6>
                      <ReactStars
                        count={5}
                        value={this.state.average}
                        onChange={this.rateRecipe}
                        size={16}
                        half={true}
                        color2={"#ffd700"}
                      />
                    </div>
                    <p className='m-0'>
                      <strong>From</strong> {this.state.recipe.username}
                    </p>
                  </div>
                  {this.props.username === "jamesshaw" && (
                    <div className='text-center' style={{ marginTop: "1rem" }}>
                      <button
                        onClick={() => this.approveRecipe()}
                        className='btn btn-primary btn-rounded'
                      >
                        Approve Recipe
                      </button>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className='h-100 d-flex flex-column justify-content-center'>
            <div className='text-center'>
              <MDSpinner singleColor='#4a4090' />
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

const listGroupScroll = {
  height: 200,
  overflowX: "hidden",
  overflowY: "scroll",
};

const textareaStyle = {
  whiteSpace: "pre-wrap",
};

const mapStateToProps = (state, ownProps) => {
  return {
    days: state.nutrition.days,
    username: state.user.username,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetDays: () => {
      dispatch(getDays());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Recipe);
