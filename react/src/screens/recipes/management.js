import React, { Component } from "react";
import Menu from "../../components/recipes/Menu";
import MenuButton from "../../components/recipes/MenuButton";
import MenuMobile from "../../components/shared/MenuMobile";
import CreateRecipe from "../../components/recipes/CreateRecipe";
import MyRecipes from "../../components/recipes/MyRecipes";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

class Management extends Component {
  constructor(props) {
    super(props);
    this.state = {
      create: true,
      myRecipes: false,
    };
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <React.Fragment>
        <MenuMobile back={true} />
        <div className='scroll-view grey-background'>
          <Heading
            class='for-desktop'
            columns={
              <React.Fragment>
                <div className='col'>
                  <h4>My Recipes</h4>
                </div>
                <div className='col-auto d-flex flex-column justify-content-center'>
                  <MenuButton />
                </div>
              </React.Fragment>
            }
          />
          <HeadingMobile
            class='m-b-25'
            columns={
              <div className='col'>
                <h4>My Recipes</h4>
              </div>
            }
          />
          <div className='container'>
            <div className='content-container'>
              <div className='text-center'>
                <div
                  className='btn-group'
                  role='group'
                  aria-label='Basic example'
                >
                  <button
                    onClick={() =>
                      this.setState({ create: true, myRecipes: false })
                    }
                    type='button'
                    className={
                      this.state.create === true
                        ? "waves-effect btn btn-primary"
                        : "waves-effect btn btn-light"
                    }
                  >
                    Create Recipe
                  </button>
                  <button
                    onClick={() =>
                      this.setState({ myRecipes: true, create: false })
                    }
                    type='button'
                    className={
                      this.state.create === true
                        ? "waves-effect btn btn-light"
                        : "waves-effect btn btn-primary"
                    }
                  >
                    My Recipes
                  </button>
                </div>
              </div>
            </div>
            {this.state.create === true && <CreateRecipe />}
            {this.state.myRecipes === true && <MyRecipes />}
          </div>
        </div>
        <Menu />
      </React.Fragment>
    );
  }
}

export default Management;
