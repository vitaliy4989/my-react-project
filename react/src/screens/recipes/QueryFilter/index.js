import React from "react";
import QueryFilter from "../../../components/recipes/QueryFilter/index";
import Filter from "../../../components/recipes/Filter/index";
import Menu from "../../../components/recipes/Menu";
import { NavLink } from "react-router-dom";
import MenuButton from "../../../components/recipes/MenuButton";
import MenuMobile from "../../../components/shared/MenuMobile";
import Heading from "../../../components/shared/Heading";
import HeadingMobile from "../../../components/shared/HeadingMobile";

export default class index extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <MenuMobile
          dropdown={true}
          dropDownTitle='Other Filters'
          dropDownOne={
            <NavLink className='dropdown-item' to='/recipes/filter/featured'>
              Top
            </NavLink>
          }
          dropDownTwo={
            <NavLink className='dropdown-item' to='/recipes/filter/new'>
              New
            </NavLink>
          }
          dropDownThree={
            <NavLink className='dropdown-item' to='/recipes/filter/favourite'>
              Favourite
            </NavLink>
          }
          dropDownFour={
            <NavLink className='dropdown-item' to='/recipes/recipes-my'>
              My Recipes
            </NavLink>
          }
        />
        <div className='scroll-view grey-background'>
          <Heading
            class='app-header'
            columns={
              <div className='col'>
                <div className='row'>
                  <div className='col d-flex flex-column justify-content-center'>
                    <h4>Recipes Search</h4>
                  </div>
                  <div className='col-auto d-flex flex-column justify-content-center'>
                    <MenuButton />
                  </div>
                </div>
              </div>
            }
          />
          <HeadingMobile
            class='app-header'
            columns={
              <div className='col'>
                <div className='row'>
                  <div className='col d-flex flex-column justify-content-center'>
                    <h4>Recipes Search</h4>
                  </div>
                </div>
              </div>
            }
          />
          <Filter />
          <QueryFilter
            name={this.props.match.params.name}
            type={this.props.match.params.type}
            calories={this.props.match.params.calories}
            protein={this.props.match.params.protein}
          />
        </div>
        <Menu />
      </React.Fragment>
    );
  }
}
