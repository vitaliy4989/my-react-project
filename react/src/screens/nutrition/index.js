import React from "react";
import MacrosCalories from "../../components/nutrition/MacrosCalories";
import MealPlans from "../../components/nutrition/meals/MealPlans";
import Days from "../../components/nutrition/logs/Days";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const NutritionTab = props => {
  return (
    <div className='scroll-view grey-background'>
      <HeadingMobile
        columns={
          <div className='col d-flex flex-column justify-content-center'>
            <h4>Nutrition</h4>
          </div>
        }
      />
      <Heading
        columns={
          <div className='col d-flex flex-column justify-content-center'>
            <h4>Nutrition</h4>
          </div>
        }
      />

      <MacrosCalories />
      <div className='container'>
        <MealPlans />
        {sessionStorage.foodLogs === "true" && <Days />}
      </div>
    </div>
  );
};

export default NutritionTab;
