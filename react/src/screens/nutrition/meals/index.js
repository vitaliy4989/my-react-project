import React from "react";
import MenuMobile from "../../../components/shared/MenuMobile";
import Search from "../../../components/nutrition/meals/Search/index";
import Heading from "../../../components/shared/Heading";
import HeadingMobile from "../../../components/shared/HeadingMobile";

const Combinations = props => {
  return (
    <React.Fragment>
      <MenuMobile back={true} />
      <div className='scroll-view grey-background'>
        <Heading
          columns={
            <div className='col d-flex flex-column justify-content-center'>
              <h4 className='m-0'>Meal Generator</h4>
              <p className='m-0 white'>Randomly generated meal plans.</p>
            </div>
          }
        />
        <HeadingMobile
          columns={
            <div className='col d-flex flex-column justify-content-center'>
              <h4 className='m-0'>Meal Generator</h4>
              <p className='m-0 white'>Randomly generated meal plans.</p>
            </div>
          }
        />
        <Search />
      </div>
    </React.Fragment>
  );
};

export default Combinations;
