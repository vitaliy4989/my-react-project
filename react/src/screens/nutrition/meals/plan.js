import React from "react";
import Plan from "../../../components/nutrition/meals/planner/Plan";

const PlannedDay = props => {
  return <Plan id={props.match.params.id} />;
};

export default PlannedDay;
