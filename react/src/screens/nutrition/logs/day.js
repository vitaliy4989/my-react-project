import React from "react";
import Day from "../../../components/nutrition/logs/Day";

const TrackingDayTab = props => {
  return <Day id={props.match.params.id} />;
};

export default TrackingDayTab;
