import React from "react";
import MenuMobile from "../../components/shared/MenuMobile";
import Calculator from "../../components/nutrition/Calculator";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const CalculatorTab = props => {
  return (
    <React.Fragment>
      <MenuMobile back={true} backLink='/nutrition' />
      <div className='scroll-view' id='scroll-top'>
        <Heading
          columns={
            <div className='col d-flex flex-column justify-content-center'>
              <h4>Macro Calculator</h4>
            </div>
          }
        />
        <HeadingMobile
          columns={
            <div className='col d-flex flex-column justify-content-center'>
              <h4>Macro Calculator</h4>
            </div>
          }
        />
        <div className='container'>
          <Calculator />
        </div>
      </div>
    </React.Fragment>
  );
};

export default CalculatorTab;
