import React from "react";
import Chart from "../../components/progress/charts/Chart";
import MenuMobile from "../../components/shared/MenuMobile";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const ChartTab = props => {
  return (
    <React.Fragment>
      <MenuMobile back={true} />
      <div className='scroll-view grey-background'>
        <Heading
          columns={
            <div className='col flex-column justify-content-center'>
              <h4 className='text-capitalize'>{props.match.params.name}</h4>
            </div>
          }
        />
        <HeadingMobile
          columns={
            <div className='col flex-column justify-content-center'>
              <h4 className='text-capitalize'>{props.match.params.name}</h4>
            </div>
          }
        />
        <Chart name={props.match.params.name} />
      </div>
    </React.Fragment>
  );
};

export default ChartTab;
