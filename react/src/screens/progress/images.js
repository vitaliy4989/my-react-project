import React from "react";
import Images from "../../components/progress/images/Images";
import MenuMobile from "../../components/shared/MenuMobile";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";

const ImagesTab = props => {
  return (
    <React.Fragment>
      <MenuMobile back={true} />
      <div className='scroll-view grey-background'>
        <Heading
          columns={
            <div className='col flex-column justify-content-center'>
              <h4>Images</h4>
            </div>
          }
        />
        <HeadingMobile
          columns={
            <div className='col flex-column justify-content-center'>
              <h4>Images</h4>
            </div>
          }
        />
        <Images />
      </div>
    </React.Fragment>
  );
};

export default ImagesTab;
