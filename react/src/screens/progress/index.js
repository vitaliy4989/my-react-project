import React, { Component } from "react";
import MainGoal from "../../components/progress/goals/MainGoal";
import Measurements from "../../components/progress/charts/Measurements";
import Images from "../../components/progress/images/Images";
import WeeklyCheckins from "../../components/progress/goals/WeeklyCheckins";
import Heading from "../../components/shared/Heading";
import HeadingMobile from "../../components/shared/HeadingMobile";
import connect from "react-redux/es/connect/connect";

const Progress = props => {
  return (
    <div className='scroll-view grey-background'>
      <Heading
        columns={
          <div className='col flex-column justify-content-center'>
            <h4>Progress</h4>
          </div>
        }
      />
      <HeadingMobile
        columns={
          <div className='col flex-column justify-content-center'>
            <h4>Progress</h4>
          </div>
        }
      />
      <div className='container'>
        <MainGoal />
        <Measurements />
        <Images progressTab={true} />
        {props.membershipType !== "Standard Plan" && <WeeklyCheckins />}
      </div>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    membershipType: state.user.membershipType,
  };
};

export default connect(
  mapStateToProps,
  null,
)(Progress);
