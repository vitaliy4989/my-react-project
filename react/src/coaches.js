import React from "react";
import ReactDOM from "react-dom";
import App from "./components/coaches/App";

import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<App />, document.getElementById("coach-home"));

registerServiceWorker();
