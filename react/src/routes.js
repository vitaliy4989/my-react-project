import React, { Component } from "react";
import { HashRouter, Redirect, Route, Switch } from "react-router-dom";
import { getUser } from "./redux/actions/user";
import MainNav from "./components/shared/MainNav";
import Home from "./screens/home";
import TrainerSupport from "./screens/chat/user";
import StepOne from "./screens/home/StepOne";
import StepTwo from "./screens/home/StepTwo";
import StepThree from "./screens/home/StepThree";
import StepFour from "./screens/home/StepFour";
import StepFive from "./screens/home/StepFive";
import StepSix from "./screens/home/StepSix";
import NutritionTab from "./screens/nutrition";
import CalculatorTab from "./screens/nutrition/calculator";
import TrackingDayTab from "./screens/nutrition/logs/day";
import RecipesFavouritesTab from "./screens/recipes/favourites";
import RecipesNewTab from "./screens/recipes/new";
import RecipesFeaturedTab from "./screens/recipes/top";
import RecipeManagementTab from "./screens/recipes/management";
import Recipe from "./screens/recipes/recipe";
import QueryFilter from "./screens/recipes/QueryFilter";
import CombinationsTab from "./screens/nutrition/meals";
import PlannedDayTab from "./screens/nutrition/meals/plan";
import PlannerCombinationsTab from "./screens/nutrition/meals/planner";
import TrainingTab from "./screens/exercise";
import RequestedTraining from "./screens/exercise/personalised/program";
import RequestedTrainingMessages from "./screens/exercise/personalised/program_messages";
import TrainingDay from "./screens/exercise/personalised/day";
import TrainingLogTab from "./screens/exercise/myworkouts/training_day";
import RequestTab from "./screens/exercise/personalised/new_request";
import ProgressTab from "./screens/progress";
import ImagesTab from "./screens/progress/images";
import ChartTab from "./screens/progress/chart";
import VideosTab from "./screens/videos";
import CategoryTab from "./screens/videos/category";
import UncompletedTab from "./screens/videos/uncompleted";
import FavouriteTab from "./screens/videos/favourite";
import Video from "./screens/videos/video";
import ChallengeTab from "./screens/challenge";
import ChallengeEntryTab from "./screens/challenge/entry";
import ChallengeDataTab from "./screens/challenge/challenge_data";
import FAQs from "./screens/faqs";
import MainNavMobile from "./components/shared/MainNavMobile";
import ChatNotifications from "./components/chat/ChatNotifications";
import { NotificationContainer } from "react-notifications";
import StartTrial from "./screens/home/StartTrial";
import connect from "react-redux/es/connect/connect";

class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      standalone: false,
    };
  }

  componentDidMount() {
    if (
      window.matchMedia("(display-mode: standalone)").matches ||
      window.navigator.standalone === true
    ) {
      this.setState({ standalone: true });
    }
    setInterval(() => {
      this.props.onGetUser();
    }, 3600000);
  }

  render() {
    if (
      this.props.membershipStatus === "active" ||
      this.props.membershipStatus === "trialing"
    ) {
      if (this.props.membershipType !== "JSA Core") {
        return (
          <HashRouter basename='/'>
            <React.Fragment>
              {$(window).width() > 768 && <MainNav />}
              <Route
                exact
                path='/'
                render={() => (
                  <Redirect
                    to={
                      this.props.membershipType === "Standard Plan"
                        ? "/training"
                        : "/home"
                    }
                  />
                )}
              />

              <Route exact path='/home' component={Home} />

              <Route exact path='/trainer-support' component={TrainerSupport} />

              <Route path='/home/bread-butter' component={StepOne} />
              <Route path='/home/goal-setting' component={StepTwo} />
              <Route path='/home/training-program' component={StepThree} />
              <Route path='/home/your-macros' component={StepFour} />
              <Route path='/home/meal-planning' component={StepFive} />
              <Route path='/home/community' component={StepSix} />

              <Route exact path='/nutrition/' component={NutritionTab} />
              <Route path='/nutrition/calculator' component={CalculatorTab} />
              <Route
                path='/nutrition/logging/day/:id'
                component={TrackingDayTab}
              />

              <Route
                exact
                path='/recipes/'
                render={() => <Redirect to='/recipes/query' />}
              />
              <Route
                path='/recipes/filter/favourite'
                component={RecipesFavouritesTab}
              />
              <Route path='/recipes/filter/new' component={RecipesNewTab} />
              <Route
                path='/recipes/filter/featured'
                component={RecipesFeaturedTab}
              />
              <Route
                path='/recipes/recipes-my'
                component={RecipeManagementTab}
              />
              <Route path='/recipes/recipe/:id' component={Recipe} />

              <Route
                path='/recipes/query/:name?/:type?/:protein?/:calories?/'
                component={QueryFilter}
              />

              <Route
                path='/nutrition/recipes-combinations'
                component={CombinationsTab}
              />
              <Route
                path='/nutrition/planned-meals-saved/:id'
                component={PlannedDayTab}
              />
              <Route
                path='/nutrition/meal-planner'
                component={PlannerCombinationsTab}
              />

              <Route exact path='/training' component={TrainingTab} />
              <Route
                exact
                path='/training/requested/:id'
                component={RequestedTraining}
              />
              <Route
                exact
                path='/training/requested/messages/:id'
                component={RequestedTrainingMessages}
              />

              <Route
                path='/training/requested/program/:id'
                component={TrainingDay}
              />
              <Route path='/training/created/:id' component={TrainingLogTab} />
              <Route path='/training/request' component={RequestTab} />

              <Route exact path='/progress/' component={ProgressTab} />
              <Route path='/progress/images' component={ImagesTab} />
              <Route path='/progress/chart/:name' component={ChartTab} />

              <Route exact path='/coaching/modules/' component={VideosTab} />
              <Route
                path='/coaching/modules/filter/:filter/:id'
                component={CategoryTab}
              />
              <Route
                path='/coaching/modules/uncompleted/'
                component={UncompletedTab}
              />
              <Route
                path='/coaching/modules/favourite/'
                component={FavouriteTab}
              />
              <Route path='/coaching/modules/module/:id' component={Video} />

              <Route exact path='/challenge' component={ChallengeTab} />
              <Route
                path='/challenge/entry/:id'
                component={ChallengeEntryTab}
              />
              <Route path='/challenge/data/:id' component={ChallengeDataTab} />

              <Route path='/frequently-asked-questions' component={FAQs} />

              {$(window).width() <= 768 ? (
                <MainNavMobile />
              ) : this.props.membershipType !== "Standard Plan" ? (
                <ChatNotifications />
              ) : null}

              <NotificationContainer />
            </React.Fragment>
          </HashRouter>
        );
      } else {
        return (
          <HashRouter basename='/'>
            <React.Fragment>
              {$(window).width() > 768 && <MainNav />}

              <Route exact path='/trainer-support' component={TrainerSupport} />

              <Route
                exact
                path='/'
                render={() => <Redirect to='/coaching/modules/' />}
              />

              <Route exact path='/coaching/modules/' component={VideosTab} />
              <Route
                path='/coaching/modules/filter/:filter/:id'
                component={CategoryTab}
              />
              <Route
                path='/coaching/modules/uncompleted/'
                component={UncompletedTab}
              />
              <Route
                path='/coaching/modules/favourite/'
                component={FavouriteTab}
              />
              <Route path='/coaching/modules/module/:id' component={Video} />

              <Route exact path='/training' component={TrainingTab} />
              <Route
                exact
                path='/training/requested/:id'
                component={RequestedTraining}
              />
              <Route
                exact
                path='/training/requested/messages/:id'
                component={RequestedTrainingMessages}
              />
              <Route
                path='/training/requested/program/:id'
                component={TrainingDay}
              />
              <Route path='/training/created/:id' component={TrainingLogTab} />
              <Route path='/training/request' component={RequestTab} />

              <Route exact path='/progress/' component={ProgressTab} />
              <Route path='/progress/images' component={ImagesTab} />
              <Route path='/progress/chart/:name' component={ChartTab} />

              <Route path='/frequently-asked-questions' component={FAQs} />

              {$(window).width() <= 768 ? (
                <MainNavMobile />
              ) : (
                <ChatNotifications />
              )}

              <NotificationContainer />
            </React.Fragment>
          </HashRouter>
        );
      }
    } else {
      return (
        <HashRouter basename='/'>
          <React.Fragment>
            {$(window).width() > 768 && <MainNav />}
            <Switch>
              <Route path='/home' component={StartTrial} />
              <Switch>
                <Redirect to='/home' />
                <Route path='/home' component={StartTrial} />
              </Switch>
              <Route path='*' render={() => <Redirect to='/home' />} />
            </Switch>
            {$(window).width() <= 768 && <MainNavMobile />}
          </React.Fragment>
        </HashRouter>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    membershipStatus: state.user.membershipStatus,
    membershipType: state.user.membershipType,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetUser: () => {
      dispatch(getUser());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Routes);
