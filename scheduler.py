import os
from django.core.wsgi import get_wsgi_application
from django.core.cache.backends.memcached import BaseMemcachedCache

BaseMemcachedCache.close = lambda self, **kwargs: None
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "academy.settings")
application = get_wsgi_application()

from transactions.views import plan_ending
from progress.views import goal_check_in

goal_check_in()
plan_ending()
