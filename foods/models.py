from django.db import models
from django.contrib.auth.models import User
from pyuploadcare.dj.models import FileField, ImageField


class Food(models.Model):
	name = models.CharField(max_length=250, null=True, db_index=True)
	source = models.TextField(null=True)
	food_type = models.CharField(max_length=250, null=True)
	protein = models.FloatField(null=True, default=0)
	carbs = models.FloatField(null=True, default=0)
	fat = models.FloatField(null=True, default=0)
	calories = models.FloatField(null=True, default=0)
	serving_size = models.CharField(max_length=250, null=True)

	def __str__(self):
		return self.name



