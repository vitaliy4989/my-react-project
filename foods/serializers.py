from rest_framework import serializers
from .models import Food
from users.serializers import UserSerializer, ProfileSerializer


class FoodSerializer(serializers.ModelSerializer):
	class Meta:
		model = Food
		fields = '__all__'





