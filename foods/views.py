from .serializers import FoodSerializer
from .models import Food
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet


class FoodViewSet(ViewSet):

    def list(self, request):
        foods = Food.objects.filter(name__icontains=request.GET.get('search'))[:50]
        serializer = FoodSerializer(foods, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = FoodSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)
