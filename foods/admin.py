from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Food


@admin.register(Food)
class FoodAdmin(ImportExportModelAdmin):
    pass

