from django.urls import path, include
from django.contrib import admin
from django.views.generic import TemplateView
from rest_framework.documentation import include_docs_urls
from rest_framework import permissions

urlpatterns = [

    path('', TemplateView.as_view(template_name="main/index.html")),
    path('docs/', include_docs_urls(title='JSA API', public=False, permission_classes=(permissions.IsAdminUser,))),
    path('terms/', TemplateView.as_view(template_name="main/terms_of_service.html")),
    path('upgrade-now/', TemplateView.as_view(template_name="main/upgrade_now.html")),
    path('fat-loss-init/', TemplateView.as_view(template_name="main/fat_loss_landing.html")),
    path('transformations/', TemplateView.as_view(template_name="main/transformations.html")),
    path('member-of-the-month/', TemplateView.as_view(template_name="main/member_of_the_month.html")),
    path('privacy/', TemplateView.as_view(template_name="main/privacypolicy.html")),
    path('cookies-policy/', TemplateView.as_view(template_name="main/cookies_policy.html")),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('users/', include(('users.urls', 'users'), namespace='users')),
    path('emails/', include(('emails.urls', 'emails'), namespace='emails')),
    path('transactions/', include(('transactions.urls', 'transactions'), namespace='transactions')),
    path('weeks/', include(('weeks.urls', 'weeks'), namespace='weeks')),
    path('chat/', include(('chat.urls', 'chat'), namespace='chat')),
    path('modules/', include(('modules.urls', 'modules'), namespace='modules')),
    path('progress/', include(('progress.urls', 'progress'), namespace='progress')),
    path('recipes/', include(('recipes.urls', 'recipes'), namespace='recipes')),
    path('training/', include(('training.urls', 'training'), namespace='training')),
    path('accounts/', include('registration.backends.default.urls')),
    path('oauth/', include(('social_django.urls', 'social_django'), namespace='social')),
    path('blog/', include(('blog.urls', 'blog'), namespace='blog')),
    path('coaches/', include(('coaches.urls', 'coaches'), namespace='coaches')),
    path('challenge/', include(('challenge.urls', 'challenge'), namespace='challenge')),
    path('analytics/', include(('analytics.urls', 'analytics'), namespace='analytics')),
    path('nutrition-tracker/', include(('nutrition_tracker.urls', 'nutrition'), namespace='nutrition_tracker')),
    path('foods/', include(('foods.urls', 'foods'), namespace='foods')),
    path('home-steps/', include(('home_steps.urls', 'home_steps'), namespace='home_steps')),
    path('jsa-admin-coach-login/', admin.site.urls),

]
