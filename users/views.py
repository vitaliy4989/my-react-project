from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.utils.html import strip_tags
from django.contrib.auth.decorators import login_required
from social_django.models import UserSocialAuth
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from .models import Profile, CoachProfile
from django.contrib.auth.models import User
from django.contrib.auth.decorators import user_passes_test
from transactions.models import MemberAccount, GiftCoupon
from transactions.views import MemberAccountViewSet
from .serializers import ProfileSerializer, CoachProfileSerializer
import pyuploadcare
import time
import calendar
import csv
from rest_framework.viewsets import ViewSet
from django.http import Http404
from rest_framework.response import Response
from rest_framework.decorators import api_view
from datetime import datetime


class ProfileViewSet(ViewSet):
    def get_or_create(self, request):
        try:
            profile = Profile.objects.get(user=request.user)
        except Profile.DoesNotExist:
            profile = Profile.objects.create(user=request.user)
        return profile

    def list(self, request):
        profile = self.get_or_create(request)
        serializer = ProfileSerializer(profile, many=False)
        return Response(serializer.data)


def set_user_session_variables(request, member_account):
    try:
        if 'member_account_status' in request.session:
            del request.session['member_account_status']
        request.session['member_account_status'] = member_account.status
    except Exception as e:
        print(e)
        pass
    return


@login_required
def index(request):
    profile_view_set = ProfileViewSet()
    member_account_view_set = MemberAccountViewSet()
    profile = profile_view_set.get_or_create(request)
    member_account = member_account_view_set.get_or_start_trial(request)
    member_account = member_account_view_set.check_membership_expired(member_account)
    set_user_session_variables(request, member_account)
    try:
        if request.user.date_joined > datetime(2019, 1, 7).astimezone():
            food_logs = 'false'
        else:
            food_logs = 'true'
    except:
        food_logs = 'false'
    return render(request, 'users/index.html', {
        'member_account': member_account,
        'profile': profile,
        'food_logs': food_logs
    })


@login_required
def account(request):
    try:
        member_account_view_set = MemberAccountViewSet()
        member_account = MemberAccount.objects.get(user=request.user)
        member_account = member_account_view_set.check_membership_expired(member_account)
        period_end = member_account.period_end
        if period_end is not None:
            period_end_date = time.strftime("%a, %d %b %Y", time.gmtime(period_end))
            current_time = calendar.timegm(time.gmtime())
            if int(current_time) > int(period_end):
                update_sub = False
            else:
                update_sub = True
        else:
            update_sub = False
            period_end_date = False
        if member_account.trial_end:
            trial_end = time.strftime("%a, %d %b %Y", time.gmtime(int(member_account.trial_end)))
        else:
            trial_end = None
    except MemberAccount.DoesNotExist:
        member_account = None
        update_sub = False
        period_end_date = None
        trial_end = None
    try:
        facebook_login = request.user.social_auth.get(provider='facebook')
    except UserSocialAuth.DoesNotExist:
        facebook_login = None
    can_disconnect = (request.user.social_auth.count() > 1 or request.user.has_usable_password())
    profile_view_set = ProfileViewSet()
    profile = profile_view_set.get_or_create(request)
    return render(request, 'users/account.html', {
        'facebook_login': facebook_login,
        'can_disconnect': can_disconnect,
        'member_account': member_account,
        'period_end': period_end_date,
        'update_sub': update_sub,
        'profile': profile,
        'profile_pic': profile_pic,
        'trial_end': trial_end,
        'gift_coupons': GiftCoupon.objects.filter(purchased_by=request.user)
    })


@login_required
def membership_options(request):
    try:
        member_account = MemberAccount.objects.get(user=request.user)
    except Exception as e:
        member_account = None
    return render(request, 'users/membership_options.html', {
        'member_account': member_account
    })


@login_required
def payment_success(request):
    try:
        member_account = MemberAccount.objects.get(user=request.user)
    except Exception as e:
        member_account = None
    return render(request, 'users/payment_success.html', {
        'member_account': member_account
    })


@login_required
def gift_coupon(request):
    try:
        member_account = MemberAccount.objects.get(user=request.user)
    except Exception as e:
        member_account = None
    return render(request, 'users/gift_coupon.html', {
        'member_account': member_account
    })


@login_required
def one_time_offers(request):
    try:
        member_account = MemberAccount.objects.get(user=request.user)
    except Exception as e:
        member_account = None
    return render(request, 'users/special_offer.html', {
        'member_account': member_account
    })


@login_required
def profile_pic(request):
    if request.method == 'POST':
        try:
            file = pyuploadcare.File(request.POST.get('my_file'))
            Profile.objects.filter(user=request.user).update(image=file)
            messages.success(request, 'Profile picture uploaded.')
            return redirect('users:account')
        except Exception as e:
            messages.error(request,
                           'Please choose an image and then wait for it to upload before clicking the upload button.')
            return redirect('users:account')

    elif request.method == 'GET':
        profile = Profile.objects.get(user=request.user)
        serializer = ProfileSerializer(profile, many=False)
        return JsonResponse(serializer.data, safe=False)


@login_required
def save_fb_picture(request):
    if request.method == 'POST':
        file = pyuploadcare.File(request.POST.get('file'))
        Profile.objects.filter(user=request.user).update(image=file)
        return HttpResponse(status=200)


@login_required
def update_user_details(request):
    if request.method == "POST":
        User.objects.filter(pk=request.user.id).update(
            first_name=strip_tags(request.POST.get('first_name')),
            last_name=strip_tags(request.POST.get('last_name'))
        )
        messages.success(request, 'Your account details have been updated.')
        return redirect('users:account')


@login_required
def set_first_name(request):
    if request.method == "POST":
        User.objects.filter(pk=request.user.id).update(
            first_name=strip_tags(request.POST.get('first_name'))
        )
        return HttpResponse(status=200)


@login_required
def social_settings_password(request):
    if request.user.has_usable_password():
        PasswordForm = PasswordChangeForm
    else:
        PasswordForm = AdminPasswordChangeForm
    if request.method == 'POST':
        form = PasswordForm(request.user, request.POST)
        if form.is_valid() and request.user.email:
            user_account = form.save()
            user_account.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('users:account')
        elif form.is_valid() and strip_tags(request.POST.get('email')):
            email = strip_tags(request.POST.get('email'))
            try:
                user_account = User.objects.get(email__iexact=email)
                form = PasswordForm(request.user)

                return render(request, 'registration/social_settings_password.html', {
                    'form': form,
                    'errors': 'Email is already registered with an account'
                })

            except User.DoesNotExist:
                user_account = form.save()
                user_account.email = email
                user_account.save()
                update_session_auth_hash(request, form.user)
                messages.success(request, 'Your password was successfully updated!')
                return redirect('users:account')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordForm(request.user)
    return render(request, 'registration/social_settings_password.html', {
        'form': form
    })


@login_required
@user_passes_test(lambda u: u.is_superuser)
def export_users_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="users.csv"'
    writer = csv.writer(response)
    writer.writerow(['Email address'])
    users = User.objects.all().values_list('email')
    for user in users:
        writer.writerow(user)
    return response


@login_required
@user_passes_test(lambda u: u.is_superuser)
def export_users_canceled(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="users.csv"'
    writer = csv.writer(response)
    writer.writerow(['email'])
    users = User.objects.filter(memberaccount__status='canceled').order_by('date_joined').values_list('email',
                                                                                                      'date_joined')[
            :10000]
    for user in users:
        writer.writerow(user)
    return response


@login_required
@user_passes_test(lambda u: u.is_superuser)
def export_users_last_challenge(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="users.csv"'
    writer = csv.writer(response)
    writer.writerow(['email'])
    users = User.objects.filter(entry__challenge=3).values_list('email')
    for user in users:
        writer.writerow(user)
    return response


@login_required
@user_passes_test(lambda u: u.is_superuser)
def restart_trial(request):
    member_accounts = MemberAccount.objects.filter(status='canceled')
    member_accounts.filter(date__range=["2017-07-14", "2018-03-22"]).delete()
    return HttpResponse('trials restarted')


@api_view(['GET', 'POST', 'PATCH'])
@login_required
@user_passes_test(lambda u: u.is_staff)
def coach_profile(request):
    if request.method == 'GET':
        try:
            coach_profile = CoachProfile.objects.get(user=request.user)
            serializer = CoachProfileSerializer(coach_profile, many=False)
            return Response(serializer.data)
        except CoachProfile.DoesNotExist:
            raise Http404
    if request.method == 'POST':
        coach_profile = CoachProfile.objects.create(
            user=request.user,
            image=request.POST.get('image')
        )
        serializer = CoachProfileSerializer(coach_profile, many=False)
        return Response(serializer.data)
    if request.method == 'PATCH':
        try:
            coach_profile = CoachProfile.objects.get(user=request.user)
            coach_profile.image = request.POST.get('image')
            coach_profile.save()
            serializer = CoachProfileSerializer(coach_profile, many=False)
            return Response(serializer.data)
        except CoachProfile.DoesNotExist:
            raise Http404
