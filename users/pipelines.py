from django.contrib.auth.models import User


def check_for_account(backend, details, user=None, *args, **kwargs):

	email = details.get('email')

	if not email:

		email = backend.strategy.request_data().get('email')

        try:
            user = User.objects.get(email_iexact=email)
            return {'user': user,
                    'is_new': False}
        except  User.DoesNotExist:
            return None
        except User.MultipleObjectsReturned:
            raise AuthException(
                backend,
                'The given email address is associated with another account'
            )


