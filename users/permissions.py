from rest_framework import permissions
from transactions.models import MemberAccount


class IsActiveOrTrialing(permissions.BasePermission):

    def has_permission(self, request, view):

        if 'member_account_status' in request.session:

            if request.session['member_account_status'] == 'active' or request.session[
                'member_account_status'] == 'trialing':

                return True

            else:

                member_account = MemberAccount.objects.get(user=request.user)

                if member_account.status == 'active' or member_account.status == 'trialing':

                    request.session['member_account_status'] = member_account.status

                    return True

                else:

                    return False

        else:

            member_account = MemberAccount.objects.get(user=request.user)

            if member_account.status == 'active' or member_account.status == 'trialing':

                request.session['member_account_status'] = member_account.status

                return True

            else:

                return False
