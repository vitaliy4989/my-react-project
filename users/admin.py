from django.contrib import admin
from .models import Profile, CoachProfile


class ProfileAdmin(admin.ModelAdmin):

    def complete_delete(modeladmin, request, queryset):
        for obj in queryset:
            obj.image.delete()
            obj.delete()

    search_fields = ['user__username']

    actions = [complete_delete]

    complete_delete.short_description = "Delete - Including image on Uploadcare"


admin.site.register(Profile, ProfileAdmin)

admin.site.register(CoachProfile)
