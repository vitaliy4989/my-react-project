from django.db import models
from django.contrib.auth.models import User
from pyuploadcare.dj.models import ImageField


class Profile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE, db_index=True)
    image = ImageField(null=True)

    def __str__(self):
    	return self.user.username


class CoachProfile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE, db_index=True)
    image = ImageField(null=True)

    def __str__(self):
    	return self.user.username
