from rest_framework import serializers
from .models import Profile, CoachProfile
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name', 'profile',)


class ProfileSerializer(serializers.ModelSerializer):

	user = UserSerializer(read_only=True)
	class Meta:
		model = Profile
		fields = '__all__'


class CoachProfileSerializer(serializers.ModelSerializer):

    user = UserSerializer(read_only=True)
    class Meta:
        model = CoachProfile
        fields = '__all__'