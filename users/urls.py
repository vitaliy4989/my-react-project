from django.urls import path
from . import views
from .backends import Register
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.index, name='home'),
    path('account/', views.account, name='account'),
    path('membership-options/', views.membership_options, name='membership_options'),
    path('payment-success/', views.payment_success, name='payment_success'),
    path('gift-coupon/', views.gift_coupon, name='gift_coupon'),
    path('coach-profile/', views.coach_profile, name='coach_profile'),
    path('one-time-offers/', views.one_time_offers, name='one_time_offers'),
    path('profile/picture/', views.profile_pic, name='profile_pic'),
    path('save-fb-picture/', views.save_fb_picture, name='save_fb_picture'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('register/', Register.as_view(), name='user_register'),
    path('social/settings/password', views.social_settings_password, name='social_settings_password'),
    path('update-details/', views.update_user_details, name='update_user_details'),
    path('set-first-name/', views.set_first_name, name='set_first_name'),
    path('csv-export/', views.export_users_csv, name='export_users_csv'),
    path('export-users-canceled/', views.export_users_canceled, name='export_users_canceled'),
    path('export-users-last-challenge/', views.export_users_last_challenge, name='export_users_last_challenge'),
]
