var webpack = require("webpack");
var BundleTracker = require("webpack-bundle-tracker");
var config = require("./webpack.config.coaches.js");

config.output.path = require("path").resolve("./react/dist/coaches");

config.plugins = config.plugins.concat([
  new BundleTracker({ filename: "./webpack-stats-prod-coaches.json" }),

  // removes a lot of debugging code in React
  new webpack.DefinePlugin({
    "process.env": {
      NODE_ENV: JSON.stringify("production"),
    },
  }),

  // keeps hashes consistent between compilations
  new webpack.optimize.OccurrenceOrderPlugin(),
]);

module.exports = config;
